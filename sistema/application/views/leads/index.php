

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/icons/icomoon/styles.css") ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/bootstrap.min.css") ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/bootstrap_limitless.min.css") ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/layout.min.css") ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/components.min.css") ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url("public/assets/css/colors.min.css") ?>" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <!-- Core JS files -->
        <script src="<?= base_url("public/assets/js/main/jquery.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/main/bootstrap.bundle.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/loaders/blockui.min.js") ?>"></script>
        <!-- /core JS files -->
        <!-- Theme JS files -->
        <script src="<?= base_url("public/assets/js/plugins/tables/datatables/datatables.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/forms/selects/select2.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/plugins/tables/datatables/extensions/buttons.min.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/app.js") ?>"></script>
        <script src="<?= base_url("public/assets/js/demo_pages/datatables_extension_buttons_html5.js") ?>"></script>
        <!-- /theme JS files -->
    </head>
    <body>
        <?php if($this->session->userdata("usuario_logado")) : ?>
        <!-- Main navbar -->
        <div class="navbar navbar-expand-md navbar-dark">
            <div class="navbar-brand">
                <a href="index.html" class="d-inline-block">
                <img src="<?= base_url("public/assets/images/logo_light.png") ?>" alt="">
                </a>
            </div>
            <div class="d-md-none">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
                </button>
                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <span class="navbar-text ml-md-3 mr-md-auto">
                <span class="badge bg-success">Online</span>
                </span>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown dropdown-user">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= base_url("public/assets/images/placeholders/placeholder.jpg") ?>" class="rounded-circle" alt="">
                        <span>Usuário</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item"><i class="icon-user-plus"></i>Perfil</a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Sair</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Page header -->
                <div class="page-header page-header-light">
                    <div class="page-header-content header-elements-md-inline">
                        <div class="page-title d-flex">
                            <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Leads</span> - Plano de Saúde</h4>
                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>
                    </div>
                    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                        <div class="d-flex">
                            <div class="breadcrumb">
                                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                                <a href="datatable_extension_buttons_html5.html" class="breadcrumb-item">Leads</a>
                                <span class="breadcrumb-item active">Plano de Saúde</span>
                            </div>
                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>
                        <div class="header-elements d-none">
                        </div>
                    </div>
                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">
                    <div class="row">
                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-body bg-blue-400 has-bg-image">
                                <div class="media">
                                    <div class="mr-3 align-self-center">
                                        <i class="icon-user icon-3x opacity-75"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <h3 class="mb-0"><?php echo $this->db->count_all_results('tmzleads'); ?></h3>
                                        <span class="text-uppercase font-size-xs">Leads Plano de Saúde</span>
                                    </div>
                                </div>
                            </div>
														<div class="card card-body bg-violet-400 has-bg-image">
                                <div class="media">
                                    <div class="mr-3 align-self-center">
                                        <i class="icon-user icon-3x opacity-75"></i>
                                    </div>
                                    <div class="media-body text-right">
                                        <h3 class="mb-0"><?php echo $this->db->count_all_results('tmzleadsauto'); ?></h3>
                                        <span class="text-uppercase font-size-xs">Leads Seguro Auto</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-9">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table text-nowrap">
                                        <tbody>
                                            <tr class="table-active table-border-double">

                                                <td colspan="3">Últimos leads cadastrados</td>

                                            </tr>
                                            <tr>
																							<?php foreach(array_slice($leads, 0, 3) as $lead) : ?>
                                                <td>
                                                    <i class="icon-checkmark3 text-success"></i>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div>
																													<a href="#" class="text-default font-weight-semibold letter-icon-title">  <i class="icon-user font-size-sm mr-1"></i><?= $lead['nome'] ?></a>

                                                            <div class="text-muted font-size-sm"></span><i class="icon-mail5 font-size-sm mr-1"></i><?= $lead['email'] ?>
                                                            </div>
                                                        </div>
                                                </td>
                                                <td>
                                                	<div class="font-weight-semibold"> <i class="icon-location3 font-size-sm mr-1"></i><?= $lead['cidade'] ?></div>
                                                	<span class="text-muted"> Fonte: <?= $lead['operadora'] ?></span>

                                            </tr>
																					 <?php endforeach ?>
                                            </div>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="preload">
    <div class="alert alert-info alert-styled-left alert-arrow-left alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <span class="font-weight-semibold"><i class="icon-spinner2 spinner"></i> Aguarde!</span> Carregando Leads...
    </div>
</div>
                    <!-- Column selectors -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Column selectors</h5>
                        </div>
                        <table class="table datatable-button-html5-columns">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Observação</th>
                                    <th>Modalidade</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Fone Principal</th>
                                    <th>Fone Celular</th>
                                    <th>Cidade</th>
                                    <th>IdFonte</th>
                                    <th>Operadora</th>
                                    <th>Data</th>
                                    <th class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($leads as $lead) : ?>
                                <tr>
                                    <td><?= $lead['strId'] ?></td>
                                    <td><?= $lead['quantidadefamiliar'] ?>  <?= $lead['quantidadepme'] ?> <?= $lead['modalidade'] ?> ; <?= $lead['possuicnpj'] ?> ; <?= $lead['tipopessoa'] ?> ; <?= $lead['cnpj'] ?> ; <?= $lead['mensagem'] ?></td>
                                    <td><?= $lead['modalidade'] ?></td>
                                    <td><?= $lead['nome'] ?></td>
                                    <td><?= $lead['email'] ?></td>
                                    <td><?= $lead['telefone'] ?></td>
                                    <td><?= $lead['telefoneAlternativo'] ?></td>
                                    <td><?= $lead['cidade'] ?></td>
                                    <td></td>
                                    <td><span class="badge badge-info"><?= $lead['operadora'] ?></span></td>
                                    <td><span class="badge badge-success"><?= $lead['strData'] ?></span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item delete_data" id="<?= $lead['strId'] ?>">
                                                        <i class="icon-x"></i> Deletar</a
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        <?= anchor("login/logout", "Sair", array("class" => "btn btn-primary")) ?>
                    </div>
                    <!-- /column selectors -->
                    <?php else : ?>
                    <div class="content d-flex justify-content-center align-items-center" >
                    <div class="card mb-0">
                    <div class="card-body">
                    <div class="text-center mb-3">
                    <img src="<?= base_url("public/assets/images/imgLogoLogin.png") ?>">
                    <h5 class="mb-0">Acesso ao sistema</h5>
                    <span class="d-block text-muted">Insira seus dados</span>
                    <?php if($this->session->flashdata("success")) : ?>
                    <div class="alert alert-danger alert-styled-left alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    <span class="font-weight-semibold"><?= $this->session->flashdata("success") ?> </a>
                    </div>
                    <?php endif ?>
                    <?php if($this->session->flashdata("danger")) : ?>
                    <div class="alert alert-danger alert-styled-left alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                    <span class="font-weight-semibold"><?= $this->session->flashdata("danger") ?> </a>
                    </div>
                    <?php endif ?>
                    </div>
                    <?php echo form_open("login/autenticar");
                        echo form_label("Email", "email");
                        echo form_input(array(
                        		"name" => "email",
                        		"id" => "email",
                        		"class" => "form-control",
                        		"maxlength" => "255",
                        			"style" => "margin-bottom:20px"

                        ));
                        echo form_label("Senha", "senha");
                        echo form_password(array(
                        		"name" => "senha",
                        		"id" => "senha",
                        		"class" => "form-control",
                        		"maxlength" => "255",
                        			"style" => "margin-bottom:20px"

                        ));
                        echo form_button(array(
                        	"class" => "btn btn-primary btn-block",
                        	"type" => "submit",
                        	"content" => "Login"
                        ));
                        echo form_close();
                        ?>
                    </div>
                    </div>
                    </div>
                    <?php endif ?>
                </div>
                <!-- /content area -->
                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                        </button>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar-footer">
                        <span class="navbar-text">
                        &copy; 2018. <a href="#">Sistema de Gestão de Leads</a> by <a href="#" target="_blank">AgênciaTMZ</a>
                        </span>
                    </div>
                </div>
                <!-- /footer -->
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </body>

</html>
