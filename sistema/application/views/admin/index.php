<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sistema de Gestão de Leads</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

</head>

<body>

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">



				<!-- Login card -->
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<img src="public/assets/images/imgLogoLogin.png">
								<h5 class="mb-0">Acesso ao sistema</h5>
								<span class="d-block text-muted">Insira seus dados</span>
								<?php if($this->session->flashdata("success")) : ?>
									<p><?= $this->session->flashdata("success") ?>  </p>
								<?php endif ?>

								<?php if($this->session->flashdata("danger")) : ?>
									<p><?= $this->session->flashdata("danger") ?>  </p>
								<?php endif ?>
							</div>
							<?php echo form_open("login/autenticar");

							echo form_label("Email", "email");
							echo form_input(array(
									"name" => "email",
									"id" => "email",
									"class" => "form-control",
									"maxlength" => "255",
										"style" => "margin-bottom:20px"

							));


							echo form_label("Senha", "senha");
							echo form_password(array(
									"name" => "senha",
									"id" => "senha",
									"class" => "form-control",
									"maxlength" => "255",
										"style" => "margin-bottom:20px"

							));

							echo form_button(array(
								"class" => "btn btn-primary btn-block",
								"type" => "submit",
								"content" => "Login"

							));

							echo form_close();

							?>
						</div>
					</div>
				<!-- /login card -->
			</div>
			<!-- /content area -->
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
  <!-- Footer -->
  <div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
      <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
        <i class="icon-unfold mr-2"></i>
        Footer
      </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
      <span class="navbar-text">
        &copy; 2018 Sistema de Gestão de Leads
      </span>


    </div>
  </div>
  <!-- /footer -->
  <!-- Core JS files -->
  <script src="public/assets/js/main/jquery.min.js"></script>
  <script src="public/assets/js/main/bootstrap.bundle.min.js"></script>
  <script src="public/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script src="public/assets/js/plugins/forms/validation/validate.min.js"></script>
  <script src="public/assets/js/plugins/forms/styling/uniform.min.js"></script>

  <script src="public/assets/js/app.js"></script>
  <script src="public/assets/js/demo_pages/login_validation.js"></script>
  <!-- /theme JS files -->

</body>
</html>
