<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends CI_Controller {

	public function index(){
		$this->load->model("leads_model");
    $lista = $this->leads_model->buscaTodos();
    $dados = array("leads" => $lista);
    $this->load->view('leads/index', $dados);
	}
}
