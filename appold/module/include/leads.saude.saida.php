
<?php
// excluir post
if(isset($_GET['delete'])){
    $id_delete = $_GET['delete'];

    // exclui o registro

    $seleciona = "DELETE from popupsaida WHERE strId=:id_delete";
    try{
        $result = $conexao->prepare($seleciona);
        $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuarioDeletadoSucesso = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Usuário deletado com <strong>Sucesso!</strong>
                                        </div>';
        }else{
            $usuarioDeletadoErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Erro!</strong> Não foi possível excluir o usuario.
                                      </div>';
        }
    }catch (PDOWException $erro){ echo $erro;}


}

?>
<?php echo $usuarioDeletadoSucesso; ?>
<?php echo $usuarioDeletadoErro; ?>

<table class="table m-b-0" data-page-size="7">
    <thead>
    <tr>
        <th data-hide="phone, tablet">Email</th>
        <th data-hide="phone">Cidade</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $select = "SELECT * from popupsaida ORDER BY strId DESC LIMIT 3";
    $contagem =1;
    try {
        $result = $conexao->prepare($select);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            while($show = $result->FETCH(PDO::FETCH_OBJ)){

                $date = date_create($show->strData);
                $date = date_format($date, 'd-m-Y');
                $dateToday = date('d-m-Y', strtotime("1 days"));

                ?>
                <tr>

                    <td><?php echo $show->strEmail;?></td>
                    <td><?php echo $show->strCidade;?></td>



                </tr>

                <?php
            }
        }else{
            echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento!
                                        </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }
    ?>

    </tbody>

</table>
