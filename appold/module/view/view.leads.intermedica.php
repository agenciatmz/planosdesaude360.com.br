<?php
require_once("../../config/dbconnect.php");
include_once("../../module/controllers/verifica.usuario.logado.php");

?>
<!DOCTYPE html>
<html>
<?php include_once("../../module/include/leads.include.header.php"); ?>
<body>
<?php include_once("../../module/include/leads.include.topnav.php"); ?>

<?php
// excluir post
if(isset($_GET['delete'])){
    $id_delete = $_GET['delete'];

    // exclui o registro

    $seleciona = "DELETE from leadsintermedica WHERE strId=:id_delete";
    try{
        $result = $conexao->prepare($seleciona);
        $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuarioDeletadoSucesso = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Usuário deletado com <strong>Sucesso! </strong>
                                        </div>'; echo "<script type='text/javascript'>    
                setTimeout(function () {
                window.location.href = \"view.leads.intermedica.php\"; 
            }, 2000);  </script>";
        }else{
            $usuarioDeletadoErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Erro!</strong> Não foi possível excluir o usuario.
                                      </div>';
        }
    }catch (PDOWException $erro){ echo $erro;}


}

?>
<div class="wrapper">
    <div class="container-fluid">


        <!-- Page-Title -->
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item ">TMZ</li>
                            <li class="breadcrumb-item ">Leads</li>
                            <li class="breadcrumb-item active">Intermédica</li>

                        </ol>
                    </div>
                    <h6>Visão Geral > <span style="color: #000;"> Leads Intermédica </span></h6>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <img src="../../public/images/logo-intermedica-full.png" width="150" class="pull-left" style="padding-bottom: 20px">

                <div class="card-box table-responsive">
                    <?php echo $usuarioDeletadoSucesso; ?>
                    <?php echo $usuarioDeletadoErro; ?>
                    <table id="datatable-buttons" class="table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Observação</th>
                            <th>Tipo Pessoa</th>
                            <th>Modalidade</th>
                            <th>CNPJ</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>FonePrincipal</th>
                            <th>FoneCelular</th>
                            <th>Cidade</th>
                            <th>IdFonte</th>
                            <th>Vidas PME</th>
                            <th>Vidas Familiar</th>
                            <th>Operadora</th>
                            <th>Data</th>
                            <th>Ações</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(empty($_GET['pg'])){}
                        else{ $pg = $_GET['pg'];
                            if(!is_numeric($pg)){
                                echo '<script language="JavaScript">
                                           location.href=" ViewClientesVisualizar.php"; </script>';
                            }

                        }
                        if(isset($pg)){ $pg = $_GET['pg'];}else{ $pg = 1;}

                        $quantidade = 1000;
                        $inicio = ($pg*$quantidade) - $quantidade;
                        $select = "SELECT * from leadsintermedica ORDER BY strId DESC LIMIT $inicio, $quantidade";
                        try {
                            $result = $conexao->prepare($select);
                            $result->execute();
                            $contar = $result->rowCount();
                            if($contar>0){
                                while($show = $result->FETCH(PDO::FETCH_OBJ)){

                                    date_default_timezone_set('America/Sao_Paulo');
                                    $date = date_create($show->strData);
                                    $date = date_format($date, 'd-m-Y H:i');

                                    ?>

                                    <tr>
                                        <td><?php echo $show->quantidadepme;?> <?php echo $show->quantidadefamiliar;?> Vidas: <?php echo $show->mensagem;?></td>
                                        <td><?php echo $show->tipopessoa;?></td>
                                        <td><?php echo $show->possuicnpj;?></td>
                                        <td><?php echo $show->cnpj;?></td>
                                        <td><?php echo $show->nome;?></td>
                                        <td><?php echo $show->email;?></td>
                                        <td><?php echo $show->telefone;?></td>
                                        <td><?php echo $show->telefoneAlternativo;?></td>
                                        <td><?php echo $show->cidade;?></td>
                                        <td>   </td>
                                        <td><span class="badge label-table badge-warning"><?php echo $show->quantidadepme;?></span></td>
                                        <td><span class="badge label-table badge-warning"><?php echo $show->quantidadefamiliar;?></span></td>
                                        <td><span class="badge label-table badge-warning"><?php echo $show->operadora;?></span></td>
                                        <td><?php echo date('d/m/Y H:i', strtotime($date . ' - 3 hour '));?></td>
                                        <td><a href="view.leads.intermedica.php?pg=<?php echo $pg;?>&delete=<?php echo $show->strId;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')"  class="icon-user-unfollow btn btn-outline-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deletar Usuário"> </a>
                                            <a href="view.unique.leads.intermedica.php?id=<?php echo $show->strId;?>" class="icon-eye btn btn-outline-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                                            <a href="view.edit.unique.leads.intermedica.php?id=<?php echo $show->strId;?>" class="icon-pencil btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Usuário">
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                               Desculpe, não existem dados cadastrados no momento !
                                        </div>';
                            }
                        }catch(PDOException $e){
                            echo $e;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

</div>



<?php include_once("../../module/include/leads.include.footer.php"); ?>

</body>
</html>