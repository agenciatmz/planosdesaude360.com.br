<?php
require_once("../../config/dbconnect.php");
include_once("../../module/controllers/verifica.usuario.logado.php");

?>
<!DOCTYPE html>
<html>

<?php include_once("../../module/include/leads.include.header.php"); ?>

    <body>

    <?php include_once("../../module/include/leads.include.topnav.php"); ?>


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">TMZ</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="row">

                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Bradesco<img class="pull-right" src="../../public/images/logo-bradesco.png" width="30"></h4>
                            <?php include_once ("../../module/include/leads.saude.bradesco.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.bradesco.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>
                        </div>
                    </div>


                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Intermédica<img class="pull-right" src="../../public/images/logo-intermedica.png" width="25"></h4>
                            <?php include_once ("../../module/include/leads.saude.intermedica.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.intermedica.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Amil<img class="pull-right" src="../../public/images/logo-amil.png" width="50"></h4>
                            <?php include_once ("../../module/include/leads.saude.amil.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.amil.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4 col-xs-12">

                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Genérico<img class="pull-right" src="../../public/images/logo-generico.png" width="40"></h4>
                            <?php include_once ("../../module/include/leads.saude.generica.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.generico.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>

                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Next<img class="pull-right" src="../../public/images/logo-next.png" width="40"></h4>
                            <?php include_once ("../../module/include/leads.saude.next.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.next.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Biovida<img class="pull-right" src="../../public/images/Logo_BV.png" width="40"></h4>
                            <?php include_once ("../../module/include/leads.saude.biovida.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.biovida.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Sulamerica<img class="pull-right" src="../../public/images/logo-sulamerica.png" width="50"></h4>
                            <?php include_once ("../../module/include/leads.saude.sulamerica.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.sulamerica.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Trasmontano<img class="pull-right" src="../../public/images/logo-trasmontano.png" width="50"></h4>
                            <?php include_once ("../../module/include/leads.saude.trasmontano.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.trasmontano.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Samed<img class="pull-right" src="../../public/images/logo-samed.png" width="50"></h4>
                            <?php include_once ("../../module/include/leads.saude.samed.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.samed.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="card-box ">
                            <h4 class="page-title" style="padding-bottom: 20px">Leads Saída<img class="pull-right" src="../../public/images/logo360.png" width="50"></h4>
                            <?php include_once ("../../module/include/leads.saude.saida.php"); ?>
                            <!-- end row -->
                            <a href="view.leads.saida.php" class="btn btn-sm btn-outline-custom waves-light waves-effect">Visualizar</a>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

    <?php include_once("../../module/include/leads.include.footer.php"); ?>


    </body>
</html>