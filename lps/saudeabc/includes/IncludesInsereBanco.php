<?php

if(isset($_POST['cadastrar'])){
    $nome            = trim(strip_tags($_POST['nome']));
    $email           = trim(strip_tags($_POST['email']));
    $telefone         = trim(strip_tags($_POST['telefone']));
    $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
    $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
    $cnpj            = trim(strip_tags($_POST['cnpj']));
    $estado          = trim(strip_tags($_POST['estado']));
    $cidade          = trim(strip_tags($_POST['cidade']));
    $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
    $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
    $operadora       = trim(strip_tags($_POST['operadora']));
    $operadoraAmil      = trim(strip_tags($_POST['operadoraAmil']));
    $operadoraBradesco     = trim(strip_tags($_POST['operadoraBradesco']));
    $operadoraIntermedica       = trim(strip_tags($_POST['operadoraIntermedica']));
    $operadoraSamed       = trim(strip_tags($_POST['operadoraSamed']));
    $operadoraBiovida       = trim(strip_tags($_POST['operadoraBiovida']));
    $operadoraTrasmontano       = trim(strip_tags($_POST['operadoraTrasmontano']));
    $operadoraSulamerica       = trim(strip_tags($_POST['operadoraSulamerica']));
    $operadoraNext       = trim(strip_tags($_POST['operadoraNext']));
    $operadoraGoldencross       = trim(strip_tags($_POST['operadoraGoldencross']));
    $operadoraMedSenior       = trim(strip_tags($_POST['operadoraMedSenior']));
    $mensagem        = trim(strip_tags($_POST['mensagem']));
    $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
    $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));
    $modalidade     = trim(strip_tags($_POST['modalidade']));

    $insert = "INSERT INTO tmzleadsgeral ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidadepme, quantidadefamiliar, operadora, operadoraAmil, operadoraBradesco, operadoraIntermedica, operadoraSamed, operadoraBiovida, operadoraTrasmontano, operadoraSulamerica, operadoraNext, operadoraGoldencross, operadoraMedSenior, mensagem, tipodeplano, tipopessoa, modalidade  )
    VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidadepme, :quantidadefamiliar, :operadora, :operadoraAmil, :operadoraBradesco, :operadoraIntermedica, :operadoraSamed, :operadoraBiovida, :operadoraTrasmontano, :operadoraSulamerica, :operadoraNext, :operadoraGoldencross, :operadoraMedSenior, :mensagem, :tipodeplano, :tipopessoa, :modalidade )";
    try{

        $result = $conexao->prepare($insert);

        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
        $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
        $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
        $result->bindParam(':estado', $estado, PDO::PARAM_STR);
        $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
        $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':operadoraAmil', $operadoraAmil, PDO::PARAM_STR);
        $result->bindParam(':operadoraBradesco', $operadoraBradesco, PDO::PARAM_STR);
        $result->bindParam(':operadoraIntermedica', $operadoraIntermedica, PDO::PARAM_STR);
        $result->bindParam(':operadoraSamed', $operadoraSamed, PDO::PARAM_STR);
        $result->bindParam(':operadoraBiovida', $operadoraBiovida, PDO::PARAM_STR);
        $result->bindParam(':operadoraTrasmontano', $operadoraTrasmontano, PDO::PARAM_STR);
        $result->bindParam(':operadoraSulamerica', $operadoraSulamerica, PDO::PARAM_STR);
        $result->bindParam(':operadoraNext', $operadoraNext, PDO::PARAM_STR);
        $result->bindParam(':operadoraGoldencross', $operadoraGoldencross, PDO::PARAM_STR);
        $result->bindParam(':operadoraMedSenior', $operadoraMedSenior, PDO::PARAM_STR);
        $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
        $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
        $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
        $result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);

        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '
                                                        <script type="text/javascript">
                                    window.location = "obrigado.php";
                                                </script>';

            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erro</strong> ao cadastrar o usuário.
                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>
