<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde UniHosp</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #2e9d39; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;"> HOSPITAIS  <span style="font-weight:800">ABCDM</span></div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital das Acácias </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #2e9d39; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;"> PRONTO <span style="font-weight:800">SOCORRO</span></div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa casa de São Bernando do Campo</div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa casa de Mauá </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Instituição Assistencial Emmanuel Bezzera de Menezes </div>
                        <div style="padding-top:5px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Innova Hospitais </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #2e9d39; padding: 10px; color:#fff;    border-top-left-radius: 15px;  border-top-right-radius: 15px;"> HOSPITAIS <span style="font-weight:800">SÃO PAULO</span></div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital Jardins </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital Santa Clara </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Complexo Santo Expedito </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Madre Care </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital e Maternidade Master Clin Eireli </div>
                        <div style="padding-top:5px;"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital e Maternidade 8 de Maio </div>
                        <div style="padding-top:5px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital Neurocenter</div>
                    </div>

                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #2e9d39; padding: 10px; color:#fff;    border-top-left-radius: 15px;  border-top-right-radius: 15px;"> PRINCIPAIS  <span style="font-weight:800">LABORATÓRIOS</span></div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > GHELFOND - São Paulo </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > IMEDI - Santo André </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > MEDIX - Mauá </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > ULTRA - São Bernardo do Campo </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > LABHORMON - Ribeirão Pires </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CLÍNICE - Santo André  </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > MEDICAL IMAGE - Santo André </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > IBABC - São Caetano do Sul </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > LABORFASE - Mauá </div>
                        <div style="padding-top:5px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > ABC IMAGEM - São Bernardo do Campo </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #2e9d39; padding: 10px; color:#fff;    border-top-left-radius: 15px;  border-top-right-radius: 15px;"> CLÍNICAS  <span style="font-weight:800">( Consultas e Exames )</span></div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CENTRO MÉDICO SANTO ANDRÉ  </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CENTRO MÉDICO RIBEIRÃO PIRES </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > AMPLAVISÃO OFTALMOLOGIA - SANTO ANDRÉ</div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CLÍNICA DA MULHER - SANTO ANDRÉ </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > AMEL ASSIS MÉDICO - SANTO ANDRÉ </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > DK UROLOGISTAS - SANTO ANDRÉ</div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CLÍNICA ALAMEDA  - SANTO ANDRÉ</div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > VISTAMED CLÍNICA - SÃO CAETANO</div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > CLÍNICA DR. FAMÍLIA - SÃO CAETANO </div>
                        <div style="padding-top:5px"> <img src="img/maps-and-flags.png" alt="" width="12" > REATIVA CENTRO MÉDICO - MAUÁ </div>
                    </div>
                  </div>

                </div>
                <div class="col-md-12"> * Para mais informações sobre rede credenciada, carência, valores e médicos referenciados solicite uma cotação</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde Intermédica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Diadema </span></div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Instituto de Radiologia São Bernardo - Unidade Diadema </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Ribeirão Pires  </span></div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Instituto de Radiologia e Ultrassonografia Ribeirão Pires </div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Med Sonic Diagnóstico Por Imagem </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Santo André  </span></div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" >  Lavoisier Medicina Diagnóstica - Santo André </div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Miranda & Wiermann - Santo André </div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > RM - Ressonância Magnética </div>

                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">São Bernardo do Campo  </span></div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Bio Feedback Centro de Fisioterapia </div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Instituto de Radiologia São Bernardo do Campo </div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Lavoisier Medicina Diagnóstica  </div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Neolabor - Centro SBC </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">São Caetano do Sul </span></div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Lavoisier Medicina Diagnóstica - São Caetano do Sul </div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > RM - Ressonância Magnética</div>
                        <div style="padding-top:10px; padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Ultrassonografia Médica </div>
                    </div>
                    <div class="col-md-6">
                        <div style="font-size:18px; background-color: #F49B00; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Mauá </span></div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Centro Médico Floresti </div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Clínica Médica Mauá Clinic</div>
                        <div style="padding-top:10px"> <img src="img/maps-and-flags.png" alt="" width="12" > Tecnolab Medicina Diagnóstica (Mauá) </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12"> * Para mais informações sobre rede credenciada, carência, valores e médicos referenciados solicite uma cotação</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde Santa Helena</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Santo André </span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Hospital Santa Helena – Pronto Atendimento 24 Horas  </div>

              </div>
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">São Bernardo do Campo </span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa Helena – Pronto Atendimento 24 Horas</div>
              </div>
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">São Caetano do Sul</span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa Helena – Centro Médico Pronto Atendimento </div>
              </div>
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Diadema</span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa Helena – Centro Médico Pronto Atendimento 24 Horas </div>
              </div>
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Mauá</span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa Helena – Centro Médico Pronto Atendimento</div>
              </div>
              <div class="col-md-6">
                  <div style="font-size:18px; background-color: #BE1E2D; padding: 10px; color:#fff;border-top-left-radius: 15px;  border-top-right-radius: 15px;">  <span style="font-weight:800">Ribeirão Pires</span></div>
                  <div style="padding-top:10px;padding-bottom:20px"> <img src="img/maps-and-flags.png" alt="" width="12" > Santa Helena – Centro Médico Pronto Atendimento </div>
              </div>
              </div>
            </div>
            <div class="col-md-12"> * Para mais informações sobre rede credenciada, carência, valores e médicos referenciados solicite uma cotação</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Orçamento Reduzido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Se você está com um orçamento reduzido, mas não quer ficar sem Plano de Saúde, o Plano mais indicado para seu perfil (seja individual, familiar ou empresarial) são os Planos com coberturas regionais, dentre eles se destacam o Next Saúde do Grupo Amil e o Intermédica 200 up. Eles possuem rede de Atendimento própria com hospitais que fazem jus ao preço cobrado pelos planos e uma rede médica com profissionais qualificados. Caso o Plano seja para 2 ou mais pessoas e você possuir CNPJ sai ainda mais em conta.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Redução de Custo (Down Grade)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Se você está procurando reduzir o custo do seu Plano de Saúde, A Opção mais indicado para seu perfil (seja individual, familiar ou empresarial) é migrar seu Plano para empresarial utilizando um CNPJ e trocar de operadora buscando manter a mesma qualidade. Os casos mais comuns são: Sulamérica para Amil ou Bradesco para Amil.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upgrade</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                No caso de você querer a melhor opção possível para você, sua família ou empresa, não tem conversa, o Plano de Saúde One Health do grupo Amil é considerado por muitos o melhor do País, cobertura internacional, acesso à todos os melhores hospitais e médicos do Brasil e tudo que um Plano de Saúde Premium pode oferecer
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
