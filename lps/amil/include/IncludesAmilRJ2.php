<div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                    <img src="assets/img/amil-logo.png" width="200">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#depoimentos" data-scroll>
                            <p>Depoimentos</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#diferencias" data-scroll>
                            <p>Diferenciais</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#contato" data-scroll>
                            <p>Contato</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/bg15.jpg'); "></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-left">
                        <h4 class="title"><span  style="font-size: 1.4em; color:#233A7C">PLANOS À PARTIR DE<br><span class="highlight" style="font-size: 2.4em; color:#233A7C"> <br> R$ 307,81<span  style="font-size: 0.7em; color:#233A7C">*</span> <br></span></h4>
                        <h3 class="description" style="color:#fff"><span style="font-size: 0.7em; color:#233A7C">* Amil 400 até 18 anos - Adesão - Preço por vida</span></h3>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">

                            <?php include("include/Form/FormAmilRJ2.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="features-8"  id="sobrenos" style="background-color: #233A7C  ">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title">Conheça o Amil Saúde</h2>
                    <h4 class="description"> Só a Amil tem as melhores opções de Plano de Saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É Rápido e Fácil. </h4>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical-emergencies.gif" class="rounded" alt="">

                                </div>
                                <div class="info text-center">

                                    <h4 class="info-title">REDE MÉDICA DOS PLANOS AMIL
                                    </h4>
                                    <p class="description">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em aproximadamente todo território nacional</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical_app.gif" class="rounded" alt="">
                                </div>
                                <div class="info text-center">
                                    <h4 class="info-title">PLANO DE SAÚDE AMIL COBERTURAS</h4>
                                    <p class="description">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções de Planos de Saúde Amil com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação e identificar qual plano de Saúde Amil se enquadra melhor às suas necessidades</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical-gif-11.gif" class="rounded" alt="">
                                </div>
                                <div class="info text-center">
                                    <h4 class="info-title">PREÇOS DOS PLANOS DE SAÚDE AMIL</h4>
                                    <p class="description">Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado. Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


    <div class="pricing-5 section-pricing-5 " id="redecredenciada" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto text-center">
                            <h1 class="title" style="font-size: 3em">
                            <span style="color: #08377F">
                            CONHEÇA A REDE CREDENCIADA DA AMIL
                            </span>
                            </h1>
                        </div>
                    </div>
                    <br><br>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/hospital-adventista.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL ADVENTISTA</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/hospital-daluz.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL DA LUZ </h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/hospital-dayhospital.png">
                                    </div>
                                    <h4 class="info-title">DAY HOSPITAL</h4>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/hospital-hcusp.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL DAS CLÍNICAS DA USP</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/hospital-nipobrasileiro.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL NIPO BRASILEIRO</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; paddingdding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/muitomais.png">
                                        <p class="description"> Hospitais e clínicas </p>
                                    </div>
                                    <a href="#formulario" data-scroll  class="btn btn-info btn-lg botaoshake">REALIZE SUA COTAÇÃO SEM COMPROMISSO</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/bg15a.jpg'); background-size: cover;" id="contato">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <div class="card card-testimonial card-plain">
                            <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                            <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                            <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                                QUERO COTAR AGORA
                            </a>
                        </div>
                    </div>
                </div>
            </div>

    <footer class="footer " style="background-color:#fff; color:#184B92">
        <div class="col-md-12">
            <div class="container">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por
                    <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#184B92">#agênciatrêsmeiazero</a>.
                </div>
            </div>
        </div>
    </footer>
</div>
