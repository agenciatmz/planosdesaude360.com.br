<div class="d-none d-sm-block">
    <div class=" qc-landing-layout">
        <div class="qc-landing-info qc-layout-info">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                        <img src="assets/img/logo-white.png" width="100">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#sobrenos" data-scroll style="background-color:#332ca9">
                                    <p>SOBRE NOS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#rede" data-scroll  style="background-color:#332ca9">
                                    <p>REDE CREDENCIADA</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="header">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('assets/img/bg14.png'); "></div>
                    <div class="content-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-8 section-image"  id="sobrenos">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title">Conheça o Amil Saúde</h2>
                    <h4 class="description"> Só a Amil tem as melhores opções de Plano de Saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É Rápido e Fácil. </h4>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical-emergencies.gif" class="rounded" alt="">
                                </div>
                                <div class="info text-center">
                                    <h4 class="info-title">COBERTURA EM TODO <BR>TERRITÓRIO NACIONAL
                                    </h4>
                                    <p class="description">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em aproximadamente todo território nacional</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical_app.gif" class="rounded" alt="">
                                </div>
                                <div class="info text-center">
                                    <h4 class="info-title">AMPLA COBERTURA DE PROCEDIMENTOS</h4>
                                    <p class="description">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções de Planos de Saúde Amil com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação e identificar qual plano de Saúde Amil se enquadra melhor às suas necessidades</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/medical-gif-11.gif" class="rounded" alt="">
                                </div>
                                <div class="info text-center">
                                    <h4 class="info-title">DESCONTOS EM ESTABELECIMENTOS PARCEIROS</h4>
                                    <p class="description">Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado. Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)</p>
                                    <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="pricing-5 section-pricing-5 " id="redecredenciada" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto text-center">
                            <h1 class="title" style="font-size: 3em">
                                <span style="color: #08377F">
                                CONHEÇA A REDE CREDENCIADA DA AMIL
                                </span>
                            </h1>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 ml-auto mr-auto ">
                                
                                    color-classes: "nav-pills-primary", "nav-pills-info", "nav-pills-success", "nav-pills-warning","nav-pills-danger"

                                <ul class="nav nav-pills nav-pills-default nav-pills-icons" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#link1" role="tablist">
                                        <img src="assets/img/card-blog1.png"  width="100px"><br><br> Zona Oeste
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#link2" role="tablist">
                                        <img src="assets/img/card-blog2.png"  width="100px"><br><br> Zona Norte
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link  " data-toggle="tab" href="#link3" role="tablist">
                                        <img src="assets/img/card-blog3.png"  width="100px"><br><br> Centro
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link  " data-toggle="tab" href="#link4" role="tablist">
                                        <img src="assets/img/card-blog3.png"  width="100px"><br><br> Zona Leste
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link  " data-toggle="tab" href="#link5" role="tablist">
                                        <img src="assets/img/card-blog3.png"  width="100px"><br><br> Zona Sul
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link  " data-toggle="tab" href="#link6" role="tablist">
                                        <img src="assets/img/card-blog6.png"  width="100px"><br><br> ABCD
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-space">
                                    <div class="tab-pane active show" id="link1">
                                        1 Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits.
                                        <br>
                                        <br> Dramatically visualize customer directed convergence without revolutionary ROI.
                                    </div>
                                    <div class="tab-pane" id="link2">
                                        2 Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas.
                                        <br>
                                        <br>Dramatically maintain clicks-and-mortar solutions without functional solutions.
                                    </div>
                                    <div class="tab-pane" id="link3">
                                        3 Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas.
                                        <br>
                                        <br>Dynamically innovate resource-leveling customer service for state of the art customer service.
                                    </div>
                                    <div class="tab-pane" id="link4">
                                        4 Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas.
                                        <br>
                                        <br>Dynamically innovate resource-leveling customer service for state of the art customer service.
                                    </div>
                                    <div class="tab-pane" id="link5">
                                        5 Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas.
                                        <br>
                                        <br>Dynamically innovate resource-leveling customer service for state of the art customer service.
                                    </div>
                                    <div class="tab-pane" id="link6">
                                        6 Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas.
                                        <br>
                                        <br>Dynamically innovate resource-leveling customer service for state of the art customer service.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="container text-center" style="padding-bottom:20px">
                <div class="row">
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-adventista.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-bosque.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-daluz.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-dayhospital.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-itaquera.png">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="info info-hover">
                            <div class="icon ">
                                <img src="assets/img/hospital-saopaolo.png">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/bg15a.jpg'); background-size: cover;" id="contato">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <div class="card card-testimonial card-plain">
                            <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                            <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                            <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                            QUERO COTAR AGORA
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer " style="background-color:#fff; color:#184B92">
                <div class="col-md-12">
                    <div class="container">
                        <div class="copyright">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, Desenvolvido por
                            <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#184B92">#agênciatrêsmeiazero</a>.
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <div class="qc-landing-form qc-layout-form formshake">
            <?php include("include/Form/FormAmil.php");?>
        </div>
    </div>
</div>
<!--End of Tawk.to Script-->
