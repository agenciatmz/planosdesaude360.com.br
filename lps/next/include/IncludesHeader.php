<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Plano de Saúde Next</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<link
        rel="shortcut icon"
        href="//v.fastcdn.co/u/b384de7c/17026856-0-logo-amil-1.png"
        type="image/ico"
/>
<link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/LandingPageNext.css" rel="stylesheet" />
