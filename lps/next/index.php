<?php require_once("include/IncludesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <?php include("include/IncludesHeader.php"); ?>

<body>

  <div class="js">

  <div id="preloader"></div>
<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Next">
                <img src="assets/img/LandingPagesNext/logo-color.png" width="140">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                        <p>Sobre nós</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#diferenciais" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Planos de Saúde Next</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Rede Credenciada</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                        <p>Cotação Online</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesNext/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 4em;"><span style="color:#F14A8F">NEXT SAÚDE:</span>  O NOVO PLANO DE SAÚDE DA AMIL </h1>
                        <h4 class="description" style="font-weight: 500">
                            A Next Amil Saúde está Presente em 83% do território nacional oferecendo Serviço de Qualidade por um Preço Acessível e a Confiança da Amil.
                        </h4>
                        <h2 class="description" style="color: #fff; background-color: #F14A8F; padding: 28px; border-radius: 19px; font-weight: 800; line-height: 20px; padding-top: 33px;">À PARTIR DE R$ 190,08* <br><br>
                            <span style="font-size: 0.35em; font-weight: 300; line-height: 10px">* Plano Individual 0 à 18 anos - SP Capital e Região.<br>
                                    Para saber os preços nas demais localidades com Rio de Janeiro, e faixa de idade solicite uma cotação clicando em saiba mais.</span>
                        </h2>
                        <button class="btn btn-info btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">
                            <?php
                            if(isset($_POST['cadastrar'])){
                                $nome            = trim(strip_tags($_POST['nome']));
                                $email           = trim(strip_tags($_POST['email']));
                                $telefone         = trim(strip_tags($_POST['telefone']));
                                $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
                                $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
                                $cnpj            = trim(strip_tags($_POST['cnpj']));
                                $estado          = trim(strip_tags($_POST['estado']));
                                $cidade          = trim(strip_tags($_POST['cidade']));
                                $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
                                $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
                                $operadora       = trim(strip_tags($_POST['operadora']));
                                $operadoraAmil      = trim(strip_tags($_POST['operadoraAmil']));
                                $operadoraBradesco     = trim(strip_tags($_POST['operadoraBradesco']));
                                $operadoraIntermedica       = trim(strip_tags($_POST['operadoraIntermedica']));
                                $operadoraSamed       = trim(strip_tags($_POST['operadoraSamed']));
                                $operadoraBiovida       = trim(strip_tags($_POST['operadoraBiovida']));
                                $operadoraTrasmontano       = trim(strip_tags($_POST['operadoraTrasmontano']));
                                $operadoraSulamerica       = trim(strip_tags($_POST['operadoraSulamerica']));
                                $operadoraNext       = trim(strip_tags($_POST['operadoraNext']));
                                $operadoraGoldencross       = trim(strip_tags($_POST['operadoraGoldencross']));
                                $operadoraMedSenior       = trim(strip_tags($_POST['operadoraMedSenior']));
                                $mensagem        = trim(strip_tags($_POST['mensagem']));
                                $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
                                $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));
                                $modalidade     = trim(strip_tags($_POST['modalidade']));

                                $insert = "INSERT INTO tmzleadsgeral ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidadepme, quantidadefamiliar, operadora, operadoraAmil, operadoraBradesco, operadoraIntermedica, operadoraSamed, operadoraBiovida, operadoraTrasmontano, operadoraSulamerica, operadoraNext, operadoraGoldencross, operadoraMedSenior, mensagem, tipodeplano, tipopessoa, modalidade  )
                                VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidadepme, :quantidadefamiliar, :operadora, :operadoraAmil, :operadoraBradesco, :operadoraIntermedica, :operadoraSamed, :operadoraBiovida, :operadoraTrasmontano, :operadoraSulamerica, :operadoraNext, :operadoraGoldencross, :operadoraMedSenior, :mensagem, :tipodeplano, :tipopessoa, :modalidade )";
                                try{

                                    $result = $conexao->prepare($insert);

                                    $result->bindParam(':nome', $nome, PDO::PARAM_STR);
                                    $result->bindParam(':email', $email, PDO::PARAM_STR);
                                    $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
                                    $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
                                    $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
                                    $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
                                    $result->bindParam(':estado', $estado, PDO::PARAM_STR);
                                    $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
                                    $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
                                    $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
                                    $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraAmil', $operadoraAmil, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraBradesco', $operadoraBradesco, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraIntermedica', $operadoraIntermedica, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraSamed', $operadoraSamed, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraBiovida', $operadoraBiovida, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraTrasmontano', $operadoraTrasmontano, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraSulamerica', $operadoraSulamerica, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraNext', $operadoraNext, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraGoldencross', $operadoraGoldencross, PDO::PARAM_STR);
                                    $result->bindParam(':operadoraMedSenior', $operadoraMedSenior, PDO::PARAM_STR);
                                    $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
                                    $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
                                    $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
                                    $result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);

                                    $result->execute();
                                    $contar = $result->rowCount();
                                    if($contar>0){

                                        {
                                            $msgClientesSucesso = '
                                                    <script type="text/javascript">
                                                    window.location = "include/Obrigado/LandingPageObrigadoNext.php";
                                                    </script>';

                                        }
                                    }else{
                                        $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Erro</strong> ao cadastrar o usuário.
                                                    </div>';
                                    }
                                }catch(PDOException $e){
                                    echo $e;
                                }

                            }else {
                                $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
                            }
                            ?>                                    <?php include("include/Form/FormNext.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-3" id="depoimentos">
    <div class="container" id="diferenciais">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                            <span style="color: #F14A8F">
                            Sobre o Plano Next Amil Saúde
                            </span>
                </h1>
            </div>
        </div>
        <br><br>
        <div class="container">
            <div class="row">
                <iframe width="100%" height="515" src="https://www.youtube.com/embed/iMCLhmxXBXU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <h4 class="description"> A <span style="color:#F14A8F; font-weight: 600"> Next Saúde </span> nasceu nos anos 70 graças à vontade de um grupo de médicos idealistas que desejavam estender o acesso à medicina de alto nível ao maior número possível de pessoas da região.</h4>
                <h4 class="description">   Qualidade e Ética fizeram com que a <span style="color:#F14A8F; font-weight: 600"> Next Saúde </span> se tornasse ao longo de 40 anos sinônimo de excelência na promoção da saúde:</h4>
                <div class="testimonials-3" id="depoimentos">
                    <div class="container" id="diferenciais">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4" >
                                    <div class="info info-hover" >
                                        <div class="icon icon-primary">
                                            <i class="now-ui-icons sport_trophy"></i>
                                        </div>
                                        <p class="description"></p>
                                        <p class="description">Uma das primeiras assistências<br> médicas no país</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="info info-hover">
                                        <div class="icon icon-success">
                                            <i class="now-ui-icons ui-2_like"></i>
                                        </div>
                                        <p class="description"></p>
                                        <p class="description">Primeira a instalar um centro exclusivo direcionado à saúde da mulher
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="info info-hover">
                                        <div class="icon icon-warning">
                                            <i class="now-ui-icons health_ambulance"></i>
                                        </div>
                                        <p class="description"></p>
                                        <p class="description">Uma das primeiras no país a conquistar a certificação de qualidade ISO 9000, Selo Nacional INMETRO e Internacional RvA</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <a href="#formulario" data-scroll class="botaoshake"><img src="//v.fastcdn.co/t/b384de7c/e41a0984/1528122751-18741831-547x100-btn-cotacao-mei-next.png" style="margin-top: 0px;" alt=""></a>
                </div>
                <h4 class="description">   Recentemente, a <span style="color:#F14A8F; font-weight: 600">Next</span> tornou-se integrante da <span style="color:#F14A8F; font-weight: 600">AMIL</span>, a maior empresa de saúde do Brasil e do <span style="color:#F14A8F; font-weight: 600">United Health Group</span>, um dos mais importantes grupos de saúde do mundo.</h4>
                <h4 class="description"> Seus planos garantem um atendimento médico atencioso e de qualidade, com uma rede selecionada e próxima dos seus clientes e beneficiários proporcionando cuidado diferenciado e resolutivo.</h4>
                <h4 class="description"> Gostou? Solicite uma cotação e garanta um Preço exclusivo de lançamento no estado do Rio de Janeiro abaixo.</h4>
            </div>
            <div class="col-md-12 text-center">
                <a href="#formulario" data-scroll  class="btn btn-danger btn-lg botaoshake" style="color:#fff; font-weight: 500; width: 100%; font-size: 1.2em">REALIZE SUA COTAÇÃO SEM COMPROMISSO</a>
            </div>
        </div>
    </div>
</div>
<div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/LandingPagesBradesco/bg31.jpg');     background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                            <span style="color: #fff">
                            Confira as opções de Planos Next Amil Saúde
                            </span>
                </h1>
            </div>
        </div>
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <h4 class="info-title">NEXT - PLANOS POR ADESÃO
                        </h4>
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake">Solicitar Cotação</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <h4 class="info-title">NEXT PLUS - PME ( 2 à 29 vidas )
                        </h4>
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake">Solicitar Cotação</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <h4 class="info-title">NEXT PLUS - RM ( 30 à 99 vidas )
                        </h4>
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake">Solicitar Cotação</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="contato">
    <div class="pricing-5 section-pricing-5 ">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="title" style="color:#EA4C89">
                        Ainda tem dúvidas?
                    </h2>
                    <h3 class="description" style="color:#000">Preencha o formulário e receba o contato de um dos nossos atendentes esclarecendo todas as suas dúvidas sobre os Planos Bradesco Saúde. Não leva 1 minuto!
                    </h3>
                    <div class="col-md-12 text-center">
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#fff; font-weight: 500; width: 100%; font-size: 1.2em">PREENCHER FORMULÁRIO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer" style="background-color:#cc092f; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>
<?php include("include/IncludesFooter.php"); ?>
</div>
</html>
