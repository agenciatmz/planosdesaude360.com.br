<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="ThemeStarz">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../../assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
	<title>Planos de Saúde Biovida</title>
    <?php require_once("../../includes/IncludesPixel.php"); ?>

</head>
<body data-spy="scroll" data-target=".navbar" >
<?php require_once("../../includes/IncludesChat.php"); ?>

    <div class="ts-page-wrapper" id="page-top">

        <header id="ts-hero" class="ts-full-screen" data-bg-parallax="scroll" data-bg-parallax-speed="3" >

            <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#1a1360">
                <div class="container">
                    <a class="navbar-brand" href="#page-top">
                        <img src="../../assets/img/Logo_BV.png" alt="" width="80">
                    </a>
                    <!--end navbar-brand-->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!--end navbar-toggler-->

                    <!--end collapse-->
                </div>
                <!--end container-->
            </nav>
            <!--end navbar-->

            <div class="container align-self-center">
                <div class="row align-items-center text-center">
                    <div class="col-sm-12">
                        <h1>OBRIGADO</h1>
                        <h3 class="ts-opacity__99">Em breve nossa equipe entrará em contato com você!</h3>
                        <h4 class="ts-opacity__70" style="font-weight: 300">Há mais de 10 anos no mercado, nossos pilares foram construídos com profissionais de grande experiência e amplo conhecimento técnico, mas principalmente com a ética e a satisfação dos clientes.
                        </h4>

                        <a href="../../index.php" class="btn btn-light btn-lg ts-scroll">Voltar</a>
                    </div>
                    <!--end col-sm-7 col-md-7-->

                    <!--end col-sm-5 col-md-5 col-xl-5-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->

            <div class="ts-background" data-bg-image-opacity=".6" data-bg-parallax="scroll" data-bg-parallax-speed="3">
                <div class="ts-svg ts-z-index__2">
                    <img src="../../assets/svg/wave-static-02.svg" class="w-100 position-absolute ts-bottom__0">
                    <img src="../../assets/svg/wave-static-01.svg" class="w-100 position-absolute ts-bottom__0">
                </div>
                <div class="owl-carousel ts-hero-slider" data-owl-loop="1">
                    <div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="../assets/img/bg-girl-01.jpg" data-bg-blend-mode="multiply"></div>
                    <!--<div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="assets/img/bg-girl-02.jpg" data-bg-blend-mode="multiply"></div>-->
                </div>
            </div>

        </header>
        <!--end #hero-->




        <!--end #footer-->
    </div>
    <!--end page-->

    <script>
        if( document.getElementsByClassName("ts-full-screen").length ) {
            document.getElementsByClassName("ts-full-screen")[0].style.height = window.innerHeight + "px";
        }
    </script>
	<script src="../../assets/js/jquery-3.3.1.min.js"></script>
	<script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/js/api-estado-cidade.js"></script>
    <script src="../../assets/js/validation.js"></script>
<script src="../../assets/js/sweetalert2.all.js"></script>
<script>$(document).ready(function () {
        swal({ title: "Obrigado", text: "Em breve nossos consultores entrarão em contato\n", type: "success" });
    });
</script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
	<script src="../../assets/js/isInViewport.jquery.js"></script>
	<script src="../../assets/js/jquery.particleground.min.js"></script>
	<script src="../../assets/js/owl.carousel.min.js"></script>
	<script src="../../assets/js/scrolla.jquery.min.js"></script>
	<script src="../../assets/js/jquery.validate.min.js"></script>
	<script src="../../assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    <script src="../../https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
    <script src="../../assets/js/jquery.wavify.js"></script>

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>-->
    <script src="../../assets/js/custom.js"></script>

</body>
</html>
