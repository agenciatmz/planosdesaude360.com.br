<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde 360</title>

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>
    <!-- Css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-icons.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="planosdesaude360.com.br" />
    <meta property="og:description" content="Os melhores Planos de Saúde" />
    <meta property="og:site_name" content="planosdesaude360.com.br" />
    <meta property="og:url" content="https://planosdesaude360.com.br/home" />
    <meta property="og:image" content="https://landingpages.planosdesaude360.com.br/generica/img/destaque.png" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
    <script type="text/javascript">
        (function(p,u,s,h){
            p._pcq=p._pcq||[];
            p._pcq.push(['_currentTime',Date.now()]);
            s=u.createElement('script');
            s.type='text/javascript';
            s.async=true;
            s.src='https://cdn.pushcrew.com/js/bc0842e12b6abdc003db3eab8145b1c0.js';
            h=u.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s,h);
        })(window,document);
    </script>

    <script src="//code.tidio.co/qvbl26alebsnjlsexlsszypqb6fkgsmo.js"></script>
    <?php include("includes/IncludesConexaoBanco.php"); ?>
    <?php include("includes/IncludesPixel.php"); ?>

</head>


<body>

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <main class="main-wrapper">

    <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="index.php" class="logo-container flex-child">
                              <img class="logo" src="img/logo.png" srcset="img/logo.png 1x, img/logo@2x.png 2x" alt="logo">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li>
                                  <a href="index.php">Home</a>
                              </li>
                              <li>
                                  <a href="#planos">Planos</a>
                              </li>
                              <li>
                                  <a href="#sobrenos">Sobre</a>

                              </li>
                              <li>
                                  <a href="blog/blog.php">Blog</a>

                              </li>
                              <li>
                                  <a href="#">Contato</a>
                              </li>
                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->

                      <div class="nav__btn-holder nav--align-right">
                          <a href="#formulario" class="btn nav__btn botaoshake">
                              <span class="nav__btn-text">Entre em contato</span>
                              <span class="nav__btn-phone">Clique aqui</span>
                          </a>
                      </div>

                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

      <!-- Triangle Image -->
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       viewBox="0 0 600 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-right">
      <g>
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img1)" />
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#triangle-gradient)" fill-opacity="0.7" />
      </g>
      <defs>
        <pattern id="img1" patternUnits="userSpaceOnUse" width="500" height="500">
          <image xlink:href="img/hero/hero.jpg" x="50" y="70" width="500" height="500"></image>
        </pattern>

        <linearGradient id="triangle-gradient" y2="100%" x2="0" y1="50%" gradientUnits="userSpaceOnUse" >
        <stop offset="0" stop-color="#4C86E7"/>
        <stop offset="1" stop-color="#B939E5"/>
        </linearGradient>
      </defs>
    </svg>


    <div class="content-wrapper oh">

      <!-- Hero -->
      <section class="hero">

        <div class="container">
          <div class="row">
            <div class="col-lg-5 offset-lg-1">
              <div class="hero__text-holder">
                <h1 class="hero__title hero__title--boxed">Os melhores <span class="highlight"> Planos de Saúde</span> você encontra aqui</h1>
                <h2 class="hero__subtitle">O Saúde 360 é um portal de dicas e noticias sobre saúde. São anos de Transparência e Dedicação, para levar à você informações uteis para uma vida mais saudável.

                    Em parceria com algumas das maiores corretoras e operadoras de Plano de Saúde do Brasil, desenvolvemos essa página de cotação online. Aqui você envia seus dados e nós encaminhamos para o corretor com o melhor preço no Plano de Saúde que você deseja! Não é perfeito? </h2>
              </div>
            </div>
          </div>

          <div class="row justify-content-center formshake" id="formulario" >
            <div class="col-lg-10">
              <!-- Optin Form -->
              <div class="optin box-shadow hover-up hover-line">
                  <h3 class="optin__title">Preencha o formulário abaixo e encontre o <span class="highlight">plano de saúde</span> perfeito para você</h3>
                  <?php include("includes/IncludesInsereBanco.php"); ?>
                  <?php echo $msgClientesSucesso; ?>
                  <?php echo $msgClientesErro; ?>
                  <?php echo $e; ?>
                  <form class="optin__form" role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
                    <?php include("includes/includeForm.php"); ?>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section> <!-- end hero -->

      <!-- Service Boxes -->
      <section class="section-wrap pb-72 pb-lg-40" id="planos">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-7">
              <div class="title-row">
                <h2 class="section-title text-center" style="color:#333B69">
                 Encontre o plano de saúde que<span class="highlight"> mais se encaixa</span> na sua necessidade.
                </h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line">
                <svg class="feature__icon "><img src="img/icons/user.svg" width="70" class="img-svg1"></svg>
                  <h4 class="feature__title">Planos de Saúde <span class="highlight">Individuais</span></h4>
                  <p class="feature__text">Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.<br><br><br></p><br>
                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>

              </div>

            </div>
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line">
                <svg class="feature__icon "><img src="img/icons/family.svg" width="70" class="img-svg1"></svg>
                <h4 class="feature__title">Planos de Saúde <span class="highlight">Familiares</span></h4>
                <p class="feature__text">Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui. De quebra, você garante uma redução de custo considerável caso possua CNPJ, pague até 30% menos por mês.</p>
                  <br>
                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line">
                <svg class="feature__icon "><img src="img/icons/group.svg" width="70" class="img-svg1"></svg>
                <h4 class="feature__title">Planos de Saúde <span class="highlight">Empresariais</span></h4>
                  <p class="feature__text">Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração em um Plano mais completo. São inúmeras possibilidades para garantir esse benefício indispensável que é o Plano de Saúde. </p>
                  <br>
                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>
              </div>
            </div>
<!---->
<!--              <div class="col-lg-3">-->
<!--                  <div class="feature box-shadow hover-up hover-line">-->
<!--                      <svg class="feature__icon "><img src="img/icons/constructor.svg" width="70" class="img-svg1"></svg>-->
<!--                      <h4 class="feature__title">Planos de Saúde <span class="highlight"><br>Adesão</span></h4>-->
<!--                      <p class="feature__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>-->
<!--                      <br>-->
<!--                      <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">-->
<!--                          <span>Cotar Agora</span>-->
<!--                          <i class="ui-arrow-right"></i>-->
<!--                      </a>-->
<!--                  </div>-->
<!--              </div>-->
          </div>
        </div>
      </section> <!-- end service boxes -->

      <!-- Promo Section -->
      <section class="section-wrap promo-section promo-section--pb-large pt-lg-40">

        <!-- Triangle Image -->
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 600 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-left">
          <g>
            <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img2)" />
          </g>
          <defs>
            <pattern id="img2" patternUnits="userSpaceOnUse" width="600" height="600">
              <image xlink:href="img/promo/promo_img_1.jpg" width="600" height="600"></image>
            </pattern>
          </defs>
        </svg>

        <div class="container" id="sobrenos">
          <div class="row justify-content-end">
            <div class="col-lg-6">
              <h2 class="promo-section__title promo-section__title--boxed">Clique e conheça os planos mais indicados para cada perfil.</h2>

                <div class="entry__article-wrap">
                <ul>
                    <li style="font-size: 2em;font-weight: 500;"><a href="#" data-toggle="modal" data-target="#exampleModal4"> Orçamento Reduzido</a></li>
                    <li style="font-size: 2em;font-weight: 500;"><a href="#" data-toggle="modal" data-target="#exampleModal5"> Redução de Custo (Down Grade) </a></li>
                    <li style="font-size: 2em;font-weight: 500;"><a href="#" data-toggle="modal" data-target="#exampleModal6"> Upgrade</a></li>
                </ul>
                </div>
              <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                <span>Cotar Agora</span>
                <i class="ui-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
      </section> <!-- end promo section -->


      <!-- Testimonials -->
      <section class="section-wrap bg-color">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-10">
              <div class="title-row">
                <h2 class="section-title">Conheça algumas pessoas que já ajudamos</h2>
                <p class="subtitle">Veja o que eles dizem de nós</p>
              </div>

              <div id="owl-testimonials" class="owl-carousel owl-theme owl-carousel--arrows-outside">

                <div class="testimonial clearfix">
                  <img src="img/testimonials/imgDepoimentos01.png" alt="" class="testimonial__img">
                  <div class="testimonial__info">
                    <span class="testimonial__author">Andreia Esposito</span>
                    <span class="testimonial__company">Analista de RH</span>
                  </div>
                  <div class="testimonial__body">
                    <p class="testimonial__text">“Fui Responsável pela cotação do Plano de Saúde na Empresa onde trabalho. Assim conheci a saúde 360 e consegui encontrar o melhor custo Beneficio em Plano de Saúde para nosso perfil. ”</p>
                    <div class="testimonial__rating">
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                    </div>
                  </div>
                </div>



                <div class="testimonial clearfix">
                  <img src="img/testimonials/imgDepoimentos03.png" alt="" class="testimonial__img">
                  <div class="testimonial__info">
                    <span class="testimonial__author">Bruno dos Santos</span>
                    <span class="testimonial__company">Músico</span>
                  </div>
                  <div class="testimonial__body">
                    <p class="testimonial__text">“Como autônomo não posso correr o risco de ficar doente e perder Shows, além disso fui Pai recentemente. Cotei um Plano de Saúde para mim e pro meu Filho, como tenho CNPJ fechei PME. Confesso que me surpreendi com os valores, são bem abaixo. Recomendo!”</p>
                    <div class="testimonial__rating">
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                    </div>
                  </div>
                </div>

                <div class="testimonial clearfix">
                  <img src="img/testimonials/imgDepoimentos02.png" alt="" class="testimonial__img">
                  <div class="testimonial__info">
                    <span class="testimonial__author">Henrique Freitas</span>
                    <span class="testimonial__company">Designer</span>
                  </div>
                  <div class="testimonial__body">
                    <p class="testimonial__text">“Estava pagando caro num plano Premium para minha Família, e não queria perder os benefícios. Migrei então para um plano Empresarial e economizei 30% em um Plano do mesmo Perfil. ”</p>
                    <div class="testimonial__rating">
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                      <i class="ui-star"></i>
                    </div>
                  </div>
                </div>

              </div> <!-- end owl-carousel -->
            </div>
          </div>
        </div>
      </section> <!-- end testimonials -->

      <!-- From Blog -->
      <section class="section-wrap">
        <div class="container">
          <div class="title-row title-row--boxed text-center">
            <h2 class="section-title">Novidades</h2>
            <p class="subtitle" style="color:#333B69">Fique por dentro de todas novidades sobre Planos de Saúde.</p>
          </div>
          <div class="row card-row">

            <div class="col-lg-4">
              <article class="entry card box-shadow hover-up">
                <div class="entry__img-holder card__img-holder">
                  <a href="blog/plano-de-saude-carencia-zero.php">
                    <img src="img/blog/post_1.jpg" class="entry__img" alt="">
                  </a>
                  <div class="entry__date">
                    <span class="entry__date-day">11</span>
                    <span class="entry__date-month">jul</span>
                  </div>
                  <div class="entry__body card__body">
                    <h4 class="entry__title">
                      <a href="blog/plano-de-saude-carencia-zero.php">Plano de Saúde carência zero existe ou não?</a>
                    </h4>
                    <ul class="entry__meta">
                      <li class="entry__meta-category">
                        <i class="ui-category"></i>
                        <a href="#">Planos de Saúde</a>
                      </li>
                      <li class="entry__meta-star">
                        <i class="ui-star"></i>
                        <a href="#">Leitura: 5 Min</a>
                      </li>
                    </ul>
                    <div class="entry__excerpt">
                      <p>Um dos maiores desejos dos brasileiros é ter um plano de saúde carência zero. Seja para tratar alguma doença, ou para simplesmente ter uma segurança para si ou para toda a família.</p>
                    </div><br>
                    <a href="blog/plano-de-saude-carencia-zero.php">Saiba mais</a>
                  </div>
                </div>
              </article>
            </div>

            <div class="col-lg-4">
              <article class="entry card box-shadow hover-up">
                <div class="entry__img-holder card__img-holder">
                  <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">
                    <img src="img/blog/post_2.jpg" class="entry__img" alt="">
                  </a>
                  <div class="entry__date">
                    <span class="entry__date-day">09</span>
                    <span class="entry__date-month">jul</span>
                  </div>
                  <div class="entry__body card__body">
                    <h4 class="entry__title">
                      <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">Como fazer um Plano de Saúde Empresarial</a>
                    </h4>
                    <ul class="entry__meta">
                      <li class="entry__meta-category">
                        <i class="ui-category"></i>
                        <a href="#">Planos de Saúde</a>
                      </li>
                      <li class="entry__meta-star">
                        <i class="ui-star"></i>
                        <a href="#">Leitura: 6 Min</a>
                      </li>
                    </ul>
                    <div class="entry__excerpt">
                      <p>O plano de saúde empresarial tem agradado a muitos usuários pelos seus muitos benefícios, mas você sabe como fazer um? A empresa que não quer dor de cabeça nem precisa pensar muito para saber que fazer um plano de saúde empresarial é a escolha certa.</p>
                      <br>
                      <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">Saiba mais</a>
                    </div>
                  </div>
                </div>
              </article>
            </div>

            <div class="col-lg-4">
              <article class="entry card box-shadow hover-up">
                <div class="entry__img-holder card__img-holder">
                  <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">
                    <img src="img/blog/post_3.jpg" class="entry__img" alt="">
                  </a>
                  <div class="entry__date">
                    <span class="entry__date-day">08</span>
                    <span class="entry__date-month">jul</span>
                  </div>
                  <div class="entry__body card__body">
                    <h4 class="entry__title">
                      <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Plano de Saúde cobre ou não cirurgia plástica?</a>
                    </h4>
                    <ul class="entry__meta">
                      <li class="entry__meta-category">
                        <i class="ui-category"></i>
                        <a href="#">Planos de Saúde</a>
                      </li>
                      <li class="entry__meta-star">
                        <i class="ui-star"></i>
                        <a href="#">Leitura: 6 Min</a>
                      </li>
                    </ul>
                    <div class="entry__excerpt">
                      <p>O Brasil é o segundo país do mundo em quantidade de cirurgia plástica, com mais de 1,22 milhão de procedimentos realizados, perdendo apenas para os Estados Unidos, que somam mais de 1,41 milhão.</p>
                      <br>
                      <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Saiba mais</a>
                    </div>
                  </div>
                </div>
              </article>
            </div>

          </div>
        </div>
      </section> <!-- end from blog -->

      <!-- Partners -->
      <section class="section-wrap section-wrap--pb-large bg-gradient" style="background-image: url(img/partners/map.png);">
        <div class="container">
          <div class="title-row title-row--boxed text-center">
            <h2 class="section-title">PLANOS COM O MELHOR CUSTO BENEFÍCIO</h2>
            <p class="subtitle">Cote agora com as melhores operadoras do mercado</p>
          </div>
          <div class="row justify-content-center text-center">
            <div class="col-lg-10">
              <div class="row pb-48">
                <div class="col-md col-sm-6">
                  <a href="#" data-toggle="modal" data-target="#exampleModal"> <img src="img/partners/1.png" alt=""></a>
                </div>
                <div class="col-md col-sm-6">
                    <a href="#" data-toggle="modal" data-target="#exampleModal2"> <img src="img/partners/2.png" alt=""></a>
                </div>
                <div class="col-md col-sm-6">
                    <a href="#" data-toggle="modal" data-target="#exampleModal3"> <img src="img/partners/3.png" alt=""></a>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section> <!-- end partners -->

      <!-- CTA -->
      <div class="container offset-top-152 pt-sm-48">
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="call-to-action box-shadow-large text-center">
              <div class="call-to-action__container">
                <h3 class="call-to-action__title">
                 Não perca a oportunidade de economizar, solicite um contato!
                </h3>
                <a href="#formulario" class="btn btn--lg btn--color">
                  <span>Solicitar Contato</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end cta -->

      <!-- Footer -->
      <footer class="footer">
        <div class="container">
          <div class="footer__widgets">
            <div class="row">

              <div class="col-lg-3 col-md-6">
                <div class="widget widget-about-us">
                  <!-- Logo -->
                  <a href="index.php" class="logo-container flex-child">
                    <img class="logo" src="img/logo.png" srcset="img/logo.png 1x, img/logo@2x.png 2x" alt="logo">
                  </a>
                  <p class="mt-24 mb-32">Encontre o plano de saúde que mais se encaixa na sua necessidade.</p>
                  <div class="socials">
                    <a href="https://www.instagram.com/planosdesaude_360/" class="social social-instagram" aria-label="instagram" title="instagram" target="_blank"><i class="ui-instagram"></i></a>
                    <a href="https://www.facebook.com/planosdesaude360/?ref=br_rs" class="social social-facebook" aria-label="facebook" title="facebook" target="_blank"><i class="ui-facebook"></i></a>
                  </div>
                </div>
              </div> <!-- end about us -->

              <div class="col-lg-2 offset-lg-5 col-md-6">
                <div class="widget widget_nav_menu">
                  <h5 class="widget-title">Saúde 360</h5>
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Planos</a></li>
                    <li><a href="#">Sobre</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contato</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-2 col-md-6">
                <div class="widget widget-address">
                  <h5 class="widget-title">Operadoras</h5>
                  <ul>
                    <li><a href="#">Amil</a></li>
                    <li><a href="#">Bradesco</a></li>
                    <li><a href="#">Sulamerica</a></li>
                    <li><a href="#">Intermedica</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- end container -->

        <div class="footer__bottom top-divider">
          <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Saúde 360, Desenvolvido por <a href="https://agenciatresmeiazero.com.br">Agência TresMeiaZero</a>
            </span>
          </div>
        </div> <!-- end footer bottom -->
      </footer> <!-- end footer -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->
  <?php include("includes/modal/modals.php"); ?>
  <div id="bio_ep">
      <div class="optin-exit">
          <h3 ><span class="highlight">Espere, não vá embora</span>! Temos algumas promoções de planos de saúde para você</h3>
          <?php include("includes/insereBancoPopup.php"); ?>
          <?php echo $msgClientesSucesso; ?>
          <?php echo $msgClientesErro; ?>
          <?php echo $e; ?>
          <form class="optin__form" role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
              <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>E-mail</label>
                      <input type="email" class="form-control" placeholder="Insira seu E-mail" name="strEmail" id="strEmail" required>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Cidade</label>
                      <input type="text" class="form-control phone_with_ddd" placeholder="Insira sua Cidade"  name="strCidade" id="strCidade" required>
                  </div>
              </div>
                  <button class="btn btn--md btn--color btn--button" id="cadastrar-exit" name="cadastrar-exit">Quero receber promoções</button>

              </div>
          </form>
      </div>
  </div>
  <!-- jQuery Scripts -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/validation.js"></script>
  <script src="js/api-estado-cidade.js"></script>
  <script src="js/FormSteps.js"></script>
  <script src="js/vidas.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="js/bioep.js"></script>
  <script type="text/javascript">
      bioEp.init({
          width: 600,
          height: 300,
      });

  </script>
  <!-- Cookies -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
  <script src="js/cookies.js"></script>

</body>
</html>
