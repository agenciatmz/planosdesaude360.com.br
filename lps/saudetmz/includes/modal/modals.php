<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde Amil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                A Amil é uma das Principais operadoras de Plano de Saúde do Brasil. Possui ampla Rede Médica credenciada e é uma das referências em Plano de Saúde no Brasil. Solicite sua cotação para mais informações.

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde Bradesco</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Uma das maiores empresas do Brasil, a Bradesco Saúde oferece diversas opções de Planos de Saúde. Com diferenciais como: Rede Médica Referenciada, Agilidade e Reembolso ela pode ser sua opção ideal. Faça uma cotação aqui.

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Plano de Saúde Sulamérica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                A SulAmérica Saúde é uma operadora com 120 anos de tradição! Reembolso e Planos com livre escolha de prestadores médicos são alguns diferenciais dessa grande Operadora. Faça uma cotação aqui e tire todas as suas dúvidas.

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Orçamento Reduzido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Se você está com um orçamento reduzido, mas não quer ficar sem Plano de Saúde, o Plano mais indicado para seu perfil (seja individual, familiar ou empresarial) são os Planos com coberturas regionais, dentre eles se destacam o Next Saúde do Grupo Amil e o Intermédica 200 up. Eles possuem rede de Atendimento própria com hospitais que fazem jus ao preço cobrado pelos planos e uma rede médica com profissionais qualificados. Caso o Plano seja para 2 ou mais pessoas e você possuir CNPJ sai ainda mais em conta.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Redução de Custo (Down Grade)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Se você está procurando reduzir o custo do seu Plano de Saúde, A Opção mais indicado para seu perfil (seja individual, familiar ou empresarial) é migrar seu Plano para empresarial utilizando um CNPJ e trocar de operadora buscando manter a mesma qualidade. Os casos mais comuns são: Sulamérica para Amil ou Bradesco para Amil.

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upgrade</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                No caso de você querer a melhor opção possível para você, sua família ou empresa, não tem conversa, o Plano de Saúde One Health do grupo Amil é considerado por muitos o melhor do País, cobertura internacional, acesso à todos os melhores hospitais e médicos do Brasil e tudo que um Plano de Saúde Premium pode oferecer
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>