<div class="d-none d-sm-block">
    <div class=" qc-landing-layout">
        <div class="qc-landing-info qc-layout-info">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde MedSênior">
                            <img src="assets/img/LandingPageMedSenior/logo.png" width="200">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#sobrenos" data-scroll style="background-color:#00984a">
                                    <p>SOBRE NOS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#rede" data-scroll  style="background-color:#00984a">
                                    <p>REDE CREDENCIADA</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="header">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('assets/img/LandingPageMedSenior/bg14.png'); "></div>
                    <div class="content-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-justify" style="padding:10px">
                            <h2 class="title" style="color:#00984A">Conheça a MedSênior</h2>
                            <h4 class="description">
                            O MedSênior é um produto voltado especialmente para a terceira idade e se diferencia dos demais produtos por cuidar da saúde de seus beneficiários de uma formar preventiva, acompanhando seu envelhecimento e visando que ocorra da forma mais saudável possível.
<br><br>
                            Com esse objetivo o MedSênior oferta, além de uma rede credenciada de referência, estruturas próprias com atendimento nas mais diferentes áreas com profissionais preocupados, zelosos e altamente qualificados.
<br><br>
                            O MedSênior oferta, sem custos adicionais, Oficinas de Saúde que visam proporcionar qualidade de vida para seus clientes aumentando sua autonomia, memória, equilíbrio, interação social e alimentação saudável.

                            </h4>
                        </div>
                    </div><br><br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                              <div class="icon icon-primary">
                                  <img src="assets/img/LandingPageMedSenior/banner1.png">
                              </div>
                                <h4 class="info-title" style="color:#00984A">Rede Própria</h4>
                                <p class="description text-justify">A MedSênior possui estrutura própria, incluindo hospitais, ambulatórios, centros de especialidades médicas, centros de diagnósticos e laboratórios.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                              <div class="icon icon-primary">
                                  <img src="assets/img/LandingPageMedSenior/banner2.png">
                              </div>
                                <h4 class="info-title" style="color:#00984A">Medicina Preventiva</h4>
                                <p class="description text-justify">O Plano de Saúde Medsênior é diferente dos outros planos porque não está focado apenas nos atendimentos ambulatoriais e hospitalares. Ele também cuida da saúde de seus beneficiários de forma preventiva.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                              <div class="icon icon-primary">
                                  <img src="assets/img/LandingPageMedSenior/banner3.png">
                              </div>
                                <h4 class="info-title" style="color:#00984A">Oficinas de Saúde</h4>
                                <p class="description text-justify">O Medsênior desenvolveu o Programa de Oficinas de Saúde para melhorar a qualidade de vida de seus beneficiários.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pricing-5 section-pricing-5 " id="rede">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h1 class="title" style="font-size: 3em">
                            <span style="color: #00984A">
                            MEDSÊNIOR BELO HORIZONTE
                            </span>
                            </h1>
                        </div>
                    </div>
                    <br><br>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageMedSenior/medseniorbh.jpg">
                                    </div>
                                    <h4 class="info-title">Unidade Belo Horizonte-MG</h4>
                                    <p class="description">A Unidade de Prevenção e Especialidades - Belo Horizonte - MG,  possui 1.200 metros quadrados de área construída, que abriga sete consultórios médicos e salas para as Oficinas de Saúde, além de toda a rede credenciada (hospitais, clínicas, laboratórios e médicos).</p>

                                </div>
                            </div>


                        </div>
                        <br>

                    </div>
                </div>
            </div>
            <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/LandingPageMedSenior/bg30.jpg'); background-size: cover;" id="contato">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <div class="card card-testimonial card-plain">
                            <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                            <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                            <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                                QUERO COTAR AGORA
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer " style="background-color:#fff; color:#ED3237">
                <div class="col-md-12">
                    <div class="container">
                        <div class="copyright">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, Desenvolvido por
                            <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#ED3237">#agênciatrêsmeiazero</a>.
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <div class="qc-landing-form qc-layout-form formshake">

            <?php include("include/Form/FormMedSenior.php");?>
        </div>
    </div>
</div>
<!--End of Tawk.to Script-->
