<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Agência TresMeiaZero</title>
    <meta name="description" content="" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <?php include("include/IncludesHeader.php"); ?>
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link href="assets/css/LandingPageB2B.css" rel="stylesheet" />
    <?php require_once("include/IncludesPixel.php"); ?>

<body>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="671b7a79-e122-4b95-80bf-e7967639648a";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Agência TresMeiaZero">
                <img src="assets/img/LandingPagesB2B/logo-color.png" width="150">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#conheca" data-scroll>
                        <i class="now-ui-icons tech_tv"></i>
                        <p>Sobre nós</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#pacotes" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Pacotes</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">

    <div class="page-header header-filter" >
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesB2B/bg14.jpg');"></div>
        <div class="content-center" id="formulario">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 6em"> <div class="lateralbar"> </div>BEM VINDO</h1>

                        <h2 class="description" style="color:#fff">Te ver por aqui é sinal que você está querendo <span class="boldheader"> VENDER MAIS</span>. E nós <span class="boldheader">somos</span> o <span class="boldheader">parceiro certo </span>para te <span class="boldheader">ajudar</span>.</h2>
                        <h4 class="description" style="color:#fff">
                            <span class="boldheader">Geramos leads de qualidade</span> para corretores de <span class="boldheader">Plano de Saúde em todo território nacional</span>. Nossas <span class="boldheader">Indicações tem ajudado</span> milhares de <span class="boldheader">corretores à baterem metas</span> e ganharem prêmios em todo Brasil!
                            <br><br>
                            Quer <span class="boldheader">conhecer</span> nossos <span class="boldheader">leads de Qualidade</span>?
                            <span class="boldheader">Preencha o formulario</span> para receber um <span class="boldheader">contato de nossos especialistas.</span></h4>
                        <button id="botaoshake" data-scroll  class="btn btn-warning btn-lg pull-left" style="color:#660069; font-weight: 500">Preencher formulário</button>
                    </div>

                    <div class="col-md-5 ml-auto mr-auto "  style="top: 43px;">

                        <iframe width="100%" height="500" src="https://agenciatresmeiazero.com.br/gerenciador/forms/wtl/e750a31d0ca41835afe0b3ae75225391" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-3" id="conheca">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-left">
                <h1 class="title" style="font-size: 3em"> <div class="lateralbarConteudo"> </div>

                    <span style="color: #e5b463">CONHEÇA À AGÊNCIA TRÊS<span style="color: #68036b">MEIA</span>ZERO</span></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 ">
                <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">A TrêsMeiaZero é uma agência de Marketing Digital que atende clientes em todo o Brasil nos mais variados segmentos.<br><br>
                    Seus Sócios fundadores: Felipe, Paulo e Dennis, tem vasta experiência no mercado de seguros, mais especificamente no de leads (Indicações) para corretoras de Planos de, Saúde e esse acabou se tornando o principal ramo de atividade da #TMZ considerada por muitos, referência em qualidade nesse mercado.</p>
                <a href="#formulario" data-scroll  class="btn btn-warning btn-lg pull-left" style="color:#660069; font-weight: 500">Solicite um orçamento agora</a>

            </div>
            <div class="col-md-5 ">
                <img src="assets/img/LandingPagesB2B/imgPersonagem360.png" class="img-responsive pull-right d-none d-sm-block" width="430px" style="position: relative; bottom: 90px;">
            </div>
        </div>
    </div>
    <div class="container" id="depoimentos">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-right">
                <h1 class="title" style="font-size: 3em"><span style="color: #e5b463">PARCERIAS DE SUCESSO<div class="lateralbarConteudoRight"> </div></span></h1>
            </div>
        </div>
        <div class="row">

            <div class="col-md-5">
                <img src="assets/img/LandingPagesB2B/imgParceria.png" class="img-responsive pull-left d-none d-sm-block" width="430px" style="position: relative; bottom: 90px;">
            </div>
            <div class="col-md-7 text-right">
                <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">"Tive algumas experiências péssimas com outras agências, cheguei a perder grandes equipes por conta de indicações ruins. Procurando por uma nova alternativa encontrei a trêsmeiazero, o atendimento me passou confiança, fizemos um teste, e hoje os corretores tem vontade de trabalhar com a gente por conta da qualidade das indicações, só tenho à agradecer o pessoal ".</p>
                <p class="description" style="font-weight: 800; color: #68036b;">- Lucas Rezende, Gerente Comercial.</p>


                <a href="#formulario" data-scroll   class="btn btn-warning btn-lg pull-right" style="color:#fff; font-weight: 500; background-color: #68036b">Seja nosso parceiro também! Saiba mais aqui</a>
            </div>
        </div>
    </div></div>
<div id="pacotes">
    <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/LandingPagesB2B/bg31.jpg')">
        <div class="container">
            <div class="row">

                <div class="col-md-7 ml-auto mr-auto">
                    <div class="tab-content tab-space">
                        <div class="tab-pane active" id="personal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box d-none d-sm-block">
                                        <div class="ribbon"><span>20% OFF</span></div>
                                    </div>
                                    <div class="card card-pricing card-raised" >
                                        <div class="card-body">

                                            <h3 class="category" style="color: #68036b">ADESÃO <p class="category" style="font-size: 18px; color: #5b70d8">20% DE DESCONTO</p> </h3>

                                            <ul>
                                                <li>
                                                    <b style="color: #5b70d8">100 Leads</b> Pessoa Física</li>
                                                <li>
                                                    <b style="color: #5b70d8">Leads</b> em tempo real</li>
                                                <li>
                                                    <b style="color: #5b70d8">Garantia</b> de interesse</li>
                                                <li>
                                                    <b style="color: #5b70d8">Exclusividade</b> garantida</li>
                                                <li>
                                                    <b style="color: #5b70d8">Escolha</b> quanto quer receber por dia</li>
                                            </ul>
                                            <a href="#formulario"  data-scroll  class="btn btn-warning btn-lg " style="color:#fff; font-weight: 500; background-color: #5b70d8">Aproveitar valor promocional</a>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card card-pricing card-raised">
                                        <div class="card-body">
                                            <h3 class="category" style="color: #68036b">MESCLADO <p class="category" style="font-size: 18px; color: #55b162">O MAIS VENDIDO</p> </h3>

                                            <ul>
                                                <li>
                                                    <b style="color: #55b162">15 Leads</b> Pessoa Física</li>
                                                <li>
                                                    <b style="color: #55b162">5 Leads</b> PME</li>
                                                <li>
                                                    <b style="color: #55b162">Garantia</b> de interesse</li>
                                                <li>
                                                    <b style="color: #55b162">Exclusividade</b> garantida</li>
                                                <li>
                                                    <b style="color: #55b162">Escolha</b> quanto quer receber por dia</li>
                                            </ul>
                                            <a href="#formulario" data-scroll  class="btn btn-warning btn-lg" style="color:#fff; font-weight: 500; background-color: #55b162">Consultar Valores</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 ">
                    <h2 class="title" style="color:#fff"><div class="lateralbarPreco"> </div>Escolha o melhor plano para você</h2>
                    <p class="description" style="color:#fff">Precisa de um volume maior de Leads ou quer personalizar seu Pacote conforme as necessidades da sua corretora?<br><br>

                        Nós também conseguimos te ajudar. Fale com nosso Gerente de Contas, o Eduardo e ele irá entender sua necessidade e te auxiliar da melhor forma possível.

                    </p>
                    <a href="#formulario" data-scroll class="btn btn-warning btn-lg pull-left d-none d-sm-block"  style="color:#650067; font-weight: 800;     background-color: #fbc933;">QUERO FALAR COM O EDUARDO</a>
                    <a href="https://api.whatsapp.com/send?phone=5519991028138&text=Olá%20Eduardo%20tudo%20bem?" class="btn btn-success btn-lg d-block d-sm-none"  style="color:#fff; font-weight: 800;">FALAR COM O EDUARDO NO WHATSAPP</a>

                </div>
            </div>
        </div>
    </div></div>
<div class="testimonials-3" id="contato">
    <div class="container">
        <div class="row">

        </div>
        <div class="row">
            <div class="col-md-7">
                <h2 class="title" style="color:#68036b"><div class="lateralbarPreco"> </div>VAMOS BATER UM PAPO?</h2>
                <div class="info info-horizontal">
                    <div class="icon icon-primary">
                        <img src="assets/img/LandingPagesB2B/imgContatosWhatsApp.png">
                    </div>
                    <div class="description">
                        <h5 class="info-title">(19) 9 9997-9569</h5>

                    </div>
                </div>
                <div class="info info-horizontal">
                    <div class="icon icon-primary">
                        <img src="assets/img/LandingPagesB2B/imgContatosSkype.png">
                    </div>
                    <div class="description">
                        <h5 class="info-title">atendimento@agenciatresmeiazero.com.br</h5>
                    </div>
                </div>
                <div class="info info-horizontal">
                    <div class="icon icon-primary">
                        <img src="assets/img/LandingPagesB2B/imgContatosEmail.png">
                    </div>
                    <div class="description">
                        <h5 class="info-title">eduardo.foleis@agenciatresmeiazero.com.br</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-5 ml-auto mr-auto">
                <img src="assets/img/LandingPagesB2B/imgContatos.png" class="img-responsive  d-none d-sm-block" width="380" style="position: relative; top: 20px; ">

            </div>

        </div>
    </div>
</div>

<footer class="footer footer-big" style="background-color:#780078; color:#fff">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5>Contato</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                atendimento@agenciatresmeiazero.com.br
                            </a>
                        </li>
                        <li>
                            <a href="#" >
                                (19) 9 9102-8138
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>Endereço</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                R. Francisco Glicério, 738<br>
                                Sala 01, Vila Embaré, Valinhos/SP.
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-4">
                    <h5>Redes Socias</h5>
                    <ul class="social-buttons">
                        <li>
                            <a href="https://www.facebook.com/agenciatresmeiazero/" class="btn btn-icon btn-neutral btn-facebook btn-round">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/ag_tresmeiazero" class="btn btn-icon btn-neutral btn-twitter btn-round">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="https://www.linkedin.com/company-beta/25018320/" class="btn btn-icon btn-neutral btn-dribbble btn-round">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/agenciatresmeiazero/" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-icon btn-neutral btn-google btn-round">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://br.pinterest.com/agenciatrsmeiazero/" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>

    </div>
</footer>


<footer class="footer " style="background-color:#460146; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>

<!--<script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>-->
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="assets/js/plugins/jquery.validate.min.js"></script>
<script src="assets/js/plugins/additional-methods.min.js"></script>
<script src="assets/js/plugins/sweet-scroll.min.js"></script>
<script src="assets/js/validation.js"></script>
<script src="assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
<script src="assets/js/plugins/api-estado-cidade.js"></script>
<script src="assets/js/plugins/custom.js"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/moment.min.js"></script>
<script src="assets/js/plugins/bootstrap-switch.js"></script>
<script src="assets/js/plugins/bootstrap-tagsinput.js"></script>
<script src="assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<script src="assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

</html>



