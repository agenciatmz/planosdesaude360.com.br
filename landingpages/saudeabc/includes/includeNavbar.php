<header class="nav">
    <div class="nav__holder nav--sticky">
        <div class="container-fluid container-semi-fluid nav__container">
            <div class="flex-parent">

                <div class="nav__header">
                    <!-- Logo -->
                    <a href="../index.php" class="logo-container flex-child">
                        <img class="logo" src="../img/logo.png" srcset="../img/logo.png 1x, ../img/logo@2x.png 2x" alt="logo">
                    </a>

                    <!-- Mobile toggle -->
                    <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="nav__icon-toggle-bar"></span>
                        <span class="nav__icon-toggle-bar"></span>
                        <span class="nav__icon-toggle-bar"></span>
                    </button>
                </div>

                <!-- Navbar -->
                <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                    <ul class="nav__menu">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a href="#planos">Planos</a>
                        </li>
                        <li>
                            <a href="#sobrenos">Sobre</a>

                        </li>
                        <li>
                            <a href="blog/blog.php">Blog</a>

                        </li>
                        <li>
                            <a href="#">Contato</a>
                        </li>
                    </ul> <!-- end menu -->
                </nav> <!-- end nav-wrap -->

                <div class="nav__btn-holder nav--align-right">
                    <a href="#formulario" class="btn nav__btn botaoshake">
                        <span class="nav__btn-text">Entre em contato</span>
                        <span class="nav__btn-phone">Clique aqui</span>
                    </a>
                </div>

            </div> <!-- end flex-parent -->
        </div> <!-- end container -->

    </div>
</header> <!-- end navigation -->