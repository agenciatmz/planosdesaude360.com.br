<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        require_once("includes/includesheader.php");
       require_once("../helpers/includesPixel.php");
       require_once("../helpers/includesChat.php"); ?>

    </head>
    <div class="js">

    <div id="preloader"></div>
    <body class="template-page">

        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png" >

                        <!-- <img src="assets/img/logo-color.png" class="d-none d-sm-block" width="150px"> -->
                          <!-- <img src="assets/img/logo-white.png" class="d-block d-sm-none" width="150px"> -->
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>Sobre nós</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>Amil Saúde</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="page-header header-filter">
                                <div class="page-header-image" style="background-image: url('assets/img/bg1.png');"></div>
                                <div class="content-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 text-left">
                                              <h1 class="title"> <span style="color:#08377F">MAIS DE <span style="font-weight: 600"><span style="color:#05AF4B">5 MILHÕES</span> DE BRASILEIROS CONFIAM NA AMIL</span> </h1>
                                                <p class="description" style="font-weight: 400; color:#08377F">
                                                A Amil oferece planos com excelente custo benefício, médicos e hospitais perto da sua casa, trabalho e escolha de seus filhos. Além de descontos em estabelecimentos parceiros como grandes redes farmacêuticas. Solicite uma cotação abaixo e faça parte da família AMIL.
                                                </p>
                                                <button class="btn btn-warning btn-lg pull-left" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Solicitar Cotação</button>
                                            </div>
                                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                                <img src="assets/img/header-first.png" width="440" class="d-none d-sm-block" style="
                                                    position: absolute;
                                                    top:-68px;
                                                    ">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">SOBRE O PLANO</span> <br><span style="font-weight:800" class="azulescuro">AMIL SAÚDE</span></h2>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque01.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#3166b7">COBERTURA EM TODO TERRITÓRIO NACIONAL</h4>
                                <p class="description" style="font-weight:400">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em todo território nacional, para maiores informações solicite uma cotação em nossa página!</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque02.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#3166b7">COBERTURA DE PROCEDIMENTOS</h4>
                                <p class="description" style="font-weight:400">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação, aqui mesmo em nossa página, para que um Corretor Autorizado lhe auxilie com maiores informações.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque03.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#3166b7">PREÇOS DOS PLANOS DE SAÚDE AMIL</h4>
                                <p class="description" style="font-weight:400">Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado. Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros) </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            <div class="container text-center">
                                Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
        </div>
        <div class="pricing-5 section-pricing-5 " id="redecredenciada" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center">
                        <h1 class="title" style="font-size: 3em">
                            <span style="color: #08377F">
                            CONHEÇA A REDE CREDENCIADA DA AMIL
                            </span>
                        </h1>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto text-center">

                            <ul class="nav nav-pills nav-pills-default nav-pills-icons" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="modal" data-target="#myModal" role="tablist">
                                    <img src="assets/img/card-blog1.png"  width="100px"><br><br> Zona Oeste
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  data-toggle="modal" data-target="#myModal2" role="tablist">
                                    <img src="assets/img/card-blog2.png"  width="100px"><br><br> Zona Norte
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal3" role="tablist">
                                    <img src="assets/img/card-blog3.png"  width="100px"><br><br> Centro
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal4" role="tablist">
                                    <img src="assets/img/card-blog4.png"  width="100px"><br><br> Zona Leste
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal5" role="tablist">
                                    <img src="assets/img/card-blog5.png"  width="100px"><br><br> Zona Sul
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal6" role="tablist">
                                    <img src="assets/img/card-blog6.png"  width="100px"><br><br> ABCD
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal7" role="tablist">
                                    <img src="assets/img/card-blog7.png"  width="100px"><br><br> Alto do Tiête
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal8" role="tablist">
                                    <img src="assets/img/card-blog8.png"  width="100px"><br><br> Itapecerica da Serra
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal9" role="tablist">
                                    <img src="assets/img/card-blog9.png"  width="100px"><br><br> Região de Osasco
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link  "  data-toggle="modal" data-target="#myModal10" role="tablist">
                                    <img src="assets/img/card-blog10.png"  width="100px"><br><br> Franco da Rocha
                                    </a>
                                </li>
                            </ul>

                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-info" role="alert">
                                <div class="container text-center">
                                    Não encontrou o Hospital desejado? Solicite uma cotação para receber a Rede Credenciada completa! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Solicitar Cotação</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--     *********    END FEATURES 1      *********      -->
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE</span> <br><span style="font-weight:800" class="azulescuro">PLANOS AMIL SAÚDE</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg23.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">AMIL <Br>INDIVIDUAL</h4>
                                    <p>Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.</p>
                                    <button class="btn btn-warning ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg24.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">AMIL <Br> FAMILIAR</h4>
                                    <p>Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui.</p>
                                    <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg25.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">AMIL <Br> EMPRESARIAL</h4>
                                    <p>Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração.</p>
                                    <button class="btn btn-warning ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-2 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <!-- <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li> -->
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
                                              "Sempre que precisei da Amil nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios, atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito."

                                            </h5>
                                            <h3 class="card-title">Arlete Almeida</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Cliente Amil Saúde</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description"> Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..

                                            </h5>
                                            <h3 class="card-title">Matheus Cardoso</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente AMIL Saúde</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="contato">
            <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-color:#082A7E">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="title" style="font-size: 3em">
                                        <span style="color: #fff">
                                            AINDA TEM DÚVIDAS?
                                            <p class="description" style="    color: #fff;font-size: 0.6em">
                                                Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde Amil ideal para você!
                                        </span>
                            </h1>
                            <div class="col-md-12 text-center">
                                <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1em">
                                COTAÇÃO SEM COMPROMISSO!
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            Sobre nós
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            Planos de Saúde
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card card-login card-plain">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>

                    </div>
                    <div class="modal-body">
                        <?php include("../helpers/insereBanco.php"); ?>
                            <?php include("includes/Form/FormBradesco.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/includesmodal.php"); ?>

    <?php include("includes/IncludesFooter.php"); ?>
  </div>
</html>
