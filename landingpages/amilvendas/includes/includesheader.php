<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
<link
    rel="shortcut icon"
    href="assets/img/favicon.ico"
    type="image/ico"
    />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>AMIL SAÚDE</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<!--     Fonts and icons     -->

<!-- CSS Files -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
