<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        require_once("includes/includesheader.php");
        require_once("../helpers/includesPixel.php");
        require_once("../helpers/includesChat.php");
         ?>

    </head>
    <body class="template-page">


        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png"  >
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>VANTAGENS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>MARCAS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 text-left">
                                <h1 class="title" style="font-size: 3em;"><span style="color:#fff"><span style="font-weight: 300"> O IMÓVEL DO SEUS <Br></span> SONHOS ESTÁ AQUI! </h1>
                                </span>
                                <img src="assets/img/header-first.png" width="800px" class="img-responsive d-none d-sm-block">

                            </div>
                            <div class="col-md-5 ml-auto mr-auto" id="formulario">
                              <div class="card card-contact card-raised formshake">
                                <?php include("../helpers/insereBanco.php"); ?>
                                     <?php include("includes/Form/form.php");?>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">

           <!--     *********    END FEATURES 1      *********      -->
           <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title azulclaro">VANTAGENS DO CONSÓRCIO</h2>
                            <h4 class="description">Confira as vatanges de adquirir um consórcio</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgSeguranca.png">
                                </div>
                                <h4 class="info-title">Segurança </h4>
                                <p class="description"> Você planeja a aquisição do seu imóvel, como uma poupança programada.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                <img src="assets/img/imgFlexibilidade.png">
                                </div>
                                <h4 class="info-title">Flexibilidade  </h4>
                                <p class="description"> No momento da compra você ainda pode negociar descontos, pois com o consórcio o pagamento é parcelado, mas a compra é à vista.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgLiberdade.png">
                                </div>
                                <h4 class="info-title">Liberdade  </h4>
                                <p class="description">  Quando contemplado, você tem total liberdade para escolher o imóvel que deseja adquirir, em qualquer local do País.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title azulclaro">PRINCIPAIS MARCAS</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imRodobens.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                <img src="assets/img/imgEmbracon.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgSicredi.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgPortoSeguro.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <div class="features-1">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-8 ml-auto mr-auto">
                                      <h2 class="title azulclaro">CONFIRA NOSSAS CARTAS DE CRÉDITO</h2>
                                  </div>
                              </div>
                              <div class="row">

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 100.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 669,62</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 105.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 703,15</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 110.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 736,58</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 115.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 770,12</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 120.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 803,54</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 125.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 837,08</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 130.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 870,50</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 140.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 937,47</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 150.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.004,43</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 160.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.071,39</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 170.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.138,35</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 180.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.205,31</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 190.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.272,28</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 200.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.339,24</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 210.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.406,20</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 220.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.473,16</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 230.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.540,12</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 240.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.607,09</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 250.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.674,05</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 260.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.741,01</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 270.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.807,97</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 280.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.874,93</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 290.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.941,90</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 300.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 2.008,86</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 350.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 2.343,67</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 400.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 2.678,48</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 450.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 3.013,29</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 500.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 3.348,10</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 550.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 3.682,90</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Carta de crédito<br> no valor de</h6>
                                                <h3 class="card-title valortotalcarta">R$ 600.000,00</h3>
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 4.017,71</h3>
                                                <a href="#pablo" class="btn btn-success-gradiant btn-round">SIMULE AQUI</a>
                                        </div>
                                    </div>
                                </div>



                              </div>
                          </div>
                      </div>
        </div>

     
        <div class="features-2 " style="background-image: url('assets/img/bg30.png'); background-size: cover;background-position: center center;">
                <div class="col-md-8 ml-auto mr-auto text-center" style="padding-top:40px">
                    <h1 class="title" style="font-size: 30px">AINDA TEM DÚVIDAS? FAÇA UMA COTAÇÃO ONLINE AGORA MESMO</h1>
                
                    <a href="#" class="btn btn-info btn-lg"  style="background-color: #fff000;color:#000; font-weight: 500">Solicitar Cotação</a>
                </div>
            </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            VANTAGENS 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            MARCAS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>

    <?php include("includes/IncludesFooter.php"); ?>
</html>
