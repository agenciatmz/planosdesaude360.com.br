<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
<link
    rel="shortcut icon"
    href="//v.fastcdn.co/u/b384de7c/17026856-0-logo-amil-1.png"
    type="image/ico"
    />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Consórcio Imóveis</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<!-- CSS Files -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />