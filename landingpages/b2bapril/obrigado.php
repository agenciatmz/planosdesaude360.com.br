<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Agência TresMeiaZero</title>
    <meta name="description" content="" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <?php include("include/IncludesHeader.php"); ?>
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link href="assets/css/LandingPageB2B.css" rel="stylesheet" />
    <?php require_once("../helpers/includesPixel.php"); ?>

<body>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="671b7a79-e122-4b95-80bf-e7967639648a";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Agência TresMeiaZero">
                <img src="assets/img/LandingPagesB2B/logo-color.png" width="150">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#conheca" data-scroll>
                        <i class="now-ui-icons tech_tv"></i>
                        <p>Sobre nós</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#pacotes" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Pacotes</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">

    <div class="page-header header-filter" >
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesB2B/bg14.jpg');"></div>
        <div class="content-center" id="formulario">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 6em"> <div class="lateralbar"> </div> OBRIGADO</h1>

                        <h2 class="description" style="color:#fff">Em breve nossos consultores entrarão em contato</h2>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>





<footer class="footer footer-big" style="background-color:#780078; color:#fff">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5>Contato</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                atendimento@agenciatresmeiazero.com.br
                            </a>
                        </li>
                        <li>
                            <a href="#" >
                                (19) 9 9102-8138
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>Endereço</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                R. Francisco Glicério, 738<br>
                                Sala 01, Vila Embaré, Valinhos/SP.
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-4">
                    <h5>Redes Socias</h5>
                    <ul class="social-buttons">
                        <li>
                            <a href="https://www.facebook.com/agenciatresmeiazero/" class="btn btn-icon btn-neutral btn-facebook btn-round">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/ag_tresmeiazero" class="btn btn-icon btn-neutral btn-twitter btn-round">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="https://www.linkedin.com/company-beta/25018320/" class="btn btn-icon btn-neutral btn-dribbble btn-round">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/agenciatresmeiazero/" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-icon btn-neutral btn-google btn-round">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://br.pinterest.com/agenciatrsmeiazero/" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>

    </div>
</footer>


<footer class="footer " style="background-color:#460146; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>

<!--<script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>-->
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="assets/js/plugins/jquery.validate.min.js"></script>
<script src="assets/js/plugins/additional-methods.min.js"></script>
<script src="assets/js/plugins/sweet-scroll.min.js"></script>
<script src="assets/js/validation.js"></script>
<script src="assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
<script src="assets/js/plugins/api-estado-cidade.js"></script>
<script src="assets/js/plugins/custom.js"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/moment.min.js"></script>
<script src="assets/js/plugins/bootstrap-switch.js"></script>
<script src="assets/js/plugins/bootstrap-tagsinput.js"></script>
<script src="assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<script src="assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>

</html>
