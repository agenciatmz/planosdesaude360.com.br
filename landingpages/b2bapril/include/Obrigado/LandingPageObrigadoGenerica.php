<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Amil Saúde - Cotação</title>
    <meta name="description" content="" />
    <meta name="keywords" content="Plano de Saúde, Plano de Saúde Amil, Amil Saúde" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Amil Saúde - Cotação" />
    <meta property="og:description" content="Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado.Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)&#38;nbsp;" />
    <meta property="og:site_name" content="Amil Saúde - Cotação" />
    <meta property="og:url" content="http://lp.360planodesaude.com.br/saude/amil/" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../assets/css/obrigado.css" rel="stylesheet" />
    <link href="../../assets/css/demo.css" rel="stylesheet" />
    <script src="../../assets/js/plugins/sweetalert2.all.js"></script>
    <?php require_once("../../include/IncludesPixel.php"); ?>
</head>
<body>
<div class="header-1">
    <nav class="navbar navbar-expand-lg  bg-white navbar-absolute">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Agência TresMeiaZero">
                    <img src="https://c.fastcdn.co/u/b384de7c/23165201-0-picasion.com-f80b3ba.gif" width="150">
                </a>
            </div>
    </nav>
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('../../assets/img/LandingPagesGenerica/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto">
                        <h1 class="title" style="font-size: 4em">OBRIGADO</h1>
                        <h3 class="title">Em breve nossos consultores entrarão em contato</h3>
                        <div class="col-md-12  ml-auto mr-auto">
                            <a href="../../LandingPageGenerica.php" class="btn btn-danger btn-lg btn-round">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer " data-background-color="black">
    <div class="container">
        <div class="copyright">
            ©
            <script>
                document.write(new Date().getFullYear())
            </script>, Desenvolvido por
            <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
        </div>
    </div>
</footer>
</body>
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="../../assets/js/plugins/jquery.validate.min.js"></script>
<script src="../../assets/js/plugins/additional-methods.min.js"></script>
<script src="../../assets/js/plugins/sweet-scroll.min.js"></script>
<script src="../../assets/js/plugins/sweetalert2.all.js"></script>
<script>$(document).ready(function () {
        swal({ title: "Obrigado", text: "Em breve nossos consultores entrarão em contato\n", type: "success" });
    });
</script>
<script src="../../assets/js/validation.js"></script>
<script src="../../assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
<script src="../../assets/js/plugins/api-estado-cidade.js"></script>
<script src="../../assets/js/plugins/custom.js"></script>
<script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/js/plugins/moment.min.js"></script>
<script src="../../assets/js/plugins/bootstrap-switch.js"></script>
<script src="../../assets/js/plugins/bootstrap-tagsinput.js"></script>
<script src="../../assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<script src="../../assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="../../assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
</html>