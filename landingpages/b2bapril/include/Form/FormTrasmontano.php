<?php

if(isset($_POST['cadastrar'])){
    $nome            = trim(strip_tags($_POST['nome']));
    $email           = trim(strip_tags($_POST['email']));
    $telefone         = trim(strip_tags($_POST['telefone']));
    $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
    $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
    $cnpj            = trim(strip_tags($_POST['cnpj']));
    $estado          = trim(strip_tags($_POST['estado']));
    $cidade          = trim(strip_tags($_POST['cidade']));
    $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
    $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
    $operadora       = trim(strip_tags($_POST['operadora']));
    $mensagem        = trim(strip_tags($_POST['mensagem']));
    $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
    $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));

    $insert = "INSERT INTO leadsamil ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidadepme, quantidadefamiliar, operadora, mensagem, tipodeplano, tipopessoa ) VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidadepme, :quantidadefamiliar, :operadora, :mensagem, :tipodeplano, :tipopessoa )";
    try{

        $result = $conexao->prepare($insert);

        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
        $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
        $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
        $result->bindParam(':estado', $estado, PDO::PARAM_STR);
        $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
        $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
        $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
        $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);

        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '
                                                        <script type="text/javascript">
                                    window.location = "include/Obrigado/LandingPageObrigadoAmil.php";
                                                </script>';

            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erro</strong> ao cadastrar o usuário.
                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>
<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #fff">
        <div class="card-body text-left">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $msgClientesSucesso; ?>
                    <?php echo $msgClientesErro; ?>
                    <?php echo $e; ?>

                </div>
                <div class="col-md-12">
                    <label>Você possui CNPJ?</label>
                    <select class="selectpicker" data-style="btn btn-round btn-simpl" title="Insira uma opção"  name="possuicnpj" id="possuicnpj" onclick="craateUserJsObject.ShowPrivileges();" required style="background-color:#fff">
                        <option selected disabled>Selecione</option>
                        <option value="Pme" id="Pme">Sim</option>
                        <option >Não</option>
                    </select>
                </div>
                <div class="resources2 col-md-12" style=" display: none;padding-bottom: 25px">
                    <label>Selecione o tipo de plano</label>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="possuicnpj" id="tipodeplano1" value="Individual" onclick="craateUserJsObject.ShowPrivileges();" style="background-color:#fff">
                                    <span class="form-check-sign"></span>
                                    Individual
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="possuicnpj" id="tipodeplano2" value="Familiar">
                                    <span class="form-check-sign"></span>
                                    Familiar
                                </label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="resources3 col-md-12" style=" display: none;padding-bottom: 25px">


                    <label>Selecione uma das alternativas</label>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipopessoa" id="gestante" value="Gestante">
                                    <span class="form-check-sign"></span>
                                    Gestante
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipopessoa" id="idoso" value="Idoso">
                                    <span class="form-check-sign"></span>
                                    Idoso
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipopessoa" id="nenhum" value="Nenhum">
                                    <span class="form-check-sign"></span>
                                    Nenhum dos dois
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="resources4 col-md-12" style=" display: none;">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="form-group">
                                <label>Quantidade de Pessoas</label>
                                <input type="text" class="form-control" placeholder="Quantidade de Vidas" name="quantidadefamiliar" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="resources col-md-12" style=" display: none;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>CNPJ ou Razão Social</label>
                                <input type="text" class="form-control" placeholder="Insira seu CNPJ ou Razão Social" name="cnpj" id="cnpj" >
                            </div>
                        </div>
                        <div class="col-md-6" >
                            <div class="form-group">
                                <label>Quantidade de Pessoas</label>
                                <input type="text" class="form-control" placeholder="Quantidade de Vidas"  name="quantidadepme" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nome</label>
                        <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="nome" id="nome" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="email" class="form-control" placeholder="Insira seu E-mail..." name="email" id="email" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Celular</label>
                        <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Telefone Alternativo</label>
                        <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="estado" class="col-form-label">Estado</label>
                    <select id="estado" name="estado" class="selectpicker" data-style="btn btn-simple btn-round"   data-size="7" required data-live-search="true">
                        <option> Choose</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="cidade" class="col-form-label">Cidade</label>
                    <select id="cidade" name="cidade" class="selectpicker" data-style="btn btn-simple btn-round"   data-size="7" required data-live-search="true">
                        <option> Choose</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Deixe uma mensagem sobre o que precisa (Opcional)</label>
                        <textarea name="mensagem" class="form-control" id="message" rows="6" ></textarea>
                    </div>
                </div>
                <div class="d-none">
                    <div class="input-group">
                        <input  type="text" class="form-control" value="Amil" name="operadora" id="operadora">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info btn-lg pull-right">Solicitar Cotação</button>
                </div>
            </div>
        </div>
    </form>