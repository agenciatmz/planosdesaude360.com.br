<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        require_once("includes/includesheader.php");
        require_once("../helpers/includesPixel.php");

       require_once("../helpers/includesChat.php"); ?>

    </head>
    <body class="template-page">
      <div class="js">

      <div id="preloader"></div>
        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png" class="d-none d-sm-block">
                          <img src="assets/img/logo-white.png" class="d-block d-sm-none">
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>Sobre nós</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>Planos de Saúde Next</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h1 class="title" style="font-size: 4em;"><span style="color:#ff0d71">NEXT SAÚDE: <span style="font-weight: 300">O NOVO PLANO DE SAÚDE DA AMIL</span> </h1>
                                </span>
                                <h4 class="description" style="font-weight: 300; color:#8a8a8a">
                                    A Next Amil Saúde está Presente em 83% do território nacional oferecendo Serviço de Qualidade por um Preço Acessível e a Confiança da Amil.
                                </h4>
                                <h2 class="description" style="font-size: 1.5em;color:#ff0d71 ">Planos à partir de: <br><span class="title" style="font-size: 4em;"><span style="color:#ff0d71">190,08</span></h2>
                                <span style="color:#ff0d71">* Plano Individual 0 à 18 anos - SP Capital e Região. Para saber os preços nas demais localidades como Rio de Janeiro, e faixa de idade solicite uma cotação clicando em saiba mais.</span><br>
                                <button class="btn btn-info btn-lg pull-left" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                            </div>
                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                <img src="assets/img/header-first.png" class="img-responsive d-none d-sm-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">SOBRE O PLANO</span> <br><span style="font-weight:800" class="azulescuro">NEXT AMIL SAÚDE</span></h2>
                            <iframe width="100%" height="480" src="https://www.youtube.com/embed/iMCLhmxXBXU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            <h4 class="description text-justify">
                                A <span class="rosabold">Next Saúde</span> nasceu nasceu nos anos 70 graças à vontade de um grupo de médicos idealistas que desejavam estender o acesso à medicina de alto nível ao maior número possível de pessoas da região.
                                <br><br>
                                Qualidade e Ética fizeram com que a <span class="rosabold">Next</span> se tornasse ao longo de 40 anos sinônimo de excelência na promoção da saúde:
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque01.png">
                                </div>
                                <h4 class="info-title" style="font-weight:300">Uma das primeiras assistências médicas<br> no país</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque02.png">
                                </div>
                                <h4 class="info-title" style="font-weight:300">Primeira a instalar um centro exclusivo direcionado à saúde da mulher</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque03.png">
                                </div>
                                <h4 class="info-title" style="font-weight:300">Uma das primeiras no país a conquistar a certificação de qualidade ISO 9000</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info" role="alert">
                            <div class="container text-center">
                                Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
        </div>
        <div class="cd-section">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-2 " style="background-image: url('assets/img/bg30.png'); background-size: cover;background-position: center center;">
                <div class="col-md-8 ml-auto mr-auto text-center" style="padding-top:40px">
                    <h1 class="title" style="font-size: 30px">REDUZA O INVESTIMENTO NO SEU<Br> PLANO INDIVIDUAL, FAMILIAR OU POR ADESÃO</h1>
                    <h3 class="description" style="font-weight:400; color:#fff">REALIZE UMA COTAÇÃO UTILIZANDO SEU MEI<br>
                        ( MICRO EMPREENDEDOR INDIVUDUAL )
                    </h3>
                    <a href="#" class="btn btn-info btn-lg" data-toggle="modal" data-target="#loginModal" style="background-color: #fff000;color:#000; font-weight: 500">Solicitar Cotação</a>
                </div>
            </div>
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h4 class="description text-justify">
                                Recentemente, a   <span class="rosabold">Next Saúde</span> tornou-se integrante da   <span class="rosabold">Amil</span>, a maior empresa de saúde do Brasil e do <span class="rosabold">United Health Group</span>, um dos mais importantes grupos de saúde do mundo.
                                <Br><br>
                                Seus planos garantem um atendimento médico atencioso e de qualidade, com uma rede selecionada e próxima dos seus clientes e beneficiários proporcionando cuidado diferenciado e resolutivo.
                                <Br><br>
                                Gostou? Solicite uma cotação e garanta um Preço exclusivo de lançamento no estado do Rio de Janeiro abaixo.
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info" role="alert">
                            <div class="container text-center">
                                Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--     *********    END FEATURES 1      *********      -->
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE</span> <br><span style="font-weight:800" class="azulescuro">PLANOS NEXT AMIL SAÚDE</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg23.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">NEXT INDIVIDUAL</h4>
                                    <p>Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg24.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">NEXT FAMILIAR</h4>
                                    <p>Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui.</p>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg25.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">NEXT EMPRESARIAL</h4>
                                    <p>Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-2 text-center" style="background-color:#f0f0f0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">Sempre que precisei da Amil Next nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios, atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito.
                                            </h5>
                                            <h3 class="card-title">Renata Bosolli</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente Amil Next</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">Gosto bastante, eu já tinha o seguro de carro com a Next né? Depois que mudei de emprego, precisei do saúde e não tenho oque reclamar.
                                            </h5>
                                            <h3 class="card-title">Maria Ferreira</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente Amil Next</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">Não é bom, é excelente! Custo beneficio e atendimento ótimos, já tive experiências com outros Planos, sem dúvidas, fico com a Next.
                                            </h5>
                                            <h3 class="card-title">Marcelo Oliveira</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente Amil Next</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            Sobre nós
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            Planos de Saúde Next
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card card-login card-plain">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>

                    </div>
                    <div class="modal-body">
                      <?php include("../helpers/insereBanco.php"); ?>
                           <?php include("includes/Form/FormNext.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
  </div>
</html>
