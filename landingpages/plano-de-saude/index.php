<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/IncludesHeader.php"); ?>
<body>
  <div class="js">

  <div id="preloader"></div>

<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank">
                <img src="https://v.fastcdn.co/u/b384de7c/23165201-0-picasion.com-f80b3ba.gif" width="110">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#conheca" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Conheça o plano</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 3.5em">Procurando um Bom Plano de Saúde que caiba no seu orçamento?</h1>
                        <h1 class="title" style="font-size: 2.5em;font-style: italic;color:#D9FE01">Nós vamos te ajudar!</h1>

                        <h4 class="description" style="color:#fff">
                                <span class="description">
                                  O Saúde 360 é um portal de dicas e noticias sobre saúde. São anos de Transparência e Dedicação, para levar à você informações uteis para uma vida mais saudável.

                                  <br><br>

                                  Em parceria com algumas das maiores corretoras e operadoras de Plano de Saúde do Brasil, desenvolvemos essa página de cotação online. Aqui você envia seus dados e nós encaminhamos para o corretor com o melhor preço no Plano de Saúde que você deseja!
                                  <br><br>
                                  Não é perfeito?

                                <br><br>

                                  Solicite uma cotação e economize                                </span>

                          </h4>
                          <h4 class="description" style="font-size: 1.5em;font-style: italic;color:#D9FE01">
Ps: Não se esqueça, Quem Possui CNPJ paga até 30% menos no Plano de Saúde para duas Pessoas ou mais!
                          </h4>
                            <button class="btn btn-warning btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario" style="position: relative; top: 42px">
                        <div class="card card-contact card-raised formshake">
                          <?php include("../helpers/insereBanco.php"); ?>


                                                        <?php include("includes/Form/FormAmil.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-8 section-image" style="background-image: url('assets/img/bg3.jpg')" id="conheca">
    <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title" style="color:#082A7E">Quais são as melhores opções de Plano de Saúde?</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/logo-amil.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                      <h4 class="info-title">AMIL</h4>
                        <p class="description">A Amil é uma das Principais operadoras de Plano de Saúde do Brasil. Possui ampla Rede Médica credenciada e é uma das referências em Plano de Saúde no Brasil. Solicite sua cotação para mais informações.</p>
                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/logo-bradesco.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                        <h4 class="info-title">BRADESCO</h4>
                        <p class="description">Uma das maiores empresas do Brasil, a Bradesco Saúde oferece diversas opções de Planos de Saúde. Com diferenciais como: Rede Médica Referenciada, Agilidade e Reembolso ela pode ser sua opção ideal.</p>
                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image">
                      <img src="assets/img/logo-intermedica.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                      <h4 class="info-title">INTERMÉDICA</h4>
                        <p class="description">Pioneira em Medicina Preventiva A NotreDame Intermédica dispõe diversos planos de abrangência nacional, com livre escolha e reembolso em uma ampla e qualificada rede credenciada.</p>

                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-image">
                      <img src="assets/img/logo-sulamerica.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                      <h4 class="info-title">SULAMÉRICA</h4>
                        <p class="description">A SulAmérica Saúde é uma operadora com 120 anos de tradição! Reembolso e Planos com livre escolha de prestadores médicos são alguns diferenciais dessa grande Operadora. <br><br></p>

                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-2 text-center" style="background-color:#f1f1f1" id="depoimentos">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class=""></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
"Fui Responsável pela cotação do Plano de Saúde na Empresa onde trabalho. Assim conheci a saúde 360 e consegui encontrar o melhor custo Beneficio em Plano de Saúde para nosso perfil. "
                                            </h5>
                                            <h3 class="card-title">Arlete Rezende</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Empreendedora</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
"Como autônomo não posso correr o risco de ficar doente e perder Shows, além disso fui Pai recentemente. Cotei um Plano de Saúde para mim e pro meu Filho, como tenho CNPJ fechei PME. Confesso que me surpreendi com os valores, são bem abaixo.
Recomendo!

"
                                            </h5>
                                            <h3 class="card-title">Guilherme Foleis</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Músico</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="carousel-item justify-content-center active">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
"Estava pagando caro num plano Premium para minha Família, e não queria perder os benefícios. Migrei então para um plano Empresarial e economizei 30% em um Plano do mesmo Perfil."
                                            </h5>
                                            <h3 class="card-title">Anderson Silva</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Arquiteto</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div id="contato">
    <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/bg3.jpg');     background-size: cover;
                background-position: center center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="title" style="font-size: 3em">
                                <span style="color:#082A7E">
                                  DÚVIDAS SOBRE COMO ESCOLHER?
                                    <p class="description" style="    color: #082A7E;font-size: 0.6em">
                                      Solicite uma cotação que entraremos em contato com os maiores especialistas em Planos de Saúde do País para te ajudar.</span>
                    </h1>

                    <div class="col-md-12 text-center">
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1em">
                        COTAÇÃO SEM COMPROMISSO!
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer " style="background-color:#08377F; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="https://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>
<?php include("includes/IncludesFooter.php"); ?>
</div>
</html>
