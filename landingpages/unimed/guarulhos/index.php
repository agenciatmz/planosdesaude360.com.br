<?php require_once("../../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link
            rel="shortcut icon"
            href="assets/img/favicon.ico"
            type="image/ico"
            />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Unimed Guarulhos</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../../helpers/includesPixel.php"); ?>
        <?php require_once("../../helpers/includesChat.php"); ?>
    </head>
    <body class="template-page">
      <div class="js">

      <div id="preloader"></div>
        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png" class="d-none d-sm-block" width="150px">
                          <img src="assets/img/logo-white.png" class="d-block d-sm-none" width="150px">
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>Sobre nós</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>Unimed Guarulhos</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg1.png');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h1 class="title"><span style="color:#009B63">UNIMED GUARULHOS: <span style="font-weight: 300">SATISFAÇÃO E ATENDIMENTO DIFERENCIADO</span> </h1>
                                </span>
                                <h4 class="description" style="font-weight: 300; color:#8a8a8a">
                                  Referência em assistência médica na região, a Unimed Guarulhos conta com mais de 240 médicos cooperados e com rede de hospitais, pronto atendimentos, laboratórios e clínicas.
                                </h4>
                                <h2 class="description" style="font-size: 1.5em;color:#009B63 ">Planos à partir de: <br><span class="title" style="font-size: 4em;"><span style="color:#009B63">	188,34</span></h2>
                                <span style="color:#009B63">* Plano Individual 0 à 18 anos - Guarulhos e Região. Para saber os preços nas demais  e faixa de idade solicite uma cotação clicando no botão abaixo.</span><br>
                                <button class="btn btn-info btn-lg pull-left" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Solicitar Cotação</button>
                            </div>
                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                <img src="assets/img/header-first.png" class="img-responsive d-none d-sm-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">SOBRE O PLANO</span> <br><span style="font-weight:800" class="azulescuro">UNIMED GUARULHOS</span></h2>
                            <h4 class="description text-justify">
                              A Confederação Nacional das Cooperativas Médicas – Unimed do Brasil foi fundada em 28 de novembro de 1975 para ser a representante institucional das cooperativas Unimed. A organização zela pelo uso da marca e pela reputação em âmbito nacional e leva pleitos e contribuições aos poderes públicos, órgãos reguladores e entidades do setor de saúde, propagando as melhores práticas na busca por gestões cada vez mais transparentes, éticas e legalistas.
                              <Br><br>
                              A Unimed do Brasil também coordena um dos principais diferenciais da Unimed, conhecido como Intercâmbio Nacional, que é o atendimento do beneficiário de uma Unimed por outra, desde que seu plano contratado permita a prática.
                              <Br><br>
                              Outra atribuição é atuar em prol da sustentabilidade econômico-financeira das Federações e Singulares Unimed por meio de consultorias e oferta de produtos e projetos reconhecidos por sua qualidade.                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque01.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600">43 ANOS <Br>NO MERCADO</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque02.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600">+ DE 348 <br> COOPERATIVAS</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque03.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600">+ DE 114.000<br> MÉDICOS COOPERADOS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info" role="alert">
                            <div class="container text-center">
                                Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
        </div>

        <!--     *********    END FEATURES 1      *********      -->
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE</span> <br><span style="font-weight:800" class="azulescuro">PLANOS UNIMED GUARULHOS</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg23.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">UNIMED GUARULHOS <Br>INDIVIDUAL</h4>
                                    <p>Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg24.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">UNIMED GUARULHOS <Br> FAMILIAR</h4>
                                    <p>Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui.</p>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg25.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">UNIMED GUARULHOS <Br> EMPRESARIAL</h4>
                                    <p>Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-2 text-center" style="background-color:#f0f0f0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">Estou muito contente com o Plano de Saúde da Unimed, sempre que precisei fui muito bem atendida. Indico para todos!
                                            </h5>
                                            <h3 class="card-title">Maria Oliveira</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed Guarulhos</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">O Plano de Saúde da Unimed está me surpreendendo a cada dia, ótimos médicos e enfermeiras prestativas! Recomendo
                                            </h5>
                                            <h3 class="card-title">Marcela Silva</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed Guarulhos</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">Unimed, o próprio nome diz UNICA empresa que conseguiu suprir todas minhas necessidades! Show de bola
                                            </h5>
                                            <h3 class="card-title">Thiago Farias</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed Guarulhos</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="contato">
            <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/bg30.png');     background-size: cover;
                        background-position: center center;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="title" style="font-size: 3em">
                                        <span style="color: #fff">
                                            AINDA TEM DÚVIDAS?
                                            <p class="description" style="    color: #fff;font-size: 0.6em">
                                                Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde Santa Helena ideal para você!
                                        </span>
                            </h1>
                            <div class="col-md-12 text-center">
                                <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1.2em">
                                    COTAÇÃO SEM COMPROMISSO!
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            Sobre nós
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            Planos de Saúde
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card card-login card-plain">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>

                    </div>
                    <div class="modal-body">
                        <?php include("../../helpers/insereBanco.php"); ?>
                            <?php include("includes/Form/FormUnimed.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
  </div>
</html>
