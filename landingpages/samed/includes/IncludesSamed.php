<div class="d-none d-sm-block">
    <div class=" qc-landing-layout">
        <div class="qc-landing-info qc-layout-info">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                            <img src="assets/img/LandingPageSamed/logo.png" width="200">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#sobrenos" data-scroll style="background-color:#00984a">
                                    <p>SOBRE NOS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#rede" data-scroll  style="background-color:#00984a">
                                    <p>REDE CREDENCIADA</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="header">
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('assets/img/LandingPageSamed/bg14.jpg'); "></div>
                    <div class="content-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ">
                            <h2 class="title" style="color:#00984A">Conheça a Samed</h2>
                            <h4 class="description"> A Samed Saúde tem mais de 40 anos dedicados ao cuidado da saúde das pessoas. Com origem no Alto Tietê, a operadora é pioneira em planos empresariais na região. Além de contar com uma ampla rede credenciada, a Samed possui estrutura própria, incluindo hospitais, ambulatórios, centros de especialidades médicas, centros de diagnósticos e laboratórios.
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">

                                <h4 class="info-title" style="color:#00984A">Rede Própria</h4>
                                <p class="description">A Samed possui estrutura própria, incluindo hospitais, ambulatórios, centros de especialidades médicas, centros de diagnósticos e laboratórios.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">

                                <h4 class="info-title" style="color:#00984A">40 anos de história</h4>
                                <p class="description">A Samed Saúde tem mais de 40 anos dedicados ao cuidado da saúde das pessoas</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">

                                <h4 class="info-title" style="color:#00984A">Pioneirismo</h4>
                                <p class="description">Com origem no Alto Tietê, a operadora é pioneira em planos empresariais na região.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pricing-5 section-pricing-5 " id="rede">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto text-center">
                            <h1 class="title" style="font-size: 3em">
                            <span style="color: #00984A">
                            CONHEÇA A REDE CREDENCIADA DA SAMED
                            </span>
                            </h1>
                        </div>
                    </div>
                    <br><br>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/hospital-santana.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL SANTANA</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/hospital-santacasa.png">
                                    </div>
                                    <h4 class="info-title">SANTA CASA DE MOGI DAS CRUZES </h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/hospital-misericordia.png">
                                    </div>
                                    <h4 class="info-title">SANTA CASA DE MISERICORDIA</h4>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/hospital-guararema.png">
                                    </div>
                                    <h4 class="info-title">SANTA CASA DE MISERICORDIA - GUARAREMA</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/aruja.png">
                                    </div>
                                    <h4 class="info-title">HOSPITAL E MATERNIDADE - IPIRANGA ARUJÁ</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover" style="background-color: #fff; border-radius: 19px; paddingdding: 11px;">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/LandingPageSamed/muitomais.png">
                                        <p class="description"> Hospitais e clínicas </p>
                                    </div>
                                    <a href="#formulario" data-scroll="" class="btn btn-info btn-lg botaoshake">COTAÇÃO SEM COMPROMISSO</a>
                                </div>
                            </div>
                            <img src="assets/img/LandingPageSamed/tabela-valores.png">


                        </div>
                    </div>
                </div>
            </div>
            <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/LandingPageSamed/bg30.jpg'); background-size: cover;" id="contato">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <div class="card card-testimonial card-plain">
                            <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                            <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                            <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                                QUERO COTAR AGORA
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer " style="background-color:#fff; color:#ED3237">
                <div class="col-md-12">
                    <div class="container">
                        <div class="copyright">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, Desenvolvido por
                            <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#ED3237">#agênciatrêsmeiazero</a>.
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <div class="qc-landing-form qc-layout-form formshake">

            <?php include("includes/Form/FormSamed.php");?>
        </div>
    </div>
</div>
<!--End of Tawk.to Script-->
