<?php require_once("includes/IncludesConexaoBanco.php"); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Agência TresMeiaZero">
    <link href="assets/img/favicon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<title>Planos de Saúde Bradesco</title>
    <?php require_once("includes/IncludesPixel.php"); ?>
</head>
<body data-spy="scroll" data-target=".navbar" class="has-loading-screen">
<?php require_once("includes/IncludesChat.php"); ?>

    <div class="ts-page-wrapper" id="page-top">

        <header id="ts-hero" class="ts-full-screen" data-bg-parallax="scroll" data-bg-parallax-speed="3" >

            <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#1a1360">
                <div class="container">
                    <a class="navbar-brand" href="#page-top">
                        <img src="assets/img/logo-w.png" alt="">
                    </a>
                    <!--end navbar-brand-->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!--end navbar-toggler-->
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">
                            <a class="nav-item nav-link active ts-scroll" href="#page-top">Home <span class="sr-only">(current)</span></a>
                            <a class="nav-item nav-link ts-scroll" href="#how-it-works">Diferenciais</a>
                            <a class="nav-item nav-link ts-scroll" href="#our-clients">Depoimentos</a>

                            <a class="nav-item nav-link ts-scroll" href="#contact">Contato</a>

                            <a class="nav-item nav-link ts-scroll btn btn-primary btn-sm text-white ml-3 px-3 ts-width__auto" href="#contact" style="margin-bottom: 10px">Cotar Agora</a>
                        </div>
                        <!--end navbar-nav-->
                    </div>
                    <!--end collapse-->
                </div>
                <!--end container-->
            </nav>
            <!--end navbar-->

            <div class="container align-self-center">
                <div class="row align-items-center text-center">
                    <div class="col-sm-12">
                        <h1>À PARTIR DE R$ 316*</h1>
                        <h3 class="ts-opacity__99">Venha para o Plano de Saúde que mais cresce no Brasil!</h3>
                        <h4 class="ts-opacity__70" style="font-weight: 300">A Bradesco Saúde é a empresa de Planos de Saúde que mais cresce no Brasil. São planos completos que fazem você se sentir mais seguro e confortável. Solicite uma cotação online ao lado e saiba mais
                        </h4>

                        <a href="#contact" class="btn btn-light btn-lg ts-scroll">Saiba Mais</a>
                    </div>
                    <!--end col-sm-7 col-md-7-->

                    <!--end col-sm-5 col-md-5 col-xl-5-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->

            <div class="ts-background" data-bg-image-opacity=".6" data-bg-parallax="scroll" data-bg-parallax-speed="3">
                <div class="ts-svg ts-z-index__2">
                    <img src="assets/svg/wave-static-02.svg" class="w-100 position-absolute ts-bottom__0">
                    <img src="assets/svg/wave-static-01.svg" class="w-100 position-absolute ts-bottom__0">
                </div>
                <div class="owl-carousel ts-hero-slider" data-owl-loop="1">
                    <div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="assets/img/bg-girl-01.jpg" data-bg-blend-mode="multiply"></div>
                    <!--<div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="assets/img/bg-girl-02.jpg" data-bg-blend-mode="multiply"></div>-->
                </div>
            </div>

        </header>
        <!--end #hero-->

        <main id="ts-content">

            <section id="how-it-works" class="ts-block text-center">
                <div class="container">
                    <div class="ts-title" style="color: #FF0024;">
                        <h3>DIFERENCIAIS DOS PLANOS BRADESCO SAÚDE</h3>
                    </div>
                    <!--end ts-title-->
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <figure data-animate="ts-fadeInUp">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-watch-heart.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-01.svg"></div>
                                </figure>
                                <h4>Credibilidade</h4>
                                <p>
                                    Sempre que necessitarem de uma consulta médica, ou então de realização de determinado exame e até mesmo algum procedimento cirúrgico, estão segurados pela Bradesco Saúde.
                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <figure data-animate="ts-fadeInUp" data-delay="0.1s">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-economia.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-02.svg"></div>
                                </figure>
                                <h4>Economia</h4>
                                <p>
                                    A Bradesco Saúde trabalha com preços diferenciados, totalmente acessíveis. Com o objetivo de fornecer segurança, conforto e economia a seus segurados.
                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-0 col-xl-4">
                            <figure data-animate="ts-fadeInUp" data-delay="0.2s">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-hospital.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-03.svg"></div>
                                </figure>
                                <h4>Rede Referenciada</h4>
                                <p>
                                    São hospitais, laboratórios, consultórios médicos, clinicas de imagens e mais de 43 mil referenciados cadastrados em aproximadamente 1.500 cidades do Brasil.
                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </section>


            <section id="our-clients" class="ts-block text-center">
                <div class="container">
                    <div class="ts-title">
                        <h2>Sua vida mais Tranquila <br>é nosso maior objetivo</h2>
                    </div>
                    <!--end ts-title-->
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="owl-carousel ts-carousel-blockquote" data-owl-dots="1" data-animate="ts-zoomInShort">
                                <blockquote class="blockquote">
                                    <!--person image-->
                                    <figure>
                                        <aside>
                                            <i class="fa fa-quote-right"></i>
                                        </aside>
                                        <div class="ts-circle__lg" data-bg-image="assets/img/person-05.jpg"></div>
                                    </figure>
                                    <!--end person image-->
                                    <!--cite-->
                                    <p>
                                        Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..

                                    </p>
                                    <!--end cite-->
                                    <!--person name-->
                                    <footer class="blockquote-footer">
                                        <h4>Matheus Cardoso</h4>
                                        <h6>Cliente Bradesco Saúde</h6>
                                    </footer>
                                    <!--end person name-->
                                </blockquote>
                                <!--end blockquote-->
                                <blockquote class="blockquote">
                                    <!--person image-->
                                    <figure>
                                        <aside>
                                            <i class="fa fa-quote-right"></i>
                                        </aside>
                                        <div class="ts-circle__lg" data-bg-image="assets/img/person-01.jpg"></div>
                                    </figure>
                                    <!--end person image-->
                                    <!--cite-->
                                    <p>
                                        A Bradesco Saúde é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata.
                                    </p>
                                    <!--end cite-->
                                    <!--person name-->
                                    <footer class="blockquote-footer">
                                        <h4>Caroline Duque</h4>
                                        <h6>Gerente de Negócios</h6>
                                    </footer>
                                    <!--end person name-->
                                </blockquote>
                                <!--end blockquote-->
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section id="partners" class="py-5 ts-block" data-bg-color="#f6f6f6">
                <!--container-->
                <div class="container">
                    <!--block of logos-->
                    <div class="d-block d-md-flex justify-content-between align-items-center text-center ts-partners " style="padding-bottom: 30px;">
                        <a href="#">
                            <img src="assets/img/logo-01rj.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-02rj.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-03rj.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-04rj.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-05rj.png" alt="">
                        </a>
                    </div>
                    <!--end logos-->
                </div>
                <!--end container-->
            </section>




        </main>

        <footer id="ts-footer">



            <section id="contact" class="ts-separate-bg-element" data-bg-image="assets/img/bg-desk.jpg" data-bg-image-opacity=".1" data-bg-color="#1b1464">
                <div class="container">
                    <div class="ts-box mb-0 p-5 ts-mt__n-10">
                        <div class="row">

													<?php
															if(isset($_POST['cadastrar'])){
																	$nome            = trim(strip_tags($_POST['nome']));
																	$email           = trim(strip_tags($_POST['email']));
																	$telefone         = trim(strip_tags($_POST['telefone']));
																	$telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
																	$possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
																	$cnpj            = trim(strip_tags($_POST['cnpj']));
																	$estado          = trim(strip_tags($_POST['estado']));
																	$cidade          = trim(strip_tags($_POST['cidade']));
																	$quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
																	$quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
																	$operadora       = trim(strip_tags($_POST['operadora']));
																	$operadoraAmil      = trim(strip_tags($_POST['operadoraAmil']));
																	$operadoraBradesco     = trim(strip_tags($_POST['operadoraBradesco']));
																	$operadoraIntermedica       = trim(strip_tags($_POST['operadoraIntermedica']));
																	$operadoraSamed       = trim(strip_tags($_POST['operadoraSamed']));
																	$operadoraBiovida       = trim(strip_tags($_POST['operadoraBiovida']));
																	$operadoraTrasmontano       = trim(strip_tags($_POST['operadoraTrasmontano']));
																	$operadoraSulamerica       = trim(strip_tags($_POST['operadoraSulamerica']));
																	$operadoraNext       = trim(strip_tags($_POST['operadoraNext']));
																	$operadoraGoldencross       = trim(strip_tags($_POST['operadoraGoldencross']));
																	$operadoraMedSenior       = trim(strip_tags($_POST['operadoraMedSenior']));
																	$operadoraSaoCristovao       = trim(strip_tags($_POST['operadoraSaoCristovao']));
																	$operadoraSantaHelena      = trim(strip_tags($_POST['operadoraSantaHelena']));
																	$mensagem        = trim(strip_tags($_POST['mensagem']));
																	$tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
																	$tipopessoa      = trim(strip_tags($_POST['tipopessoa']));
																	$modalidade     = trim(strip_tags($_POST['modalidade']));

																	$insert = "INSERT INTO tmzleadsgeral ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidadepme, quantidadefamiliar, operadora, operadoraAmil, operadoraBradesco, operadoraIntermedica, operadoraSamed, operadoraBiovida, operadoraTrasmontano, operadoraSulamerica, operadoraNext, operadoraGoldencross, operadoraMedSenior, operadoraSaoCristovao, operadoraSantaHelena, mensagem, tipodeplano, tipopessoa, modalidade  )
																	VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidadepme, :quantidadefamiliar, :operadora, :operadoraAmil, :operadoraBradesco, :operadoraIntermedica, :operadoraSamed, :operadoraBiovida, :operadoraTrasmontano, :operadoraSulamerica, :operadoraNext, :operadoraGoldencross, :operadoraMedSenior, :operadoraSaoCristovao, :operadoraSantaHelena, :mensagem, :tipodeplano, :tipopessoa, :modalidade )";
																	try{

																			$result = $conexao->prepare($insert);

																			$result->bindParam(':nome', $nome, PDO::PARAM_STR);
																			$result->bindParam(':email', $email, PDO::PARAM_STR);
																			$result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
																			$result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
																			$result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
																			$result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
																			$result->bindParam(':estado', $estado, PDO::PARAM_STR);
																			$result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
																			$result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
																			$result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
																			$result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
																			$result->bindParam(':operadoraAmil', $operadoraAmil, PDO::PARAM_STR);
																			$result->bindParam(':operadoraBradesco', $operadoraBradesco, PDO::PARAM_STR);
																			$result->bindParam(':operadoraIntermedica', $operadoraIntermedica, PDO::PARAM_STR);
																			$result->bindParam(':operadoraSamed', $operadoraSamed, PDO::PARAM_STR);
																			$result->bindParam(':operadoraBiovida', $operadoraBiovida, PDO::PARAM_STR);
																			$result->bindParam(':operadoraTrasmontano', $operadoraTrasmontano, PDO::PARAM_STR);
																			$result->bindParam(':operadoraSulamerica', $operadoraSulamerica, PDO::PARAM_STR);
																			$result->bindParam(':operadoraNext', $operadoraNext, PDO::PARAM_STR);
																			$result->bindParam(':operadoraGoldencross', $operadoraGoldencross, PDO::PARAM_STR);
																			$result->bindParam(':operadoraMedSenior', $operadoraMedSenior, PDO::PARAM_STR);
																			$result->bindParam(':operadoraSaoCristovao', $operadoraSaoCristovao, PDO::PARAM_STR);
																			$result->bindParam(':operadoraSantaHelena', $operadoraSantaHelena, PDO::PARAM_STR);
																			$result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
																			$result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
																			$result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
																			$result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);

																			$result->execute();
																			$contar = $result->rowCount();
																			if($contar>0){

																					{
																							$msgClientesSucesso = '
																											<script type="text/javascript">
																											window.location = "include/obrigado/obrigado.php";
																											</script>';

																					}
																			}else{
																					$msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
																											<strong>Erro</strong> ao cadastrar o usuário.
																											</div>';
																			}
																	}catch(PDOException $e){
																			echo $e;
																	}

															}else {
																	$msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
															}
															?>                                   <?php include("includes/Form/FormBradescoRJ.php");?>

                        </div>
                        <!--end row-->
                    </div>
                    <!--end ts-box-->
                    <div class="text-center text-white py-4">
                        <small>© 2018 <a href="https://agenciatresmeiazero.com.br/home" target="_blank"> Agência TresMeiaZero</a>, Todos Direitos Reservados</small>
                    </div>
                </div>
                <!--end container-->
            </section>

        </footer>
        <!--end #footer-->
    </div>
    <!--end page-->

    <script>
        if( document.getElementsByClassName("ts-full-screen").length ) {
            document.getElementsByClassName("ts-full-screen")[0].style.height = window.innerHeight + "px";
        }
    </script>
	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
    <script src="assets/js/api-estado-cidade.js"></script>
    <script src="assets/js/validation.js"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
	<script src="assets/js/isInViewport.jquery.js"></script>
	<script src="assets/js/jquery.particleground.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/scrolla.jquery.min.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
    <script src="assets/js/jquery.wavify.js"></script>

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>-->
    <script src="assets/js/custom.js"></script>


</body>
</html>
