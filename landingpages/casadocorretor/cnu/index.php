<?php require_once("../../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link
            rel="shortcut icon"
            href="assets/img/favicon.ico"
            type="image/ico"
            />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Unimed </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../../helpers/includesPixel.php"); ?>
        <?php require_once("../../helpers/includesChat.php"); ?>
    </head>
    <body class="template-page">
        <div class="js">
            <div id="preloader"></div>
            <!-- Navbar -->
            <div class="header-2">
                <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                    <div class="container">
                        <div class="navbar-translate">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                            </button>
                            <img src="assets/img/logo-color.png" class="d-none d-sm-block" width="250px">
                            <img src="assets/img/logo-white.png" class="d-block d-sm-none" width="250px">
                        </div>
                        <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#quemsomos" data-scroll>
                                        <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                        <p>Sobre nós</p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#planos" data-scroll>
                                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                        <p>Unimed</p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                        <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                        <p>Cotação Online</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('assets/img/bg1.png');"></div>
                    <div class="content-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <h1 class="title"><span style="color:#009B63">UNIMED: <span style="font-weight: 300">SATISFAÇÃO E ATENDIMENTO DIFERENCIADO</span> </h1>
                                    <h4 class="description" style="font-weight: 400; color:#009B63">
                                        Referência em assistência médica no pais, a Unimed conta com mais de 114.000 médicos cooperados e com rede de hospitais, pronto atendimentos, laboratórios e clínicas.
                                    </h4>
                                    <button class="btn btn-info btn-lg pull-left" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Solicitar Cotação</button>
                                </div>
                                <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                    <img src="assets/img/header-first.png" width="440" class="d-none d-sm-block" style="
                                        position: absolute;
                                        top:-48px;
                                        ">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="cd-section" id="quemsomos">
                <!--     *********     FEATURES 1      *********      -->
                <div class="features-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 mr-auto ml-auto">
                                <h2 class="title"><span style="font-weight:400" class="azulclaro">CONHEÇA OS PLANOS DA</span> <br><span style="font-weight:800" class="azulescuro">CENTRAL NACIAONAL UNIMED</span></h2>
    <div class="row">
                                <div class="col-md-6 col-lg-3">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <img class="img rounded" src="assets/img/project13.jpg">
                                        </div>
                                        <div class="card-body" style="border-bottom: 11px solid #FFCB2A;">

                                            <h5><span style="font-weight:400;color:#FFCB2A">PLANO</span> <br><span style="font-weight:800;color:#FFCB2A" >CLÁSSICO</span></h5>
                                            <p class="card-description" style="font-size: 1em;">  Acomodação em enfermaria<Br> Opções de planos com e sem coparticipação <Br> Abrangência regional <br> Sem reembolso</p><br><br>
                                              <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Cotar agora</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <img class="img rounded" src="assets/img/project14.jpg">
                                        </div>
                                        <div class="card-body" style="border-bottom: 11px solid #F67D38;">

                                          <h5><span style="font-weight:400;color:#F67D38">PLANO</span> <br><span style="font-weight:800;color:#F67D38" >ESTILO</span></h5>

                                            <p class="card-description" style="font-size: 1em;">
                                              Acomodação em enfermaria ou apartamento <br> Opções de planos com e sem coparticipação <br> Abrangência regional e nacional <br> Reembolso para consultas
                                            </p>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Cotar agora</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <img class="img rounded" src="assets/img/project15.jpg">
                                        </div>
                                        <div class="card-body" style="border-bottom: 11px solid #A43792;">
                                          <h5><span style="font-weight:400;color:#A43792">PLANO</span> <br><span style="font-weight:800;color:#A43792" >ABSOLUTO</span></h5>

                                            <p class="card-description" style="font-size: 1em;">
                                              Acomodação em apartamento <br> Opções de planos com e sem coparticipação <br> Abrangência regional e nacional <br> Reembolso 2x a tabela
                                            </p>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Cotar agora</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <img class="img rounded" src="assets/img/project16.jpg">
                                        </div>
                                        <div class="card-body" style="border-bottom: 11px solid #4D2F6B;">
                                          <h5><span style="font-weight:400;color:#4D2F6B">PLANO</span> <br><span style="font-weight:800;color:#4D2F6B" >SUPERIOR</span></h5>

                                            <p class="card-description" style="font-size: 1em;">
                                              Acomodação em apartamento <br> Sem coparticipação <br> Abrangência regional e nacional <br> Reembolso 3x a tabela
                                            </p><br>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Cotar agora</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-3">
                                    <div class="card card-blog">
                                        <div class="card-image">
                                            <img class="img rounded" src="assets/img/project17.jpg">
                                        </div>
                                        <div class="card-body" style="border-bottom: 11px solid #63636D;">
                                          <h5><span style="font-weight:400;color:#63636D">PLANO</span> <br><span style="font-weight:800;color:#63636D" >EXCLUSIVO</span></h5>

                                            <p class="card-description" style="font-size: 1em;">
                                              Acomodação em apartamento <br> Sem coparticipação <br> Abrangência nacional <br> Reembolso 8x a tabela
                                            </p><br><br>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#loginModal">Cotar agora</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/imgDestaque01.png">
                                    </div>
                                    <h4 class="info-title" style="font-weight:600">43 ANOS <Br>NO MERCADO</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/imgDestaque02.png">
                                    </div>
                                    <h4 class="info-title" style="font-weight:600">+ DE 348 <br> COOPERATIVAS</h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info info-hover">
                                    <div class="icon icon-primary">
                                        <img src="assets/img/imgDestaque03.png">
                                    </div>
                                    <h4 class="info-title" style="font-weight:600">+ DE 114.000<br> MÉDICOS COOPERADOS</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                              <h2 class="title"><span style="font-weight:400" class="azulclaro">REDE CREDENCIADA</span> <br><span style="font-weight:800" class="azulescuro"> UNIMED</span></h2>
                              <p class="description" style="    color: #000;font-size: 1.6em">
Os Planos Central Nacional Unimed Possuem Ampla Rede Credenciada, composta por Hospitais de Qualidade como Sirio-Libânes (Zona Sul), Oswaldo Cruz e São Camilo (Centro), Cristovão da Gama (ABCD).
Para conhecer a Rede Completa, com Laboratórios e todos os Hospitais que fazem parte de nossa Rede Credenciada, solicite uma cotação com a Casa do Corretor nesse site.</p>
                            </div>


                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-info" role="alert">
                                <div class="container text-center">
                                    Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--     *********    END FEATURES 1      *********      -->
            </div>
            <!--     *********    END FEATURES 1      *********      -->
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE</span> <br><span style="font-weight:800" class="azulescuro">PLANOS UNIMED</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg23.jpg')">
                                <div class="info">
                                    <div class="description">
                                        <h4 style="font-weight:800">UNIMED  <Br>INDIVIDUAL (Adesão)</h4>
                                        <p>Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.</p>
                                        <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg24.jpg')">
                                <div class="info">
                                    <div class="description">
                                        <h4 style="font-weight:800">UNIMED <Br> FAMILIAR (Adesão)</h4>
                                        <p>Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui.</p>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg25.jpg')">
                                <div class="info">
                                    <div class="description">
                                        <h4 style="font-weight:800">UNIMED <Br> EMPRESARIAL </h4>
                                        <p>Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração.</p>
                                        <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                          <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA A NOSSA</span> <br><span style="font-weight:800" class="azulescuro">AREA DE ATENDIMENTO</span></h2>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <img  src="assets/img/imgMapaAtuacao.png" >

                        </div>


                    </div>
                </div>
            </div>
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                          <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA A </span> <br><span style="font-weight:800" class="azulescuro">TABELA DE PREÇO</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <img  src="assets/img/imgTabelaValores.png" class="d-none d-sm-block">
                            <img  src="assets/img/imgTabelaValoresBasico.png" class="d-block d-sm-none"><br>
                              <img  src="assets/img/imgTabelaValoresEspecial.png" class="d-block d-sm-none"><br>
                                <img  src="assets/img/imgTabelaValoresMaster.png" class="d-block d-sm-none" style="position:relative; left:40px"><br>
                        </div>


                    </div>
                </div>
            </div>
            <div class="testimonials-2 text-center" style="background-color:#f0f0f0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="carouselExampleIndicators2" class="carousel slide">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                </ol>
                                <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">Estou muito contente com o Plano de Saúde da Unimed, sempre que precisei fui muito bem atendida. Indico para todos!
                                                </h5>
                                                <h3 class="card-title">Maria Oliveira</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">O Plano de Saúde da Unimed está me surpreendendo a cada dia, ótimos médicos e enfermeiras prestativas! Recomendo
                                                </h5>
                                                <h3 class="card-title">Marcela Silva</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">Unimed, o próprio nome diz UNICA empresa que conseguiu suprir todas minhas necessidades! Show de bola
                                                </h5>
                                                <h3 class="card-title">Thiago Farias</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary" style="color:#2b8c59">Cliente Unimed </h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                <i class="now-ui-icons arrows-1_minimal-left"></i>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                <i class="now-ui-icons arrows-1_minimal-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="contato">
                <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/bg30.png');     background-size: cover;
                    background-position: center center;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1 class="title" style="font-size: 3em">
                                    <span style="color: #fff">
                                        AINDA TEM DÚVIDAS?
                                        <p class="description" style="    color: #fff;font-size: 0.6em">
                                            Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde Unimed ideal para você!</p>
                                    </span>
                                </h1>
                                <div class="col-md-12 text-center">
                                    <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1em">
                                    COTAÇÃO SEM COMPROMISSO!
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer ">
                <div class="container">
                    <nav>
                        <ul>
                            <li class="nav-item active">
                                <a class="nav-link" href="#quemsomos">
                                Sobre nós
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#planos">
                                Planos de Saúde
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                                Cote agora
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                    </div>
                </div>
            </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="card card-login card-plain">
    <div class="modal-header justify-content-center">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
    <i class="now-ui-icons ui-1_simple-remove"></i>
    </button>
    </div>
    <div class="modal-body">
    <?php include("../../helpers/insereBanco.php"); ?>
    <?php include("includes/Form/FormUnimed.php");?>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
    </div>
</html>
