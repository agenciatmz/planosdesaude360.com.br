<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link
            rel="shortcut icon"
            href="assets/img/favicon.ico"
            type="image/ico"
            />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>BRADESCO SAÚDE</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../helpers/includesPixel.php"); ?>
        <?php require_once("../helpers/includesChat.php"); ?>

    </head>
    <body class="template-page">
      <div class="js">

      <div id="preloader"></div>
        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png"  width="250px">

                        <!-- <img src="assets/img/logo-color.png" class="d-none d-sm-block" width="150px"> -->
                          <!-- <img src="assets/img/logo-white.png" class="d-block d-sm-none" width="150px"> -->
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>Sobre nós</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>Bradesco Saúde</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="page-header header-filter">
                                <div class="page-header-image" style="background-image: url('assets/img/bg1.png');"></div>
                                <div class="content-center">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 text-left">
                                              <h1 class="title"><span style="color:#f44242">BRADESCO SAÚDE: <span style="font-weight: 300"> VENHA PARA O PLANO DE SAÚDE QUE MAIS CRESCE NO BRASIL!</span> </h1>
                                                <h4 class="description" style="font-weight: 400; color:#f44242">
                                                  A Bradesco Saúde é a empresa de Planos de Saúde que mais cresce no Brasil. São planos completos que fazem você se sentir mais seguro e confortável. Solicite uma cotação online ao lado e saiba mais
                                                </h4>
                                                <button class="btn btn-info btn-lg pull-left" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Solicitar Cotação</button>
                                            </div>
                                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                                <img src="assets/img/header-first.png" width="440" class="d-none d-sm-block" style="
                                                    position: absolute;
                                                    top:-48px;
                                                    ">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">SOBRE O PLANO</span> <br><span style="font-weight:800" class="azulescuro">BRADESCO SAÚDE</span></h2>
                            <h4 class="description text-justify" style="font-weight:400; color:#888">
                              A Bradesco Saúde conta com diversos tipos de planos que agradam todos os perfis de clientes. E quando se trata dos Melhores Planos, você pode contar com essa operadora de olhos fechados.
  <Br><br>
  <strong>Bradesco Perfil:</strong> é uma das linhas que se caracteriza principalmente pelo ótimo custo-benefício. Mesmo optando por um plano mais econômico, o segurado tem direito a ser atendido por uma boa e ampla rede de credenciados em sua cidade ou nas principais capitais do Brasil, com profissionais qualificados e equipamentos de ponta. O segurado também pode utilizar os serviços de locais que não fazem parte da rede referenciada e em seguida ser reembolsado. Dependendo do número de funcionários segurados pela empresa, ainda é possível conseguir a isenção do período de carência;
  <Br><br>
  <strong>Bradesco Nacional Flex:</strong> é ideal para empresas que queiram oferecer atendimento qualificado e assistência médica completa aos funcionários da sua empresa. Tem abrangência nacional e tem como principal característica a flexibilidade com coparticipação ou sem. Além disso, o cliente ainda pode optar pela acomodação enfermaria e individual, em casos de internamento. O cliente também tem direito a uma série de benefícios adicionais, como o Meu Doutor Bradesco Saúde, onde tem à disposição um médico para a realização de consultas;
  <Br><br>
  <strong>Bradesco Nacional Plus:</strong> oferece a possibilidade de adicionar o serviço de Saúde Concierge, que inclui Salas VIP nos principais centros de referência de todo o Brasil. Pode ser contratado com ou sem coparticipação e conta com atendimento hospitalar e ambulatorial com obstetrícia e total flexibilidade para escolher onde ser atendimento, já que o plano também oferece sistema de reembolso de despesas, de acordo com a tabela do contrato de adesãoA Bradesco Saúde conta com diversos tipos de planos que agradam todos os perfis de clientes. E quando se trata dos Melhores Planos, você pode contar com essa operadora de olhos fechados.
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque01.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#DD0333">Credibilidade</h4>
                                <p class="description" style="font-weight:400">Sempre que necessitarem de uma consulta médica, ou então de realização de determinado exame e até mesmo algum procedimento cirúrgico, estão segurados pela Bradesco Saúde.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque02.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#DD0333">Economia</h4>
                                <p class="description" style="font-weight:400">A Bradesco Saúde trabalha com preços diferenciados, totalmente acessíveis. Com o objetivo de fornecer segurança, conforto e economia a seus segurados.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque03.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600;color:#DD0333">Rede Referenciada</h4>
                                <p class="description" style="font-weight:400">São hospitais, laboratórios, consultórios médicos, clinicas de imagens e mais de 43 mil referenciados cadastrados em aproximadamente 1.500 cidades do Brasil.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-info" role="alert">
                            <div class="container text-center">
                                Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#" class="botaoshake" data-toggle="modal" data-target="#loginModal" style="color:yellow">Cote agora</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
        </div>

        <!--     *********    END FEATURES 1      *********      -->
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE</span> <br><span style="font-weight:800" class="azulescuro">PLANOS BRADESCO SAÚDE</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg23.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">BRADESCO <Br>INDIVIDUAL</h4>
                                    <p>Criança, Adulto ou Idoso. Planos completos ou Planos mais simples. Independente da Condição Nossos Parceiros tem a opção Ideal para você.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg24.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">BRADESCO <Br> FAMILIAR</h4>
                                    <p>Cuidar de quem amamos é fundamental, os Planos de Saúde com o melhor custo benefício para a família brasileira estão aqui.</p>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" style="background-image: url('assets/img/bg25.jpg')">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">BRADESCO <Br> EMPRESARIAL</h4>
                                    <p>Quem tem CNPJ paga menos no Plano de Saúde, aqui você pode pagar baratinho em um Plano regional, ou reduzir seus custos com uma migração.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA OS VALORES DOS</span> <br><span style="font-weight:800" class="azulescuro">PLANOS BRADESCO SAÚDE</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3" style="background-color: #D45352; color: #fff; padding-bottom: 30px;margin-right: 20px;">
                      <h3 class="h4">0 à 18 ANOS <br><span class="categoria">Adesão</span></h3><span>A PARTIR DE <br> R$254,53</span>
                    </div>
                    <div class="col-md-3" style="background-color: #EFEFEF; color: #D45352; padding-bottom: 30px;margin-right: 20px;">
                    <h3 class="h4">34 à 38 ANOS <br><span class="categoria">Adesão</span></h3><span>A PARTIR DE <br> R$506,53</span>
                    </div>
                    <div class="col-md-3" style="background-color: #EFEFEF; color: #D45352; padding-bottom: 30px;margin-right: 20px;">
                    <h3 class="h4">54 à 58 ANOS <br><span class="categoria">Adesão</span></h3><span>A PARTIR DE <br> R$866,78</span>
                    </div>
                </div><Br>
                <div class="row">
                    <div class="col-md-3" style="background-color: #D45352; color: #fff; padding-bottom: 30px;margin-right: 20px;">
                      <h3 class="h4">0 à 18 ANOS <br><span class="categoria">Empresarial</span></h3><span>A PARTIR DE <br> R$236,67</span>
                    </div>
                    <div class="col-md-3" style="background-color: #EFEFEF; color: #D45352; padding-bottom: 30px;margin-right: 20px;">
                    <h3 class="h4">34 à 38 ANOS <br><span class="categoria">Empresarial</span></h3><span>A PARTIR DE <br> R$418,15</span>
                    </div>
                    <div class="col-md-3" style="background-color: #EFEFEF; color: #D45352; padding-bottom: 30px;margin-right: 20px;">
                    <h3 class="h4">54 à 58 ANOS <br><span class="categoria">Empresarial</span></h3><span>A PARTIR DE <br> R$802,28</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-2 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
                                            A Bradesco Saúde é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata.

                                            </h5>
                                            <h3 class="card-title">Caroline Almeida</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Gerente de Novos Negócios</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description"> Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..

                                            </h5>
                                            <h3 class="card-title">Matheus Cardoso</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente Bradesco Saúde</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div id="contato">
            <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-color:#DD0333">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="title" style="font-size: 3em">
                                        <span style="color: #fff">
                                            AINDA TEM DÚVIDAS?
                                            <p class="description" style="    color: #fff;font-size: 0.6em">
                                                Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde Santa Helena ideal para você!
                                        </span>
                            </h1>
                            <div class="col-md-12 text-center">
                                <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1em">
                                COTAÇÃO SEM COMPROMISSO!
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            Sobre nós
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            Planos de Saúde
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card card-login card-plain">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>

                    </div>
                    <div class="modal-body">
                        <?php include("../helpers/insereBanco.php"); ?>
                            <?php include("includes/Form/FormBradesco.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
  </div>
</html>
