<div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                    <img src="assets/img/amil-logo.png" width="200">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#depoimentos" data-scroll>
                            <p>Depoimentos</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#diferencias" data-scroll>
                            <p>Diferenciais</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#contato" data-scroll>
                            <p>Contato</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-left">
                        <h4 class="title"><span  style="font-size: 1.4em; color:#233A7C">PLANOS À PARTIR DE<br><span class="highlight" style="font-size: 2.4em; color:#233A7C"> <br> R$ 307,81<span  style="font-size: 0.7em; color:#233A7C">*</span> <br></span></h4>
                        <h3 class="description" style="color:#fff"><span style="font-size: 0.7em; color:#233A7C">* Amil 400 até 18 anos - Adesão - Preço por vida</span></h3>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">

                            <?php include("includes/Form/FormAmil2.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-8 section-image"  id="sobrenos">
    <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title">Conheça o Amil Saúde</h2>
        <h4 class="description"> Só a Amil tem as melhores opções de Plano de Saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É Rápido e Fácil. </h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical-emergencies.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <h4 class="info-title">COBERTURA EM TODO <BR>TERRITÓRIO NACIONAL
                        </h4>
                        <p class="description">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em todo território nacional, para maiores informações solicite uma cotação em nossa página!</p>
                        <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical_app.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <h4 class="info-title">COBERTURA DE PROCEDIMENTOS</h4>
                        <p class="description">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação, aqui mesmo em nossa página, para que um Corretor Autorizado lhe auxilie com maiores informações.</p>
                        <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical-gif-11.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <h4 class="info-title">PREÇOS DOS PLANOS DE SAÚDE AMIL
</h4>
                        <p class="description">


                          Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado. Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros) <br> <br>
<b>Além disso a Amil tem parcerias que oferecem desontos incríveis para você, confira:</b> <br><br>
                          Smart Fit – Programa Saúde em Dobro <br>

Drogaria São Paulo<br>

Drogaria Pacheco<br>

E Muito mais.

</p>
                        <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pricing-5 section-pricing-5 " id="redecredenciada" >
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                    <span style="color: #08377F">
                    CONHEÇA A REDE CREDENCIADA DA AMIL
                    </span>
                </h1>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto text-center">

                    <ul class="nav nav-pills nav-pills-default nav-pills-icons" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#myModal" role="tablist">
                            <img src="assets/img/card-blog1.png"  width="100px"><br><br> Zona Oeste
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  data-toggle="modal" data-target="#myModal2" role="tablist">
                            <img src="assets/img/card-blog2.png"  width="100px"><br><br> Zona Norte
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal3" role="tablist">
                            <img src="assets/img/card-blog3.png"  width="100px"><br><br> Centro
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal4" role="tablist">
                            <img src="assets/img/card-blog4.png"  width="100px"><br><br> Zona Leste
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal5" role="tablist">
                            <img src="assets/img/card-blog5.png"  width="100px"><br><br> Zona Sul
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal6" role="tablist">
                            <img src="assets/img/card-blog6.png"  width="100px"><br><br> ABCD
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal7" role="tablist">
                            <img src="assets/img/card-blog7.png"  width="100px"><br><br> Alto do Tiête
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal8" role="tablist">
                            <img src="assets/img/card-blog8.png"  width="100px"><br><br> Itapecerica da Serra
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal9" role="tablist">
                            <img src="assets/img/card-blog9.png"  width="100px"><br><br> Região de Osasco
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  "  data-toggle="modal" data-target="#myModal10" role="tablist">
                            <img src="assets/img/card-blog10.png"  width="100px"><br><br> Franco da Rocha
                            </a>
                        </li>
                    </ul>

                </div>

                <div class="col-md-12 ml-auto mr-auto text-center">
                    <h1 class="title" style="font-size: 3em">
                        <span style="color: #08377F">
                        CARÊNCIA AMIL
                        </span>
                    </h1>
                    <p>Essa é a tabela de carências da Amil. Aqui você vai encontrar os prazos de espera para vários procedimentos, como por exemplo: Consultas Programadas em clínicas ou centros médicos, Procedimentos e exames básicos ou terapêuticos em ambulatórios e muito mais.</p>
                    <img src="assets/img/carencias-amil-min.jpg" class="img-responsive text-center"><br><br>
                    <p>Lembrando que a Amil conta com o Aditivo de redução de Carência, caso você venha de algum Plano compatível em outra Operadora ou esteja contratando um Plano como Pessoa Jurídica.</p>
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title"><span style="font-weight:400" class="azulclaro"> CONFIRA A TABELA DE PREÇO</span></h2>
                    </div>
                    <img src="assets/img/1.jpg" class="img-responsive text-center"><br><br>
                    <img src="assets/img/2.jpg" class="img-responsive text-center"><br><br>
                </div>
                <div class="text-center">

              </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-2 " style="background-color:#f0f0f0">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
              <div class="col-md-12 ml-auto mr-auto text-center">
                  <h1 class="title" style="font-size: 3em">
                      <span style="color: #08377F">
                      O PLANO AMIL É BOM?
                      </span>
                  </h1>
              </div>
                <div id="carouselExampleIndicators2" class="carousel slide">

                    <div class="carousel-inner" role="listbox">

                        <div class="carousel-item justify-content-center active">
                            <div class="card card-testimonial card-plain">

                                <div class="card-body">
                                    <h5 class="card-description">"Sempre que precisei da Amil nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios,  atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito."
                                    </h5>
                                    <h3 class="card-title">Arlete Rezende</h3>
                                    <div class="card-footer">
                                        <h6 class="category text-primary">Empreendedora e cliete Amil Saúde.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/bg15a.jpg'); background-size: cover;" id="contato">
    <div class="row">
        <div class="col-md-8 ml-auto mr-auto text-center">
            <div class="card card-testimonial card-plain">
                <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                QUERO COTAR AGORA
                </a>
            </div>
        </div>
    </div>
</div>
<footer class="footer " style="background-color:#fff; color:#184B92">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#184B92">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</div>
    </footer>
</div>
