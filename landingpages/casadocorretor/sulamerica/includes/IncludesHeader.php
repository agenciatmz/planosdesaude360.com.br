<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Plano de Saúde Sulamerica</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<link href="assets/img/LandingPagesSulamerica/favicon.ico" rel="shortcut icon">
<link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/LandingPageSulamerica.css" rel="stylesheet" />
  <?php require_once("../helpers/includesPixel.php"); ?>

  <?php require_once("../helpers/includesChat.php"); ?>
