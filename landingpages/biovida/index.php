<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde Biovida</title>

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>
    <!-- Css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-icons.css" />
    <link rel="stylesheet" href="css/style.css" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="planosdesaude360.com.br" />
    <meta property="og:description" content="Os melhores Planos de Saúde" />
    <meta property="og:site_name" content="planosdesaude360.com.br" />
    <meta property="og:url" content="https://planosdesaude360.com.br/home" />
    <meta property="og:image" content="https://landingpages.planosdesaude360.com.br/generica/img/destaque.png" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <?php require_once("../helpers/includesConexaoBanco.php"); ?>


    <?php require_once("../helpers/includesPixel.php"); ?>

    <?php require_once("../helpers/includesChat.php"); ?>

</head>


<body>

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      "Loading..."
    </div>
  </div>

  <main class="main-wrapper">

    <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="index.php" class="logo-container flex-child">
                              <img class="logo" src="img/logo.png"  alt="logo" width="100px">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li>
                                  <a href="#">Home</a>
                              </li>
                              <li>
                                  <a href="#diferenciais">Diferenciais</a>
                              </li>

                              </li>
                              <li>
                                  <a href="#depoimentos">Depoimentos</a>
                              </li>
                              <li>
                                  <a href="#redecredenciada">Rede Credenciada</a>
                              </li>
                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->

                      <div class="nav__btn-holder nav--align-right">
                          <a href="#formulario" class="btn nav__btn botaoshake">
                              <span class="nav__btn-text">Entre em contato</span>
                              <span class="nav__btn-phone">Clique aqui</span>
                          </a>
                      </div>

                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

      <!-- Triangle Image -->
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       viewBox="0 0 600 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-right">
      <g>
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img1)" />
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#triangle-gradient)" fill-opacity="0.7" />
      </g>
      <defs>
        <pattern id="img1" patternUnits="userSpaceOnUse" width="500" height="500">
          <image xlink:href="img/hero/hero.jpg" x="50" y="70" width="500" height="500"></image>
        </pattern>

        <linearGradient id="triangle-gradient" y2="100%" x2="0" y1="50%" gradientUnits="userSpaceOnUse" >
        <stop offset="0" stop-color="#4C86E7"/>
        <stop offset="1" stop-color="#B939E5"/>
        </linearGradient>
      </defs>
    </svg>


    <div class="content-wrapper oh">

      <!-- Hero -->
      <section class="hero">

        <div class="container">
          <div class="row">
            <div class="col-lg-5 offset-lg-1">
              <div class="hero__text-holder">
                <h1 class="hero__title hero__title--boxed">A partir de<span class="highlight"> R$ 375,11*</span><span style="font-size:10px">* 54 à 58 anos</span> </h1><Br>
                  <h1 class="hero__title hero__title--boxed">A partir de<span class="highlight"> R$ 545,79*</span><span style="font-size:10px">* 59 ou mais anos</span> </h1>
<br>
                <h2 class="hero__subtitle">

Há mais de 10 anos no mercado, nossos pilares foram construídos com profissionais de grande experiência e amplo conhecimento técnico, mas principalmente com a ética e a satisfação dos clientes.

                  </h2>
              </div>
            </div>
          </div>

          <div class="row justify-content-center formshake" id="formulario" >
            <div class="col-lg-10">
              <!-- Optin Form -->
              <div class="optin ">

                  <h3 class="optin__title text-center">Preencha o formulário abaixo e saiba tudo sobre essas opções de<span class="highlight"> Planos de Saúde BIOVIDA SÊNIOR </span></h3>
                  <?php include("../helpers/insereBanco.php"); ?>

                    <?php include("includes/includeForm.php"); ?>
              </div>
            </div>
          </div>
        </div>
      </section> <!-- end hero -->

      <!-- Service Boxes -->
      <section class="section-wrap pb-72 pb-lg-40" id="diferenciais">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <div class="title-row">
                <h2 class="section-title text-center" style="color:#333B69;    line-height: 40px;">


              <span class="highlight">  DIFERENCIAIS DOS PLANOS BIOVIDA SAÚDE</span>
                </h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line" style="padding-bottom: 52px;">

                <svg class="feature__icon "><img src="img/icons/imgDezAnosMercado.png" width="70" class="img-svg1"></svg>
                  <h4 class="feature__title"><span class="highlight">+ de 10 anos no mercado</span></h4>
                  <p class="feature__text" style="text-align: justify;"> Há mais de 10 anos no mercado, nossos pilares foram construídos com profissionais de grande experiência e amplo conhecimento técnico <br></p>
<br>
                <br>
                <br>
                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>

              </div>

            </div>
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line" style="padding-bottom: 52px;">

                <svg class="feature__icon "><img src="img/icons/imgTransparencia.png" width="70" class="img-svg1"></svg>
                  <h4 class="feature__title"><span class="highlight">Política de Transparência</span></h4>
                  <p class="feature__text" style="text-align: justify;">  Trabalhamos com uma política de transparência e seriedade em todos nossos processos, para que o associado sinta-se a vontade em ter como plano de saúde a Biovida Saúde  <br></p>
<br>
<br>

                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>

              </div>
            </div>
            <div class="col-lg-4">
              <div class="feature box-shadow hover-up hover-line" style="padding-bottom: 52px;">

                <svg class="feature__icon "><img src="img/icons/imgRedeAtendimento.png" width="70" class="img-svg1"></svg>
                  <h4 class="feature__title"><span class="highlight">Rede de Atendimento</span></h4>
                  <p class="feature__text" style="text-align: justify;">  Nossa rede de atendimento é planejada para melhor atende-lo, temos um departamento que monitora a demanda de nossos clientes a fim de manter nossos credenciados clinicas e hospitais sempre adequados as nossas necessidade.  <br></p>
<br>

                  <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Cotar Agora</span>
                      <i class="ui-arrow-right"></i>
                  </a>

              </div>
            </div>
<!---->
<!--              <div class="col-lg-3">-->
<!--                  <div class="feature box-shadow hover-up hover-line">-->
<!--                      <svg class="feature__icon "><img src="img/icons/constructor.svg" width="70" class="img-svg1"></svg>-->
<!--                      <h4 class="feature__title">Planos de Saúde <span class="highlight"><br>Adesão</span></h4>-->
<!--                      <p class="feature__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>-->
<!--                      <br>-->
<!--                      <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">-->
<!--                          <span>Cotar Agora</span>-->
<!--                          <i class="ui-arrow-right"></i>-->
<!--                      </a>-->
<!--                  </div>-->
<!--              </div>-->
          </div>
        </div>
      </section> <!-- end service boxes -->

      <!-- Promo Section -->
      <section class="section-wrap promo-section promo-section--pb-large pt-lg-40">

        <!-- Triangle Image -->
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 600 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-left">
          <g>
            <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img2)" />
          </g>
          <defs>
            <pattern id="img2" patternUnits="userSpaceOnUse" width="600" height="600">
              <image xlink:href="img/promo/promo_img_1.jpg" width="600" height="600"></image>
            </pattern>
          </defs>
        </svg>


      </section> <!-- end promo section -->


      <!-- Testimonials -->
      <section class="section-wrap bg-color" id="depoimentos">
        <div class="container" style="margin-bottom:120px">
          <div class="row justify-content-center">
            <div class="col-lg-12">
              <div class="title-row">
                <h2 class="section-title">Sua vida mais Tranquila é nosso maior objetivo</h2>
                <p class="subtitle">Veja o que nossos clientes falam de nós</p>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature box-shadow" style="padding-bottom: 72px; padding-top: 71px;">
                    <img src="img/testimonials/imgDepoimentos01.png" alt="" class="testimonial__img">
                      <h4 class="feature__title">Regina <span class="highlight">Nobre <br></span> <span style="font-size:10px"> Aponsentada<br>   São Bernardo do Campo<span></h4><br>
                      <p class="feature__text">                                        Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..<br></p><br>
                      <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                          <span>Cotar Agora</span>
                          <i class="ui-arrow-right"></i>
                      </a>

                  </div>

                </div>
                <div class="col-lg-6">
                  <div class="feature box-shadow" style="padding-bottom: 72px; padding-top: 71px;">

                    <img src="img/testimonials/imgDepoimentos02.png" alt="" class="testimonial__img">
                      <h4 class="feature__title">Jorge  <span class="highlight">Pires <br></span> <span style="font-size:10px">  Aposentado <br> Santo André<span></h4><br>
                    <p class="feature__text">A BIOVIDA é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata.Hoje, após 2 anos com o Plano, nunca tive problema e recomendo.
              </p>
                      <br>
                      <a href="#formulario" class="btn btn--lg btn--color btn--icon formshake">
                          <span>Cotar Agora</span>
                          <i class="ui-arrow-right"></i>
                      </a>
                  </div>
                </div>


              </div>

            </div>
          </div>
        </div>
      </section> <!-- end testimonials -->

      <!-- From Blog -->


      <!-- Partners -->


      <!-- CTA -->
      <div class="container offset-top-152 pt-sm-48">
        <div class="row justify-content-center">
          <div class="col-lg-10">
            <div class="call-to-action box-shadow-large text-center">
              <div class="call-to-action__container">
                <h3 class="call-to-action__title">
                Dúvidas sobre Rede Credenciada e outras informações ? Solicite um contato agora mesmo
                </h3>
                <a href="#formulario" class="btn btn--lg btn--color">
                  <span>Solicitar Contato</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end cta -->

      <!-- Footer -->
      <footer class="footer">

        <div class="footer__bottom top-divider">
          <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Saúde 360, Desenvolvido por <a href="https://agenciatresmeiazero.com.br">Agência TresMeiaZero</a>
            </span>
          </div>
        </div> <!-- end footer bottom -->
      </footer> <!-- end footer -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->
  <?php include("includes/modal/modals.php"); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/core.js"></script>

  <script src="js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
  <script src="js/validation.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/api-estado-cidade.js"></script>
  <script src="js/FormSteps.js"></script>
  <script src="js/custom.js"></script>



</body>
</html>
