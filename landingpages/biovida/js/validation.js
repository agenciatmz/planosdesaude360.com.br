$(function() {
    $.validator.setDefaults( {
        errorClass:'form-control-feedback text-danger', highlight:function(element) {
            $(element).closest('.form-group ').addClass('has-danger');
        }
        , unhighlight:function(element) {
            $(element).closest('.form-group')
            .removeClass('has-danger')
            .addClass('has-success');
        }
    }
    );
    $("#contact-form").validate( {
        rules: {
            email: {
                required: true, email: true
            }
            , nome: {
                required: true
            }
            , telefone: {
                required: true, number: true, minlength: 11
            }
            , corretora: {
                required: true
            }
            , estado: {
                required: true
            }
            , cidade: {
                required: true
            }
            , quantidade: {
                required: true
            }
            , cnpj: {
                required: true
            }
            , quantidadefamiliar: {
                required: true
            }
            , modalidade: {
                required: true
            }
            , tipopessoa: {
                required: true
            }
        }
        , messages: {
            email: {
                required:'Insira um email válido', email:'E-mail <em>inválido</em>, tente novamente .', remote:$.validator.format("{0} is already associated with an account.")
            }
            , nome: {
                required: 'Insira seu nome'
            }
            , telefone: {
                required: 'Insira seu Telefone'
            }
            , corretora: {
                required: 'Insira o nome da corretora'
            }
            , estado: {
                required: 'Insira o seu estado'
            }
            , cidade: {
                required: 'Insira a sua cidade'
            }
            , quantidade: {
                required: 'Insira a quantidade'
            }
            , cnpj: {
                required: 'Insira o cnpj'
            }
            , quantidadefamiliar: {
                required: 'Insira a quantidade'
            }
            , modalidade: {
                required: 'Insira pelo menos uma opção'
            }
            , tipopessoa: {
                required: 'Insira pelo menos uma opção'
            }
        }
        , errorPlacement:function(error, element) {
            if(element.is(":radio")) {
                error.appendTo(element.parents());
            }
            else {
                error.insertAfter(element);
            }
        }
    }
    );
}

);
