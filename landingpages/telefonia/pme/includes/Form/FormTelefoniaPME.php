<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
  <div class="card-body text-left ">
  <div class="row">
      <div class="col-md-12">
        <?php echo $msgClientesSucesso; ?>
        <?php echo $msgClientesErro; ?>
        <?php echo $e; ?>

      </div>

          <div class="col-md-6">
              <div class="form-group">
                  <label>CNPJ ou Razão Social</label>
                  <input type="text" class="form-control" placeholder="Insira seu CNPJ ou Razão Social" name="tmzCnpjEmpresa" id="tmzCnpjEmpresa">
              </div>
          </div>


      <div class="col-md-6">
          <div class="form-group">
              <label>Nome da sua Empresa</label>
              <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="tmzNomeEmpresa" id="tmzNomeEmpresa" required>
          </div>
      </div>

      <div class="col-md-6">
          <div class="form-group">
              <label>E-mail Corporativo</label>
              <input type="email" class="form-control" placeholder="Insira seu E-mail..." name="tmzEmail" id="tmzEmail" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Seu Nome</label>
              <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="tmzNome" id="tmzNome" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Celular</label>
              <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="tmzTelefone" id="tmzTelefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Telefone Alternativo</label>
              <input type="text" class="form-control phone" placeholder="Insira com DDD" name="tmzTelefoneAlternativo" id="tmzTelefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6">
          <label for="estado" class="col-form-label">Estado</label>
          <select id="estado" name="estado"  data-style="btn btn-simple btn-round" class="selectpicker"   data-size="7" required >
              <option> Choose</option>
          </select>
      </div>
      <div class="col-md-6">
          <label for="cidade" class="col-form-label">Cidade</label>
          <select id="cidade" name="cidade" data-style="btn btn-simple btn-round" class="selectpicker"   data-size="7" required >
              <option> Choose</option>
          </select>
      </div>

      <div class="col-md-12">
          <div class="form-group">
              <label>Quantidade de Linhas</label>
              <input type="text" class="form-control phone" placeholder="Insira com DDD" name="tmzQtdLinhas" id="tmzQtdLinhas" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
      <div class="d-none">
          <div class="input-group">
              <input  type="text" class="form-control" value="" name="tmzPlanoEscolhido" id="tmzPlanoEscolhido">
          </div>
      </div>
  </div>
  <div class="row">
            <div class="col-md-12">
                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-warning btn-lg pull-right ">Solicitar Cotação</button>
            </div>
        </div>
</div>

</form>
