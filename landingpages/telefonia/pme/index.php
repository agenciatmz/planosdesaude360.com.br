

<?php require_once("../../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link
            rel="shortcut icon"
            href="//v.fastcdn.co/u/b384de7c/17026856-0-logo-amil-1.png"
            type="image/ico"
            />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>TELEFONIA PME</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../../helpers/includesPixel.php"); ?>
        <?php require_once("../../helpers/includesChat.php"); ?>
    </head>
    <body class="template-page">
        <!-- Navbar -->
        <div class="header-2" >
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png" class="d-none d-sm-block">
                        <img src="assets/img/logo-color.png" class="d-block d-sm-none">
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>TIM</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>VIVO</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>OI</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h1 class="title" style="font-size: 3em;"><span style="color:#fff">TELEFONIA PME <span style="font-weight: 300">PARA PEQUENAS E MÉDIAS EMPRESAS</span> </h1>
                                </span>
                                <h4 class="description" style="font-weight: 300; color:#fff">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna ligula, vulputate eget dui ut, venenatis ornare ante. Nulla aliquam tincidunt felis et lacinia.
                                </h4>
                                <button class="btn btn-info btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500" data-toggle="modal" data-target="#loginModal">Solicitar Cotação</button>
                            </div>
                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                <div class="card card-contact card-raised formshake">
                                    <?php
                                        if(isset($_POST['cadastrar'])){

                                            $tmzNomeEmpresa            = trim(strip_tags($_POST['tmzNomeEmpresa']));
                                            $tmzCnpjEmpresa            = trim(strip_tags($_POST['tmzCnpjEmpresa']));
                                            $tmzNome            = trim(strip_tags($_POST['tmzNome']));
                                            $tmzEmail            = trim(strip_tags($_POST['tmzEmail']));
                                            $tmzTelefone            = trim(strip_tags($_POST['tmzTelefone']));
                                            $tmzTelefoneAlternativo            = trim(strip_tags($_POST['tmzTelefoneAlternativo']));
                                            $estado            = trim(strip_tags($_POST['estado']));
                                            $cidade            = trim(strip_tags($_POST['cidade']));
                                            $tmzQtdLinhas            = trim(strip_tags($_POST['tmzQtdLinhas']));
                                            $tmzPlanoEscolhido            = trim(strip_tags($_POST['tmzPlanoEscolhido']));


                                            $insert =
                                            "INSERT INTO tmzleadstelefonia (  tmzNomeEmpresa, tmzCnpjEmpresa, tmzNome, tmzEmail, tmzTelefone, tmzTelefoneAlternativo, estado, cidade, tmzQtdLinhas, tmzPlanoEscolhido )
                                            VALUES ( :tmzNomeEmpresa, :tmzCnpjEmpresa, :tmzNome, :tmzEmail, :tmzTelefone, :tmzTelefoneAlternativo, :estado, :cidade, :tmzQtdLinhas, :tmzPlanoEscolhido )";
                                            try{

                                                $result = $conexao->prepare($insert);

                                                $result->bindParam(':tmzNomeEmpresa', $tmzNomeEmpresa, PDO::PARAM_STR);
                                                $result->bindParam(':tmzCnpjEmpresa', $tmzCnpjEmpresa, PDO::PARAM_STR);
                                                $result->bindParam(':tmzNome', $tmzNome, PDO::PARAM_STR);
                                                $result->bindParam(':tmzEmail', $tmzEmail, PDO::PARAM_STR);
                                                $result->bindParam(':tmzTelefone', $tmzTelefone, PDO::PARAM_STR);
                                                $result->bindParam(':tmzTelefoneAlternativo', $tmzTelefoneAlternativo, PDO::PARAM_STR);
                                                $result->bindParam(':estado', $estado, PDO::PARAM_STR);
                                                $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
                                                $result->bindParam(':tmzQtdLinhas', $tmzQtdLinhas, PDO::PARAM_STR);
                                                $result->bindParam(':tmzPlanoEscolhido', $tmzPlanoEscolhido, PDO::PARAM_STR);


                                                $result->execute();
                                                $contar = $result->rowCount();
                                                if($contar>0){

                                                    {
                                                        $msgClientesSucesso = '
                                                                <script type="text/javascript">
                                                                window.location = "includes/obrigado/obrigado.php";
                                                                </script>';

                                                    }
                                                }else{
                                                    $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                <strong>Erro</strong> ao cadastrar o usuário.
                                                                </div>';
                                                }
                                            }catch(PDOException $e){
                                                echo $e;
                                            }

                                        }else {
                                            $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
                                        }
                                        ?>
                                    <?php include("includes/Form/FormTelefoniaPME.php");?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-4" id="tim" style="background-color: #7451C5;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title">
                                <span style="font-weight:400; color:#FFF">CONFIRA OS PLANOS</span> <br>
                                <span style="font-weight:800;color:#FFF">OPERADORA TIM</span>
                            </h2>
                            <img src="assets/img/Logo_TIM.jpg" width="200px" style="background-color: #fff;padding: 11px;border-radius: 10px;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                          <div class="card card-pricing efeitoBgTim" >
                            <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:#ff3a3a">6GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$69,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800;color: white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px;color: white;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">SMS Ilimitadas</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">Apps ilimitados</span><br><span style="font-size:10px;color: white">Whatsapp, Messenger, Telegram, Waze,<br> Easy Taxi, Instagram, Facebook, Twitter </span></li>
                                        <li><span style="font-weight:800;color: white">TIM Banca Premium</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Protect Backup</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Finanças</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='TIM 6GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                          <div class="card card-pricing efeitoBgTim" >

<div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:#ff3a3a">10GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$99,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800;color: white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">SMS Ilimitadas</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">Apps ilimitados</span><br><span style="font-size:10px;color: white">Whatsapp, Messenger, Telegram, Waze,<br> Easy Taxi, Instagram, Facebook, Twitter </span></li>
                                        <li><span style="font-weight:800;color: white">TIM Banca Premium</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Protect Backup</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Finanças</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='TIM 10GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                          <div class="card card-pricing efeitoBgTim" >

                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:#ff3a3a">20GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$139,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800;color: white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">SMS Ilimitadas</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">Apps ilimitados</span><br><span style="font-size:10px;color: white">Whatsapp, Messenger, Telegram, Waze,<br> Easy Taxi, Instagram, Facebook, Twitter </span></li>
                                        <li><span style="font-weight:800;color: white">TIM Banca Premium</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Protect Backup</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Finanças</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='TIM 20GB';" class="btn btn-success btn-default btn-round botaoshake" >
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                          <div class="card card-pricing efeitoBgTim" >

                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:#ff3a3a">50GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$199,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800;color: white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">SMS Ilimitadas</span><br><span style="font-size:10px;color: white">usando DDD 41</span></li>
                                        <li><span style="font-weight:800;color: white">Apps ilimitados</span><br><span style="font-size:10px;color: white">Whatsapp, Messenger, Telegram, Waze,<br> Easy Taxi, Instagram, Facebook, Twitter </span></li>
                                        <li><span style="font-weight:800;color: white">TIM Banca Premium</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Protect Backup</span></li>
                                        <li><span style="font-weight:800;color: white">TIM Finanças</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='TIM 50GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-4" id="vivo" style="background-color: #f1f1f1;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title">
                                <span style="font-weight:400; color:#BE2886">CONFIRA OS PLANOS</span> <br>
                                <span style="font-weight:800;color:#BE2886">OPERADORA VIVO</span>
                            </h2>
                            <img src="assets/img/Logo_VIVO.svg" width="200px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgVivo">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800; color:white">10GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$69,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">SMS Ilimitadas</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">2.500 Minutos em ligações</span><br><span style="font-size:10px; color:white">p/ países das Américas</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo BIS</span></li>
                                        <li><span style="font-weight:800; color:white">Gestão de Custo Light</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo Protege de 100GB</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='VIVO 10GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgVivo2">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">15GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$99,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">SMS Ilimitadas</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">5.000 Minutos em ligações</span><br><span style="font-size:10px; color:white">p/ países das Américas e Europa</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo BIS</span></li>
                                        <li><span style="font-weight:800; color:white">Gestão de Custo Light</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo Protege de 100GB</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='VIVO 15GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgVivo3" style="">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">20GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$139,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">SMS Ilimitadas</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">5.000 Minutos em ligações</span><br><span style="font-size:10px; color:white">p/ países das Américas e Europa</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo BIS</span></li>
                                        <li><span style="font-weight:800; color:white">Gestão de Custo Light</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo Protege de 1TB</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='VIVO 20GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgVivo4">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">50GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$199,90</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas<br> para todo o Brasil</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">SMS Ilimitadas</span><br><span style="font-size:10px; color:white">p/ qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">7.000 Minutos em ligações</span><br><span style="font-size:10px; color:white">p/ países das Américas e Europa</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo BIS</span></li>
                                        <li><span style="font-weight:800; color:white">Gestão de Custo Light</span></li>
                                        <li><span style="font-weight:800; color:white">Vivo Protege de 1TB</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='VIVO 50GB';" class="btn btn-success btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-4" id="oi" style="background-color: #fff;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto text-center">
                            <h2 class="title">
                                <span style="font-weight:400; color:#25B2A2">CONFIRA OS PLANOS</span> <br>
                                <span style="font-weight:800;color:#25B2A2">OPERADORA OI</span>
                            </h2>
                            <img src="assets/img/Logotipo_da_Oi.png" width="150px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgOi" >
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800; color:white">6GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$59,00</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas</span><br><span style="font-size:10px; color:white">pra celulares e fixo<br> de qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">Oi gestor</span><br><span style="font-size:10px; color:white">Controle o uso das linhas móveis<br> da sua empresa</span></li>
                                        <li><span style="font-weight:800; color:white">3.000 SMS</span><br><span style="font-size:10px; color:white">p/ todas operadoras</span></li>
                                        <li><span style="font-weight:800; color:white">Valor fixo todo mês</span></li>
                                        <li><span style="font-weight:800; color:white">Acesso à rede WiFi da Oi</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='Oi 6GB';" class="btn btn-danger btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgOi">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">10GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$89,00</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas</span><br><span style="font-size:10px; color:white">pra celulares e fixo<br> de qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">Oi gestor</span><br><span style="font-size:10px; color:white">Controle o uso das linhas móveis<br> da sua empresa</span></li>
                                        <li><span style="font-weight:800; color:white">3.000 SMS</span><br><span style="font-size:10px; color:white">p/ todas operadoras</span></li>
                                        <li><span style="font-weight:800; color:white">Valor fixo todo mês</span></li>
                                        <li><span style="font-weight:800; color:white">Acesso à rede WiFi da Oi</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='Oi 10GB';" class="btn btn-danger btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgOi">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">15GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$99,00</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas</span><br><span style="font-size:10px; color:white">pra celulares e fixo<br> de qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">Oi gestor</span><br><span style="font-size:10px; color:white">Controle o uso das linhas móveis<br> da sua empresa</span></li>
                                        <li><span style="font-weight:800; color:white">3.000 SMS</span><br><span style="font-size:10px; color:white">p/ todas operadoras</span></li>
                                        <li><span style="font-weight:800; color:white">Valor fixo todo mês</span></li>
                                        <li><span style="font-weight:800; color:white">Acesso à rede WiFi da Oi</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='Oi 15GB';" class="btn btn-danger btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="card card-pricing efeitoBgOi">
                                <div class="card-body">
                                    <h1 class="card-title" style="font-weight:800;color:white">25GB<br>
                                        <span style="font-size:0.8em;font-weight:500">R$165,00</span>
                                    </h1>
                                    <ul>
                                        <li><span style="font-weight:800; color:white">Ligações Ilimitadas</span><br><span style="font-size:10px; color:white">pra celulares e fixo<br> de qualquer operadora</span></li>
                                        <li><span style="font-weight:800; color:white">Oi gestor</span><br><span style="font-size:10px; color:white">Controle o uso das linhas móveis<br> da sua empresa</span></li>
                                        <li><span style="font-weight:800; color:white">3.000 SMS</span><br><span style="font-size:10px; color:white">p/ todas operadoras</span></li>
                                        <li><span style="font-weight:800; color:white">Valor fixo todo mês</span></li>
                                        <li><span style="font-weight:800; color:white">Acesso à rede WiFi da Oi</span></li>
                                    </ul>
                                    <a href="#" onclick="document.getElementById('tmzPlanoEscolhido').value='Oi 25GB';" class="btn btn-danger btn-default btn-round botaoshake">
                                    CONTRATAR AGORA
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#tim">
                            TIM
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#vivo">
                            VIVO
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#oi">
                            OI
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
    </div>
</html>
