<?php require_once("../../helpers/includesConexaoBanco.php"); ?>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Template - Material Kit PRO by Creative Tim</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,800|Material+Icons' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/lpTelefoniaTimPME.css" rel="stylesheet"/>
        <link href="assets/css/hover.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-fixed-top" id="sectionsNav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/imgLogoGenerico.png" alt="BomCotar">
                            </div>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#comofunciona" >
                            <span class="hvr-underline-from-left">MENU 01</span>
                            </a>
                        </li>
                        <li>
                            <a href="#vantagens">
                            <span class="hvr-underline-from-left">MENU 02</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="txt">
                            <span class="hvr-underline-from-left">AGENDAR UM CONSULTOR</span>
                            </a>
                        </li>
                        <li class="button-container txt" style="top: 13px">
                            <button class="btn btnTopoProposta btn-xs  ">
                            RECEBER PROPOSTA
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="wrapper" id="topo">
            <div class="page-header" style="background-image: url('assets/img/bg12.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="tituloHeader hidden-xs hidden-sm"> <span class="fw800">PLANOS EMPRESARIAIS PARA PEQUENAS E MÉDIAS EMPRESAS</span> </h1>
                            <h1 class="tituloHeaderM hidden-lg hidden-md">PLANOS EMPRESARIAIS PARA PEQUENAS E MÉDIAS EMPRESAS</h1>
                            <div class="text-center hidden-xs  hidden-sm" style="position: relative;top: 45px;">
                                <img src="assets/img/carroHeader.png" alt="BomCotar" >
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1 formMenor" id="formshake">


                              <div class="card card-contact">
                                <form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" >
                                      <div class="header header-raised header-primary text-center">
                                          <h3 class="card-title">SIMULE AQUI</h3>
                                          <?php echo $msgClientesSucesso; ?>
                                          <?php echo $msgClientesErro; ?>
                                          <?php echo $e; ?>
                                      </div>
                                      <div class="card-content">
                                          <div class="row">
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">domain</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="Nome da Empresa" required name="tmzNomeEmpresa" id="tmzNomeEmpresa">
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">portrait</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="CNPJ da Empresa" required name="tmzCnpjEmpresa" id="tmzCnpjEmpresa">
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">face</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="Nome Completo" required name="tmzNome" id="tmzNome">
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">mail_outline</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="E-mail Corporativo" required name="tmzEmail" id="tmzEmail">
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">phonelink_ring</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="Tel. Móvel" required name="tmzTelefone" id="tmzTelefone">
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">phone</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="Tel. Fixo" name="tmzTelefoneAlternativo" id="tmzTelefoneAlternativo">
                                                  </div>
                                              </div>
                                              <div class="col-lg-6">
                                                <select class="selectpicker" data-style="select-with-transition" title="Selecione" name="estado" id="estado" data-size="7" tabindex="-98">
                                                  <option class="bs-title-option" value="">Selecione</option>
                                                    <option disabled="">Selecione</option>

                                                      </select>
                                              </div>
                                              <div class="col-lg-6">
                                                <select class="selectpicker" data-style="select-with-transition" title="Selecione" name="cidade" id="cidade" data-size="7" tabindex="-98">
                                                  <option class="bs-title-option" value="">Selecione</option>
                                                    <option disabled="">Selecione</option>

                                                      </select>
                                              </div>
                                              <div class="col-lg-12">
                                                  <div class="input-group">
                                                      <span class="input-group-addon">
                                                      <i class="material-icons">label</i>
                                                      </span>
                                                      <input type="text" class="form-control" placeholder="Já possui plano corporativo ?" required name="tmzQtdLinhas" id="tmzQtdLinhas">
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <button type="submit" id="cadastrar" name="cadastrar" class="btn btnTopoProposta pull-right">Simular agora</button>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>

</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="header">
            </div>
            <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->
            <div class="main">
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" >
                            <div class="col-md-12">
                                <h2 class="title tituloDuvidas"> LIGUE PARA <span class="fw800sub">QUALQUER OPERADORA</span> E LUGAR DO BRASIL ILIMITADO! <br>
                                    <span style="font-size:20px;">(Local e DDD para celulares e Fixos com 41)</span>
                                </h2>
                                <h2 class="title tituloTimBlack">TIM BLACK EMPRESAS</h2>
																<div class="container">
                                <div class="containerbox">
																	<div class="col-md-2 box-planos" style="padding-top:25px">
																			<span class="planoDados">6 GIGA</span><br>
																			<span class="planoDadosSub">DE INTERNET 4G</span>
																	</div>

                                    <div class="col-md-1 boxcontainer" > + </div>
																		<div class="col-md-2 box-planos" style="padding-top:25px">
                                        <span class="ligacoesBlack">LIGAÇÕES ILIMITADAS</span> <br>
                                        <span class="ligacoesBlackSub">Fixo + Móvel com DDD 41</span>
                                    </div>
                                    <div class="col-md-1 boxcontainer"> + </div>
                                    <div class="col-md-2 box-planos" >
                                      <img src="assets/img/imgFaleIlimitadoTim.png" alt="Telefonia PME" >

                                    </div>
                                    <div class="col-md-1 boxcontainer"> = </div>
                                    <div class="col-md-2 box-planos-preco valorPlano">  <span class="valorPlano">R$ 69,90</span><br>
																			<span class="planoDadosSub">/mês</span><br>
                                  </div>
                                </div>

                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-lg txt">Solicitar Cotação</a>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-xs txt " style="position: relative;left: 380px;">  RECEBER PROPOSTA</a>

                                <br><br>

                                <div class="containerbox">
																	<div class="col-md-2 box-planos" style="padding-top:25px">
																			<span class="planoDados">10 GIGA</span><br>
																			<span class="planoDadosSub">DE INTERNET 4G</span>
																	</div>

                                    <div class="col-md-1 boxcontainer" > + </div>
																		<div class="col-md-2 box-planos" style="padding-top:25px" >
																				<span class="ligacoesBlack">LIGAÇÕES ILIMITADAS</span> <br>
																				<span class="ligacoesBlackSub">Fixo + Móvel com DDD 41</span>
																		</div>
                                    <div class="col-md-1 boxcontainer"> + </div>
                                    <div class="col-md-2 box-planos" >
                                      <img src="assets/img/imgFaleIlimitadoTim.png" alt="Telefonia PME" >
                                    </div>
                                    <div class="col-md-1 boxcontainer"> = </div>
																		<div class="col-md-2 box-planos-preco valorPlano">  <span class="valorPlano">R$ 99,90</span><br>
																			<span class="planoDadosSub">/mês</span> </div>
                                </div>

                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-lg txt">  RECEBER PROPOSTA</a>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-xs txt " style="position: relative;left: 380px;">  RECEBER PROPOSTA</a>

                                <br><br>

                                <div class="containerbox " >
																	<div class="col-md-2 box-planos" style="padding-top:25px">
																			<span class="planoDados">20 GIGA</span><br>
																			<span class="planoDadosSub">DE INTERNET 4G</span>
																	</div>
                                    <div class="col-md-1 boxcontainer" > + </div>
																		<div class="col-md-2 box-planos" style="padding-top:25px">
																				<span class="ligacoesBlack">LIGAÇÕES ILIMITADAS</span> <br>
																				<span class="ligacoesBlackSub">Fixo + Móvel com DDD 41</span>
																		</div>

                                    <div class="col-md-1 boxcontainer"> + </div>
                                    <div class="col-md-2 box-planos" style="border: 1px solid #fff">
                                    <img src="assets/img/imgFaleIlimitadoTim.png" alt="Telefonia PME" >
                                    </div>
                                    <div class="col-md-1 boxcontainer"> = </div>
																		<div class="col-md-2 box-planos-preco valorPlano">  <span class="valorPlano">R$ 139,90</span><br>
																			<span class="planoDadosSub">/mês</span> </div>
                                </div>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-lg txt">  RECEBER PROPOSTA</a>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-xs txt " style="position: relative;left: 380px;">  RECEBER PROPOSTA</a>

                                <br><br>
                                <div class="containerbox ">
																	<div class="col-md-2 box-planos" style="padding-top:25px">
																			<span class="planoDados">50 GIGA</span><br>
																			<span class="planoDadosSub">DE INTERNET 4G</span>
																	</div>
                                    <div class="col-md-1 boxcontainer" > + </div>
																		<div class="col-md-2 box-planos" style="padding-top:25px" >
																				<span class="ligacoesBlack">LIGAÇÕES ILIMITADAS</span> <br>
																				<span class="ligacoesBlackSub">Fixo + Móvel com DDD 41</span>
																		</div>

                                    <div class="col-md-1 boxcontainer"> + </div>
                                    <div class="col-md-2 box-planos" style="border: 1px solid #fff">
                                    <img src="assets/img/imgFaleIlimitadoTim.png" alt="Telefonia PME" >
                                    </div>
                                    <div class="col-md-1 boxcontainer"> = </div>
																		<div class="col-md-2 box-planos-preco valorPlano">  <span class="valorPlano">R$ 199,90</span><br>
																			<span class="planoDadosSub">/mês</span><Br>

                                    </div>
                                </div>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-lg txt">  RECEBER PROPOSTA</a>
                                <a href="#" class="btn btnTopoPropostaDois btn-xs hidden-xs txt " style="position: relative;left: 380px;">  RECEBER PROPOSTA</a>

                                <br><br>
                            </div>
                        </div>
											</div>

                    </div>
                </div>


	    </div>


					 <div class="container">
								<div class="features-1">
										<div class="cd-section text-center">
											<h2 class="title tituloTimFlat">TIM TARIFA FLAT EMPRESAS</h2>

											<div class="container">
											<div class="containerbox">
												<div class="col-md-2 box-planos-flat">
														<span class="planoDados-flat">4 GIGA</span><br>
														<span class="planoDados-flat-sub">DE INTERNET 4G</span>
												</div>

												<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-flat" >
															<span class="ligacoesFlat">500 MINUTOS</span> <br>
															<span class="planoDados-flat-sub">Fixo com DDD 41</span>
													</div>
													<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-flat" >
															<span class="planoDados-flat">50</span><br>
															<span class="planoDados-flat-sub">SMS</span>
													</div>
													<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-preco-flat valorPlano">  <span class="valorPlano">R$ 99,90</span><br>
														<span class="planoDadosSub">/mês</span> </div>
											</div>

											<br><br>

											<div class="containerbox">
												<div class="col-md-2 box-planos-flat">
														<span class="planoDados-flat">10 GIGA</span><br>
														<span class="planoDados-flat-sub">DE INTERNET 4G</span>
												</div>

												<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-flat" >
															<span class="ligacoesFlat">500 MINUTOS</span> <br>
															<span class="planoDados-flat-sub">Fixo com DDD 41</span>
													</div>
													<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-flat" >
															<span class="planoDados-flat">50</span><br>
															<span class="planoDados-flat-sub">SMS</span>
													</div>
													<div class="col-md-1 boxcontainer-flat" > + </div>
													<div class="col-md-2 box-planos-preco-flat valorPlano">  <span class="valorPlano">R$ 99,90</span><br>
														<span class="planoDadosSub">/mês</span> </div>
											</div>
											<br><br>

									</div>
										</div>
									</div>


								</div>

        </div>
        <!-- you can use the class main-raised if you want the main area to be as a page with shadows -->
        <div class="footer text-center">
            <div class="container">
                <div class="row padding-content ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    </div>
                </div>
            </div>
        </div>
    </body>
    <!--   Core JS Files   -->
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
    <script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/nouislider.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="assets/js/atv-img-animation.js" type="text/javascript"></script>
    <script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
    <script src="assets/js/scroll.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/additional-methods.min.js"></script>
    <script src="assets/js/api-estado-cidade.js"></script>
    <script>$(".txt").click(function(){$("#formshake").shake()});</script>
    <script type="text/javascript">/*<![CDATA[*/jQuery.fn.shake=function(d,e,f){d=d||10;e=e||4;f=f||1000;this.each(function(){$(this).css("position","relative");for(var a=1;a<=d;a++){$(this).animate({left:(e*-1)},(((f/d)/4))).animate({left:e},((f/d)/2)).animate({left:0},(((f/d)/4)))}});return this};/*]]>*/</script>
</html>
