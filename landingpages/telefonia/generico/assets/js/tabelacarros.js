   $(document).ready(function() {
       var containerEl = document.querySelector('.mixitupContainer');

       var mixer = mixitup(containerEl, {
           controls: {
               toggleLogic: 'and'
           },


           selectors: {
               control: '[data-mixitup-control]'
           },
           load: {
               filter: 'none'
           },
           animation: {
               effects: 'fade',
               duration: 200,
               nudge: false,
               reverseOut: false
           }
       });
  

   });