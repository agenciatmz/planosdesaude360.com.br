$(function() {

  $.validator.setDefaults({

      errorClass: 'form-group has-danger bmd-form-group',
    highlight: function(element) {
      $(element)
        .closest('.form-group ')
        .addClass('has-danger');
    },
      unhighlight: function(element) {
      $(element)
        .closest('.form-group')
        .removeClass('has-danger')
        .addClass('has-success');
    }

  });


  $("#contact-form").validate({
    rules: {
      tmzNomeEmpresa: {
        required: true
      },
      tmzCnpjEmpresa: {
          required: true,
          number: true,
          minlength: 14
      }

    },

    messages: {

        tmzNomeEmpresa: {
            required: 'Insira o nome da sua empresa'
        },
        tmzCnpjEmpresa: {
            required: 'Insira o CNPJ da sua empresa'
        },
        tmzNome: {
            required: 'Insira o seu nome'
        },
        tmzEmail: {
          required: 'Insira um email válido',
          email: 'E-mail <em>inválido</em>, tente novamente .',
          remote: $.validator.format("{0} is already associated with an account.")
        },
        tmzTelefone: {
            required: 'Insira o seu telefone'
        },
        estado: {
            required: 'Insira o seu Estado'
        },
        cidade: {
            required: 'Insira a sua Cidade'
        }

    },
    errorPlacement: function(error, element)
          {
              if ( element.is(":radio") )
              {
                  error.appendTo( element.parents('.containererror') );
              }
              else
              { // This is the default behavior
                  error.insertAfter( element );
              }
           }
  });

});
