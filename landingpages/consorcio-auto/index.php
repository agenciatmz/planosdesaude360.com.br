<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        require_once("includes/includesheader.php");
        require_once("../helpers/includesPixel.php");
        require_once("../helpers/includesChat.php");
         ?>

    </head>
    <body class="template-page">


        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png"  >
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>VANTAGENS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>MARCAS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12  text-center ">
                 <h1 class="title" style="font-size: 3em;"><span style="color:#fff"><span style="font-weight: 300"> O CARRO DO SEUS <Br></span> SONHOS ESTÁ AQUI! </h1>
                 <button class="learn-more" data-toggle="modal" data-target="#loginModal">
                    <div class="circle">
                    <span class="icon arrow"></span>
                    </div>
                    <p class="button-text" >COTAR AGORA</p>
                </button>
               
                                <img src="assets/img/imgConsorcioAutoHeader.png"  style="position: relative;top: 100px;" class="img-responsive d-none d-sm-block">
                                <img src="assets/img/imgConsorcioAutoHeader.png"  style="position: relative;top: 10px;" class="img-responsive d-block d-sm-none">

                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">

           <!--     *********    END FEATURES 1      *********      -->
           <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title azulclaro">VANTAGENS DO CONSÓRCIO</h2>
                            <h4 class="description">Confira as vatanges de adquirir um consórcio</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgSeguranca.png">
                                </div>
                                <h4 class="info-title">Parcelamento integral </h4>
                                <p class="description"> No consórcio, o valor do bem ou serviço é dividido integralmente na quantidade de mensalidades pré-estabelecidas em contrato entre o consorciado e a administradora.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                <img src="assets/img/imgFlexibilidade.png">
                                </div>
                                <h4 class="info-title">Flexibilidade  </h4>
                                <p class="description">   Quando for contemplado, o consorciado pode optar por adquirir qualquer bem ou serviço pertencente à categoria de seu grupo de consórcio.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgLiberdade.png">
                                </div>
                                <h4 class="info-title">Baixos custos  </h4>
                                <p class="description"> Não existem juros, você só paga taxa de administração, que é a remuneração da administradora pelos serviços prestados</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ml-auto mr-auto">
                            <h2 class="title azulclaro">PRINCIPAIS EMPRESAS DE CONSÓRCIO</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imRodobens.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                <img src="assets/img/imgEmbracon.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgDisal.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                <img src="assets/img/imgPortoSeguro.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <div class="features-1">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-8 ml-auto mr-auto">
                                      <h2 class="title azulclaro">CONFIRA NOSSAS CARTAS DE CRÉDITO</h2>
                                  </div>
                              </div>
                              <div class="row">

                                <div class="col-md-4">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Chevrolet ONIX</h6>
                                            <img src="assets/img/imgCarroOnix.png">
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 438,29</h3>
                                                
                                                <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                        </div>
                                    </div>
                                </div>


                                    <div class="col-md-4">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Chevrolet PRISMA</h6>
                                            <img src="assets/img/imgCarroPrisma.png">
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 786,16</h3>
                                                <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="card card-pricing">
                                        <div class="card-body">
                                            <h6>Chevrolet COBALT</h6>
                                            <img src="assets/img/imgCarroCobalt.png">
                                                <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 1.069,88</h3>
                                                <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                        </div>
                                    </div>
                                </div>   

                              </div>

                              <div class="row">

                             <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                <h6>Fiat UNO</h6>
                                    <img src="assets/img/imgCarroUno.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 531,76</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                <h6>Fiat MOBI</h6>
                                    <img src="assets/img/imgCarroMobi.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 580,81</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>   

                        <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                    <h6>Fiat ARGO</h6>
                                    <img src="assets/img/imgCarroArgo.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 748,96</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>

                       

                           

                            </div>
                            <div class="row">


                        <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                    <h6>volkswagen UP</h6>
                                    <img src="assets/img/imgCarroUp.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 479,65</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>

                           <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                <h6>volkswagen Gol</h6>
                                    <img src="assets/img/imgCarroGol.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 527,74</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card card-pricing">
                                <div class="card-body">
                                <h6>volkswagen Virtus</h6>
                                    <img src="assets/img/imgCarroVirtus.png">
                                        <h3 class="card-title bordavalorparcela"><span style="font-size:0.5em">PARCELAS A PARTIR DE</span> <br>R$ 720,12</h3>
                                        <a href="#" class="btn btn-success-gradiant btn-round">CONTRATAR AGORA</a>
                                </div>
                            </div>
                        </div>   
                           
                            </div>
                          </div>
                      </div>
        </div>

     
        <div class="features-2 " style="background-image: url('assets/img/bg30.png'); background-size: cover;background-position: center center;">
                <div class="col-md-8 ml-auto mr-auto text-center" style="padding-top:40px">
                    <h1 class="title" style="font-size: 30px">AINDA TEM DÚVIDAS? FAÇA UMA COTAÇÃO ONLINE AGORA MESMO</h1>
                
                    <a href="#" class="btn btn-info btn-lg"  style="background-color: #fff000;color:#000; font-weight: 500">Solicitar Cotação</a>
                </div>
            </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            VANTAGENS 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            MARCAS
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="card card-login card-plain">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>

                    </div>
                    <div class="modal-body">
                      <?php include("../helpers/insereBanco.php"); ?>
                           <?php include("includes/Form/FormNext.php");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
    
</html>
