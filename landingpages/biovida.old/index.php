<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Agência TresMeiaZero">
    <link href="assets/img/favicon.ico" rel="shortcut icon">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,500,600">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <title>Planos de Saúde Biovida</title>
		<?php require_once("../helpers/includesPixel.php"); ?>
		<?php require_once("../helpers/includesChat.php"); ?>


</head>
<body data-spy="scroll" data-target=".navbar" class="has-loading-screen">

    <div class="ts-page-wrapper" id="page-top">

        <header id="ts-hero" class="ts-full-screen" data-bg-parallax="scroll" data-bg-parallax-speed="3" >

            <nav class="navbar navbar-expand-lg navbar-dark fixed-top ts-separate-bg-element" data-bg-color="#1a1360">
                <div class="container">
                    <a class="navbar-brand" href="#page-top">
                        <img src="assets/img/Logo_BV.png" alt="" width="120">
                    </a>
                    <!--end navbar-brand-->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!--end navbar-toggler-->
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">
                            <a class="nav-item nav-link active ts-scroll" href="#page-top">Home <span class="sr-only">(current)</span></a>
                            <a class="nav-item nav-link ts-scroll" href="#how-it-works">Diferenciais</a>
                            <a class="nav-item nav-link ts-scroll" href="#our-clients">Depoimentos</a>

                            <a class="nav-item nav-link ts-scroll" href="#contact">Contato</a>

                            <a class="nav-item nav-link ts-scroll btn btn-info btn-sm text-white ml-3 px-3 ts-width__auto" href="#contact" style="margin-bottom: 10px">Cotar Agora</a>
                        </div>
                        <!--end navbar-nav-->
                    </div>
                    <!--end collapse-->
                </div>
                <!--end container-->
            </nav>
            <!--end navbar-->

            <div class="container align-self-center">
                <div class="row align-items-center text-center">
                    <div class="col-sm-12">À PARTIR DE
                        <h1><span>R$ 375,11*</span> <span class="ts-opacity__70" style="font-weight: 300; font-size: 20px">* 54 à 58 anos</span><br>
                            R$ 545,79* <span class="ts-opacity__70" style="font-weight: 300; font-size: 20px">* 59 ou mais</span></h1>

                        <h4 class="ts-opacity__70" style="font-weight: 300">Há mais de 10 anos no mercado, nossos pilares foram construídos com profissionais de grande experiência e amplo conhecimento técnico, mas principalmente com a ética e a satisfação dos clientes.
                        </h4>

                        <a href="#contact" class="btn btn-light btn-lg ts-scroll">Saiba Mais</a>
                    </div>
                    <!--end col-sm-7 col-md-7-->

                    <!--end col-sm-5 col-md-5 col-xl-5-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->

            <div class="ts-background" data-bg-image-opacity=".6" data-bg-parallax="scroll" data-bg-parallax-speed="3">
                <div class="ts-svg ts-z-index__2">
                    <img src="assets/svg/wave-static-02.svg" class="w-100 position-absolute ts-bottom__0">
                    <img src="assets/svg/wave-static-01.svg" class="w-100 position-absolute ts-bottom__0">
                </div>
                <div class="owl-carousel ts-hero-slider" data-owl-loop="1">
                    <div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="assets/img/bg-girl-01.jpg" data-bg-blend-mode="multiply"></div>
                    <!--<div class="ts-background-image ts-parallax-element" data-bg-color="#d24354" data-bg-image="assets/img/bg-girl-02.jpg" data-bg-blend-mode="multiply"></div>-->
                </div>
            </div>

        </header>
        <!--end #hero-->

        <main id="ts-content">

            <section id="how-it-works" class="ts-block text-center">
                <div class="container">
                    <div class="ts-title" style="color: #000099;">
                        <h3>DIFERENCIAIS DOS PLANOS BIOVIDA SAÚDE</h3>
                    </div>
                    <!--end ts-title-->
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <figure data-animate="ts-fadeInUp">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-watch-heart.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-01.svg"></div>
                                </figure>
                                <h4>+ de 10 anos no mercado</h4>
                                <p>
                                    Há mais de 10 anos no mercado, nossos pilares foram construídos com profissionais de grande experiência e amplo conhecimento técnico                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class="col-sm-6 col-md-4 col-xl-4">
                            <figure data-animate="ts-fadeInUp" data-delay="0.1s">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-economia.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-02.svg"></div>
                                </figure>
                                <h4>Política de Transparência</h4>
                                <p>
                                    Trabalhamos com uma política de transparência e seriedade em todos nossos processos, para que o associado sinta-se a vontade em ter como plano de saúde a Biovida Saúde                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                        <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-0 col-xl-4">
                            <figure data-animate="ts-fadeInUp" data-delay="0.2s">
                                <figure class="icon mb-5 p-2">
                                    <img src="assets/img/icon-hospital.png" alt="">
                                    <div class="ts-svg" data-animate="ts-zoomInShort" data-bg-image="assets/svg/organic-shape-03.svg"></div>
                                </figure>
                                <h4>Rede de Atendimento</h4>
                                <p>
                                    Nossa rede de atendimento é planejada para melhor atende-lo, temos um departamento que monitora a demanda de nossos clientes a fim de manter nossos credenciados clinicas e hospitais sempre adequados as nossas necessidades qualitativas e quantitativas.                                </p>
                            </figure>
                        </div>
                        <!--end col-xl-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </section>


            <section id="our-clients" class="ts-block text-center">
                <div class="container">
                    <div class="ts-title">
                        <h2>Sua vida mais Tranquila <br>é nosso maior objetivo</h2>
                    </div>
                    <!--end ts-title-->
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="owl-carousel ts-carousel-blockquote" data-owl-dots="1" data-animate="ts-zoomInShort">
                                <blockquote class="blockquote">
                                    <!--person image-->
                                    <figure>
                                        <aside>
                                            <i class="fa fa-quote-right"></i>
                                        </aside>
                                        <div class="ts-circle__lg" data-bg-image="assets/img/person-05.jpg"></div>
                                    </figure>
                                    <!--end person image-->
                                    <!--cite-->
                                    <p>
                                        Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..

                                    </p>
                                    <!--end cite-->
                                    <!--person name-->
                                    <footer class="blockquote-footer">
                                        <h4>Roberta Nobre</h4>
                                        <h6>Aposentado</h6>
                                    </footer>
                                    <!--end person name-->
                                </blockquote>
                                <!--end blockquote-->
                                <blockquote class="blockquote">
                                    <!--person image-->
                                    <figure>
                                        <aside>
                                            <i class="fa fa-quote-right"></i>
                                        </aside>
                                        <div class="ts-circle__lg" data-bg-image="assets/img/person-01.jpg"></div>
                                    </figure>
                                    <!--end person image-->
                                    <!--cite-->
                                    <p>
                                        A BIOVIDA é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata.
                                    </p>
                                    <!--end cite-->
                                    <!--person name-->
                                    <footer class="blockquote-footer">
                                        <h4>Jorge Pires</h4>
                                        <h6>Aposentada</h6>
                                    </footer>
                                    <!--end person name-->
                                </blockquote>
                                <!--end blockquote-->
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section id="partners" class="py-5 ts-block" data-bg-color="#f6f6f6">
                <!--container-->
                <div class="container">
                    <!--block of logos-->
                    <div class="d-block d-md-flex justify-content-between align-items-center text-center ts-partners " style="padding-bottom: 30px;">
                        <a href="#">
                            <img src="assets/img/logo-01.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-02.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-03.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-04.png" alt="">
                        </a>
                        <a href="#">
                            <img src="assets/img/logo-05.png" alt="">
                        </a>
                    </div>
                    <!--end logos-->
                </div>
                <!--end container-->
            </section>




        </main>

        <footer id="ts-footer">



            <section id="contact" class="ts-separate-bg-element" data-bg-image="assets/img/bg-desk.jpg" data-bg-image-opacity=".1" data-bg-color="#1b1464">
                <div class="container">
                    <div class="ts-box mb-0 p-5 ts-mt__n-10">
                        <div class="row">

													<?php include("../helpers/insereBanco.php"); ?>

													                                    <?php include("includes/Form/FormBiovida.php");?>

                        </div>
                        <!--end row-->
                    </div>
                    <!--end ts-box-->
                    <div class="text-center text-white py-4">
                        <small>© 2018 <a href="https://agenciatresmeiazero.com.br/home" target="_blank"> Agência TresMeiaZero</a>, Todos Direitos Reservados</small>
                    </div>
                </div>
                <!--end container-->
            </section>

        </footer>
        <!--end #footer-->
    </div>
    <!--end page-->

    <script>
        if( document.getElementsByClassName("ts-full-screen").length ) {
            document.getElementsByClassName("ts-full-screen")[0].style.height = window.innerHeight + "px";
        }
    </script>
		<script src="assets/js/jquery-3.3.1.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/api-estado-cidade.js"></script>
		<script src="assets/js/vidas.js"></script>
		<script src="assets/js/validation.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/js/imagesloaded.pkgd.min.js"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
		<script src="assets/js/isInViewport.jquery.js"></script>
		<script src="assets/js/jquery.particleground.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/scrolla.jquery.min.js"></script>
		<script src="assets/js/jquery.validate.min.js"></script>
		<script src="assets/js/jquery-validate.bootstrap-tooltip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
		<script src="assets/js/jquery.wavify.js"></script>
		<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>-->
		<script src="assets/js/custom.js"></script>
</body>
</html>
