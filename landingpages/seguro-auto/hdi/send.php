<?php
/*
 * A web form that both generates and uses PHPMailer code.
 * revised, updated and corrected 27/02/2013
 * by matt.sturdy@gmail.com
 */
require_once __DIR__ . '/vendor/autoload.php';

$to = 'gustavo.saraiva@agenciatmz.com.br';
$subject = 'Form Seguro Auto';
$from = 'leads@agenciatmz.com.br';

$strMarca = $_POST['strMarca'];
$strModelo = $_POST['strModelo'];
$strAno = $_POST['strAno'];
$q1 = $_POST['q1'];
$q2 = $_POST['q2'];
$q3 = $_POST['q3'];
$strNome = $_POST['strNome'];
$strEmail = $_POST['strEmail'];
$strDataNascimento = $_POST['strDataNascimento'];
$strTelefoneMovel = $_POST['strTelefoneMovel'];
$strTelefoneFixo = $_POST['strTelefoneFixo'];
$strCEP = $_POST['strCEP'];
$strCPF = $_POST['strCPF'];
$strEstado = $_POST['strEstado'];
$strCidade = $_POST['strCidade'];


$msg = '<html><body>';
$msg .= '<h1>Olá, você tem uma nova solicitação de contato!</h1>';
$msg .= '</body></html>';
$msg .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
$msg .= "<tr style='background: #eee;'><td><strong>Marca:</strong> </td><td>" . strip_tags($_POST['strMarca']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Modelo:</strong> </td><td>" . strip_tags($_POST['strModelo']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Ano:</strong> </td><td>" . strip_tags($_POST['strAno']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Possui o veículo?:</strong> </td><td>" . strip_tags($_POST['q1']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Renovação?:</strong> </td><td>" . strip_tags($_POST['q2']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Sexo:</strong> </td><td>" . strip_tags($_POST['q3']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Nome:</strong> </td><td>" . strip_tags($_POST['strNome']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>E-mail:</strong> </td><td>" . strip_tags($_POST['strEmail']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Data de nascimento:</strong> </td><td>" . strip_tags($_POST['strDataNascimento']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Celular:</strong> </td><td>" . strip_tags($_POST['strTelefoneMovel']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Fixo:</strong> </td><td>" . strip_tags($_POST['strTelefoneFixo']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>CEP:</strong> </td><td>" . strip_tags($_POST['strCEP']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Estado:</strong> </td><td>" . strip_tags($_POST['strEstado']) . "</td></tr>";
$msg .= "<tr style='background: #eee;'><td><strong>Cidade:</strong> </td><td>" . strip_tags($_POST['strCidade']) . "</td></tr>";

$msg .= "</table>";
$msg .= "</body></html>";

// Create the Transport
$transporter = Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl')
$transport
    ->setUsername('saraiva.gus@gmail.com')
    ->setPassword('449555483');

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = new Swift_Message($subject);
$message
    ->setFrom([$from => 'Agência TMZ'])
    ->setTo([$to])
    ->setBody($msg, 'text/html', 'utf-8');

$logger = new Swift_Plugins_Loggers_ArrayLogger();
$mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

// Send the message
if ($mailer->send($message))
{
    header('Location: https://terra.com.br');
    exit;
}
else
{
  header('Location: https://www.google.com.br');
  exit;
}

?>
