<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Fullscreen Form Interface</title>

        <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/lpSeguroAutoNF-form.css" />



        <script src="assets/js/modernizr.custom.js"></script>
    </head>
    <body>
        <div class="container-form">
        <div class="fs-form-wrap" id="fs-form-wrap">
        <div class="fs-title">
            <div class="codrops-top">
                <a class="codrops-icon codrops-icon-prev" onclick="history.go(-1)" href="javascript:void(0)"></a>
            </div>
            <h1><img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" class="bomcotarlogo" width="100" height="auto" alt="LOGO">Seguro Auto</h1>
            <span class="small_head">Selecionou a categoria errada? <a href="#" class="btnVoltaCategorias">Voltar</a></span>
        </div>
        <form id="myform" class="fs-form fs-form-full" autocomplete="off" method="post" action="send.php">
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="strMarca">Qual a marca do seu carro?</label>
                    <input class="fs-anim-lower" id="q1" name="strMarca" type="text" placeholder="Ex. Honda" required/> <br>
                    <label class="fs-field-label fs-anim-upper" for="strModelo">Qual o modelo?</label>
                    <input class="fs-anim-lower" id="q2" name="strModelo" type="text" placeholder="Ex. Honda Fit EX" required/> <br>
                    <label class="fs-field-label fs-anim-upper" for="strAno">Qual o ano?</label>
                    <input class="fs-anim-lower" id="q3" name="strAno" type="text" placeholder="Ex. 2009" required/>
                </li>
                <li data-input-trigger>
                    <label class="fs-field-label fs-anim-upper" for="possuiveiculo">Você já possui o veículo ?</label>
                    <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                        <span><input id="possuisim" name="q1" type="radio" value="sim" required tabindex="4"/><label class="radio-conversion" for="possuisim" >Sim</label></span>
                        <span><input id="possuinao" name="q1" type="radio" value="não" required  tabindex="5"/><label class="radio-social" for="possuinao" >Não</label></span>
                    </div>
                </li>
                <li data-input-trigger>

                        <label class="fs-field-label fs-anim-upper " for="renovacaoveiculo">Este seguro é uma renovação ?</label>
                        <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower ">
                            <span><input id="renovasim" name="q2" type="radio" value="Sim" tabindex="12" required="required"/><label class="radio-conversion" for="renovasim" >Sim</label></span>
                            <span><input id="renovanao" name="q2" type="radio" value="Não" tabindex="13" required="required"/><label class="radio-social" for="renovanao" >Não</label></span>
													</div>
                </li>
								<li data-input-trigger>
										<label class="fs-field-label fs-anim-upper" for="strSexo">Qual o seu sexo?</label>
										<div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
												<span><input id="strSexoMasc" name="q3" type="radio" value="sim" required tabindex="17"/><label class="radio-boy" for="strSexoMasc" >Masculino</label></span>
												<span><input id="strSexoFem" name="q3" type="radio" value="não" required  tabindex="18"/><label class="radio-girl" for="strSexoFem" >feminino</label></span>
										</div>
								</li>
                <li>
                <label class="fs-field-label fs-anim-upper" for="q2" data-info="Preencha seus dados pessoais">Dados pessoais</label>
                <input class="fs-anim-lower" id="strNome" name="strNome" type="text" placeholder="Nome Completo" required/><br>
                <input class="fs-anim-lower" id="strEmail" name="strEmail" type="email" placeholder="E-mail" required/><br>
                <input class="fs-anim-lower date" id="strDataNascimento" name="strDataNascimento" type="text" placeholder="Data de Nascimento" required/><br>
                <input class="fs-anim-lower phone_with_ddd" id="strTelefoneMovel" name="strTelefoneMovel" type="text" placeholder="Telefone Celular" required/><br>
                <input class="fs-anim-lower fixo_with_ddd" id="strTelefoneFixo" name="strTelefoneFixo" type="text" placeholder="Telefone Fixo"/><br>
                <input class="fs-anim-lower cep" id="strCEP" name="strCEP" type="text" placeholder="CEP" required/><br>
                <input class="fs-anim-lower cpf" id="strCPF" name="strCPF" type="text" placeholder="CPF" required/><br>
                <input class="fs-anim-lower" id="strEstado" name="strEstado" type="text" placeholder="Estado" required/><br>
                <input class="fs-anim-lower" id="strCidade" name="strCidade" type="text" placeholder="Cidade" required/><br>
                </li>
            </ol>
            <!-- /fs-fields -->
            <button class="fs-submit" type="submit">Enviar</button>
        </form>
        <!-- /fs-form -->
        </div><!-- /fs-form-wrap -->
        </div><!-- /container -->
				<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/selectFx.js"></script>
        <script src="assets/js/fullscreenForm.js"></script>
				<script src="assets/js/jquery.mask.js"></script>

        <script>
            (function() {
            	var formWrap = document.getElementById( 'fs-form-wrap' );

            	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
            		new SelectFx( el, {
            			stickyPlaceholder: false,
            			onChange: function(val){
            				document.querySelector('span.cs-placeholder').style.backgroundColor = val;
            			}
            		});
            	} );

            	new FForm( formWrap, {
            		onReview : function() {
            			classie.add( document.body, 'overview' ); // for demo purposes only
            		}
            	} );
            })();
        </script>
				<script>
						$(document).ready(function(){
						$('.date').mask('00/00/0000');
						$('.cep').mask('00000-000');
						$('.phone_with_ddd').mask('(00) 00000-0000');
						$('.fixo_with_ddd').mask('(00) 0000-0000');
						$('.cpf').mask('000.000.000-00', {reverse: true});

						});
				</script>
    </body>
</html>
