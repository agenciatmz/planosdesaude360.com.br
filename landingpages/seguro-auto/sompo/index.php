<?php
    try{

      $conexao = new PDO('mysql:host=localhost;dbname=u574922737_appv2', 'u574922737_appv2', 'hQu7e95^', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }catch(PDOException $e){
        echo 'ERROR:' . $e->getMessage();

    };


    ?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Seguro Auto</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,800|Material+Icons' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/lpSeguroAutoNF.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="assets/css/lpSeguroAutoNF-form.css" />
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-fixed-top menuTopHome" id="sectionsNav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#topo">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" alt="Seguro Auto">
                            </div>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#home" >
                            <span class="hvr-underline-from-left">Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="#vantagens">
                            <span class="hvr-underline-from-left">Vantagens</span>
                            </a>
                        </li>
                        <li>
                            <a href="#como-cotar" class="txt">
                            <span class="hvr-underline-from-left">Benefícios</span>
                            </a>
                        </li>
                      
                        <li class="button-container txt" >
                            <button class="btn btnTopoProposta btn-md" style="top: 4px">
                            <i class="material-icons">play_circle_outline</i> Iniciar cotação
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="firstStep">
            <div class="wrapper" id="topo">
                <div class="page-header" style="background-image: url('assets/img/bg12.jpg');">
                    <div class="container">
                        <?php if(isset($_GET['c'])){ ?>
                        <script type="text/javascript">
                            function sweet() {
                                swal(
                                    'Obrigado por sua cotação!',
                                    'Em breve nossos consultores entrarão em contato',
                                    'success'
                                );
                            }
                            window.onload = sweet;
                        </script>
                        <?php } elseif(isset($_GET['e'])){ ?>
                        <script type="text/javascript">
                            function sweet() {
                                swal(
                                    '<?php echo $mensagensAlerta ?>',
                                    'Tente novamente',
                                    'error'
                                );
                            }
                            window.onload = sweet;
                        </script>
                        <?php }?>
                        <div class="row" id="home">
                            <div class="col-md-8">
                                <h2 class="tituloHeaderM text-left" style="padding-top:20px">
                                seguro auto fácil, rápido e seguro.<br>
                                <h1 class="tituloHeader  text-left">Com a SEGURO AUTO você fica protegido em apenas alguns passos!</h1>
                                <h2 class="tituloHeaderMS text-left" style="padding-top:20px">Dirija tranquilo. Cote agora e descubra como é fácil ficar protegido. com a seguro auto você cota em  minutos o seu seguro auto.<br>
                                    <button class="btn btnIniciarCotacao btn-lg " >  <i class="material-icons">play_circle_outline</i> Iniciar cotação</button>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="tituloHeaderMid  text-center">Conheça a Sompo Seguros</h1>
                            <p class="text-justify descricaoSeguradorasM">A Sompo Seguros é uma empresa do Grupo Sompo Holdings, um dos maiores grupos seguradores do mundo, fundado no Japão há mais de 128 anos.</p>
                            <p class="text-justify descricaoSeguradorasM">No Brasil, a Sompo nasceu da integração das operações da Marítima Seguros, companhia fundada em Santos em 1943, e da Yasuda Seguros, que está no Brasil desde 1959. Reunimos o conhecimento de uma empresa local e a experiência de uma empresa global.</p>
                            <div class="features-1">
                                <div class="cd-section text-center" id="vantagens">
                                    <div class="row">
                                        <div class="col-lg-3 box-comocotar">
                                            <img src="assets/img/imgComoCotar01.png" alt="Seguro Auto"><br>
                                            <h4>Mais de 125 anos<br> de história </h4>
                                        </div>
                                        <div class="col-lg-3 box-comocotar">
                                            <img src="assets/img/imgComoCotar02.png" alt="Seguro Auto"><br>
                                            <h4>Mais de 20 milhões de clientes </h4>
                                        </div>
                                        <div class="col-lg-3 box-comocotar">
                                            <img src="assets/img/imgComoCotar03.png" alt="Seguro Auto"><br>
                                            <h4>Assistência <br>24 horas </h4>
                                        </div>
                                        <div class="col-lg-3 box-comocotar">
                                            <img src="assets/img/imgComoCotar04.png" alt="Seguro Auto"><br>
                                            <h4>Coberturas e pacotes exclusivos</h4>
                                        </div>
                                        <p class="text-justify descricaoSeguradorasM">A Sompo foi fundada com o intuito de humanizar a relação entre o seguro e o segurado, oferecendo um plano de estudo frente às melhores técnicas securitárias, possibilitando que cada cliente se sinta único.</p>
                                        <p class="text-justify descricaoSeguradorasM">Em 32 países, o Grupo Sompo Holdings reúne 50 mil colaboradores empenhados em garantir que os mais de 20 milhões de clientes estejam sempre bem. No Brasil, contamos com 2 mil colaboradores e filiais localizadas em todas as regiões para oferecer solidez e excelência.</p>
<br>

                                        <button class="btn btnIniciarCotacaoMid btn-lg pull-center" >  <i class="material-icons">play_circle_outline</i> Iniciar cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                </div>
            </div>
            <div class="main-white">
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" id="como-cotar">
                            <div class="col-md-12">
                                <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> Confira alguns dos benefícios e vantagens</h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 box-comocotar">
                                    <img src="assets/img/imgCobertura01.png" alt="Seguro Auto"><br>
                                    <h4 style="color:#fff">Cobertura contra Danos </h4>
                                    <p class="text-justify descricaoSeguradoras">Cobre dano parcial ou integral ao automóvel por colisão, incêndio ou roubo/furto</p>
                                </div>
                                <div class="col-lg-3 box-comocotar">
                                    <img src="assets/img/imgCobertura02.png" alt="Seguro Auto"><br>
                                    <h4 style="color:#fff">Cobertura Roubo e Furto </h4>
                                    <p class="text-justify descricaoSeguradoras">Caso seu veículo seja furtado ou roubado, você recebe indenização conforme contratação.</p>
                                </div>
                                <div class="col-lg-3 box-comocotar">
                                    <img src="assets/img/imgCobertura03.png" alt="Seguro Auto"><br>
                                    <h4 style="color:#fff">Cobertura para Terceiros </h4>
                                    <p class="text-justify descricaoSeguradoras">Indenização em caso de danos materiais ou corporais sofridos por terceiros.</p>
                                </div>
                                <div class="col-lg-3 box-comocotar">
                                    <img src="assets/img/imgCobertura04.png" alt="Seguro Auto"><br>
                                    <h4 style="color:#fff">Cobertura para Passageiros </h4>
                                    <p class="text-justify descricaoSeguradoras">Indenização em caso de danos materiais ou corporais sofridos por terceiros.</p>
                                </div>
                                <button class="btn btnIniciarCotacao btn-lg pull-center" >  <i class="material-icons">play_circle_outline</i> Iniciar cotação</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="secondStep hide">
            <nav class="navbar navbar-primary navbar-fixed-top " id="sectionsNav">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a href="#topo">
                            <div class="logo-container">
                                <div class="logo">
                                    <img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" alt="Seguro Auto">
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="button-container txt" >
                                <button onclick="history.go(0)" class="btn btnTopoProposta btn-md  " style="top: 4px">
                                <i class="material-icons">refresh</i> Voltar
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="wrapper" id="topo">
                <div class="page-header">
                    <div class="container">
                        <div class="features-1">
                            <div class="cd-section text-center" id="vantagens">
                                <div class="col-md-12">
                                    <h2 class="tituloform"> SELECIONE O <span class="fw800sub">TIPO DO VEÍCULO</span> QUE VOCÊ DESEJA COTAR   </h2>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 box-form-seleciona-veiculo">
                                        <img src="assets/img/imgEscolhaCotacaoCarro.png" alt="Seguro Auto"><br>
                                        <p class="text-center descricaoSeguradoras">Quero cotar para o meu carro</p>
                                        <button class="btn btnTopoProposta btn-md btnIniciaCotacaoCarro" id="botaoCarro" style="top: 4px">
                                        <i class="material-icons">play_circle_outline</i> Iniciar cotação
                                        </button>
                                    </div>
                                    <div class="col-lg-4 box-form-seleciona-veiculo">
                                        <img src="assets/img/imgEscolhaCotacaoMoto.png" alt="Seguro Auto"><br>
                                        <p class="text-center descricaoSeguradoras">Quero cotar para a minha moto</p>
                                        <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                                        <i class="material-icons">play_circle_outline</i> Iniciar cotação
                                        </button>
                                    </div>
                                    <div class="col-lg-4 box-form-seleciona-veiculo">
                                        <img src="assets/img/imgEscolhaCotacaoOutros.png" alt="Seguro Auto"><br>
                                        <p class="text-center descricaoSeguradoras">Tenho outro tipo de veículo</p>
                                        <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                                        <i class="material-icons">play_circle_outline</i> Iniciar cotação
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="thirdStep hide">
            <div class="container-form">
                <div class="fs-form-wrap" id="fs-form-wrap">
                    <div class="fs-title">
                        <div class="codrops-top">
                            <a class="codrops-icon codrops-icon-prev btnVoltaCategoria" href="javascript:void(0)"></a>
                        </div>
                        <h1><img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" class="bomcotarlogo" width="100" height="auto" alt="LOGO">Seguro Auto</h1>
                        <span class="small_head">Selecionou a categoria errada? <a href="#" class="btnVoltaCategorias btnVoltaCategoria">Voltar</a></span>
                    </div>
                    <?php
                        if(isset($_POST['cadastrar'])){
                            $strMarca            = trim(strip_tags($_POST['strMarca']));
                            $strModelo           = trim(strip_tags($_POST['strModelo']));
                            $strAno              = trim(strip_tags($_POST['strAno']));
                            $q1                  = trim(strip_tags($_POST['q1']));
                            $q2                  = trim(strip_tags($_POST['q2']));
                            $q3                  = trim(strip_tags($_POST['q3']));
                            $strNome             = trim(strip_tags($_POST['strNome']));
                            $strEmail            = trim(strip_tags($_POST['strEmail']));
                            $strDataNascimento   = trim(strip_tags($_POST['strDataNascimento']));
                            $strTelefoneMovel    = trim(strip_tags($_POST['strTelefoneMovel']));
                            $strTelefoneFixo     = trim(strip_tags($_POST['strTelefoneFixo']));
                            $strCEP              = trim(strip_tags($_POST['strCEP']));
                            $strCPF              = trim(strip_tags($_POST['strCPF']));
                            $strEstado           = trim(strip_tags($_POST['strEstado']));
                            $strCidade           = trim(strip_tags($_POST['strCidade']));
                            $strSeguradora       = trim(strip_tags($_POST['strSeguradora']));

                            $insert = "INSERT INTO tmzleadsauto ( strMarca, strModelo, strAno, q1, q2, q3, strNome, strEmail, strDataNascimento, strTelefoneMovel, strTelefoneFixo, strCEP, strCPF, strEstado, strCidade, strSeguradora )
                            VALUES ( :strMarca, :strModelo, :strAno, :q1, :q2, :q3, :strNome, :strEmail, :strDataNascimento, :strTelefoneMovel, :strTelefoneFixo, :strCEP, :strCPF, :strEstado, :strCidade, :strSeguradora )";
                            try{

                                $result = $conexao->prepare($insert);
                                $result->bindParam(':strMarca', $strMarca, PDO::PARAM_STR);
                                $result->bindParam(':strModelo', $strModelo, PDO::PARAM_STR);
                                $result->bindParam(':strAno', $strAno, PDO::PARAM_STR);
                                $result->bindParam(':q1', $q1, PDO::PARAM_STR);
                                $result->bindParam(':q2', $q2, PDO::PARAM_STR);
                                $result->bindParam(':q3', $q3, PDO::PARAM_STR);
                                $result->bindParam(':strNome', $strNome, PDO::PARAM_STR);
                                $result->bindParam(':strEmail', $strEmail, PDO::PARAM_STR);
                                $result->bindParam(':strDataNascimento', $strDataNascimento, PDO::PARAM_STR);
                                $result->bindParam(':strTelefoneMovel', $strTelefoneMovel, PDO::PARAM_STR);
                                $result->bindParam(':strTelefoneFixo', $strTelefoneFixo, PDO::PARAM_STR);
                                $result->bindParam(':strCEP', $strCEP, PDO::PARAM_STR);
                                $result->bindParam(':strCPF', $strCPF, PDO::PARAM_STR);
                                $result->bindParam(':strEstado', $strEstado, PDO::PARAM_STR);
                                $result->bindParam(':strCidade', $strCidade, PDO::PARAM_STR);
                                $result->bindParam(':strSeguradora', $strSeguradora, PDO::PARAM_STR);
                                $result->execute();
                                $contar = $result->rowCount();
                                if($contar>0){

                                    {
                                        $msgClientesSucesso = '
                                        <script type="text/javascript">
                                        window.location = "https://landingpages.planosdesaude360.com.br/seguro-auto/sompo/index.php?c=sim";
                                        </script>';

                                    }
                                }else{
                                    $msgClientesErro = '  <script type="text/javascript">
                                      window.location = "https://landingpages.planosdesaude360.com.br/seguro-auto/sompo/index.php?e=sim";
                                      </script>';
                                }
                            }catch(PDOException $e){
                                echo $e;
                            }

                        }else {
                            $msg[] = "Desculpe! Ocorreu um erro...";
                        }
                        ?>
                    <form id="myform" class="fs-form fs-form-full" autocomplete="off" method="post" action="" enctype="multipart/form-data">
                        <ol class="fs-fields">
                            <li>
                                <?php echo $msgClientesSucesso; ?>
                                <?php echo $msgClientesErro; ?>
                                <?php echo $e; ?>
                                <label class="fs-field-label fs-anim-upper" for="strMarca">Qual a marca do seu carro?</label>
                                <input class="fs-anim-lower" id="q1" name="strMarca" type="text" placeholder="Ex. Honda" required/> <br>
                                <label class="fs-field-label fs-anim-upper" for="strModelo">Qual o modelo?</label>
                                <input class="fs-anim-lower" id="q2" name="strModelo" type="text" placeholder="Ex. Honda Fit EX" required/> <br>
                                <label class="fs-field-label fs-anim-upper" for="strAno">Qual o ano?</label>
                                <input class="fs-anim-lower" id="q3" name="strAno" type="text" placeholder="Ex. 2009" required/>
                            </li>
                            <li data-input-trigger>
                                <label class="fs-field-label fs-anim-upper" for="possuiveiculo">Você já possui o veículo ?</label>
                                <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                                    <span><input id="possuisim" name="q1" type="radio" value="Sim" required tabindex="4"/><label class="radio-conversion" for="possuisim" >Sim</label></span>
                                    <span><input id="possuinao" name="q1" type="radio" value="Não" required  tabindex="5"/><label class="radio-social" for="possuinao" >Não</label></span>
                                </div>
                            </li>
                            <li data-input-trigger>
                                <label class="fs-field-label fs-anim-upper " for="renovacaoveiculo">Este seguro é uma renovação ?</label>
                                <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower ">
                                    <span><input id="renovasim" name="q2" type="radio" value="Sim" tabindex="12" required="required"/><label class="radio-conversion" for="renovasim" >Sim</label></span>
                                    <span><input id="renovanao" name="q2" type="radio" value="Não" tabindex="13" required="required"/><label class="radio-social" for="renovanao" >Não</label></span>
                                </div>
                            </li>
                            <li data-input-trigger>
                                <label class="fs-field-label fs-anim-upper" for="strSexo">Qual o seu sexo?</label>
                                <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                                    <span><input id="strSexoMasc" name="q3" type="radio" value="Masculino" required tabindex="17"/><label class="radio-boy" for="strSexoMasc" >Masculino</label></span>
                                    <span><input id="strSexoFem" name="q3" type="radio" value="Feminino" required  tabindex="18"/><label class="radio-girl" for="strSexoFem" >feminino</label></span>
                                </div>
                            </li>
                            <li>
                                <label class="fs-field-label fs-anim-upper" for="q2" data-info="Preencha seus dados pessoais">Dados pessoais</label>
                                <input class="fs-anim-lower" id="strNome" name="strNome" type="text" placeholder="Nome Completo" required/><br>
                                <input class="fs-anim-lower" id="strEmail" name="strEmail" type="email" placeholder="E-mail" required/><br>
                                <input class="fs-anim-lower date" id="strDataNascimento" name="strDataNascimento" type="text" placeholder="Data de Nascimento" required/><br>
                                <input class="fs-anim-lower phone_with_ddd" id="strTelefoneMovel" name="strTelefoneMovel" type="text" placeholder="Telefone Celular" required/><br>
                                <input class="fs-anim-lower fixo_with_ddd" id="strTelefoneFixo" name="strTelefoneFixo" type="text" placeholder="Telefone Fixo"/><br>
                                <input class="fs-anim-lower cep" id="strCEP" name="strCEP" type="text" placeholder="CEP" required/><br>
                                <input class="fs-anim-lower cpf" id="strCPF" name="strCPF" type="text" placeholder="CPF" required/><br>
                                <input class="fs-anim-lower" id="strEstado" name="strEstado" type="text" placeholder="Estado" required/><br>
                                <input class="fs-anim-lower" id="strCidade" name="strCidade" type="text" placeholder="Cidade" required/><br>
                                <input class="fs-anim-lower hidden" id="strSeguradora" name="strSeguradora" type="text" placeholder="Sompo" value="Sompo"/><br>
                            </li>
                        </ol>
                        <!-- /fs-fields -->
                        <button class="fs-submit" type="submit" id="cadastrar" name="cadastrar">Enviar</button>
                    </form>
                    <!-- /fs-form -->
                </div>
                <!-- /fs-form-wrap -->
            </div>
            <!-- /container -->
        </div>
        <!-- <div class="footer text-center">
            <div class="container">
                <div class="row padding-content ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <input type="checkbox" name="optionsCheckboxes" checked> <span style="color: #808186; font-size: 10px; padding-top:20px; padding-bottom: 10px">Ao informar os seus dados nesta página, você concorda que a BomCotar, possa utilizá-los para o envio de ofertas de produtos e serviços de parceiros, comprometendo-se a mesma a resguardar e proteger a privacidade destas informações.</span>
                    </div>
                </div>
            </div>
            </div> -->
    </body>
    <script src="assets/js/modernizr.custom.js"></script>
    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/selectFx.js"></script>
    <script src="assets/js/fullscreenForm.js"></script>
    <script src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/nouislider.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="assets/js/atv-img-animation.js" type="text/javascript"></script>
    <script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
    <script src="assets/js/scroll.js"></script>
    <script>
        (function() {
          var formWrap = document.getElementById( 'fs-form-wrap' );

          [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
            new SelectFx( el, {
              stickyPlaceholder: false,
              onChange: function(val){
                document.querySelector('span.cs-placeholder').style.backgroundColor = val;
              }
            });
          } );

          new FForm( formWrap, {
            onReview : function() {
              classie.add( document.body, 'overview' ); // for demo purposes only
            }
          } );
        })();
    </script>
    <script>
        $(document).ready(function(){
        $('.date').mask('00/00/0000');
        $('.cep').mask('00000-000');
        $('.phone_with_ddd').mask('(00) 00000-0000');
        $('.fixo_with_ddd').mask('(00) 0000-0000');
        $('.cpf').mask('000.000.000-00', {reverse: true});

        });
    </script>
    <script>
        $(document).ready(function() {

            $('.btnTopoProposta').click(function(){
              $('.firstStep').addClass('hide');
              $('.secondStep').addClass('hide');
              $('.thirdStep').removeClass('hide');
              $('.menuTopHome').addClass('hide');
            });
            $('.btnIniciarCotacao').click(function(){
              $('.firstStep').addClass('hide');
              $('.secondStep').addClass('hide');
              $('.thirdStep').removeClass('hide');
              $('.menuTopHome').addClass('hide');
            });
            $('.btnIniciaCotacaoCarro').click(function(){
              $('.firstStep').addClass('hide');
              $('.secondStep').addClass('hide');
              $('.thirdStep').removeClass('hide');
            });
            $('.btnVoltaCategoria').click(function(){
              $('.firstStep').removeClass('hide');
              $('.secondStep').addClass('hide');
              $('.thirdStep').addClass('hide');
                $('.menuTopHome').removeClass('hide');
            });
        });
    </script>
</html>
