
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Seguro Auto - BomCotar</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,800|Material+Icons' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css" />

        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/lpSeguroAutoNF.css" rel="stylesheet"/>
        <link href="assets/css/jfade/animsition.css" rel="stylesheet"/>
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-fixed-top" id="sectionsNav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#topo">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/imgLogoSeguroVeiculoBomCotar.png" alt="BomCotar">
                            </div>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="button-container txt" >
                            <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                              <i class="material-icons">refresh</i> Voltar
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="animsition firstStep">
        <div class="wrapper" id="topo">
            <div class="page-header">
                <div class="container">


                  <div class="features-1">
                      <div class="cd-section text-center" id="vantagens">
                          <div class="col-md-12">
                              <h2 class="tituloform"> SELECIONE O <span class="fw800sub">TIPO DO VEÍCULO</span> QUE VOCÊ DESEJA COTAR   </h2>

                          </div>
                          <div class="row">
                          <div class="col-lg-4 box-form-seleciona-veiculo">
                            <img src="assets/img/imgEscolhaCotacaoCarro.png" alt="BomCotar"><br>
                            <p class="text-center descricaoSeguradoras">Quero cotar para o meu carro</p>
                            <button class="btn btnTopoProposta btn-md" id="botaoCarro" style="top: 4px">
                              <i class="material-icons">play_circle_outline</i> Iniciar cotação
                            </button>
                          </div>
                          <div class="col-lg-4 box-form-seleciona-veiculo">
                            <img src="assets/img/imgEscolhaCotacaoMoto.png" alt="BomCotar"><br>
                            <p class="text-center descricaoSeguradoras">Quero cotar para a minha moto</p>
                            <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                              <i class="material-icons">play_circle_outline</i> Iniciar cotação
                            </button>
                          </div>
                          <div class="col-lg-4 box-form-seleciona-veiculo">
                            <img src="assets/img/imgEscolhaCotacaoOutros.png" alt="BomCotar"><br>
                            <p class="text-center descricaoSeguradoras">Tenho outro tipo de veículo</p>
                            <button class="btn btnTopoProposta btn-md  " style="top: 4px">
                              <i class="material-icons">play_circle_outline</i> Iniciar cotação
                            </button>
                          </div>

                        </div>
                      </div>
                  </div>

                </div>
            </div>
        </div>
</div>
    </body>

    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
    <script src="assets/js/animsition.js"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/nouislider.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="assets/js/atv-img-animation.js" type="text/javascript"></script>
    <script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
    <script src="assets/js/scroll.js"></script>
    <script> $(document).ready(function() {
  $(".animsition").animsition({
    inClass: 'fade-in',
    outClass: 'fade-out',
    inDuration: 1500,
    outDuration: 800,
    linkElement: '.animsition-link',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });
});</script>
</html>
