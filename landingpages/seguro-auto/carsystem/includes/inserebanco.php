<?php
    if(isset($_POST['cadastrar'])){
        $nome            = trim(strip_tags($_POST['nome']));
        $email           = trim(strip_tags($_POST['email']));
        $telefone         = trim(strip_tags($_POST['telefone']));
        $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
        $possuicnpj      = trim(strip_tags($_POST['tipocarro']));

        $insert = "INSERT INTO tmzleadscar ( nome, email, telefone, telefoneAlternativo, tipocarro ) VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :tipocarro )";
        try{

            $result = $conexao->prepare($insert);
            $result->bindParam(':nome', $nome, PDO::PARAM_STR);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
            $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
            $result->bindParam(':tipocarro', $possuicnpj, PDO::PARAM_STR);
            $result->execute();
            $contar = $result->rowCount();
            if($contar>0){

                {
                    $msgClientesSucesso = '
                            <script type="text/javascript">
                            window.location = "http://landingpages.planosdesaude360.com.br/carsystem/index.php?c=sim";
                            </script>';

                }
            }else{
                $msgClientesErro = '<script type="text/javascript">
                window.location = "http://landingpages.planosdesaude360.com.br/carsystem/index.php?e=sim";
                </script>';
            }
        }catch(PDOException $e){
            echo $e;
        }

    }else {
        $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
    }
    ?>
