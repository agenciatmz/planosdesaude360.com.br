<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
  <div class="card-body text-left ">
  <div class="row">
      <div class="col-md-12">
        <?php echo $msgClientesSucesso; ?>
        <?php echo $msgClientesErro; ?>
        <?php echo $e; ?>

      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Nome</label>
              <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="nome" id="nome" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>E-mail</label>
              <input type="email" class="form-control" placeholder="Insira seu E-mail..." name="email" id="email" required>
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Celular</label>
              <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group">
              <label>Telefone Alternativo</label>
              <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
      </div>
      <div class="col-md-6 hidden">

<div class="input-group">
<input  type="text" class="form-control" value="Carro" name="tipocarro" id="tipocarro">
</div>

      </div>
  </div>

  <div class="row">
            <div class="col-md-12">
                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-warning btn-lg pull-right">Solicitar Cotação</button>
            </div>
        </div>
</div>

</form>
