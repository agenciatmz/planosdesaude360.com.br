<?php
    try{
    
      $conexao = new PDO('mysql:host=localhost;dbname=u574922737_appv2', 'u574922737_appv2', 'hQu7e95^', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conexao ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }catch(PDOException $e){
        echo 'ERROR:' . $e->getMessage();
    
    };
    
    ?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Seguro Auto - CarSystem</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!--     Fonts and icons     -->
        <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,800|Material+Icons' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/lpSeguroAutoNF.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="assets/css/lpSeguroAutoNF-form.css" />
    </head>
    <body>
        <nav class="navbar navbar-primary navbar-fixed-top menuTopHome" id="sectionsNav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="#topo">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="assets/img/imgLogoSeguroVeiculoCarsystem.png" alt="CarSystem">
                            </div>
                        </div>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#proteja" >
                            <span class="hvr-underline-from-left">Proteja-se</span>
                            </a>
                        </li>
                        <li>
                            <a href="#exclusividades">
                            <span class="hvr-underline-from-left">Exclusividades</span>
                            </a>
                        </li>
                        <li>
                            <a href="#vantagens" class="txt">
                            <span class="hvr-underline-from-left">Vantagens</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="firstStep">
        <div class="wrapper" id="topo">
            <div class="page-header" style="background-image: url('assets/img/bg12.jpg');">
                <div class="container">
                    <?php if(isset($_GET['c'])){ ?>
                    <script type="text/javascript">
                        function sweet() {
                            swal(
                                'Obrigado por sua cotação!',
                                'Em breve nossos consultores entrarão em contato',
                                'success'
                            );
                        }
                        window.onload = sweet;
                    </script>
                    <?php } elseif(isset($_GET['e'])){ ?>
                    <script type="text/javascript">
                        function sweet() {
                            swal(
                                '<?php echo $mensagensAlerta ?>',
                                'Tente novamente',
                                'error'
                            );
                        }
                        window.onload = sweet;
                    </script>
                    <?php }?>
                    <div class="row" style="position: relative;top: 100px;">
                        <div class="col-md-7" style="position: relative; top:   0px">
                            <h1 class="tituloHeader text-left">FIQUE PROTEGIDO COM O MELHOR<span class="fw800">RASTREADOR</span> DO MERCADO</h1>
                            <h2 class="tituloHeaderM text-left" style="padding-top:20px">Proteja-se agora <span class="fw800-iso">e fique tranquilo</span> <br>
                                <button class="btn btnIniciarCotacao btn-lg botaoshake" >  <i class="material-icons">play_circle_outline</i> Cotar agora</button>
                            </h2>
                            <div class="text-center" style="position: relative;">
                                <img src="assets/img/imgHeaderCarrosMobile.png" alt="Carsystem" class="hidden-lg hidden-sm hidden-md img-responsive" >
                            </div>
                        </div>
                        <div class="col-md-5" style="position: relative; top: 80px">
                            <?php
                                if(isset($_POST['cadastrar'])){
                                    $nome            = trim(strip_tags($_POST['nome']));
                                    $email           = trim(strip_tags($_POST['email']));
                                    $telefone         = trim(strip_tags($_POST['telefone']));
                                    $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
                                    $tipocarro      = trim(strip_tags($_POST['tipocarro']));
                                
                                    $insert = "INSERT INTO tmzleadscar ( nome, email, telefone, telefoneAlternativo, tipocarro ) VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :tipocarro )";
                                    try{
                                
                                        $result = $conexao->prepare($insert);
                                        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
                                        $result->bindParam(':email', $email, PDO::PARAM_STR);
                                        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
                                        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
                                        $result->bindParam(':tipocarro', $tipocarro, PDO::PARAM_STR);
                                        $result->execute();
                                        $contar = $result->rowCount();
                                        if($contar>0){
                                
                                            {
                                                $msgClientesSucesso = '
                                                        <script type="text/javascript">
                                                        window.location = "http://landingpages.planosdesaude360.com.br/carsystem/index.php?c=sim";
                                                        </script>';
                                
                                            }
                                        }else{
                                            $msgClientesErro = '<script type="text/javascript">
                                            window.location = "http://landingpages.planosdesaude360.com.br/carsystem/index.php?e=sim";
                                            </script>';
                                        }
                                    }catch(PDOException $e){
                                        echo $e;
                                    }
                                
                                }else {
                                    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
                                }
                                ?>
                            <div class="card card-contact card-raised formshake">
                                <form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #fff">
                                    <div class="card-body text-left ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-info">
                                                    <div class="alert-icon">
                                                        <i class="material-icons">info_outline</i>
                                                    </div>
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                                    </button>
                                                    <b>Aviso:</b> Cote agora e ganhe 20% de desconto
                                                </div>
                                                <?php echo $msgClientesSucesso; ?>
                                                <?php echo $msgClientesErro; ?>
                                                <?php echo $e; ?>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nome" style="color:#000" >Nome</label>
                                                    <input type="text" class="form-control" placeholder="Insira seu nome" aria-label="Insira seu nome" name="nome" id="nome" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="color:#000">E-mail</label>
                                                    <input type="email" class="form-control" placeholder="Insira seu e-mail" name="email" id="email" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="color:#000">Celular</label>
                                                    <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="color:#000">Telefone Alternativo</label>
                                                    <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="color:#000">Tipo do Veículo</label>
                                                    <select class="selectpicker" data-style="btn btn-round btn-simple" title="Insira uma opção"  name="tipocarro" id="tipocarro" required>
                                                        <option selected disabled>Selecione</option>
                                                        <option value="Carro">Carro</option>
                                                        <option value="Moto">Moto</option>
                                                        <option value="Caminhão">Caminhão</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-success btn-lg pull-right">Solicitar Cotação</button>
                                            </div>
                                        </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main" >
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" id="proteja">
                            <div class="col-md-12">
                                <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> Você sabia que o roubo só aumenta? <span class="fw800sub">PROTEJA-SE AGORA </span></h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="assets/img/imgSeguradorasItau.png" alt="CarSystem"><br>
                                    <p class="text-center descricaoSeguradoras">Mais de um veículo é roubado por minuto no Brasil!</p>
                                    <a href="#" class="btn btn-1 btn-xs  botaoshake" id="botaoCarro" style="color:#fff">
                                        COTAR AGORA
                                        <div class="ripple-container"></div>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <img src="assets/img/imgSeguradorasAzul.png" alt="CarSystem">
                                    <p class="text-center descricaoSeguradoras">Temos um dos mais altos índices de recuperação do mercado.</p>
                                    <a href="#"  class="btn btn-2 btn-xs botaoshake" id="botaoCarro" style="color:#fff">
                                        COTAR AGORA
                                        <div class="ripple-container"></div>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <img src="assets/img/imgSeguradorasPorto.png" alt="CarSystem">
                                    <p class="text-center descricaoSeguradoras">Em 2017 a média de roubo de motos subiu para 107 por dia!</p>
                                    <a href="#"  class="btn btn-3 btn-xs botaoshake" id="botaoCarro" style="color:#fff">
                                        COTAR AGORA
                                        <div class="ripple-container"></div>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <img src="assets/img/imgSeguradorasGenerali.png" alt="CarSystem">
                                    <p class="text-center descricaoSeguradoras">O roubo de um carro demora menos do que 3 minutos para acontecer.</p>
                                    <a href="#"  class="btn btn-4 btn-xs botaoshake" id="botaoCarro" style="color:#fff">
                                        COTAR AGORA
                                        <div class="ripple-container"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-white">
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" id="exclusividades">
                            <div class="col-md-12">
                                <h2 class="title tituloSeguradoras"> CONHEÇA AS EXCLUSIVIDADES DA <span class="fw800sub">CARSYSTEM </span></h2>
                                <h3 class="tituloSeguradorasSub"> Saiba porque é tão   <span class="fw800sub">fácil e rápido </span>cotar conosco.</h3>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 box-vantagens" style="padding-bottom: 50px;">
                                    <img src="assets/img/imgVantagensAgilidade.png" alt="CarSystem"><br>
                                    <p class="text-center descricaoSeguradoras">APP EXCLUSIVO</p>
                                    <p class="text-center descricaoSeguradoras">Rastreie seu veículo na palma da sua mão, fácil e rápido!</p>
                                </div>
                                <div class="col-lg-3 box-vantagens" style="padding-bottom: 50px;">
                                    <img src="assets/img/imgVantagensOpcoes.png" alt="CarSystem"><br>
                                    <p class="text-center descricaoSeguradoras">PLANO PLUS</p>
                                    <p class="text-center descricaoSeguradoras">Receba em caso de roubo sem recuperação*! </p>
                                </div>
                                <div class="col-lg-3 box-vantagens">
                                    <img src="assets/img/imgVantagensCoberturas.png" alt="CarSystem"><br>
                                    <p class="text-center descricaoSeguradoras">AMPLA ACEITAÇÃO</p>
                                    <p class="text-center descricaoSeguradoras">Aceitamos Taxi, Uber, Veículos Sinistrados<Br><Br></p>
                                </div>
                                <div class="col-lg-3 box-vantagens">
                                    <img src="assets/img/imgVantagensPersonalizada.png" alt="CarSystem"><br>
                                    <p class="text-center descricaoSeguradoras">ASSISTÊNCIA 24H</p>
                                    <p class="text-center descricaoSeguradoras">Não fique na mão com nossos serviços de Guincho, Chaveiro e Troca de pneu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="features-1">
                    <div class="cd-section text-center" id="vantagens">
                        <div class="col-md-12">
                            <h2 class="title tituloSeguradoras" style="padding-bottom:20px"> CONFIRA AS VANTAGENS DA<span class="fw800sub"> CARSYSTEM </span></h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 box-comocotar">
                                <img src="assets/img/imgComoCotar01.png" alt="CarSystem"><br>
                                <h4 style="text-transform: uppercase">Sem análise<br>de perfil </h4>
                            </div>
                            <div class="col-lg-3 box-comocotar">
                                <img src="assets/img/imgComoCotar01.png" alt="CarSystem"><br>
                                <h4 style="text-transform: uppercase">Para todos os<br> tipos de veículos </h4>
                            </div>
                            <div class="col-lg-3 box-comocotar">
                                <img src="assets/img/imgComoCotar01.png" alt="CarSystem"><br>
                                <h4 style="text-transform: uppercase">Funciona em<br> todo o Brasil </h4>
                            </div>
                            <div class="col-lg-3 box-comocotar">
                                <img src="assets/img/imgComoCotar01.png" alt="CarSystem"><br>
                                <h4 style="text-transform: uppercase">Rastreie pelo<br> seu celular </h4>
                            </div>
                        </div>
                        <div class="alert alert-info">
                            <div class="alert-icon">
                                <i class="material-icons">info_outline</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <b>Gostou do que viu?</b> Cote agora e ganhe 20% de desconto <a href="#" class="botaoshake" id="botaoCarro" style="color:yellow; font-weight: 600">
                                        COTAR AGORA
                                        <div class="ripple-container"></div>
                                    </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-footer">
                <div class="container">
                    <div class="features-1">
                        <div class="cd-section text-center" id="exclusividades">
                            <div class="col-md-12">
                                <h2 class="title tituloSeguradoras"> FIQUE PROTEGIDO AGORA MESMO COM A <span class="fw800sub">CARSYSTEM </span></h2>
                                <h3 class="tituloSeguradorasSub"> Cote agora mesmo, é  <span class="fw800sub">fácil e rápido </span>cotar conosco.</h3>
                            </div>
                            <div class="col-md-6">          
                                <img src="assets/img/personagem-carsystem.png" alt="CarSystem" style="padding-left: 20px;" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <div class="card card-contact card-raised formshake">
                                    <form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #fff">
                                        <div class="card-body text-left ">
                                            <div class="row">
                                                <div class="col-md-12">
                                                <div class="alert alert-info">
                                                    <div class="alert-icon">
                                                        <i class="material-icons">info_outline</i>
                                                    </div>
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                                    </button>
                                                    <b>Aviso:</b> Cote agora e ganhe 20% de desconto
                                                </div>
                                                    <?php echo $msgClientesSucesso; ?>
                                                    <?php echo $msgClientesErro; ?>
                                                    <?php echo $e; ?>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="nome" style="color:#000">Nome</label>
                                                        <input type="text" class="form-control" placeholder="Insira seu nome" aria-label="Insira seu nome" name="nome" id="nome" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label style="color:#000">E-mail</label>
                                                        <input type="email" class="form-control" placeholder="Insira seu e-mail" name="email" id="email" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label style="color:#000">Celular</label>
                                                        <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label style="color:#000">Telefone Alternativo</label>
                                                        <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label style="color:#000">Tipo do Veículo</label>
                                                        <select class="selectpicker" data-style="btn btn-round btn-simple" title="Insira uma opção"  name="tipocarro" id="tipocarro" required>
                                                            <option selected disabled>Selecione</option>
                                                            <option value="Carro">Carro</option>
                                                            <option value="Moto">Moto</option>
                                                            <option value="Caminhão">Caminhão</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info btn-lg pull-right">Solicitar Cotação</button>
                                                </div>
                                            </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/modernizr.custom.js"></script>
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>  
      <script src="assets/js/classie.js"></script>
    <script src="assets/js/selectFx.js"></script>
    <script src="assets/js/fullscreenForm.js"></script>
    <script src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/nouislider.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src="assets/js/jasny-bootstrap.min.js"></script>
    <script src="assets/js/atv-img-animation.js" type="text/javascript"></script>
    <script src="assets/js/material-kit.js?v=1.1.0" type="text/javascript"></script>
    <script src="assets/js/scroll.js"></script>
    <script>
        $(document).ready(function(){
        $('.date').mask('00/00/0000');
        $('.cep').mask('00000-000');
        $('.phone_with_ddd').mask('(00) 00000-0000');
        $('.fixo_with_ddd').mask('(00) 0000-0000');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        
        });
    </script>
    <script>
        $(document).ready(function() {
        
        $(".botaoshake").click(function(){
            $(".formshake").effect( "shake", {times:6}, 800 );
        });
        
        });
    </script>
</html>