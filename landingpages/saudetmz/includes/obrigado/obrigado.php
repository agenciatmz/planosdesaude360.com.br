<!DOCTYPE html>
<html lang="en">
<head>
  <title>Planos de Saúde 360</title>

    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>

    <!-- Css -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../css/font-icons.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link rel="stylesheet" href="../../css/style.css" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="planosdesaude360.com.br" />
    <meta property="og:description" content="Os melhores Planos de Saúde" />
    <meta property="og:site_name" content="planosdesaude360.com.br" />
    <meta property="og:url" content="https://planosdesaude360.com.br/home" />
    <meta property="og:image" content="https://landingpages.planosdesaude360.com.br/generica/img/destaque.png" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
    <?php require_once("../../../helpers/includesPixel.php"); ?>
    <!-- Google Code for leads Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 850855984;
    var google_conversion_label = "14JVCKW85HEQsJDclQM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/850855984/?label=14JVCKW85HEQsJDclQM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

</head>


<body>

  <!-- Preloader -->
<!--  <div class="loader-mask">-->
<!--    <div class="loader">-->
<!--      "Loading..."-->
<!--    </div>-->
<!--  </div>-->

  <main class="main-wrapper">

    <!-- Navigation -->
      <header class="nav">
          <div class="nav__holder nav--sticky">
              <div class="container-fluid container-semi-fluid nav__container">
                  <div class="flex-parent">

                      <div class="nav__header">
                          <!-- Logo -->
                          <a href="index.php" class="logo-container flex-child">
                              <img class="logo" src="img/logo.png" srcset="img/logo.png 1x, img/logo@2x.png 2x" alt="logo">
                          </a>

                          <!-- Mobile toggle -->
                          <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                              <span class="nav__icon-toggle-bar"></span>
                          </button>
                      </div>

                      <!-- Navbar -->
                      <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                          <ul class="nav__menu">
                              <li>
                                  <a href="../../index.php">Home</a>
                              </li>

                          </ul> <!-- end menu -->
                      </nav> <!-- end nav-wrap -->


                  </div> <!-- end flex-parent -->
              </div> <!-- end container -->

          </div>
      </header> <!-- end navigation -->

      <!-- Triangle Image -->
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       viewBox="0 0 600 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-right">
      <g>
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img1)" />
        <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#triangle-gradient)" fill-opacity="0.7" />
      </g>
        <defs>
            <pattern id="img1" patternUnits="userSpaceOnUse" width="500" height="500">
                <image xlink:href="img/hero/hero.jpg" x="50" y="70" width="500" height="500"></image>
            </pattern>

            <linearGradient id="triangle-gradient" y2="100%" x2="0" y1="50%" gradientUnits="userSpaceOnUse" >
                <stop offset="0" stop-color="#4C86E7"/>
                <stop offset="1" stop-color="#B939E5"/>
            </linearGradient>
        </defs>
    </svg>


    <div class="content-wrapper oh">

      <!-- Hero -->
      <section class="hero">

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="hero__text-holder">
                <h1 class="hero__title hero__title--boxed">Agradecemos pelo o seu tempo</h1>
                <h2 class="hero__subtitle">Obrigado por cotar conosco, em breve nossos consultores entrará em contato!</h2>
                  <a href="../../index.php" class="btn btn--lg btn--color btn--icon formshake">
                      <span>Voltar</span>
                      <i class="ui-arrow-right"></i>
                  </a>
              </div>
            </div>
          </div>

            <section class="section-wrap">
                <div class="container">
                    <div class="title-row title-row--boxed text-center">
                        <h2 class="section-title" style="color:#000">Novidades</h2>
                        <p class="subtitle" style="color:#333B69">Fique por dentro de todas novidades sobre Planos de Saúde.</p>
                    </div>
                    <div class="row card-row">

                        <div class="col-lg-4">
                            <article class="entry card box-shadow hover-up">
                                <div class="entry__img-holder card__img-holder">
                                    <a href="blog/plano-de-saude-carencia-zero.php">
                                        <img src="img/blog/post_1.jpg" class="entry__img" alt="">
                                    </a>
                                    <div class="entry__date">
                                        <span class="entry__date-day">11</span>
                                        <span class="entry__date-month">jul</span>
                                    </div>
                                    <div class="entry__body card__body">
                                        <h4 class="entry__title">
                                            <a href="blog/plano-de-saude-carencia-zero.php">Plano de Saúde carência zero existe ou não?</a>
                                        </h4>
                                        <ul class="entry__meta">
                                            <li class="entry__meta-category">
                                                <i class="ui-category"></i>
                                                <a href="#">Planos de Saúde</a>
                                            </li>
                                            <li class="entry__meta-star">
                                                <i class="ui-star"></i>
                                                <a href="#">Leitura: 5 Min</a>
                                            </li>
                                        </ul>
                                        <div class="entry__excerpt">
                                            <p>Um dos maiores desejos dos brasileiros é ter um plano de saúde carência zero. Seja para tratar alguma doença, ou para simplesmente ter uma segurança para si ou para toda a família.</p>
                                        </div><br>
                                        <a href="blog/plano-de-saude-carencia-zero.php">Saiba mais</a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="col-lg-4">
                            <article class="entry card box-shadow hover-up">
                                <div class="entry__img-holder card__img-holder">
                                    <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">
                                        <img src="img/blog/post_2.jpg" class="entry__img" alt="">
                                    </a>
                                    <div class="entry__date">
                                        <span class="entry__date-day">09</span>
                                        <span class="entry__date-month">jul</span>
                                    </div>
                                    <div class="entry__body card__body">
                                        <h4 class="entry__title">
                                            <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">Como fazer um Plano de Saúde Empresarial</a>
                                        </h4>
                                        <ul class="entry__meta">
                                            <li class="entry__meta-category">
                                                <i class="ui-category"></i>
                                                <a href="#">Planos de Saúde</a>
                                            </li>
                                            <li class="entry__meta-star">
                                                <i class="ui-star"></i>
                                                <a href="#">Leitura: 6 Min</a>
                                            </li>
                                        </ul>
                                        <div class="entry__excerpt">
                                            <p>O plano de saúde empresarial tem agradado a muitos usuários pelos seus muitos benefícios, mas você sabe como fazer um? A empresa que não quer dor de cabeça nem precisa pensar muito para saber que fazer um plano de saúde empresarial é a escolha certa.</p>
                                            <br>
                                            <a href="blog/como-fazer-um-plano-de-saude-empresarial.php">Saiba mais</a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="col-lg-4">
                            <article class="entry card box-shadow hover-up">
                                <div class="entry__img-holder card__img-holder">
                                    <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">
                                        <img src="img/blog/post_3.jpg" class="entry__img" alt="">
                                    </a>
                                    <div class="entry__date">
                                        <span class="entry__date-day">08</span>
                                        <span class="entry__date-month">jul</span>
                                    </div>
                                    <div class="entry__body card__body">
                                        <h4 class="entry__title">
                                            <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Plano de Saúde cobre ou não cirurgia plástica?</a>
                                        </h4>
                                        <ul class="entry__meta">
                                            <li class="entry__meta-category">
                                                <i class="ui-category"></i>
                                                <a href="#">Planos de Saúde</a>
                                            </li>
                                            <li class="entry__meta-star">
                                                <i class="ui-star"></i>
                                                <a href="#">Leitura: 6 Min</a>
                                            </li>
                                        </ul>
                                        <div class="entry__excerpt">
                                            <p>O Brasil é o segundo país do mundo em quantidade de cirurgia plástica, com mais de 1,22 milhão de procedimentos realizados, perdendo apenas para os Estados Unidos, que somam mais de 1,41 milhão.</p>
                                            <br>
                                            <a href="blog/plano-de-saude-cobre-ou-nao-cirurgia-plastica.php">Saiba mais</a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>
                </div>
            </section> <!-- end from blog -->

      <div id="back-to-top">
        <a href="#top"><i class="ui-arrow-up"></i></a>
      </div>

    </div> <!-- end content wrapper -->
  </main> <!-- end main wrapper -->
  <?php include("includes/modal/modals.php"); ?>

  <!-- jQuery Scripts -->
  <script src="../../js/jquery.min.js"></script>
  <script src="../../js/sweetalert2.all.js"></script>

  <script>$(document).ready(function () {
          swal({ title: "Obrigado", text: "Em breve nossos consultores entrará em contato\n", type: "success" });
      });
  </script>
  <script src="../../js/bootstrap.min.js"></script>
  <script src="../../js/plugins.js"></script>
  <script src="../../js/scripts.js"></script>
  <script src="../../js/validation.js"></script>
  <script src="../../js/api-estado-cidade.js"></script>
  <script src="../../js/FormSteps.js"></script>
  <script src="../../js/jquery.validate.min.js"></script>
  <script src="../../js/custom.js"></script>

  <!-- Cookies -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
  <script src="../../js/cookies.js"></script>

</body>
</html>
