<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>


    <?php include("includes/IncludesHeader.php"); ?>


<body>
  <div class="js">

  <div id="preloader"></div>


<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank">
                <img src="assets/img/logo-color.png" width="110">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#conheca" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Conheça o plano</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 3.5em">
                        GARANTA O MELHOR CUSTO BENEFICIO EM PLANO DE SAÚDE DO BRASIL

                        </h1>

                        <h4 class="description" style="color:#fff">
                                <span class="description">
                              Só a Amil tem as melhores opções de planos de saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É rápido e fácil!
                                </span>
                            <br><br>
                            <button class="btn btn-warning btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">
                          <?php include("../helpers/insereBanco.php"); ?>


                                                        <?php include("includes/Form/FormAmil.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-8 section-image" style="background-image: url('assets/img/bg3.jpg')" id="conheca">
    <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title">Conheça o Amil Saúde</h2>
        <h4 class="description"> Só a Amil tem as melhores opções de Plano de Saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É Rápido e Fácil. </h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical-emergencies.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                        <h4 class="info-title">COBERTURA EM TODO TERRITÓRIO NACIONAL</h4>
                        <p class="description">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em todo território nacional, para maiores informações solicite uma cotação em nossa página!</p>
                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical_app.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                        <h4 class="info-title">COBERTURA DE <br>PROCEDIMENTOS</h4>
                        <p class="description">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação, aqui mesmo em nossa página, para que um Corretor Autorizado lhe auxilie com maiores informações.</p>
                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/medical-gif-11.gif" class="rounded" alt="">
                    </div>
                    <div class="info text-center">

                        <h4 class="info-title">PREÇOS DOS PLANOS <br>DE SAÚDE AMIL</h4>
                        <p class="description">


                                      Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado. Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros) <br> <br>
<b>Além disso a Amil tem parcerias que oferecem desontos incríveis para você, confira:</b> <br><br>
                                      Smart Fit – Programa Saúde em Dobro <br>

Drogaria São Paulo<br>

Drogaria Pacheco<br>

E Muito mais.

</p>
                        <a href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-2 text-center" style="background-color:#f1f1f1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselExampleIndicators2" class="carousel slide">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class=""></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="1" class="active"></li>
                                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                            </ol>
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
                                          "Sempre que precisei da Amil nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios, atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito."

                                            </h5>
                                            <h3 class="card-title">Arlete Rezende</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Empreendedora E Cliete Amil Saúde.</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="carousel-item justify-content-center">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
                                          "Tenho Amil há 10 anos e não troco por nada! Uma porque eu gosto muito do atendimento, outra porque os Planos mais Baratos são Piores e os mais caros são do mesmo nível, não tenho porque sair da Amil."

                                            </h5>
                                            <h3 class="card-title">Guilherme Foleis</h3>
                                            <div class="card-footer">
                                              <h6 class="category text-primary">Gerente de Novos Negócios</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="carousel-item justify-content-center active">
                                    <div class="card card-testimonial card-plain">
                                        <div class="card-avatar">
                                            <a href="#pablo">
                                            <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-description">
                                              "Sou Autônomo, não posso correr o risco de me afastar por problemas de saúde... Na Amil tenho um Convênio de Qualidade sem pagar os absurdos que cobram por aí. Antes eu tinha Sulamérica, que também é muito bom, mas pelo custo benefício da Amil essa troca for a melhor coisa que eu fiz"
                                            </h5>
                                            <h3 class="card-title">Anderson Silva</h3>
                                            <div class="card-footer">
                                                <h6 class="category text-primary">Cliente Amil Saúde</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                            <i class="now-ui-icons arrows-1_minimal-left"></i>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                            <i class="now-ui-icons arrows-1_minimal-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div id="contato">
    <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/bg3.jpg');     background-size: cover;
                background-position: center center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="title" style="font-size: 3em">
                                <span style="color: #fff">
                                    AINDA TEM DÚVIDAS?
                                    <p class="description" style="    color: #fff;font-size: 0.6em">
                                        Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde Amil ideal para você!
                                </span>
                    </h1>
                    <div class="col-md-12 text-center">
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1em">
                        COTAÇÃO SEM COMPROMISSO!
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer " style="background-color:#08377F; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>
<?php include("includes/IncludesFooter.php"); ?>
</div>
</html>
