<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("includes/IncludesHeader.php"); ?>
        <div class="js">
        <div id="preloader"></div>
    <body>
        <?php include("../helpers/insereBanco.php"); ?>
        <section id="header-and-image" data-editable="true" data-content="background" data-field-name="landing_page[values[header-background-color]]">
            <div class="container">
                <div class="pure-u-1">
                    <div class="l-box">
                      <div class="col-md-8 ml-auto mr-auto text-center">
                          <h2 class="title">Conheça o Amil Saúde</h2>
                          <h4 class="description"> Só a Amil tem as melhores opções de Plano de Saúde para você, sua família ou empresa! Faça uma cotação em nossa página e economize. É Rápido e Fácil. </h4>
                      </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="fixed-form" data-editable="true" data-content="conversion" data-editor-position="inside">
        <div id="conversion">
            <section>
            </section>
            <?php include("includes/Form/FormAmil.php");?>

    </div>
      </section>
        </body>
    <?php include("includes/IncludesFooter.php"); ?>
</html>
