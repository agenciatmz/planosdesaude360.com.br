<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>São Cristóvão Saúde</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../helpers/includesPixel.php"); ?>

<?php require_once("../helpers/includesChat.php"); ?>
    </head>
    <body class="template-page">
        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png">
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#quemsomos">
                                Sobre nós
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#redecredenciada">
                                Rede Credenciada
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#formulario">
                                Cote agora
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg1.png');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h2 class="title" style="font-size: 4em;"><span class="azulclaro">PLANOS DE SAÚDE À PARTIR DE</span><span class="azulescuro"> R$ 141,76</span></h2>
                                <h4 class="description azulescuro" style="font-weight: 500">
                                    <span class="azulescuro">Você precisa de um plano de saúde que te trate de forma humanizada, com profissionais qualificados e com custos acessíveis? Preencha o formulário ao lado e saiba mais sobre a ampla rede credenciada, laboratórios referenciados e valores para você, família ou empresa.</span>
                                </h4>
                                <button class="btn btn-info btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                            </div>
                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                                <div class="card card-contact card-raised formshake">
                                  <?php include("../helpers/insereBanco.php"); ?>
                             <?php include("includes/Form/FormSaoCristovao.php");?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">SOBRE A</span> <br><span style="font-weight:800" class="azulescuro">SÃO CRISTÓVÃO</span></h2>
                            <h4 class="description text-justify">Além das certificações, a Instituição também passou por grandes processos de melhorias e conquistas, dentre elas: parcerias com empresas conceituadas no âmbito da saúde; aquisição de novos terrenos; modernização de diversos setores; criação de novas áreas assistenciais; revitalização dos ambientes; implantação e atualização de novos softwares na área de Tecnologia da Informação; investimento na compra de equipamentos de ponta, além de outros projetos em fase de desenvolvimento e execução.<br><br>
                                Com as novas diretrizes administrativas, foi possível realizar uma grande modernização na estrutura física e na parte de atendimento, investindo em múltiplas áreas, equipamentos, certificações e em profissionais cada vez mais qualificados, contando, atualmente, com 1.500 colaboradores.
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgDestaque01.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600"><span class=" azulescuro">AMPLA COBERTURA<br>DE PROCEDIMENTOS</span></h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                    <img src="assets/img/imgDestaque02.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600"><span class=" azulescuro">AMPLA REDE<br> HOSPITALAR</span></h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                    <img src="assets/img/imgDestaque03.png">
                                </div>
                                <h4 class="info-title" style="font-weight:600"><span class=" azulescuro">PLANOS DE SAÚDE<br> MAIS BARATOS</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                                  <div class="alert alert-info" role="alert">
              <div class="container text-center">
                  Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#formulario" class="botaoshake" style="color:yellow">Cote agora</a>
              </div>
          </div>
      </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
        </div>
        <div class="cd-section">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1" id="redecredenciada" style="background-color:#f0f0f0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">REDE CREDENCIADA DA</span> <br><span style="font-weight:800" class="azulescuro">SÃO CRISTÓVÃO</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgRedeCredenciadaHospitalGuaianases.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgRedeCredenciadaHospitalItaquera.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                    <img src="assets/img/imgRedeCredenciadaHospitalSaoCristovao.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info info-hover">
                                <div class="icon icon-warning">
                                    <img src="assets/img/imgRedeCredenciadaHospitalPresidente.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgRedeCredenciadaHospitalSantaMarcelina.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-primary">
                                    <img src="assets/img/imgRedeCredenciadaHospitalHSANP.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info info-hover">
                                <div class="icon icon-success">
                                    <img src="assets/img/imgRedeCredenciadaHospitalAlbertSabin.png">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                                  <div class="alert alert-success" role="alert">
              <div class="container text-center">
                  Possui CNPJ? Economiza até <strong>30%</strong> à partir de 2 vidas! <a href="#formulario" class="botaoshake" style="color:yellow">Cote agora</a>
              </div>
          </div>
      </div>
                </div>
            </div>
            <div class="features-2 " style="background-image: url('assets/img/bg30.png'); background-size: cover;background-position: center center;">
                <div class="col-md-8 ml-auto mr-auto text-center" style="padding-top:40px">
                    <h1 class="title">AINDA TEM DÚVIDAS?</h1>
                    <h3 class="description" style="font-weight:400; color:#fff">Solicite uma cotação e saiba mais sobre os Planos</h3>
                    <a href="#formulario" class="btn btn-info btn-lg botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</a>

            </div>
        </div>
          </div>
            <!--     *********    END FEATURES 1      *********      -->

        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                      <li class="nav-item active">
                          <a class="nav-link" href="#quemsomos">
                          Sobre nós
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#redecredenciada">
                          Rede Credenciada
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link botaoshake" href="#formulario">
                          Cote agora
                          </a>
                      </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeizero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <?php include("includes/IncludesFooter.php"); ?>
</html>
