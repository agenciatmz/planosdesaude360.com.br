<?php require_once("includes/IncludesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>


    <?php include("includes/IncludesHeader.php"); ?>


<body>
  <div class="js">

  <div id="preloader"></div>


<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank">
                <img src="assets/img/LandingPagesSulamerica/logo-color.png" width="300">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#diferenciais" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Diferenciais</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesSulamerica/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h1 class="title" style="font-size: 4em">À PARTIR DE <br><span style="font-size: 0.5em">R$</span><span style="font-size: 1.5em">352</span><span style="font-size: 0.5em">,15 - ADESÃO</span><br>
                            <span style="font-size: 0.5em">R$</span><span style="font-size: 1.5em">189</span><span style="font-size: 0.5em">,03* - EMPRESARIAL</span>
                        </h1>
                        <p>*A partir de 3 vidas - Valor por vida
                        </p>
                        <h4 class="description" style="color:#fff">
                                <span class="description">
                                A Sul América Saúde assegura qualidade de vida e bem-estar para você e sua família. Além da ampla rede referenciada, os Planos Sul América Saúde oferecem diversas vantagens, benefícios, descontos e programas especiais para você!
                                Solicite uma cotação ao lado e saiba mais
                                </span>
                            <br><br>
                            <button class="btn btn-info btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">
                        <?php include("../helpers/insereBanco.php"); ?>

                                                        <?php include("includes/Form/FormSulamericaRJ.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-8 section-image" style="background-image: url('assets/img/LandingPagesSulamerica/bg3.jpg')">
    <div class="col-md-8 ml-auto mr-auto text-center">
        <h2 class="title">Diferenciais dos Planos de Saúde SulAmérica</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/LandingPagesSulamerica/imgdiferenciasclinica.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <div class="icon">
                            <i class="now-ui-icons business_bank"></i>
                        </div>
                        <h4 class="info-title">16 Mil Clínicas</h4>
                        <button href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake" type="button">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/LandingPagesSulamerica/imgdiferenciassegura.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <div class="icon">
                            <i class="now-ui-icons location_world"></i>
                        </div>
                        <h4 class="info-title">Maior Grupo Segurador do Brasil</h4>
                        <button href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake" type="button">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-image">
                        <img src="assets/img/LandingPagesSulamerica/imgdiferenciasrede.jpg" class="rounded" alt="">
                    </div>
                    <div class="info text-center">
                        <div class="icon">
                            <i class="now-ui-icons health_ambulance"></i>
                        </div>
                        <h4 class="info-title">Ampla Rede Referenciada</h4>
                        <button href="#formulario" data-scroll  class="btn btn-danger btn-round btn-xs botaoshake" type="button">
                            <i class="now-ui-icons ui-2_favourite-28"></i> COTE ONLINE AGORA
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-6">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h2 class="title" style="color: red">Confira abaixo o que a Sulamerica pode lhe oferecer</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ">
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Planos Individuais</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Planos Familiares</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Planos Empresariais</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">22 Mil Referenciados</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">1.400 Hospitais</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Desconto em Medicamentos</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Modo Enfermaria</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Mensalidade Acessíveis</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Pré Natal</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Centros de Diagnósticos</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Medicina de 1º Mundo</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Aconselhamento via Tel.</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title"> Assistência Viagem</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Desconto em Spa</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Desconto em Academias</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">Remoção Médica</h5>
                    </div>
                </div>
                <div class="info info-horizontal ">
                    <div class="icon icon-danger">
                        <i class="now-ui-icons ui-1_check"></i>
                    </div>
                    <div class="description">
                        <h5 class="info-title">E MUITO MAIS</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <div class="col-md-12 text-center">
                <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1.2em">
                    SOLICITE UMA COTAÇÃO SEM COMPROMISSO!
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto text-left">
            <h1 class="title" style="font-size: 3em">
                        <span style="color: #D05617">SULAMÉRICA SAÚDE É BOM?
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 ">
            <img src="assets/img/LandingPagesSulamerica/depoimento01.jpg" class="img-responsived-none d-sm-block" width="230px" >
        </div>
        <div class="col-md-9 ">
            <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">Não é bom, é excelente! Custo beneficio e atendimento ótimos, já tive experiências com outros Planos, sem dúvidas, fico com o SulAmérica.</p>
            <p class="description" style="font-weight: 800; color: #D05617;">- Robson Oliveira, cliente SulAmérica Saúde</p>
            <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 ">
            <img src="assets/img/LandingPagesSulamerica/depoimento02.jpg" class="img-responsive  d-none d-sm-block" width="230px" >
        </div>
        <div class="col-md-9 ">
            <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">Gosto bastante, eu já tinha o seguro de carro com a SulAmérica né? Depois que mudei de emprego, precisei do saúde e não tenho oque reclamar.</p>
            <p class="description" style="font-weight: 800; color: #D05617;">- Giovanna Ferreira, cliente SulAmérica Saúde</p>
            <a href="#formulario" data-scroll class="btn btn-warning btn-lg botaoshake" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
        </div>
    </div>
</div>
<br><br>
<div id="contato">
    <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/LandingPagesSulamerica/bg3.jpg');     background-size: cover;
                background-position: center center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="title" style="font-size: 3em">
                                <span style="color: #fff">
                                    AINDA TEM DÚVIDAS?
                                    <p class="description" style="    color: #fff;font-size: 0.6em">
                                        Solicite uma cotação personalizada abaixo e Encontre o Plano de Saúde SulAmérica ideal para você!
                                </span>
                    </h1>
                    <div class="col-md-12 text-center">
                        <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1.2em">
                            SOLICITE UMA COTAÇÃO SEM COMPROMISSO!
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer " style="background-color:#08377F; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>
<?php include("includes/IncludesFooter.php"); ?>
</div>
</html>
