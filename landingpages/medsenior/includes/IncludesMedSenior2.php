<div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                    <img src="assets/img/LandingPageMedSenior/logo.png" width="200">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse"  data-color="green" >
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#sobrenos" data-scroll >
                            <p>SOBRE NOS</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#redecredenciada" data-scroll  >
                            <p>REDE CREDENCIADA</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPageMedSenior/bg15.jpg'); "></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-left">
                        <h4 class="title"><span  style="font-size: 1.4em; color:#fff">PLANOS À PARTIR DE<br><span class="highlight" style="font-size: 2.4em; color:#fff"> <br> R$ 467,35<span class="highlight" style="font-size: 0.7em">*</span> <br></span></h4>
                    </div><br><br>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">

                            <?php include("includes/Form/FormMedSenior2.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="features-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <h2 class="title" style="color:#00984A">Conheça a MedSênior</h2>
                <h4 class="description">
                  Os brasileiros de 60 anos ou mais chegam a aproximadamente 15 milhões, cerca de 8% da população. Desses 15 milhões, quase três milhões tem renda superior a três salários mínimos. A mudança no perfil da população brasileira com o aumento da participação da terceira idade, tem implicações no consumo e as empresas preparam-se para conquistar sua participação neste emergente mercado.
<br><br>
                  Com a chegada da terceira idade, alguns problemas de saúde passam a ser mais freqüentes, e outros, incomuns nas fases de vida anteriores, começam a aparecer.

                </h4>
            </div>
        </div><br><br>
        <div class="row">
            <div class="col-md-4">
                <div class="info info-hover">
                  <div class="icon icon-primary">
                      <img src="assets/img/LandingPageMedSenior/banner1.png">
                  </div>
                    <h4 class="info-title" style="color:#00984A">Rede Própria</h4>
                    <p class="description text-justify">A MedSênior possui estrutura própria, incluindo hospitais, ambulatórios, centros de especialidades médicas, centros de diagnósticos e laboratórios.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info info-hover">
                  <div class="icon icon-primary">
                      <img src="assets/img/LandingPageMedSenior/banner2.png">
                  </div>
                    <h4 class="info-title" style="color:#00984A">Medicina Preventiva</h4>
                    <p class="description text-justify">O Plano de Saúde Medsênior é diferente dos outros planos porque não está focado apenas nos atendimentos ambulatoriais e hospitalares. Ele também cuida da saúde de seus beneficiários de forma preventiva.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info info-hover">
                  <div class="icon icon-primary">
                      <img src="assets/img/LandingPageMedSenior/banner3.png">
                  </div>
                    <h4 class="info-title" style="color:#00984A">Oficinas de Saúde</h4>
                    <p class="description text-justify">O Medsênior desenvolveu o Programa de Oficinas de Saúde para melhorar a qualidade de vida de seus beneficiários.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="pricing-5 section-pricing-5 " id="rede">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                <span style="color: #00984A">
                CONHEÇA A REDE CREDENCIADA DA MEDSÊNIOR
                </span>
                </h1>
            </div>
        </div>
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/vitoria.png">
                        </div>
                        <h4 class="info-title">Centro de Diagnósticos - Vitória/ES</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/vitoria-unidade-2.png">
                        </div>
                        <h4 class="info-title">Centro de Oncologia - Vitória/ES </h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/serra-es.png">
                        </div>
                        <h4 class="info-title">Pronto-atendimento Serra/ES</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/vitoria-unidade-3.png">
                        </div>
                        <h4 class="info-title">Unidade Vitória/ES</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/vilha-velha.png">
                        </div>
                        <h4 class="info-title">Centro de Oftalmologia - Vilha Velha/ES</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover" style="background-color: #fff; border-radius: 19px; paddingdding: 11px;">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPageMedSenior/muitomais.png">
                            <p class="description"> Hospitais e clínicas </p>
                        </div>
                        <a href="#formulario" data-scroll="" class="btn btn-info btn-lg botaoshake">COTAÇÃO SEM COMPROMISSO</a>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

    <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/LandingPageMedSenior/bg30.jpg'); background-size: cover;" id="contato">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <div class="card card-testimonial card-plain">
                    <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                    <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                    <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                        QUERO COTAR AGORA
                    </a>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer " style="background-color:#fff; color:#ED3237">
        <div class="col-md-12">
            <div class="container">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por
                    <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#ED3237">#agênciatrêsmeiazero</a>.
                </div>
            </div>
        </div>
    </footer>
</div>
