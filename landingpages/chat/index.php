<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Leads Chat</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="assets/img/LandingPagesIntermedica/favicon.ico" rel="shortcut icon">
    <?php include("includes/IncludesHeader.php"); ?>


<body>


<div class="header">



    <div class="testimonials-2" style="background-color:#f1f1f1" id="depoimento">
        <div class="container">
            <div class="col-md-12 " id="formulario">
                <div class="card card-contact card-raised formshake">
                  <?php
                      if(isset($_POST['cadastrar'])){
                          $nome            = trim(strip_tags($_POST['nome']));
                          $email           = trim(strip_tags($_POST['email']));
                          $telefone         = trim(strip_tags($_POST['telefone']));
                          $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
                          $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
                          $cnpj            = trim(strip_tags($_POST['cnpj']));
                          $estado          = trim(strip_tags($_POST['estado']));
                          $cidade          = trim(strip_tags($_POST['cidade']));
                          $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
                          $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
                          $operadora       = trim(strip_tags($_POST['operadora']));
                          $mensagem        = trim(strip_tags($_POST['mensagem']));
                          $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
                          $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));
                          $modalidade     = trim(strip_tags($_POST['modalidade']));

                          $insert = "INSERT INTO tmzleads ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidadepme, quantidadefamiliar, operadora, mensagem, tipodeplano, tipopessoa, modalidade  )
                          VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidadepme, :quantidadefamiliar, :operadora, :mensagem, :tipodeplano, :tipopessoa, :modalidade )";
                          try{

                              $result = $conexao->prepare($insert);

                              $result->bindParam(':nome', $nome, PDO::PARAM_STR);
                              $result->bindParam(':email', $email, PDO::PARAM_STR);
                              $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
                              $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
                              $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
                              $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
                              $result->bindParam(':estado', $estado, PDO::PARAM_STR);
                              $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
                              $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
                              $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
                              $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
                              $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
                              $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
                              $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
                              $result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);

                              $result->execute();
                              $contar = $result->rowCount();
                              if($contar>0){

                                  {
                                      $msgClientesSucesso = '
                                      <div class="alert alert-success" role="alert">
                    <div class="container">
                        <div class="alert-icon">
                            <i class="now-ui-icons ui-2_like"></i>
                        </div>
                        <strong>Muito bem!</strong> O lead foi cadastrado com sucesso!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                        </button>
                    </div>
                </div>';

                                  }
                              }else{
                                  $msgClientesErro = '<div class="alert alert-danger" role="alert">
                    <div class="container">
                        <div class="alert-icon">
                            <i class="now-ui-icons objects_support-17"></i>
                        </div>
                        <strong>Oops!</strong> Algo deu errado, tente novamente!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                        </button>
                    </div>
                </div>';
                              }
                          }catch(PDOException $e){
                              echo $e;
                          }

                      }else {
                          $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
                      }
                      ?>

                                                      <?php include("includes/Form/FormChat.php");?>
                </div>
            </div>
        </div>
    </div>







</body>

<?php include("includes/IncludesFooter.php"); ?>
</html>
