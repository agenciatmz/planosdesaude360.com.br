<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        require_once("includes/includesheader.php");
        require_once("../helpers/includesPixel.php");
        require_once("../helpers/includesChat.php");
         ?>

    </head>
    <body class="template-page">


        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="assets/img/logo-color.png"  >
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#quemsomos" data-scroll>
                                    <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                    <p>SERVIÇOS</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#planos" data-scroll>
                                    <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                    <p>COMO FUNCIONA</p>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                    <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                    <p>Cotação Online</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <h1 class="title" style="font-size: 3em;"><span style="color:#fff">HABILITAÇÃO <span style="font-weight: 300">SUSPENSA OU CASSADA?</span> </h1>
                                </span>
                                <img src="assets/img/header-first.png" width="800px" class="img-responsive d-none d-sm-block">

                            </div>
                            <div class="col-md-6 ml-auto mr-auto" id="formulario">
                              <div class="card card-contact card-raised formshake">
                                <?php include("../helpers/insereBanco.php"); ?>
                                     <?php include("includes/Form/form.php");?>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cd-section" id="quemsomos">
          <div class="features-1">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-8 ml-auto mr-auto">
                                      <h2 class="title azulclaro">SERVIÇOS QUE OFERECEMOS</h2>
                                      <h4 class="description"> Nosso trabalho consiste em solucionar todos os problemas relacionados à habilitação, visando trazer segurança, comodidade e tranquilidade para todos os nossos clientes!</h4>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-3">
                                      <div class="info info-hover">
                                          <div class="icon">
                                            <img src="assets/img/imgSuspensaoHabilitacao.png">
                                          </div>
                                          <h4 class="info-title" style="font-weight: 800">SUSPENSÃO</h4>
                                          <p class="description">Defesa para habilitação suspensa</p>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="info info-hover">
                                          <div class="icon">
                                          <img src="assets/img/imgSuspensaoCassacao.png">
                                          </div>
                                          <h4 class="info-title ">CASSAÇÃO</h4>
                                          <p class="description">Recursos para habilitação cassada</p>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="info info-hover">
                                          <div class="icon">
                                              <img src="assets/img/imgSuspensaoMultas.png">
                                          </div>
                                          <h4 class="info-title">MULTAS</h4>
                                          <p class="description">Defesa para multas na habilitação</p>
                                      </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="info info-hover">
                                          <div class="icon">
                                              <img src="assets/img/imgSuspensaoPontos.png">
                                          </div>
                                          <h4 class="info-title">PONTOS</h4>
                                          <p class="description">Regularização de pontos na habilitação</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
        </div>

        <!--     *********    END FEATURES 1      *********      -->
        <div class="features-4" id="planos">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title azulclaro">COMO FUNCIONA?</h2>
                        <h4 class="description"> Confira abaixo os passos necessários para que possamos encontrar uma solução para o seu problema.</h4>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" >
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">RECOLHA DE DADOS</h4>
                                    <p>Sua CNH foi suspensa por excesso de pontuação ou ocorrências de multas suspensivas, vamos conversar sobre a sua situação, e entender melhor o seu caso, recolhendo os dados necessários.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="">
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">ANÁLISE DA SITUAÇÃO</h4>
                                    <p>Vamos analisar a sua situação, e ver os próximos passos para a regularização completa do seu documento, para que saia dirigindo tranquilamente</p>
                                        <bR>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-background card-raised" data-background-color="" >
                            <div class="info">
                                <div class="description">
                                    <h4 style="font-weight:800">EXECUÇÃO DO PROCESSO</h4>
                                    <p> Enquanto você já estiver dirigindo, estaremos tomando as medidas necessárias para a regularização. Para que volte a ter o documento dentro dos padrões, e sem problemas maiores.</p>
                                    <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal">Saiba Mais</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="features-2 " style="background-image: url('assets/img/bg30.png'); background-size: cover;background-position: center center;">
                <div class="col-md-8 ml-auto mr-auto text-center" style="padding-top:40px">
                    <h1 class="title" style="font-size: 30px">AINDA TEM DÚVIDAS? NÃO FIQUE SEM DIRIGIR,<br>FAÇA UMA COTAÇÃO ONLINE AGORA MESMO</h1>
                
                    <a href="#" class="btn btn-info btn-lg"  style="background-color: #fff000;color:#000; font-weight: 500">Solicitar Cotação</a>
                </div>
            </div>
        <footer class="footer ">
            <div class="container">
                <nav>
                    <ul>
                        <li class="nav-item active">
                            <a class="nav-link" href="#quemsomos">
                            SERVIÇOS 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#planos">
                            COMO FUNCIONA
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                            Cote agora
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>

    <?php include("includes/IncludesFooter.php"); ?>
</html>
