<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Plano de Saúde Golden Cross</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="assets/img/LandingPagesGoldenCross/favicon.ico" rel="shortcut icon">
    <?php include("includes/IncludesHeader.php"); ?>


<body>
  <div class="js">

<div id="preloader"></div>
<!--Start of Tawk.to Script-->

<!--End of Tawk.to Script-->
<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Golden Cross">
                <img src="assets/img/LandingPagesGoldenCross/logo-color.png" width="150">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/LandingPagesGoldenCross/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimentos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#diferenciais" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Diferenciais</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesGoldenCross/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-left">
                        <h1 class="title" style="font-size: 4em">À PARTIR DE <br>R$ 204,30*</h1>
                        <h2 class="description" style="color:#fff">
                            A Golden Cross se orgulha de ser a empresa pioneira no setor de saúde suplementar no Brasil e atualmente conta com cerca de 500 mil clientes empresariais.
                        </h2>
                        <br>
                        <button class="btn btn-info btn-lg pull-left botaoshake" style="color:#fff; font-weight: 500">Solicitar Cotação</button>
                    </div>
                    <div class="col-md-7 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">
                          <?php include("../helpers/insereBanco.php"); ?>

                            <?php include("includes/Form/FormGoldenCross.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-3" id="depoimentos">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-left">
                <h1 class="title" style="font-size: 3em">
                            <span style="color: #f9d716">CONHEÇA A GOLDEN CROSS
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">
                    Para se ter uma ideia da qualidade dos serviços prestados e da confiança adquirida no mercado a Golden Cross, empresa pioneira no setor de saúde suplementar no Brasil, com muito orgulho, conta com números impressionantes, como por exemplo os cerca de 500 mil clientes empresariais que possui atualmente.
                </p>
                <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">Desde seu surgimento, a Golden Cross busca constantemente a satisfação de seus clientes, oferecendo serviços de qualidade e buscando uma relação transparente e sempre comprometida com o atendimento das necessidades de seus associados. </p>
                <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">Não é atoa que nos últimos anos, a Golden Cross tem sido a operadora de saúde com melhores índices de avaliação em pesquisa elaborada pela Revista Exame e pelo Instituto Brasileiro de Relacionamento com o Cliente (IBRC).
                </p>
                <a href="#formulario" data-scroll  class="btn btn-warning btn-lg pull-left botaoshake" style="color:#000; font-weight: 500">Gostei, quero uma cotação</a>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-3" id="depoimentos">
    <div class="container" id="diferenciais">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                            <span style="color: #f9d716">
                            HOSPITAIS CREDENCIADOS
                            </span>
                </h1>
            </div>
        </div>
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-saojose.png">
                        </div>
                        <h4 class="info-title">CASA DE SAÚDE SÃO JOSÉ</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-barrador.png">
                        </div>
                        <h4 class="info-title">HOSPITAL BARRA D’OR</h4>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-perinatal.png">
                        </div>
                        <h4 class="info-title">LARANJEIRAS CLIN. PERINATAL</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-saovicente.png">
                        </div>
                        <h4 class="info-title">HOSPITAL SÃO VICENTE DE PAULO</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-riosdor.png">
                        </div>
                        <h4 class="info-title">HOSPITAL RIOS D’OR</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-santalucia.png">
                        </div>
                        <h4 class="info-title">CASA DE SAÚDE SANTA LÚCIA</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-saolucas.png">
                        </div>
                        <h4 class="info-title">HOSPITAL SÃO LUCAS</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-badim.png">
                        </div>
                        <h4 class="info-title">HOSPITAL DR. BADIM</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-chn.png">
                        </div>
                        <h4 class="info-title">COMPLEXO HOSP. NITERÓI - CHN</h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/hospital-sergiofranco.png">
                        </div>
                        <h4 class="info-title">COMPLEXO HOSP. NITERÓI - CHN</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#fff; font-weight: 500; width: 100%; font-size: 1.2em">QUERO ACESSO AOS MELHORES HOSPITAIS</a>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-3" id="depoimentos" style="background-image: url('assets/img/LandingPagesGoldenCross/bg31.jpg');     background-size: cover;
            background-position: center center;">
    <div class="container" id="diferenciais">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <h1 class="title" style="font-size: 3em">
                            <span style="color: #fff">
                            Benefícios: diferenciais Golden Cross
                            </span>
                </h1>
            </div>
        </div>
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="info info-hover">
                        <div class="icon icon-primary">
                            <img src="assets/img/LandingPagesGoldenCross/diferenciais01.png">
                        </div>
                        <h4 class="info-title" style="color: #fff">Descontos em medicamentos</h4>
                        <p class="description" style="color: #fff">Pensando sempre em proporcionar inúmeras vantagens e facilidades aos seus associados, a Golden Cross oferece o Programa Golden de Descontos em Medicamentos, com descontos em diversas farmácias conveniadas.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover">
                        <div class="icon icon-success">
                            <img src="assets/img/LandingPagesGoldenCross/diferenciais02.png">
                        </div>
                        <h4 class="info-title" style="color: #fff">Assistência 24 horas em viagem</h4>
                        <p class="description" style="color: #fff">Oferecemos uma completa assistência aos associados, garantindo a tranquilidade e segurança durante suas viagens pelo Brasil ou pelo exterior.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info info-hover">
                        <div class="icon icon-warning">
                            <img src="assets/img/LandingPagesGoldenCross/diferenciais03.png">
                        </div>
                        <h4 class="info-title" style="color: #fff">Reembolso</h4>
                        <p class="description" style="color: #fff">Além da rede médica contratada, você pode escolher médicos, hospitais e laboratórios que não façam parte do seu plano, pagar pelo atendimento e pedir reembolso.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <a href="#formulario" data-scroll="" class="btn btn-warning btn-lg botaoshake" style="color:#fff; font-weight: 500; width: 100%; font-size: 1.2em">REALIZE SUA COTAÇÃO SEM COMPROMISSO</a>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-3" id="depoimentos">
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto text-center">
            <h1 class="title" style="font-size: 3em">
                        <span style="color: #f9d716">
                        TABELA DE PREÇO ADESÃO<br><br>
                        </span>
                <div class="col-md-12">
                    <img src="assets/img/LandingPagesGoldenCross/tabela-preco.png" class="img-responsive">
                </div>
            </h1>
        </div>
    </div>
</div>
<div id="contato">
    <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/LandingPagesGoldenCross/bg31.jpg');     background-size: cover;
                background-position: center center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="title" style="color:#fff">
                        Ainda tem dúvidas?
                    </h2>
                    <h3 class="description" style="color:#fff">Preencha o formulário e receba contato de um dos nossos atendentes esclarecendo todas as suas dúvidas sobre os Planos Bradesco Saúde. Não leva 1 minuto!
                    </h3>
                    <div class="col-md-12 text-center">
                        <a href="#formulario" data-scroll class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1.2em">PREENCHER FORMULÁRIO</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer " style="background-color:#FFB236; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#000">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
</body>
<?php include("includes/IncludesFooter.php"); ?>
</div>
</html>
