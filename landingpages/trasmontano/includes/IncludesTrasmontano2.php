<div class="d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Plano de Saúde Amil">
                    <img src="assets/img/LandingPageTrasmontano/logo-white.png" width="300">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange" >
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#depoimentos" data-scroll>
                            <p>Depoimentos</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#diferencias" data-scroll>
                            <p>Diferenciais</p>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#contato" data-scroll>
                            <p>Contato</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<div class="header">
    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPageTrasmontano/bg15.jpg'); "></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-left">
                        <h4 class="title"><span  style="font-size: 1.4em; color:#fff">PLANOS À PARTIR DE<br><span class="highlight" style="font-size: 2.4em; color:#fff"> <br> R$ 211,00<span style="font-size: 0.7em">*</span> <br></span></h4>
                        <h3 class="description" style="color:#fff"><span style="font-size: 0.7em; color:#fff">*ESSENCIAL I 100 | 0 a 18 anos - Adesão - Preço por vida.</span></h3>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto" id="formulario">
                        <div class="card card-contact card-raised formshake">

                            <?php include("includes/Form/FormTrasmontano2.php");?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="features-8 section-image" style="background-image: url('assets/img/bg30.jpg')" id="sobrenos">
        <div class="col-md-8 ml-auto mr-auto text-center">
            <h2 class="title">Conheça o Trasmontano</h2>
            <h4 class="description"> Num mercado cada vez mais dinâmico e competitivo é preciso estar apto a superar desafios e identificar novas oportunidades, por isso desenvolvemos equipes profissionais dedicadas e preparadas para garantir a qualidade dos serviços e reafirmar nosso compromisso com o crescimento e a solidez da marca Trasmontano. </h4>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image">
                            <img src="assets/img/LandingPageTrasmontano/bg28.jpg" class="rounded" alt="">

                        </div>
                        <div class="info text-center">

                            <h4 class="info-title">Central de Emergência</h4>
                            <p class="description">24 horas por dia, 7 dias por semana - Orientação Médica por telefone em casos de urgência ou emergência.</p>
                            <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image">
                            <img src="assets/img/LandingPageTrasmontano/bg27.jpg" class="rounded" alt="">
                        </div>
                        <div class="info text-center">

                            <h4 class="info-title">Hospital IGESP</h4>
                            <p class="description">Hospital próprio totalmente planejado para a realização de cirurgias e procedimentos de complexidade.</p>
                            <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image">
                            <img src="assets/img/LandingPageTrasmontano/bg26.jpg" class="rounded" alt="">
                        </div>
                        <div class="info text-center">

                            <h4 class="info-title">Centros médicos</h4>
                            <p class="description">Ambulatórios exclusivos para a realização de consultas e exames nas mais diversas especialidades.</p>
                            <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image">
                            <img src="assets/img/LandingPageTrasmontano/bg25.jpg" class="rounded" alt="">
                        </div>
                        <div class="info text-center">

                            <h4 class="info-title">Medicina Preventiva</h4>
                            <p class="description">Programa Saúde Integral - Roteiro de atividades voltadas para a Promoção da Saúde e Qualidade de Vida.</p>
                            <a href="#formulario" data-scroll="" class="btn btn-danger btn-lg botaoshake">COTAR AGORA</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="pricing-5 section-pricing-5 " id="rede">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto text-center">
                    <h1 class="title" style="font-size: 3em">
                            <span style="color: #08377F">
                            CONHEÇA A REDE CREDENCIADA TRASMONTANO
                            </span>
                    </h1>
                </div>
            </div>
            <br><br>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/hospital-adventista.png">
                            </div>
                            <h4 class="info-title">HOSPITAL ADVENTISTA</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/hospital-bosque.png">
                            </div>
                            <h4 class="info-title">HOSPITAL BOSQUE DA SAÚDE </h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/hospital-igesp.png">
                            </div>
                            <h4 class="info-title">HOSPITAL IGESP</h4>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/hospital-saopaolo.png">
                            </div>
                            <h4 class="info-title">HOSPITAL SAN PAOLO</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; padding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/hospital-itaquera.png">
                            </div>
                            <h4 class="info-title">HOSPITAL ITAQUERA</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info info-hover" style="background-color: #fff; border-radius: 19px; paddingdding: 11px;">
                            <div class="icon icon-primary">
                                <img src="assets/img/LandingPageTrasmontano/muitomais.png">
                                <p class="description"> Hospitais e clínicas </p>
                            </div>
                            <a href="#formulario" data-scroll="" class="btn btn-info btn-lg botaoshake">COTAÇÃO SEM COMPROMISSO</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="pricing-5 section-pricing-5 " style="background-image: url('assets/img/LandingPageTrasmontano/bg30.jpg'); background-size: cover;" id="contato">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <div class="card card-testimonial card-plain">
                    <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                    <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                    <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll>
                        QUERO COTAR AGORA
                    </a>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer " style="background-color:#fff; color:#ED3237">
        <div class="col-md-12">
            <div class="container">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por
                    <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#ED3237">#agênciatrêsmeiazero</a>.
                </div>
            </div>
        </div>
    </footer>
</div>
