<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <?php include("includes/IncludesHeader.php");
    require_once("../helpers/includesPixel.php");
    require_once("../helpers/includesChat.php"); ?>

<body>
<!--Start of Tawk.to Script-->
<div class="js">
<div id="preloader"></div>

<!--End of Tawk.to Script-->
<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank">
                <img src="assets/img/LandingPagesIntermedica/logo-color.png" width="200">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#planos" data-scroll>
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Planos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#sobrenos" data-scroll>
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Sobre Nos </p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#contato" data-scroll>
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Contato</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header">
        <div class="page-header-image header-filter" style="background-image: url('assets/img/LandingPagesIntermedica/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <img src="assets/img/LandingPagesIntermedica/header.png" class="img-responsive">
                            </div>
                            <div class="col-md-6 text-left">
                                <h3 class="description" style="color:#fff">
                                        <span class="description">
                                        Os Planos de saúde Intermédica são ideais para quem procura por uma opção com qualidade e rede de atendimento ampla. É possível contratar o plano de saúde com ou sem coparticipação, inclusive com opção de cobertura local.
                                        </span><br><br>
                                    <span class="description">
                                        Os planos de saúde Intermédica abrangem Rede Própria e Rede Credenciada certificada, com acesso a todas as especialidades médicas e profissionais referenciados.
                                        </span>
                                </h3>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="testimonials-2" style="background-image: url('assets/img/LandingPagesIntermedica/bg15.jpg');" id="depoimento">
        <div class="container">
            <div class="col-md-12 " id="formulario">
                <div class="card card-contact card-raised formshake">
             <!-- [INÍCIO] - Script Integração Formulário Leads - Agência TMZ -->
<!-- Atenção: Não alterar o código abaixo -->
<!-- Dica: Inserir código completo no local onde o formulário deverá ser exibido -->
<iframe id="form-lead-iframe" src="http://sistema.agenciatmz.com.br/parceiro/formulario-lead#?id=12" frameborder="0" style="background-color: transparent; margin:0; width:100%; border:none; overflow: auto;" scrolling="auto"></iframe>
<script type="text/javascript" src="http://sistema.agenciatmz.com.br/parceiro/scripts/iframe-lead.js"></script>
<!-- [TÉRMINO] - Código Integração Formulário Leads - Script TMZ -->
                </div>
            </div>
        </div>
    </div>

    <div class="testimonials-3" id="sobrenos">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto text-left">
                    <h1 class="title" style="font-size: 3em">
                        <span style="color: #e5b463">SOBRE NÓS</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 ">
                    <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">O Grupo NotreDame Intermédica (GNDI) é pioneiro em Medicina Preventiva e oferece as melhores soluções em saúde. </p>
                    <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Fundada há 50 anos em São Paulo (SP), a Intermédica se destaca por oferecer serviços de qualidade nas Redes Própria e Credenciada de Centros Clínicos, Hospitais, prontos-socorros e maternidades.</p>
                    <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Além disso o Grupo NotreDame Intermédica se diferencia pelo relacionamento transparente, ético e comprometido com seus beneficiários. Prestamos serviços personalizados a preços acessíveis.</p>
                    <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Como resultado disso Temos um dos melhores índices de satisfação de atendimento segundo nossos próprios beneficiários, de acordo com indicadores da ANS (Agência Nacional de Saúde Suplementar).</p>
                    <a href="#formulario" data-scroll class="btn btn-success btn-lg pull-left" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
                </div>
                <div class="col-md-7 ">
                    <img src="assets/img/LandingPagesIntermedica/imgPersonagem360.png"  class="img-responsive d-none d-sm-block pull-right"  style="position: relative; bottom: 50px;">
                </div>
            </div>
        </div>
        <div class="container" id="redecredenciada">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto text-right">
                    <h1 class="title" style="font-size: 3em">
                    <span style="color: #e5b463">
                    REDE CREDENCIADA
                    </span>
                    </h1>
                </div>
            </div>
            <div class="row" id="redecredenciada">
                <div class="col-md-7">
                    <img src="assets/img/LandingPagesIntermedica/imgHospitalFlat.png" class="img-responsive pull-left d-none d-sm-block"  style="position: relative; top: 60px;">
                </div>
                <div class="col-md-5 text-right">
                    <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">
                        HOSPITAL MONTEMAGNO<br>
                        HOSPITAL SANTA CECILIA<br>
                        HOSPITAL CRUZEIRO DO SUL<br>
                        HOSPITAL E MATERNIDADE GUARULHOS<br>
                        HOSPITAL NOVA VIDA<br>
                        HOSPITAL E MATERNIDADE INTERMÉDICA ABC<br>
                        HOSPITAL E MATERNIDADE RENASCENÇA<br>
                        HOSPITAL BOSQUE DA SAÚDE<br>
                        HOSPITAL E MATERNIDADE NOSSA SENHORA DO ROSÁRIO<br>
                        HOSPITAL E MATERNIDADE SACRECOEUR<br>
                        HOSPITAL FAMILY<br>
                        HOSPITAL BAETA NEVES<br>
                        HOSPITAL SÃO BERNARDO<br>
                        Mais centenas de Clínicas e Hospitais com diversas especialidades à sua disposição.
                    </p>
                    <a href="#formulario" data-scroll class="btn btn-success btn-lg pull-right" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
                </div>
            </div>
        </div>
    </div>

    <div class="pricing-3"  id="planos">
        <div class="container">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto text-center">
                    <h2 class="title">CONHEÇA AS LINHAS DE PLANO DE SAÚDE DA INTERMÉDICA</h2>
                    <div class="section-space"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card card-pricing"  data-background-color="orange">
                        <div class="card-body">
                            <h2 class="category">
                                Linha Smart
                            </h2>
                            <ul>
                                <li>Assistência médica com alta qualidade e custo acessível</li>
                                <li>Ampla gama de opções para contratação regionalizada com ou sem participação</li>
                                <li>Rede própria certificada e credenciada, com excelência na prestação de serviços</li>
                                <br><br>
                            </ul>
                            <a href="#formulario"  class="btn btn-warning btn-lg btn-round botaoshake">
                                Cotação Online
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-pricing" style="background-color: #D09E3B">
                        <div class="card-body">
                            <h2 class="category" style="color:#fff">
                                Linha Premium
                            </h2>
                            <ul>
                                <li style="color:#fff">Atendimento personalizado com conforto e qualidade</li>
                                <li style="color:#fff">Rede credenciada altamente <br>qualificada</li>
                                <li style="color:#fff">Rede própria certificada e credenciada, com excelência na prestação de serviços</li>
                                <br><br>
                            </ul>
                            <a href="#formulario"  class="btn btn-warning btn-lg btn-round botaoshake">
                                Cotação Online
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-pricing" style="background-color: #9C9C9C">
                        <div class="card-body">
                            <h2 class="category" style="color:#fff">
                                Linha Advance
                            </h2>
                            <ul>
                                <li style="color:#fff">Cobertura em todo território nacional</li>
                                <li style="color:#fff">Rede própria certificada e credenciada com excelência na prestação de serviços</li>
                                <li style="color:#fff">Disponibilidade de reembolso</li>
                                <li style="color:#fff">Assistência viagem nacional</li>
                                <li style="color:#fff">Ala exclusiva de atendimento oncológico</li>
                            </ul>
                            <a href="#formulario"  class="btn btn-warning btn-lg btn-round botaoshake">
                                Cotação Online
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-pricing" style="background-color: #000">
                        <div class="card-body">
                            <h2 class="category" style="color:#fff">
                                Linha Infinity
                            </h2>
                            <ul>
                                <li style="color:#fff">Os melhores serviços e coberturas em todo o País</li>
                                <li style="color:#fff">Check-up anual preventivo</li>
                                <li style="color:#fff">Ala exclusiva de atendimento oncológico</li>
                                <li style="color:#fff">Coleta domiciliar</li>
                                <li style="color:#fff">Courier</li>
                            </ul>
                            <a href="#formulario"  class="btn btn-warning btn-lg btn-round botaoshake">
                                Cotação Online
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonials-2" style="background-image: url('assets/img/LandingPagesIntermedica/bg15.jpg');" id="depoimento">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="carouselExampleIndicators2" class="carousel slide">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="1" class=""></li>
                            <li data-target="#carouselExampleIndicators2" data-slide-to="2" class=""></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item justify-content-center">
                                <div class="card card-testimonial card-plain">
                                    <div class="card-body">
                                        <h5 class="card-description" style="color:#fff">"Temos uma parceria de sucesso com a Intermédica há oito anos, sempre priorizando a qualidade e excelência no atendimento ao nosso maior patrimônio: nossos funcionários e familiares."
                                        </h5>
                                        <h3 class="card-title" style="color: #fff">Milton Martins</h3>
                                        <div class="card-footer">
                                            <h6 class="category text-primary" style="color: #fff">Gerente de Recursos Humanos e Jurídico da Dura Automotive</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item justify-content-center active">
                                <div class="card card-testimonial card-plain">
                                    <div class="card-body">
                                        <h5 class="card-description" style="color:#fff">"A Notredame Intermédica é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata."
                                        </h5>
                                        <h3 class="card-title" style="color: #fff"> Caroline Duque</h3>
                                        <div class="card-footer">
                                            <h6 class="category text-primary" style="color: #fff">Diretora de RH da LATAM</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item justify-content-center ">
                                <div class="card card-testimonial card-plain">
                                    <div class="card-body">
                                        <h5 class="card-description" style="color:#fff">"Na minha opinião, é o único Convênio médico que pratica um preço justo aqui em São Paulo. A Qualidade do serviço e rede credenciada são excelentes, não troco o Plano da minha família por nenhum outro"
                                        </h5>
                                        <h3 class="card-title" style="color: #fff"> Hélio de Oliveira</h3>
                                        <div class="card-footer">
                                            <h6 class="category text-primary" style="color: #fff">Técnico em logística</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                        <i class="now-ui-icons arrows-1_minimal-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                        <i class="now-ui-icons arrows-1_minimal-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="contato">
        <div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/LandingPagesIntermedica/bg3.jpg');     background-size: cover;
                    background-position: center center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="title" style="font-size: 3em">
                                    <span style="color: #fff">
                                        AINDA TEM DÚVIDAS?
                                        <p class="description" style="    color: #fff;font-size: 0.6em">
                                            Solicite uma cotação personalizada abaixo e Encontre o ideal para você!
                                    </span>
                        </h1>
                        <div class="col-md-12 text-center">
                            <a href="#formulario" data-scroll  class="btn btn-warning btn-lg botaoshake" style="color:#000; font-weight: 500; width: 100%; font-size: 1.2em">
                                SOLICITE UMA COTAÇÃO SEM COMPROMISSO!
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer " style="background-color:#c77800; color:#fff">
        <div class="col-md-12">
            <div class="container">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por
                    <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
                </div>
            </div>
        </div>
    </footer>
</body>
</div>
<?php include("includes/IncludesFooter.php"); ?>
</html>
