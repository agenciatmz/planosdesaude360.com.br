<?php require_once("../helpers/includesConexaoBanco.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link
            rel="shortcut icon"
            href="assets/img/favicon.ico"
            type="image/ico"
            />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>PROMOÇÃO RELAMPAGO</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/now-ui-kit.css?v=1.2.0" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../helpers/includesPixel.php"); ?>

<?php require_once("../helpers/includesChat.php"); ?>
    </head>
    <body class="template-page">
        <div class="js">
            <div id="preloader"></div>
            <!-- Navbar -->
            <div class="header-2">
                <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                    <div class="container">
                        <div class="navbar-translate">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                            </button>
                            <img src="assets/img/logo-color.png">
                        </div>
                        <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#quemsomos" data-scroll>
                                        <i class="now-ui-icons business_badge" aria-hidden="true"></i>
                                        <p>Sobre nós</p>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#planos" data-scroll>
                                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                                        <p>Opções de Planos</p> 
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#contato" data-scroll data-toggle="modal" data-target="#loginModal">
                                        <i class="now-ui-icons business_briefcase-24" aria-hidden="true"></i>
                                        <p>Cotação Online</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="page-header header-filter">
                    <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
                    <div class="content-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12" id="formulario">
                                    <img src="assets/img/header-first.png" width="500" class="img-responsive">
                                </div>
                                <div class="col-md-12 ">
                                    <h1 class="title" style="font-size: 2em;"><span style="color:#fff">PROCURANDO UM PLANO DE SAÚDE?<br> <span style="font-weight: 300"> SOLICITE UMA COTAÇÃO EM NOSSO SITE E GARANTA O MENOR PREÇO</span> </h1>
                                    </span>
                                    <h5 class="description" style="font-weight: 300; color:#fff">
                                        O Saúde 360 é um portal de dicas e noticias sobre saúde. São anos de Transparência e Dedicação, para levar à você informações uteis para uma vida mais saudável. Em parceria com algumas das maiores corretoras e operadoras de Plano de Saúde do Brasil, desenvolvemos essa página de cotação online. Aqui você envia seus dados e nós encaminhamos para o corretor com o melhor preço no Plano de Saúde que você deseja! Não é perfeito?
                                    </h5>
                                    <button class="btn btn-warning btn-lg text-center" style="color:#000; font-weight: 800" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Generico';">Solicitar Cotação</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cd-section" id="quemsomos">
                <!--     *********     FEATURES 1      *********      -->
                <div class="features-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 mr-auto ml-auto">
                                <h2 class="title"><span style="font-weight:400" class="azulclaro">ENCONTRE O PLANO </span> <br><span style="font-weight:800" class="azulescuro">IDEAL PARA VOCÊ</span></h2>
                                <h4 class="description text-justify">
                                  Todo ano seu Plano de Saúde aumenta o valor da mensalidade e o Orçamento está ficando apertado para tantos reajustes?
                                  Nós temos a Solução!
                                                                      <br><br>
                                                                      Ainda não tem Plano de Saúde mas sabe que precisa e Procura algo que caiba no seu orçamento?
                                                                      Nós temos a Solução!
                                                                        <br><br>
                                                                        Saiu do emprego e não quer ficar sem Plano, procura algo que caiba no seu bolso e mantenha a qualidade de vida da sua família?
                                                                        Nós temos a solução!

                                                                          <br><br>
                                                                          Está Insatisfeito no seu Plano atual e procura um upgrade para um Plano melhor mantendo a relação custo benefício?
                                                                          Nós também temos a Solução!

                                                                            <br><br>
                                                                            Só aqui você encontra a opção de Plano de Saúde que mais se adapta ao seu perfil, solicite uma cotação nessa página que um corretor autorizado entrará em contato com a melhor proposta para você.
  <br><br>

  Confira às opções de Plano de Saúde abaixo.

                                                                                                    </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <!--     *********    END FEATURES 1      *********      -->
            </div>
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE <br>PLANOS DE SAÚDE </span> <span style="font-weight:800;color:#FF3636">MELHOR CUSTO BENEFÍCIO</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #56B9FF">
                                <div class="info">
                                    <div class="description">
                                        <div class="alert alert-info text-center" role="alert" style="margin-left: 30px;
                                            margin-right: 30px;">
                                            <div class="container">
                                                À partir de <strong>R$ 307,81*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                            </div>
                                        </div>
                                        <img src="assets/img/operadoras/logo-amil.png" width="180px"><br><br>
                                        <!-- <h4 style="font-weight:800; color:#014171;   padding-top:28px;">AMIL</h4> -->
                                            <p style="color:#014171">A opção com melhor custo beneficio do mercado, ampla rede credenciada, cobertura nacional e médicos de extrema qualidade.</p>
                                        <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal"  onclick="document.getElementById('operadora').value='Amil';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid red">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-danger text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 316,00*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-bradesco.png" width="180px"><br><br>
                                            <p style="color:#014171">Alguns especialistas consideram o Bradesco Saúde como uns dos melhores do Brasil, por isso é o convênio médico que mais cresce no pais. </p>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Bradesco';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #FFC15E">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-warning text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 306,82*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-sulamerica.png" width="180px"><br><br>
                                        <!-- <h4 style="font-weight:800; color:#014171;padding-top:6px;">SULAMÉRICA</h4> -->
                                            <p style="color:#014171">São mais de 100 anos oferecendo soluções em seguros para a família brasileira, quem tem Sulamérica não troca por nenhum outro.</p>
                                        <button class="btn btn-warning ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Sulamérica';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE <br>PLANOS DE SAÚDE </span> <span style="font-weight:800;color:#FF3636">INDIVIDUAL/FAMILIAR</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #FFC15E">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-warning text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 190,08*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-next.png">
                                         <!-- <h4 style="font-weight:800; color:#014171;   padding-top: 18px;">NEXT</h4> -->
                                            <p style="color:#014171">A opção mais em conta do grupo AMIL, cote com seu CPF plano individual ou familiar. Atendimento regional e de qualidade.</p>
                                        <button class="btn btn-warning ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Next';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #FF5E5E">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-danger text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 127,98*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-santa.png">
                                        <!-- <h4 style="font-weight:800; color:#014171 ;   padding-top: 18px;">SANTA HELENA</h4> -->
                                            <p style="color:#014171">A Santa Helena acredita que cuidar da sua saúde significa pensar que é sempre possível fazer mais pelo seu bem-estar e tranquilidade.</p>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Santa Helena';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #46D83F">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-success text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 114,77*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-uni.png">
                                        <!-- <h4 style="font-weight:800; color:#014171;padding-top:6px;">UNIHOSP</h4> -->
                                            <p style="color:#014171">Mora no ABCD, Mauá ou Ribeirão Pires? Esse é o plano mais indicado para você. Cote agora e não perca essa oportunidade.</p>
                                        <button class="btn btn-success ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='UniHosp';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--     *********    END FEATURES 1      *********      -->
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE <br>PLANOS DE SAÚDE </span> <span style="font-weight:800;color:#FF3636">PREMIUM</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #56B9FF">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-info text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 1813,84*</strong> <br> <span style="font-size:12px">Preço médio do plano individual</span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-omint.png" width="120px">
                                        <!-- <h4 style="font-weight:800; color:#014171">OMINT</h4> -->
                                            <p style="color:#014171">Quer acesso a hospitais como Sírio-Libanês ou Albert Einstein com atendimento diferenciado? Esse é o plano ideal para você.</p>
                                        <button class="btn btn-info ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Omint';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #FF5E5E">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-danger text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 1025,71*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-one.png" width="120px">
                                        <!-- <h4 style="font-weight:800; color:#014171 ;   padding-top: 18px;">ONE HEALTH</h4> -->
                                            <p style="color:#014171">Esse é o plano PREMIUM do grupo AMIL, considerado o melhor do pais na atualidade, para você ter uma ideia ele cobre até transplantes. </p>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='One Health';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #46D83F">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-success text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 481,84*</strong> <br> <span style="font-size:12px">0 a 18 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-lincx.png" width="120px">
                                        <!-- <h4 style="font-weight:800; color:#014171;padding-top:6px;">LINCX</h4> -->
                                            <p style="color:#014171">A Lincx também pertence ao grupo AMIL, possibilita acesso a laboratórios como Fleury, cobertura de vacinas, resgate saúde e muito mais.</p>
                                        <button class="btn btn-success ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Lincx';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="features-4" id="planos">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="title"><span style="font-weight:400" class="azulclaro">CONFIRA AS OPÇÕES DE <br>PLANOS DE SAÚDE PARA</span> <span style="font-weight:800;color:#FF3636">3ª IDADE</span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid red">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-danger text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 375,11*</strong> <br> <span style="font-size:12px">54 a 58 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-biovida.png" width="120px">
                                        <!-- <h4 style="font-weight:800; color:#014171">BIOVIDA</h4> -->
                                            <p style="color:#014171">O plano com o menor custo para você que está na melhor idade, atendimento regional em Sâo Paulo e algumas cidades da região.</p>
                                        <button class="btn btn-danger ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Biovida';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #56B9FF">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-info text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                              À partir de <strong>R$ 582,00*</strong> <br> <span style="font-size:12px">49 a 53 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-prevent.png" width="120px">
                                        <!-- <h4 style="font-weight:800; color:#014171 ;   padding-top: 18px;">PREVENT SÊNIOR</h4> -->
                                            <p style="color:#014171">Considerado por muitos como o melhor custo beneficio para quem é sênior. Rede própria com atendimento de extrema qualidade.</p>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Prevent Sênior';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-background card-raised" data-background-color="" style="border-top: 4px solid #46D83F">
                                <div class="info">
                                    <div class="description">
                                      <div class="alert alert-success text-center" role="alert" style="margin-left: 30px;
                                          margin-right: 30px;">
                                          <div class="container">
                                            À partir de <strong>R$ 443,73*</strong> <br> <span style="font-size:12px">49 a 53 anos </span>
                                          </div>
                                      </div>
                                        <img src="assets/img/operadoras/logo-green.png" width="120px"><br><br>
                                        <!-- <h4 style="font-weight:800; color:#014171">GREEN LINE</h4> -->
                                            <p style="color:#014171">Uma opção com preço competitivo para quem mora em São Paulo e é idoso, rede própria de qualidade e preço justo.</p>
                                        <button class="btn btn-success ml-3" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Green Line';">Solicitar Cotação</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="testimonials-2 text-center" style="background-color:#f0f0f0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="carouselExampleIndicators2" class="carousel slide">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                                </ol>
                                <h2 class="title"><span style="font-weight:400" class="azulclaro">VEJA O QUE NOSSOS CLIENTES</span> <br><span style="font-weight:800" class="azulescuro">FALAM DE NÓS</span></h2>
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento01.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">Sempre que precisei da Amil Next nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios, atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito.
                                                </h5>
                                                <h3 class="card-title">Renata Bosolli</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary">Cliente Amil Next</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento02.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">Gosto bastante, eu já tinha o seguro de carro com a Bradesco né? Depois que mudei de emprego, precisei do saúde e não tenho oque reclamar.
                                                </h5>
                                                <h3 class="card-title">Maria Ferreira</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary">Cliente Bradesco</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/person-05.jpg" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">
                                        Sempre que precisei usar o Plano nunca tive problemas, pelo contrário, a rede de hospital é ampla e os atendimentos são rápidos tanto em hospitais próximos como em outras cidades. A facilidade no agendamento das consultas também é fenomenal..


                                                </h5>
                                                <h3 class="card-title">Roberta Nobre</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary">Cliente Biovida</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item justify-content-center">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <a href="#pablo">
                                                <img class="img img-raised rounded" src="assets/img/imgDepoimento03.png" width="220">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">Não é bom, é excelente! Custo beneficio e atendimento ótimos, já tive experiências com outros Planos, sem dúvidas, fico com a Sulamerica.
                                                </h5>
                                                <h3 class="card-title">Marcelo Oliveira</h3>
                                                <div class="card-footer">
                                                    <h6 class="category text-primary">Cliente Sulamérica</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                <i class="now-ui-icons arrows-1_minimal-left"></i>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                                <i class="now-ui-icons arrows-1_minimal-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pricing-5 section-pricing-5 " style="background-color:#014171" id="contato">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <div class="card card-testimonial card-plain">
                            <h2 class="title" style="color:#fff">AINDA TEM DÚVIDAS?</h2>
                            <p class="card-description" style="color:#fff">Solicite uma cotação e saiba mais sobre os Planos  </p>
                            <a href="#formulario" class="btn btn-info btn-round btn-lg botaoshake" data-scroll="" data-toggle="modal" data-target="#loginModal" onclick="document.getElementById('operadora').value='Generico';">
                            QUERO COTAR AGORA
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer ">
                <div class="container">
                    <nav>
                        <ul>
                            <li class="nav-item active">
                                <a class="nav-link" href="#quemsomos">
                                Sobre nós
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#planos">
                                Opções de Planos
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#" data-toggle="modal" data-target="#loginModal">
                                Cote agora
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                    </div>
                </div>
            </footer>
    </body>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="card card-login card-plain">
    <div class="modal-header justify-content-center">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
    <i class="now-ui-icons ui-1_simple-remove"></i>
    </button>
    </div>
    <div class="modal-body">
      <?php include("../helpers/insereBanco.php"); ?>

    <?php include("includes/Form/FormOfertasRelampago.php");?>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!--  End Modal -->
    <?php include("includes/IncludesFooter.php"); ?>
    </div>
</html>
