<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title></title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <!-- CSS Files -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../../assets/css/obrigado.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../../assets/css/demo.css" rel="stylesheet" />
        <?php require_once("../../../helpers/includesPixel.php"); ?>
        <!-- Google Code for leads Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 850855984;
        var google_conversion_label = "14JVCKW85HEQsJDclQM";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/850855984/?label=14JVCKW85HEQsJDclQM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>

    </head>
    <body class="template-page">
        <!-- Navbar -->
        <div class="header-2">
            <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
                <div class="container">
                    <div class="navbar-translate">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#example-navbar-primary" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                        </button>
                        <img src="../../assets/img/logo-color.png">
                    </div>
                    <div class="collapse navbar-collapse" id="example-navbar-primary" data-nav-image="./assets/img/blurred-image-1.jpg">

                    </div>
                </div>
            </nav>
            <div class="page-header header-filter">
                <div class="page-header-image" style="background-image: url('../../assets/img/bg1.png');"></div>
                <div class="content-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2 class="title" style="font-size: 4em;"><span class="azulclaro">Obrigado</span></h2>
                                <h4 class="description azulescuro" style="font-weight: 500">
                                    <span class="azulescuro">Em breve um de nossos consultores entrará em contato!</span>
                                </h4>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



        <footer class="footer ">
            <div class="container">

                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Desenvolvido por <a href="https://agenciatresmeiazero.com.br" target="_blank"> #agênciatresmeiazero</a>.
                </div>
            </div>
        </footer>
    </body>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
    <script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
    <script src="../../assets/js/plugins/jquery.validate.min.js"></script>
    <script src="../../assets/js/plugins/additional-methods.min.js"></script>
    <script src="../../assets/js/plugins/sweet-scroll.min.js"></script>
    <script src="../../assets/js/plugins/sweetalert2.all.js"></script>
    <script>$(document).ready(function () {
            swal({ title: "Obrigado", text: "Em breve nossos consultores entrará em contato\n", type: "success" });
        });
    </script>
    <script src="../../assets/js/validation.js"></script>
    <script src="../../assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/api-estado-cidade.js"></script>
    <script src="../../assets/js/plugins/custom.js"></script>
    <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/moment.min.js"></script>
    <script src="../../assets/js/plugins/bootstrap-switch.js"></script>
    <script src="../../assets/js/plugins/bootstrap-tagsinput.js"></script>
    <script src="../../assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script src="../../assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../../assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
</html>
