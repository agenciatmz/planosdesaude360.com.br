<?php
// make sure to not include translations
$args['presets']['default'] = array(
	'title' => 'Default',
	'demo' => 'http://demo.mythemeshop.com/socialnow/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/default/thumb.jpg',
	'menus' => array( 'primary-menu' => 'Top Menu', 'secondary-menu' => 'Main Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts' ),
);

$args['presets']['fashion'] = array(
	'title' => 'Fashion',
	'demo' => 'http://demo.mythemeshop.com/socialnow-fashion/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/fashion/thumb.jpg',
	'menus' => array( 'primary-menu' => 'Top Menu', 'secondary-menu' => 'Main Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page' ), // To set static front page
);

$args['presets']['health'] = array(
	'title' => 'Health',
	'demo' => 'http://demo.mythemeshop.com/socialnow-health/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/health/thumb.jpg',
	'menus' => array( 'primary-menu' => 'Top Menu', 'secondary-menu' => 'Main Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page' ), // To set static front page
);

$args['presets']['news'] = array(
	'title' => 'News',
	'demo' => 'http://demo.mythemeshop.com/socialnow-news/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/news/thumb.jpg',
	'menus' => array( 'primary-menu' => 'Top Menu', 'secondary-menu' => 'Main Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page' ), // To set static front page
);

$args['presets']['viral'] = array(
	'title' => 'Viral',
	'demo' => 'http://demo.mythemeshop.com/socialnow-viral/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/viral/thumb.jpg',
	'menus' => array( 'primary-menu' => 'Top Menu', 'secondary-menu' => 'Main Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page' ), // To set static front page
);

global $mts_presets;
$mts_presets = $args['presets'];
