<?php
$mts_options = get_option(MTS_THEME_NAME);
if ( empty( $mts_options['mts_full_featured_slider_cat'] ) ) {
	$mts_options['mts_full_featured_slider_cat'] = array();
}
if ( empty( $mts_options['mts_featured_slider_cat'] ) ) {
	$mts_options['mts_featured_slider_cat'] = array();
}
get_header(); ?>

<?php if ( is_home() && $mts_options['mts_full_featured_slider'] == '1' && !is_paged() ) { ?>
	<div class="full-slider-container clearfix">
		<?php $mts_full_slider_nav_option = $mts_options['mts_full_slider_nav_option']; ?>
		<div class="big-image-container">
			<div id="<?php echo ( $mts_full_slider_nav_option == 'image_navigation') ? 'big-image' : 'big-image1'; ?>" class="slider-big-image owl-carousel loading">
				<?php
				$mts_full_slider = $mts_options['mts_full_featured_slider_cat'];
				$item_count = $mts_options['mts_full_featured_slider_num'];
				// prevent implode error
				if ( empty( $mts_full_slider ) || !is_array( $mts_full_slider ) ) {
					   $mts_full_slider = array('0');
				}
				$slider_cat = implode( ",", $mts_full_slider );
				$slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page='.$item_count);
				while ( $slider_query->have_posts() ) : $slider_query->the_post(); ?>
				<?php $i=0; ?>
				<div class="slider-item slider-item-<?php echo ++$i; ?>" style="background: url(<?php echo mts_get_thumbnail_url('socialnow-fullslider') ?>); background-position: center center; background-repeat:no-repeat; background-size:cover; min-height:550px">
					<div class="container">
						<a href="<?php echo esc_url( get_the_permalink() ); ?>">   
							<div class="slide-caption">
								<div class="thecategory">
									<?php $category = get_the_category(); echo $category[0]->cat_name; ?>
								</div>
								<?php if ( $mts_full_slider_nav_option == 'image_navigation' ) { ?>
									<div class="slide-caption-inner">
										<h2 class="title"><?php the_title(); ?></h2>
											<?php
											if( $post->post_content != "" ) { ?>
												<div class="slide-content">
													<?php echo mts_excerpt(); ?>
												</div>
										  <?php  } ?>	
										<div class="post-info">
											<span class="theauthor"><?php _e('by','socialnow'); ?>&nbsp;<span><?php the_author(); ?></span></span>
										</div>
									</div>	
								<?php } 
								else { ?>
									<h2 class="title"><?php the_title(); ?></h2>
									<div class="post-info">
										<span class="theauthor"><?php _e('by','socialnow'); ?>&nbsp;<span><?php the_author(); ?></span></span>
									</div>
							 <?php  } ?>
							</div>
						</a>		  
					</div>  
				</div>	
				<?php endwhile; wp_reset_postdata(); ?>
			</div>		
		</div><!-- .big-image-container -->
		<div class="thumb-container">
			<div class="container">
				<div id="<?php echo ( $mts_full_slider_nav_option == 'image_navigation' ) ? 'thumb' : 'thumb1'; ?>" class="slider-thumb owl-carousel">
					<?php
						$slider_cat = implode( ",", $mts_options['mts_full_featured_slider_cat'] );
						$slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page='.$item_count);
						while ( $slider_query->have_posts() ) : $slider_query->the_post();
						?>
							<div class="item"> 
							 <?php if ( $mts_full_slider_nav_option == 'image_navigation' ) { ?>
							   <?php the_post_thumbnail('socialnow-thumbslider',array('title' => '')); ?> 
							<?php  }
							else { ?>
								<h4 class="title"><?php the_title(); ?></h4>
								<div class="post-info">
								  <span class="theauthor"><?php _e('by','socialnow'); ?>&nbsp;<span><?php the_author(); ?></span></span>
								</div>
							  <?php  } ?>
							</div>
						<?php endwhile; wp_reset_postdata(); ?>
				</div>
		  </div>  
		</div>
	</div><!-- .full-slider-container -->

<?php } ?>
<div id="page">
	<div class="article">
		<div id="content_box">
			<?php if ( !is_paged() ) { ?>
				<?php if ( is_home() && $mts_options['mts_featured_slider'] == '1' && $mts_options['mts_featured_slider_position'] == 'above-posts' ) { ?>
						<div class="primary-slider-container clearfix loading">
							<div id="slider" class="primary-slider">
							<?php if ( empty( $mts_options['mts_custom_slider'] ) ) { ?>
								<?php
								$mts_featured_slider = $mts_options['mts_featured_slider_cat'];
								// prevent implode error
								if ( empty( $mts_featured_slider ) || !is_array( $mts_featured_slider) ) {
									$mts_featured_slider = array('0');
								}

								$slider_cat = implode( ",", $mts_featured_slider );
								$slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page='.$mts_options['mts_featured_slider_num']);
								while ( $slider_query->have_posts() ) : $slider_query->the_post();
								?>
								<div class="primary-slider-item"> 
									<a href="<?php echo esc_url( get_the_permalink() ); ?>">
										<?php the_post_thumbnail('socialnow-featuredfull',array('title' => '')); ?>
										<div class="slide-caption">
											<div class="thecategory">
												<?php $category = get_the_category(); echo $category[0]->cat_name; ?>
											</div>
											<h2 class="title"><?php the_title(); ?></h2>
											<div class="post-info">
												<span class="theauthor"><?php _e('by','socialnow'); ?>&nbsp;<span><?php the_author(); ?></span></span>
											</div>
										</div>
									</a> 
								</div>
								<?php endwhile; wp_reset_postdata(); ?>
							<?php } else { ?>
								<?php foreach( $mts_options['mts_custom_slider'] as $slide ) : ?>
									<div class="primary-slider-item">
										<a href="<?php echo esc_url( $slide['mts_custom_slider_link'] ); ?>">
											<?php echo wp_get_attachment_image( $slide['mts_custom_slider_image'], 'socialnow-featuredfull', false, array('title' => '') ); ?>
											<div class="slide-caption">
												<h2 class="title"><?php echo esc_html( $slide['mts_custom_slider_title'] ); ?></h2>
											</div>
										</a>
									</div>
								<?php endforeach; ?>
							<?php } ?>
							</div><!-- .primary-slider -->
						</div><!-- .primary-slider-container -->
				<?php } ?>	

				<?php if ( !empty( $mts_options['mts_grid4_category'] ) && $mts_options['mts_grid4_posts'] == 1 ) { ?>
					<div class="grid-4">
						<?php $category_id = implode(",", $mts_options['mts_grid4_category']);
						$cat_query = new WP_Query('cat='.$category_id.'&posts_per_page=4&ignore_sticky_posts=1');
						if ($cat_query->have_posts()) : while ($cat_query->have_posts()) : $cat_query->the_post(); ?>
							<article class="latestPost">
								<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="post-image post-image-left">
									<?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('socialnow-featured-cat',array('title' => '')); echo '</div>'; ?>
									<?php if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
								</a>			
								<header>
									<h2 class="title front-view-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h2>
									<?php mts_the_postinfo(); ?>
								</header>
							</article>
						<?php endwhile; endif; ?>
					</div>
				<?php } ?>

				<div class ="grid-1">
					<?php $featured_categories = array();
					if ( !empty( $mts_options['mts_featured_categories'] ) ) {
						foreach ( $mts_options['mts_featured_categories'] as $section ) {
							$category_id = $section['mts_featured_category'];
							$featured_categories[] = $category_id;
							$posts_num = $section['mts_featured_category_postsnum'];
							if ( 'latest' == $category_id ) { ?>
								<h3 class="featured-category-title"><?php _e('Latest News', 'socialnow' ); ?></h3>
								<?php $j = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<article class="latestPost excerpt">
										<?php mts_archive_post(); ?>
									</article>
								<?php $j++; endwhile; endif; ?>
								<?php if ( $j !== 0 ) { // No pagination if there is no posts ?>
									<?php mts_pagination(); ?>
								<?php } ?>
								
							<?php } else { // if $category_id != 'latest': ?>
								<h3 class="featured-category-title"><a href="<?php echo esc_url( get_category_link( $category_id ) ); ?>" title="<?php echo esc_attr( get_cat_name( $category_id ) ); ?>"><?php echo esc_html( get_cat_name( $category_id ) ); ?></a></h3>
								<?php
								$j = 0;
								$cat_query = new WP_Query('cat='.$category_id.'&posts_per_page='.$posts_num);
								if ( $cat_query->have_posts() ) : while ( $cat_query->have_posts() ) : $cat_query->the_post(); ?>
									<article class="latestPost excerpt">
										<?php mts_archive_post(); ?>
									</article>
								<?php $j++;
								endwhile; endif; wp_reset_postdata(); ?>
							<?php }
						}
					} ?>
				</div>
			<?php } else { //Paged ?>
				<div class ="grid-1">
					<?php $j = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article class="latestPost excerpt">
						<?php mts_archive_post(); ?>
					</article>
					<?php $j++; endwhile; endif; ?>
					<?php if ( $j !== 0 ) { // No pagination if there is no posts ?>
						<?php mts_pagination(); ?>
					<?php } ?>
				</div>
			<?php } ?>

			<?php if ( is_home() && !is_paged() && $mts_options['mts_featured_slider'] == '1' && $mts_options['mts_featured_slider_position'] == 'below-posts' ) { ?>
				<div class="primary-slider-container primary-slider-below clearfix loading">
					<div id="slider" class="primary-slider">
					<?php if ( empty( $mts_options['mts_custom_slider'] ) ) { ?>
						<?php
						$mts_featured_slider = $mts_options['mts_featured_slider_cat'];
						// prevent implode error
						if ( empty( $mts_featured_slider ) || !is_array( $mts_featured_slider) ) {
							$mts_featured_slider = array('0');
						}

						$slider_cat = implode( ",", $mts_featured_slider );
						$slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page='.$mts_options['mts_featured_slider_num']);
						while ( $slider_query->have_posts() ) : $slider_query->the_post();
						?>
						<div class="primary-slider-item"> 
							<a href="<?php echo esc_url( get_the_permalink() ); ?>">
								<?php the_post_thumbnail('socialnow-featuredfull',array('title' => '')); ?>
								<div class="slide-caption">
									<div class="thecategory">
										<?php $category = get_the_category(); echo $category[0]->cat_name; ?>
									</div>
									<h2 class="title"><?php the_title(); ?></h2>
									<div class="post-info">
										<span class="theauthor"><?php _e('by','socialnow'); ?>&nbsp;<span><?php the_author(); ?></span></span>
									</div>
								</div>
							</a> 
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
					<?php } else { ?>
						<?php foreach( $mts_options['mts_custom_slider'] as $slide ) : ?>
							<div class="primary-slider-item">
								<a href="<?php echo esc_url( $slide['mts_custom_slider_link'] ); ?>">
									<?php echo wp_get_attachment_image( $slide['mts_custom_slider_image'], 'socialnow-featuredfull', false, array('title' => '') ); ?>
									<div class="slide-caption">
										<h2 class="title"><?php echo esc_html( $slide['mts_custom_slider_title'] ); ?></h2>
									</div>
								</a>
							</div>
						<?php endforeach; ?>
					<?php } ?>
					</div><!-- .primary-slider -->
				</div><!-- .primary-slider-container -->
			<?php } ?>	
		</div>
	</div>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>