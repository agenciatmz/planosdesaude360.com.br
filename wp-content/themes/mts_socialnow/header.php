<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 */
?>
<!DOCTYPE html>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-3713967785795249",
    enable_page_level_ads: true
  });
</script>
<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<html class="no-js" <?php language_attributes(); ?>>
<head itemscope itemtype="http://schema.org/WebSite">
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php mts_meta(); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
<meta name="msvalidate.01" content="BA5B51015EE1C67E23980098C153848F" />
</head>
<body id="blog" <?php body_class('main'); ?> itemscope itemtype="http://schema.org/WebPage">	   
	<div class="main-container">
		<header id="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
				<div class="upper-navigation">
					<div class="container clearfix">
						<?php if ( !empty($mts_options['mts_header_social']) && is_array($mts_options['mts_header_social']) && !empty($mts_options['mts_social_icon_head'])) { ?>
								<div class="header-social">
									<?php foreach( $mts_options['mts_header_social'] as $header_icons ) : ?>
										<?php if( ! empty( $header_icons['mts_header_icon'] ) && isset( $header_icons['mts_header_icon'] ) && ! empty( $header_icons['mts_header_icon_link'] )) : ?>
											<a href="<?php print $header_icons['mts_header_icon_link'] ?>" class="header-<?php print $header_icons['mts_header_icon'] ?>" target="_blank"><span class="fa fa-<?php print $header_icons['mts_header_icon'] ?>"></span></a>
										<?php endif; ?>
									<?php endforeach; ?>
								</div>
							<?php } ?>
						<?php if ( $mts_options['mts_show_primary_nav'] == '1' ) { ?>
							<div id="primary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<?php if ( $mts_options['mts_show_secondary_nav'] !== '1' ) {?><a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'socialnow' ); ?></a><?php } ?>
								<nav class="navigation clearfix<?php if ( $mts_options['mts_show_secondary_nav'] !== '1' ) echo ' mobile-menu-wrapper'; ?>">
									<?php if ( has_nav_menu( 'primary-menu' ) ) { ?>
										<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
									<?php } else { ?>
										<ul class="menu clearfix">
											<?php wp_list_pages('title_li='); ?>
										</ul>
									<?php } ?>
								</nav>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php if ( $mts_options['mts_sticky_nav'] == '1' ) { ?>
				<div class="clear" id="catcher"></div>
					<div id="header" class="sticky-navigation">
			<?php } else { ?>
				<div id="header">
			<?php } ?>
				<?php if ( !empty($mts_options['mts_header_search']) ) { ?>
					<div class="overlay overlay-slideleft">
						<div class="search-row">
							<form method="get" id="searchform" class="search-form" action="<?php echo home_url(); ?>" _lpchecked="1">
								<button type="button" class="overlay-close"><svg enable-background="new 0 0 100 100" id="Layer_1" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polygon fill="#fff" points="77.6,21.1 49.6,49.2 21.5,21.1 19.6,23 47.6,51.1 19.6,79.2 21.5,81.1 49.6,53 77.6,81.1 79.6,79.2   51.5,51.1 79.6,23 "/></svg></button>
								<input type="text" name="s" id="s" value="<?php the_search_query(); ?>" placeholder="<?php _e('Type to search...','socialnow'); ?>" <?php if (!empty($mts_options['mts_ajax_search'])) echo ' autocomplete="off"'; ?>/>
							</form>
						</div>
					</div>
				<?php } ?>
				<div class="container clearfix">	
					<div class="logo-wrap">
						<?php if ( $mts_options['mts_logo'] != '' && $mts_logo = wp_get_attachment_image_src( $mts_options['mts_logo'], 'full' ) ) { ?>
							<?php if ( is_front_page() || is_home() || is_404() ) { ?>
								<h1 id="logo" class="image-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $mts_logo[0] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="<?php echo esc_attr( $mts_logo[1] ); ?>" height="<?php echo esc_attr( $mts_logo[2] ); ?>"></a>
								</h1><!-- END #logo -->
							<?php } else { ?>
								<h2 id="logo" class="image-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>">
										<img src="<?php echo esc_url( $mts_logo[0] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="<?php echo esc_attr( $mts_logo[1] ); ?>" height="<?php echo esc_attr( $mts_logo[2] ); ?>"></a>
								</h2><!-- END #logo -->
							<?php } ?>

						<?php } else { ?>

							<?php if ( is_front_page() || is_home() || is_404() ) { ?>
								<h1 id="logo" class="text-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
								</h1><!-- END #logo -->
							<?php } else { ?>
								<h2 id="logo" class="text-logo" itemprop="headline">
									<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
								</h2><!-- END #logo -->
							<?php } ?>
						<?php } ?>
					</div>				
					<?php if ( !empty($mts_options['mts_header_search']) ) { ?>
					   <div class="search-style-one">
							<a id="trigger-overlay"><i class="fa fa-search"></i></a>							
						</div>
					<?php } ?>
					<?php if ( $mts_options['mts_show_secondary_nav'] == '1' ) { ?>
						<div id="secondary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
							<a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'socialnow' ); ?></a>
							<?php if ( has_nav_menu( 'mobile' ) ) { ?>
								<nav class="navigation clearfix">
									<?php if ( has_nav_menu( 'secondary-menu' ) ) { ?>
										<?php wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
									<?php } ?>
								</nav>
								<nav class="navigation mobile-only clearfix mobile-menu-wrapper">
									<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
								</nav>
							<?php } else { ?>
								<nav class="navigation clearfix mobile-menu-wrapper">
									<?php if ( has_nav_menu( 'secondary-menu' ) ) { ?>
										<?php wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
									<?php } ?>
								</nav>
							<?php } ?>			
						</div> 
					<?php } ?> 
				</div>
			</div><!--#header-->
		</header>