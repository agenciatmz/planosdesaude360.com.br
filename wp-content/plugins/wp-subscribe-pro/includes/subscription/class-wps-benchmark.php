<?php
/**
 * BenchmarkEmail Subscription
 */

class WPS_Subscription_Benchmark extends WPS_Subscription_Base {

	public function init( $user, $pass ) {

		require_once 'libs/bmeapi.php';
		return new BMEAPI( $user, $pass );
	}

	public function get_lists( $user, $pass ) {

		$api = $this->init( $user, $pass );
		$result = $api->listGet( '', 1, 50, '', '' );

		$lists = array();
		foreach( $result as $id => $list ) {
			$lists[ $list['id'] ] = $list['listname'];
		}

		return $lists;
	}

    public function subscribe( $identity, $options ) {

		$api = $this->init( $options['user'], $options['pass'] );

		$vars = array();
		$vars['email'] = $identity['email'];

        if ( !empty( $identity['name'] ) ) {
            $vars['firstname'] = $identity['name'];
        }

		try {
			$double_optin = isset( $options['double_optin'] ) && $options['double_optin'] ? 1 : 0;
			$api->listAddContactsOptin( $options['list_name'], array( $vars ), $double_optin );
		}
		catch( Exception $e ) {
			throw new Exception ( $e->getMessage() );
		}

		return array(
			'status' => 'subscribed'
		);
	}

	public function get_fields() {

		$fields = array(
			'benchmark_user' => array(
				'id'    => 'benchmark_user',
				'name'  => 'benchmark_user',
				'type'  => 'text',
				'title' => esc_html__( 'BenchmarkEmail Username', 'wp-subscribe' ),
			),

			'benchmark_pass' => array(
				'id'    => 'benchmark_pass',
				'name'  => 'benchmark_pass',
				'type'  => 'text',
				'title' => esc_html__( 'BenchmarkEmail Password', 'wp-subscribe' ),
			),

			'benchmark_list_name' => array(
				'id'    => 'benchmark_list_name',
				'name'  => 'benchmark_list_name',
				'type'  => 'select',
				'title' => esc_html__( 'BenchmarkEmail List', 'wp-subscribe' ),
				'options' => array( 'none' => esc_html__( 'Select List', 'wp-subscribe' ) ) + wps_get_service_list('benchmark'),
				'is_list' => true
			),

			'benchmark_double_optin' => array(
				'id'    => 'benchmark_double_optin',
				'name'  => 'benchmark_double_optin',
				'type'  => 'checkbox',
				'title' => esc_html__( 'Send double opt-in notification', 'wp-subscribe' )
			)
		);

		return $fields;
	}
}
