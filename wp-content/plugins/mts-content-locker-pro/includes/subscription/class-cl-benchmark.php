<?php
/**
 * BenchmarkEmail Subscription
 */
class CL_Subscription_Benchmark extends CL_Subscription_Base {

	public function init( $username, $password ) {

		require_once 'libs/bmeapi.php';
		return new BMEAPI( $username, $password );
	}

	public function get_lists( $username, $password ) {

		$api = $this->init( $username, $password );
		$result = $api->listGet( '', 1, 50, '', '' );

		$lists = array();
		foreach ( $result as $id => $list ) {
			$lists[ $list['id'] ] = $list['listname'];
		}

		return $lists;
	}

	public function subscribe( $identity, $context, $options ) {

		$api = $this->init( $options['username'], $options['password'] );

		$vars = array();
		$vars['email'] = $identity['email'];

		if ( ! empty( $identity['name'] ) ) {
			$vars['firstname'] = $identity['name'];
		}

		if ( ! empty( $identity['family'] ) ) {
			$fields['lastname'] = $identity['family'];
		}

		try {

			$double_optin = isset( $options['double_optin'] ) && $options['double_optin'] ? 1 : 0;
			$api->listAddContactsOptin( $options['list'], array( $vars ), $double_optin );
		} catch ( Exception $e ) {
			throw new Exception( $e->getMessage() );
		}

		return array( 'status' => 'subscribed' );
	}
}
