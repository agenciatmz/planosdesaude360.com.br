/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */
 $(function() {
     $(".preload").fadeOut(2000, function() {
         $(".cardinfo").fadeIn(1000);
     });
 });
