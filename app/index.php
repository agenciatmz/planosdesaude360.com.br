<?php
ob_start();
session_start();
if(isset($_SESSION['usuariosistema']) && isset($_SESSION['senhasistema'])){
    header("Location: module/view/leads");exit;
}
require_once("config/connect.php");

if(isset($_GET['acao'])){

    if(!isset($_POST['logar'])){
        $acao = $_GET['acao'];
        if($acao=='negado'){
            $erroLogar = '<div class="alert alert-warning alert-styled-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">Erro!</span> Faça o login...
							    </div>';
        }
    }
}
if(isset($_POST['logar'])){

    //Recuperar Dados do Form
    $usuario = trim(strip_tags($_POST['usuario']));
    $senha = trim(strip_tags($_POST['senha']));

    //Selecionar Banco de Dados
    $select = "SELECT * from tmzleadsusers WHERE BINARY usuario=:usuario AND BINARY senha=:senha";

    try{
        $result = $conexao->prepare($select);
        $result->bindParam(':usuario', $usuario, PDO::PARAM_STR);
        $result->bindParam(':senha', $senha, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $usuario = $_POST['usuario'];
            $senha = $_POST['senha'];
            $_SESSION['usuariosistema'] = $usuario;
            $_SESSION['senhasistema'] = $senha;
            $loginsucesso = '<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible">
									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
									<span class="font-weight-semibold">Sucesso!</span> Redirecionando
							    </div>';

            echo "<script type='text/javascript'>
                setTimeout(function () {
                window.location.href = \"module/view/home?acao=welcome\";
            }, 2000);  </script>";

        }else{
            $loginerro = '<div class="alert alert-warning alert-styled-left alert-arrow-left alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                 Dados incorretos
                  </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }
}//Se Clicar no Botão Entra no Sistema
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sistema de Gestão de Leads</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="public/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->


</head>

<body>

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				<form class="login-form form-validate" action="#" method="post" enctype="multipart/form-data">
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<img src="public/assets/images/imgLogoLogin.png">
								<h5 class="mb-0">Acesso ao sistema</h5>
								<span class="d-block text-muted">Insira seus dados</span>
							</div>
              <?php
                              if(isset($_GET['acao'])){
                                $acao = $_GET['acao'];

                                  if($acao=='sair'){
                                    echo
                               '<div class="alert alert-warning alert-styled-left alert-arrow-left alert-dismissible">
                                     <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                    Deslogado com sucesso
                                     </div>';
                                  }
                              }
                              ?>
              <?php echo $loginsucesso; ?>
              <?php echo $loginerro; ?>
              <?php echo $erroLogar; ?>
							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuário" required>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block"  name="logar">Entrar <i class="icon-circle-right2 ml-2"></i></button>
							</div>
						</div>
					</div>
				</form>
				<!-- /login card -->
			</div>
			<!-- /content area -->
		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->
  <!-- Footer -->
  <div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
      <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
        <i class="icon-unfold mr-2"></i>
        Footer
      </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-footer">
      <span class="navbar-text">
        &copy; 2018 Sistema de Gestão de Leads
      </span>


    </div>
  </div>
  <!-- /footer -->
  <!-- Core JS files -->
  <script src="public/assets/js/main/jquery.min.js"></script>
  <script src="public/assets/js/main/bootstrap.bundle.min.js"></script>
  <script src="public/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script src="public/assets/js/plugins/forms/validation/validate.min.js"></script>
  <script src="public/assets/js/plugins/forms/styling/uniform.min.js"></script>

  <script src="public/assets/js/app.js"></script>
  <script src="public/assets/js/demo_pages/login_validation.js"></script>
  <!-- /theme JS files -->

</body>
</html>