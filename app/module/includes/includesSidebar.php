<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

  <!-- Sidebar mobile toggler -->
  <div class="sidebar-mobile-toggler text-center">
    <a href="#" class="sidebar-mobile-main-toggle">
      <i class="icon-arrow-left8"></i>
    </a>
    Navigation
    <a href="#" class="sidebar-mobile-expand">
      <i class="icon-screen-full"></i>
      <i class="icon-screen-normal"></i>
    </a>
  </div>
  <!-- /sidebar mobile toggler -->


  <!-- Sidebar content -->
  <div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-user">
      <div class="card-body">
        <div class="media">
          <div class="mr-3">
            <a href="#"><img src="../../public/assets/data/uploads/perfil/<?php echo $imagem?>" width="38" height="38" class="rounded-circle" alt=""></a>
          </div>

          <div class="media-body">
            <div class="media-title font-weight-semibold"><?php echo $nomeLogado ?></div>
            <div class="font-size-xs opacity-50">
            <?php echo $cargoLogado ?>
            </div>
          </div>


        </div>
      </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="card card-sidebar-mobile">
      <ul class="nav nav-sidebar" data-nav-type="accordion">

        <!-- Main -->
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menu</div> <i class="icon-menu" title="Main"></i></li>
        <!-- <li class="nav-item">
          <a href="Home.php" class="nav-link active">
            <i class="icon-home4"></i>
            <span>
              Dashboard
            </span>
          </a>
        </li> -->
        <li class="nav-item ">
          <a href="home?acao=goback" class="nav-link"><i class="icon-home4"></i> <span>Dashboard</span></a>

        </li><li class="nav-item nav-item-submenu">
          <a href="#" class="nav-link"><i class="icon-list-unordered"></i> <span>Leads</span></a>

          <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
            <li class="nav-item"><a href="leads" class="nav-link">Planos de Saúde</a></li>
            <li class="nav-item nav-item-submenu">
              <a href="#" class="nav-link">Seguro Auto</a>
              <ul class="nav nav-group-sub">
                <li class="nav-item"><a href="leadscarsystem" class="nav-link">Carsystem</a></li>
                <li class="nav-item"><a href="leadsauto" class="nav-link">Auto</a></li>
              </ul>
            </li>
            <!-- <li class="nav-item nav-item-submenu">
              <a href="#" class="nav-link">Saúde</a>
              <ul class="nav nav-group-sub">
                <li class="nav-item"><a href="leadsamil" class="nav-link">Amil</a></li>
                <li class="nav-item"><a href="leadsbiovida" class="nav-link">Biovida</a></li>
              </ul>
            </li> -->
            <li class="nav-item nav-item-submenu">
              <a href="#" class="nav-link">Mês</a>
              <ul class="nav nav-group-sub">
                <li class="nav-item"><a href="leadsagosto" class="nav-link">Agosto</a></li>
              </ul>
            </li>


          </ul>
        </li>

        <li class="nav-item">
          <a href="landingpages" class="nav-link"><i class="icon-stack"></i> <span>Landing Pages</span></a>

        </li>



      </ul>
    </div>
    <!-- /main navigation -->

  </div>
  <!-- /sidebar content -->

</div>
