<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Sistema de Gestão de Leads</title>
	<link rel="shortcut icon" href="../../public/assets/images/favicon.ico">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../public/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->


</head>

<body>
