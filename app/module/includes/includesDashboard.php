<div class="content">
    <?php include("../controllers/controllersAcoes.php");?>
    <?php include("../controllers/controllersCheckLeads.php");?>
    <div class="row">
      <div class="col-sm-6 col-xl-3">
      						<div class="card card-body bg-blue-400 has-bg-image">
      							<div class="media">
      								<div class="mr-3 align-self-center">
      									<i class="icon-user icon-3x opacity-75"></i>
      								</div>

      								<div class="media-body text-right">
                        <h3 class="mb-0"><?php echo $checkLeads; ?></h3>
                        <span class="text-uppercase font-size-xs">total leads</span>
      								</div>
      							</div>
      						</div>
      					</div>



          <div class="col-sm-6 col-xl-3">
						<div class="card card-body bg-success-400 has-bg-image">
							<div class="media">
								<div class="mr-3 align-self-center">
									<i class="icon-location3 icon-3x opacity-75"></i>
								</div>

								<div class="media-body text-right">
									<h3 class="mb-0"><?php echo $checkLeadsSP; ?></h3>
									<span class="text-uppercase font-size-xs">leads são paulo</span>
								</div>
							</div>
						</div>
					</div>
          <div class="col-sm-6 col-xl-3">
          						<div class="card card-body bg-indigo-400 has-bg-image">
          							<div class="media">
          								<div class="mr-3 align-self-center">
          									<i class="icon-location3 icon-3x opacity-75"></i>
          								</div>

          								<div class="media-body text-right">
          									<h3 class="mb-0"><?php echo $checkLeadsRJ; ?></h3>
          									<span class="text-uppercase font-size-xs">leads rio de janeiro</span>
          								</div>
          							</div>
          						</div>
          					</div>
                    <div class="col-sm-6 col-xl-3">
      						<div class="card card-body bg-purple-400 has-bg-image">
      							<div class="media">
      								<div class="mr-3 align-self-center">
      									<i class="icon-bubbles4  icon-3x opacity-75"></i>
      								</div>

      								<div class="media-body text-right">
                        <h3 class="mb-0"><?php echo $checkLeadsChat; ?></h3>
      									<span class="text-uppercase font-size-xs">leads chat</span>
      								</div>
      							</div>
      						</div>
      					</div>
        <div class="col-lg-12">
          <div class="card">



							<div class="table-responsive">
								<table class="table text-nowrap">

									<tbody>
										<tr class="table-active table-border-double">
											<td colspan="3">Últimos leads cadastrados</td>
											<td class="text-right">
												<span class="badge bg-blue badge-pill"></span>
											</td>
                      <td class="text-right">
												<span class="badge bg-blue badge-pill"></span>
											</td>
										</tr>
                    <?php

                        $select = "SELECT * from tmzleads ORDER BY strId DESC LIMIT 5";
                        try {
                            $result = $conexao->prepare($select);
                            $result->execute();
                            $contar = $result->rowCount();
                            if($contar>0){
                                while($show = $result->FETCH(PDO::FETCH_OBJ)){
                                    date_default_timezone_set('America/Sao_Paulo');
                                    $date = date_create($show->strData);
                                    $date = date_format($date, 'd-m-Y H:i');
                                    ?>
										<tr><td>
												<i class="icon-checkmark3 text-success"></i>
											</td>
											<td >
												<h6 class="font-size-sm mb-0">Cadastrado</h6>
												<div class="font-size-sm text-muted line-height-1"><?php  $time_ago = strtotime($date); echo timeAgo($time_ago);  ?></div>
											</td>
											<td>
												<div class="d-flex align-items-center">

													<div>
														<a href="leadsunico?id=<?php echo $show->strId;?>" class="text-default font-weight-semibold letter-icon-title">  <i class="icon-user font-size-sm mr-1"></i><?php echo $show->nome;?></a>
														<div class="text-muted font-size-sm"></span><i class="icon-mail5 font-size-sm mr-1"></i> <?php echo $show->email;?>
													</div>
												</div>
											</td>
											<td>

													<div class="font-weight-semibold"> <i class="icon-location3 font-size-sm mr-1"></i><?php echo $show->cidade;?></div>
													<span class="text-muted"> Fonte: <?php echo $show->operadora;?></span>

											</td>
											<td class="text-center">
												<div class="list-icons">
													<div class="list-icons-item dropdown">
														<a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
														<div class="dropdown-menu dropdown-menu-right">

															<a href="leadsunico?id=<?php echo $show->strId;?>" class="dropdown-item"><i class="icon-eye"></i> Visualizar Lead</a>
														</div>
													</div>
												</div>
											</td>
										</tr>



                    <?php
                        }
                        }else{
                        echo '<div class="alert alert-warning alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        <span class="font-weight-semibold">Aviso!</span> Não existem leads cadastrados no momento!
                        </div>';
                        }
                        }catch(PDOException $e){
                        echo $e;
                        }
                        ?>

									</tbody>
								</table>
							</div>
						</div>


        </div>



<div class="content">
          <div class="preload">
              <div class="alert alert-info alert-styled-left alert-arrow-left alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
               <span class="font-weight-semibold"><i class="icon-spinner2 spinner"></i> Aguarde!</span> Carregando Leads...
             </div>
         </div>
            <!-- Column selectors -->
            <div class="card">

                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Planos de Saúde</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
                <div class="cardinfo" style="padding: 30px;"> 

                <div class="row">
                <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderAmil.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float bg-primary border-1 border-white" style="border-radius: 20px"><?php echo $checkLeadsAmil; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Amil</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/amil/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsamil" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderAmil.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float bg-primary border-1 border-white" style="border-radius: 20px"><?php echo $checkLeadsAmilBR; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Amil BR</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/amilbr/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsamilbr" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderGenerico.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1 bg-purple border-white" style="border-radius: 20px;background-color:#673AB7; color:#fff"><?php echo $checkLeadsChat; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Chat</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/chat" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadschat" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderGenerico.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1 bg-purple border-white" style="border-radius: 20px;background-color:#673AB7; color:#fff"><?php echo $checkLeadsAbc; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Genérico ABC</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/saudeabc" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsabc" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderBiovida.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-danger border-white" style="border-radius: 20px"><?php echo $checkLeadsBiovida; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">BioVida</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/biovida" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsbiovida" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderBradesco.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-danger border-white" style="border-radius: 20px"><?php echo $checkLeadsBradesco; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Bradesco</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/bradesco" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsbradesco" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderGoldenCross.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  border-white" style="border-radius: 20px;background-color:#FECF1C"><?php echo $checkLeadsGoldenCross; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Golden Cross</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/goldencross" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsgoldencross" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderIntermedica.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-warning border-white" style="border-radius: 20px"><?php echo $checkLeadsIntermedica; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Intermédica</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/intermedica" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsintermedica" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderMedsenior.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsMedSenior; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">MedSênior</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/medsenior" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsmedsenior" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderNext.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1 border-white" style="border-radius: 20px;background-color:#FF0D71;color:#fff"><?php echo $checkLeadsNext; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Amil Next</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/next" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsnext" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderSamed.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsSamed; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Samed</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/samed" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadssamed" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderSaoCristovao.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-primary border-white" style="border-radius: 20px"><?php echo $checkLeadsSaoCristovao; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">São Cristóvão</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/saocristovao" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadssaocristovao" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderSulamerica.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-warning border-white" style="border-radius: 20px"><?php echo $checkLeadsSulamerica; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Sulamérica</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/sulamerica" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadssulamerica" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderTrasmontano.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-danger border-white" style="border-radius: 20px"><?php echo $checkLeadsTrasmontano; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Trasmontano</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/trasmontano" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadstrasmontano" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderSantaHelena.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-danger border-white" style="border-radius: 20px"><?php echo $checkLeadsSantaHelena; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Santa Helena</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/santahelena" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadssantahelena" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderUnimed.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsUnimed; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Unimed</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/unimed/guarulhos" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsunimed" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderUniHosp.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsUniHosp; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">UniHosp</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsunimed" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderOmint.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsOmint; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Omint</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsomint" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderOnehealth.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsOneHealth; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">One Health</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsonehealth" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderLincx.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsLincx; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Lincx</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadslincx" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderPrevent.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsPreventSenior; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Prevent Sênior</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsprevent" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card card-body">
                <div class="media">
                    <div class="mr-3">
                      <li class="list-inline-item">
                          <img src="../../public/assets/images/placeholders/placeholderGreenline.png" class="rounded-circle" width="32" height="32" alt="">
                          <span class="badge badge-pill badge-float border-1  bg-success border-white" style="border-radius: 20px"><?php echo $checkLeadsGreenLine; ?></span>
                      </li>
                    </div>
                    <div class="media-body">
                        <h6 class="mb-0">Green Line</h6>
                        <span class="text-muted">Planos de Saúde</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="https://landingpages.planosdesaude360.com.br/ofertasrelampago" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Landing Page</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="leadsgreen" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

                
            </div>
          </div>
            <!-- /column selectors -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
       
    