<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
  <div class="text-center d-lg-none w-100">
    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
      <i class="icon-unfold mr-2"></i>
      Footer
    </button>
  </div>

  <div class="navbar-collapse collapse" id="navbar-footer">
    <span class="navbar-text">
      &copy; 2018 Sistema de Gestão de Leads
    </span>

    <!-- <ul class="navbar-nav ml-lg-auto">
      <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
      <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
      <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
    </ul> -->
  </div>
</div>
<!-- /footer -->

<!-- Core JS files -->
<script src="../../public/assets/js/main/jquery.min.js"></script>
<script src="../../public/assets/js/main/bootstrap.bundle.min.js"></script>
<script src="../../public/assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="../../public/assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="../../public/assets/js/plugins/forms/selects/select2.min.js"></script>
<script src="../../public/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
<script src="../../public/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
<script src="../../public/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
<script src="../../public/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
<script src="../../public/assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
<script src="../../public/assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
<script src="../../public/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="../../public/assets/js/plugins/forms/styling/switch.min.js"></script>
<script src="../../public/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="../../public/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script src="../../public/assets/js/plugins/forms/inputs/touchspin.min.js"></script>
<script src="../../public/assets/js/plugins/sliders/nouislider.min.js"></script>
<script src="../../public/assets/js/plugins/buttons/spin.min.js"></script>
<script src="../../public/assets/js/plugins/buttons/ladda.min.js"></script>
<script src="../../public/assets/js/app.js"></script>
<script src="../../public/assets/js/custom.js"></script>
<script src="../../public/assets/js/demo_pages/components_popups.js"></script>
<script src="../../public/assets/js/demo_pages/components_buttons.js"></script>
<script src="../../public/assets/js/demo_pages/content_cards_header.js"></script>

<script src="../../public/assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>

<script src="../../public/assets/js/plugins/visualization/echarts/echarts.min.js"></script>

<script src="../../public/assets/js/demo_pages/charts/echarts/pies_donuts.js"></script>
<!-- /theme JS files -->

<!-- /theme JS files -->
</body>
</html>
