<?php
    // excluir post
    if(isset($_GET['delete'])){
        $id_delete = $_GET['delete'];

        // exclui o registro

        $seleciona = "DELETE from tmzleads WHERE strId=:id_delete";
        try{
            $result = $conexao->prepare($seleciona);
            $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
            $result->execute();
            $contar = $result->rowCount();
            if($contar>0){
                $usuarioDeletadoSucesso = '<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                  <span class="font-weight-semibold">Feito!</span> Usuário deletado com sucesso.
                  </div>';
            }else{
                $usuarioDeletadoErro = '<div class="alert alert-danger alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                  <span class="font-weight-semibold">Aviso!</span> Erro ao excluir o lead. 	<a href="leads.php">Recarregar</a>
                  </div>';
            }
        }catch (PDOWException $erro){ echo $erro;}


    }

    ?>
