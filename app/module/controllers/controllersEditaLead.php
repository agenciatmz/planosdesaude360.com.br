<?php
// RECUPERA DADOS
if(!isset($_GET['id'])){
    header("Location: leads"); exit;
}
$id = $_GET['id'];
$select = "SELECT * from tmzleads WHERE strId=:id";
$contagem =1;
try{
    $result = $conexao->prepare($select);
    $result->bindParam(':id', $id, PDO::PARAM_INT);
    $result->execute();
    $contar = $result->rowCount();
    if($contar>0){
        while($show = $result->FETCH(PDO::FETCH_OBJ)){

            $nome            = $show->nome;
            $email  	     = $show->email;
            $telefone         = $show->telefone;
            $telefoneAlternativo  = $show->telefoneAlternativo;
            $possuicnpj      = $show->possuicnpj;
            $modalidade        = $show->modalidade;
            $cnpj            = $show->cnpj;
            $estado          = $show->estado;
            $cidade          = $show->cidade;
            $quantidadefamiliar      = $show->quantidadefamiliar;
            $quantidadepme     = $show->quantidadepme ;
            $operadora       = $show->operadora;
            $mensagem        = $show->mensagem;
            $tipodeplano        = $show->tipodeplano;
            $tipopessoa        = $show->tipopessoa;

        }
    }else{
        echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                         <strong>Aviso</strong> Não existem dados cadastrados com o id informado.
                                        </div>';exit;
    }
}catch(PDOException $e){
    $msgErroID = $e;
}
// ATUALIZA
if(isset($_POST['atualizar'])){

  $nome            = trim(strip_tags($_POST['nome']));
  $email           = trim(strip_tags($_POST['email']));
  $telefone         = trim(strip_tags($_POST['telefone']));
  $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
  $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
  $modalidade     = trim(strip_tags($_POST['modalidade']));
  $cnpj            = trim(strip_tags($_POST['cnpj']));
  $estado          = trim(strip_tags($_POST['estado']));
  $cidade          = trim(strip_tags($_POST['cidade']));
  $quantidadepme      = trim(strip_tags($_POST['quantidadepme']));
  $quantidadefamiliar     = trim(strip_tags($_POST['quantidadefamiliar']));
  $operadora       = trim(strip_tags($_POST['operadora']));
  $mensagem        = trim(strip_tags($_POST['mensagem']));
  $tipodeplano       = trim(strip_tags($_POST['tipodeplano']));
  $tipopessoa      = trim(strip_tags($_POST['tipopessoa']));

    $update = "UPDATE tmzleads SET  nome=:nome, email=:email, telefone=:telefone, telefoneAlternativo=:telefoneAlternativo, possuicnpj=:possuicnpj, modalidade=:modalidade, cnpj=:cnpj, estado=:estado, cidade=:cidade, quantidadepme=:quantidadepme, quantidadefamiliar=:quantidadefamiliar, operadora=:operadora, mensagem=:mensagem, tipodeplano=:tipodeplano, tipopessoa=:tipopessoa  WHERE strId=:id";

    try{
        $result = $conexao->prepare($update);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
        $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
        $result->bindParam(':modalidade', $modalidade, PDO::PARAM_STR);
        $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
        $result->bindParam(':estado', $estado, PDO::PARAM_STR);
        $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $result->bindParam(':quantidadepme', $quantidadepme, PDO::PARAM_STR);
        $result->bindParam(':quantidadefamiliar', $quantidadefamiliar, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);
        $result->bindParam(':tipodeplano', $tipodeplano, PDO::PARAM_STR);
        $result->bindParam(':tipopessoa', $tipopessoa, PDO::PARAM_STR);
        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){
            $msgAtualizaClientesSucesso =  '<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
              <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
              <span class="font-weight-semibold">Feito!</span> Usuário atualizado com sucesso.
              </div>'; echo "<script type='text/javascript'>
                setTimeout(function () {
                window.location.href = \"leads\";
            }, 2000);  </script>";
        }else{
            $msgAtualizaClientesErro = '<div class="alert alert-danger alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
              <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
              <span class="font-weight-semibold">Aviso!</span> Erro ao atualizar o lead. 	<a href="leads.php">Recarregar</a>
              </div>';
        }
    }catch(PDOException $e){
        $error = $e;
    }

}

?>
