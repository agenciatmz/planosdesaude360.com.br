<?php
function timeAgo($time_ago){
$cur_time 	= time();
$time_elapsed 	= $cur_time - $time_ago;
$seconds 	= $time_elapsed ;
$minutes 	= round($time_elapsed / 60 );
$hours 		= round($time_elapsed / 3600);
$days 		= round($time_elapsed / 86400 );
$weeks 		= round($time_elapsed / 604800);
$months 	= round($time_elapsed / 2600640 );
$years 		= round($time_elapsed / 31207680 );
// Seconds
if($seconds <= 60){
	echo "$seconds segundos atrás!";
}
//Minutes
else if($minutes <=60){
	if($minutes==1){
		echo "há um minuto atrás!";
	}
	else{
		echo "há $minutes minutos atrás!";
	}
}
//Hours
else if($hours <=24){
	if($hours==1){
		echo "há uma hora atrás!";
	}else{
		echo "há $hours horas atrás!";
	}
}
//Days
else if($days <= 7){
	if($days==1){
		echo "Ontem";
	}else{
		echo "há $days dias atrás!";
	}
}
//Weeks
else if($weeks <= 4.3){
	if($weeks==1){
		echo "há uma semana atrás!";
	}else{
		echo "há $weeks semanas atrás!";
	}
}
//Months
else if($months <=12){
	if($months==1){
		echo "há um mês atrás!";
	}else{
		echo "há $months meses atrás!";
	}
}
//Years
else{
	if($years==1){
		echo "há um ano atrás!";
	}else{
		echo "há $years anos atrás!";
	}
}
}

?>
