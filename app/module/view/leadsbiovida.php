<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");?>
<?php include("../includes/includesHeader.php");?>
<?php include("../controllers/controllersDeletaUsuario.php");?>
<!-- Main navbar -->
<?php include("../includes/includesTopNav.php");?>
<!-- /main navbar -->
<!-- Page content -->
<div class="page-content">
    <!-- Main sidebar -->
    <?php include("../includes/includesSidebar.php");?>
    <!-- /main sidebar -->
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="datatable_extension_buttons_html5.html" class="breadcrumb-item">Leads</a>
                        <span class="breadcrumb-item active">Visão Geral</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>



        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
            <!-- Column selectors -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Planos de Saúde</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div>
                <?php echo $usuarioDeletadoSucesso; ?>
                <?php echo $usuarioDeletadoErro; ?>
                <table class="table datatable-button-html5-columns" id="example">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Observação</th>
                            <th>Modalidade</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>FonePrincipal</th>
                            <th>FoneCelular</th>
                            <th>Cidade</th>
                            <th>IdFonte</th>
                            <th>Operadora</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(empty($_GET['pg'])){}
                            else{ $pg = $_GET['pg'];
                                if(!is_numeric($pg)){
                                    echo '<script language="JavaScript">
                                               location.href=" leads"; </script>';
                                }

                            }
                            $select = "SELECT * from tmzleads where operadora = 'Biovida'";
                            try {
                                $result = $conexao->prepare($select);
                                $result->execute();
                                $contar = $result->rowCount();



                                if($contar>0){
                                    while($show = $result->FETCH(PDO::FETCH_OBJ)){
                                        date_default_timezone_set('America/Sao_Paulo');
                                        $date = date_create($show->strData);
                                        $date = date_format($date, 'd-m-Y H:i');

                                        ?>
                        <tr>
                          <td><?php echo $show->strId;?></td>
                            <td><?php echo $show->quantidadefamiliar;?>  <?php echo $show->quantidadepme;?>  <?php echo $show->modalidade;?> ; <?php echo $show->possuicnpj;?> ; <?php echo $show->tipopessoa;?> <?php echo $show->cnpj;?>  ;  <?php echo $show->mensagem;?></td>
                            <td><?php echo $show->modalidade;?></td>
                            <td><?php echo $show->nome;?></td>
                            <td><?php echo $show->email;?></td>
                            <td><?php echo $show->telefone;?></td>
                            <td><?php echo $show->telefoneAlternativo;?></td>
                            <td><?php echo $show->cidade;?></td>
                            <td></td>
                            <td>
                                <span class="badge bg-purple"><?php echo $show->operadora;?></span>
                            
                            </td>
                            <td><span class="badge bg-success "><?php echo date('d/m H:i', strtotime($date));?></span></td>
                            <td>
                                <a href="leads?pg=<?php echo $pg;?>&delete=<?php echo $show->strId;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')" id="confirmaExclusao" class="btn btn-sm btn-danger btn-icon"><i class="icon-x"></i></a>
                                <!-- <a href="view.unique.leads.php?id=<?php echo $show->strId;?>" class="icon-eye btn btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar Usuário"> </a>
                                    <a href="view.edit.unique.leads.php?id=<?php echo $show->strId;?>" class="icon-pencil btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Usuário">  -->
                            </td>
                        </tr>
                        <?php
                            }
                            }else{
                            echo '<div class="alert alert-warning alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                            <span class="font-weight-semibold">Aviso!</span> Não existem leads cadastrados no momento!
                            </div>';
                            }
                            }catch(PDOException $e){
                            echo $e;
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <!-- /column selectors -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
<?php include("../includes/includesFooter.php");?>
