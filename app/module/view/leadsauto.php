<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");?>
<?php include("../includes/includesHeader.php");?>
<?php
    // excluir post
    if(isset($_GET['delete'])){
        $id_delete = $_GET['delete'];

        // exclui o registro

        $seleciona = "DELETE from tmzleadsauto WHERE strId=:id_delete";
        try{
            $result = $conexao->prepare($seleciona);
            $result->bindParam('id_delete',$id_delete, PDO::PARAM_STR);
            $result->execute();
            $contar = $result->rowCount();
            if($contar>0){
                $usuarioDeletadoSucesso = '<div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                  <span class="font-weight-semibold">Feito!</span> Usuário deletado com sucesso.
                  </div>';
            }else{
                $usuarioDeletadoErro = '<div class="alert alert-danger alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                  <span class="font-weight-semibold">Aviso!</span> Erro ao excluir o lead. 	<a href="leads.php">Recarregar</a>
                  </div>';
            }
        }catch (PDOWException $erro){ echo $erro;}


    }

    ?>
<!-- Main navbar -->
<?php include("../includes/includesTopNav.php");?>
<!-- /main navbar -->
<!-- Page content -->
<div class="page-content">
    <!-- Main sidebar -->
    <?php include("../includes/includesSidebar.php");?>
    <!-- /main sidebar -->
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="datatable_extension_buttons_html5.html" class="breadcrumb-item">Leads</a>
                        <span class="breadcrumb-item active">Visão Geral</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>

        <!-- /page header -->
        <!-- Content area -->

        <div class="content">
          <div class="preload">
              <div class="alert alert-info alert-styled-left alert-arrow-left alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
               <span class="font-weight-semibold"><i class="icon-spinner2 spinner"></i> Aguarde!</span> Carregando Leads...
             </div>
         </div>
            <!-- Column selectors -->
            <div class="card">

                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Seguro Auto - Genérico</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                            <a class="list-icons-item" data-action="reload"></a>
                            <a class="list-icons-item" data-action="remove"></a>
                        </div>
                    </div>
                </div><div class="cardinfo">
                <?php echo $usuarioDeletadoSucesso; ?>
                <?php echo $usuarioDeletadoErro; ?>
                <table class="table datatable-button-html5-columns" id="example">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Ano</th>
                            <th>Já Possui</th>
                            <th>Renovação</th>
                            <th>Sexo</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data Nasc</th>
                            <th>Telefone</th>
                            <th>Telefone Fixo</th>
                            <th>CEP</th>
                            <th>CPF</th>
                            <th>Estado</th>
                            <th>Cidade</th>
                            <th>Seguradora</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(empty($_GET['pg'])){}
                            else{ $pg = $_GET['pg'];
                                if(!is_numeric($pg)){
                                    echo '<script language="JavaScript">
                                               location.href=" leadsauto"; </script>';
                                }

                            }
                            $select = "SELECT * from tmzleadsauto ORDER BY strId DESC";
                            try {
                                $result = $conexao->prepare($select);
                                $result->execute();
                                $contar = $result->rowCount();



                                if($contar>0){
                                    while($show = $result->FETCH(PDO::FETCH_OBJ)){
                                        date_default_timezone_set('America/Sao_Paulo');
                                        $date = date_create($show->strData);
                                        $date = date_format($date, 'd-m-Y H:i');

                                        ?>
                        <tr>
                          <td><?php echo $show->strId;?></td>
                            <td><?php echo $show->strMarca;?></td>
                              <td><?php echo $show->strModelo;?></td>
                                <td><?php echo $show->strAno;?></td>
                                  <td><?php echo $show->q1;?></td>
                                    <td><?php echo $show->q2;?></td>
                                      <td><?php echo $show->q3;?></td>
                                        <td><?php echo $show->strNome;?></td>
                                          <td><?php echo $show->strEmail;?></td>
                                            <td><?php echo $show->strDataNascimento;?></td>
                                              <td><?php echo $show->strTelefoneMovel;?></td>
                                                <td><?php echo $show->strTelefoneFixo;?></td>
                                                  <td><?php echo $show->strCEP;?></td>
                                                    <td><?php echo $show->strCPF;?></td>
                                                      <td><?php echo $show->strEstado;?></td>
                                                        <td><?php echo $show->strCidade;?></td>
                                                        <td><span class="badge bg-success ">  <?php echo $show->strSeguradora;?></span></td>

                            <td><span class="badge bg-success "><?php echo date('d/m H:i', strtotime($date));?></span></td>
                            <td>
                              <?php if($nivelLogado ==1){ ?>
                                <a href="leadsauto?pg=<?php echo $pg;?>&delete=<?php echo $show->strId;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')" id="confirmaExclusao" class="btn btn-sm btn-danger btn-icon"><i class="icon-x"></i></a>
                                <!-- <a href="leadsunico?id=<?php echo $show->strId;?>"  class="btn btn-sm btn-info btn-icon"><i class="icon-eye"></i></a>
                                <a href="leadseditar?id=<?php echo $show->strId;?>"  class="btn btn-sm btn-success btn-icon"><i class="icon-pencil"></i></a> -->
                                    <?php } ?>
                                    <?php if($nivelLogado ==0){ ?>
                                      <!-- <a href="leads?pg=<?php echo $pg;?>&delete=<?php echo $show->strId;?>" onClick="return confirm('Deseja realmente excluir este usuário ?')" id="confirmaExclusao" class="btn btn-sm btn-danger btn-icon"><i class="icon-x"></i></a> -->
                                      <a href="leadsunico?id=<?php echo $show->strId;?>"  class="btn btn-sm btn-info btn-icon"><i class="icon-eye"></i></a>
                                        <!--   <a href="view.edit.unique.leads.php?id=<?php echo $show->strId;?>" class="icon-pencil btn btn-outline-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Usuário">  -->
                                          <?php } ?>
                            </td>
                        </tr>
                        <?php
                            }
                            }else{
                            echo '<div class="alert alert-warning alert-styled-left alert-dismissible" style="margin-left: 10px; margin-right: 10px;">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                            <span class="font-weight-semibold">Aviso!</span> Não existem leads cadastrados no momento!
                            </div>';
                            }
                            }catch(PDOException $e){
                            echo $e;
                            }
                            ?>
                    </tbody>
                </table>
            </div>
          </div>
            <!-- /column selectors -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>


<!-- /page content -->
<?php include("../includes/includesFooter.php");?>
