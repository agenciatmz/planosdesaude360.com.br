<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");?>
<?php include("../includes/includesHeader.php");?>
<?php include("../controllers/controllersDeletaUsuario.php");?>
<!-- Main navbar -->
<?php include("../includes/includesTopNav.php");?>
<!-- /main navbar -->
<!-- Page content -->
<div class="page-content">
    <!-- Main sidebar -->
    <?php include("../includes/includesSidebar.php");?>
    <!-- /main sidebar -->
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="datatable_extension_buttons_html5.html" class="breadcrumb-item">Leads</a>
                        <span class="breadcrumb-item active">Visão Geral</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <?php
            if(!isset($_GET['id'])){
                header("Location: leads"); exit;
                exit; }
            $id = $_GET['id'];
            $select = "SELECT * FROM tmzleads WHERE strId=:id";
            $contagem =1;
            try {
            $result = $conexao->prepare($select);
            $result->bindParam(':id',$id, PDO::PARAM_INT);
            $result->execute();
            $contar = $result->rowCount();
            if($contar>0){
            while($show = $result->FETCH(PDO::FETCH_OBJ)){
            $id = $show->id;
            $date = date_create($show->strData);
            $date = date_format($date, 'd-m-Y');
            $dateToday = date('d-m-Y', strtotime("1 days"));

            ?>
        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
            <!-- Column selectors -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">                                Lead ID #<?php echo $show->strId;?></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                    <a href="leadseditar?id=<?php echo $show->strId;?>" <i class="icon-pencil"></i></a>
                    <a href="leads" <i class="icon-undo"></i></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="#">
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Detalhes do Lead</legend>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Nome</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" value="<?php echo $show->nome;?>" readonly="" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" value="<?php echo $show->email;?>" readonly="" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Telefone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->telefone;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Telefone Alternativo</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->telefoneAlternativo;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Possui CNPJ ?</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->possuicnpj;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Modalidade</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->modalidade;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">CNPJ</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->cnpj;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Estado</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->estado;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Cidade</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->cidade;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Vidas Familiar</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->quantidadefamiliar;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Vidas PME</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->quantidadepme;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Operadora</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->operadora;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Tipo de Plano</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->tipodeplano;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Tipo de Pessoa</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="" value="<?php echo $show->tipopessoa;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Mensagem</label>
                                <div class="col-lg-10">
                                    <textarea rows="3" cols="3" class="form-control" readonly="" ><?php echo $show->mensagem;?></textarea>
                                </div>
                            </div>
                        </fieldset>
                        <?php
                            }
                            }else{
                                echo '<div class="alert media fade in alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                      <strong>Aviso!</strong> Ainda não existem posts cadastrados. </div>';
                            }
                            }catch(PDOException $e){
                                echo $e;
                            }
                            ?>
                        <div class="text-right">
                            <a  href="leads" class="btn btn-primary">Voltar</a>

                        </div>
                    </form>
                </div>
            </div>
            <!-- /column selectors -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
<?php include("../includes/includesFooter.php");?>
