<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");?>
<?php include("../includes/includesHeader.php");?>
<?php include("../controllers/controllersDeletaUsuario.php");?>
<!-- Main navbar -->
<?php include("../includes/includesTopNav.php");?>
<!-- Page content -->
<div class="page-content">
    <!-- Main sidebar -->
    <?php include("../includes/includesSidebar.php");?>
    <!-- /main sidebar -->
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="#" class="breadcrumb-item">Landing Pages</a>
                        <span class="breadcrumb-item active">Visão Geral</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
            <div class="mb-3 pt-2">
                <h6 class="mb-0 font-weight-semibold">
                    Landing Pages
                </h6>
                <span class="text-muted d-block">Planos de Saúde</span>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderAmil.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Amil</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/amil/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Amil SP</a>
                                            <a href="https://landingpages.planosdesaude360.com.br/amil/amilrj.php" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Amil RJ</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderBiovida.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Biovida</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/biovida/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderBradesco.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Bradesco</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/bradesco/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderGoldenCross.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Golden Cross</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/goldencross/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderIntermedica.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Intermédica</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/intermedica/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Intermédica SP</a>
                                            <a href="https://landingpages.planosdesaude360.com.br/intermedica/intermedicarj.php" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Intermédica RJ</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderMedsenior.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">MedSênior</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/medsenior/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderNext.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Next</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/next/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Next SP</a>
                                            <a href="https://landingpages.planosdesaude360.com.br/next/nextrj.php" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Next RJ</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderRcampos.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">RCampos</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/rcampos/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderSamed.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Samed</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/samed/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderSaoCristovao.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">São Cristóvão</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/saocristovao/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderGenerico.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Saúde ABC</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/saudeabc/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderGenerico.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Saúde Genérico</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/saudetmz/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderSulamerica.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Sulamérica</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/sulamerica/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderTrasmontano.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Trasmontano</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/trasmontano/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderSantaHelena.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Santa Helena</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/santahelena" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderUnimed.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">Unimed</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/unimed/guarulhos" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Unimed Guarulhos</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#">
                                <img src="../../public/assets/images/placeholders/placeholderB2B.png" class="rounded-circle" width="42" height="42" alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h6 class="mb-0">TMZ B2B</h6>
                                <span class="text-muted">Planos de Saúde</span>
                            </div>
                            <div class="ml-3 align-self-center">
                                <div class="list-icons">
                                    <div class="list-icons-item dropdown">
                                        <a href="#" class="list-icons-item dropdown-toggle caret-0" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="https://landingpages.planosdesaude360.com.br/b2b/" class="dropdown-item" target="_blank"><i class="icon-eye"></i>Visualizar</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="leads" class="dropdown-item"><i class="icon-user"></i>Leads</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
<?php include("../includes/includesFooter.php");?>
</body>
</html>
