<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");
    include_once("../../module/controllers/controllersEditaLead.php");
    ?>
<?php include("../includes/includesHeader.php");?>
<?php include("../controllers/controllersDeletaUsuario.php");?>
<!-- Main navbar -->
<?php include("../includes/includesTopNav.php");?>
<!-- /main navbar -->
<!-- Page content -->
<div class="page-content">
    <!-- Main sidebar -->
    <?php include("../includes/includesSidebar.php");?>
    <!-- /main sidebar -->
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="datatable_extension_buttons_html5.html" class="breadcrumb-item">Leads</a>
                        <span class="breadcrumb-item active">Visão Geral</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
          <?php echo $error; ?>
          <?php echo $msgErroID; ?>
          <?php echo $msgAtualizaClientesSucesso; ?>
          <?php echo $msgAtualizaClientesErro; ?>
            <!-- Column selectors -->
            <div class="card">

                <div class="card-header header-elements-inline">

                    <h5 class="card-title">Lead ID #<?php echo $id;?></h5>
                    <div class="header-elements">
                        <div class="list-icons">
                          <a href="leads" <i class="icon-undo"></i></a>

                        </div>
                    </div>
                </div>
                <div class="card-body">
                  <form  role="form" class="form-signin" action="#" method="post">
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Detalhes do Lead</legend>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Nome</label>
                                <div class="col-lg-10">
                                  <input class="form-control" name="nome" id="nome"  value="<?php echo $nome;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                  <input  class="form-control" name="email" id="email"  value="<?php echo $email;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Telefone</label>
                                <div class="col-lg-10">
                                  <input class="form-control" name="telefone" id="telefone"  value="<?php echo $telefone;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Telefone Alternativo</label>
                                <div class="col-lg-10">
                                  <input class="form-control" name="telefoneAlternativo" id="telefoneAlternativo"  value="<?php echo $telefoneAlternativo;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Possui CNPJ ?</label>
                                <div class="col-lg-10">
                                  <input  class="form-control" name="possuicnpj" id="possuicnpj"  value="<?php echo $possuicnpj;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Modalidade</label>
                                <div class="col-lg-10">
                                  <input  class="form-control" name="modalidade" id="modalidade"  value="<?php echo $modalidade;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">CNPJ</label>
                                <div class="col-lg-10">
                                  <input class="form-control" name="cnpj" id="cnpj"  value="<?php echo $cnpj;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Estado</label>
                                <div class="col-lg-10">
                                  <input class="form-control" name="estado" id="estado" value="<?php echo $estado;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Cidade</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="cidade" id="cidade" value="<?php echo $cidade;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Vidas Familiar</label>
                                <div class="col-lg-10">
                                  <input type="text" class="form-control" name="quantidadefamiliar" id="quantidadefamiliar" value="<?php echo $quantidadefamiliar;?>" placeholder="Não Consta">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Vidas PME</label>
                                <div class="col-lg-10">
                                  <input type="text" class="form-control" name="quantidadepme" id="quantidadepme" value="<?php echo $quantidadepme;?>" placeholder="Não Consta">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Operadora</label>
                                <div class="col-lg-10">
                                  <input type="text" class="form-control" name="operadora" id="operadora" value="<?php echo $operadora;?>" placeholder="Não Consta">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Tipo de Plano</label>
                                <div class="col-lg-10">
                                  <input type="text" class="form-control" name="tipodeplano" id="tipodeplano" value="<?php echo $tipodeplano;?>" placeholder="Não Consta">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2 cursor-pointer" for="clickable-label">Tipo de Pessoa</label>
                                <div class="col-lg-10">
                                  <input type="text" class="form-control" name="tipopessoa" id="tipopessoa" value="<?php echo $tipopessoa;?>" placeholder="Não Consta">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Mensagem</label>
                                <div class="col-lg-10">
                                  <textarea rows="3" cols="3" class="form-control" id="mensagem" name="mensagem" ><?php echo $mensagem;?></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <div class="text-right">
                            <button type="submit" name="atualizar" class="btn btn-success">Atualizar</button>

                        </div>
                    </form>
                </div>
            </div>
            <!-- /column selectors -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->
<?php include("../includes/includesFooter.php");?>
