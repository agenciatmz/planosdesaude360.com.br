<?php
    require_once("../../config/connect.php");
    include_once("../../module/controllers/controllersVerificaUsuarioLogado.php");?>
<?php include("../includes/includesHeader.php");?>
<?php include("../controllers/controllersDeletaUsuario.php");?>
<?php include("../includes/includesTopNav.php");?>
<div class="page-content">
    <?php include("../includes/includesSidebar.php");?>

    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <span class="breadcrumb-item active">Dashboard</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <?php include("../includes/includesDashboard.php");?>

    </div>

</div>
<?php include("../includes/includesFooter.php");?>
