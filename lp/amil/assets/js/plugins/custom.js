$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
$(document).ready(function(){

    $('.cep').mask('00000000');
    $('.phone').mask('00000000000');
    $('.phone_with_ddd').mask('00000000000');

    $('.cpf').mask('00000000000', {reverse: true});
    $('.cnpj').mask('00000000000000', {reverse: true});


});