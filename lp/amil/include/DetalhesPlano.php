<div class="col-md-8 ml-auto mr-auto text-center">
    <h2 class="title">SAIBA MAIS SOBRE A AMIL SAÚDE</h2>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-image">
                    <img src="assets/img/bg28.jpg" class="rounded" alt="">
                </div>
                <div class="info text-center">
                    <h4 class="info-title">REDE MÉDICA <Br>DOS PLANOS AMIL</h4>
                    <p class="description">A rede Amil Saúde é famosa pela qualidade! São hospitais, laboratórios, consultórios médicos, clinicas de imagens e milhares de referenciados médicos cadastrados em aproximadamente todo território nacional</p>
                    <a href="#formulario" class="btn btn-danger btn-round">
                        <i class="now-ui-icons ui-2_favourite-28"></i> SEJA CLIENTE AMIL TAMBÉM
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-image">
                    <img src="assets/img/bg26.jpg" class="rounded" alt="">
                </div>
                <div class="info text-center">
                    <h4 class="info-title">PLANO DE SAÚDE AMIL COBERTURAS</h4>
                    <p class="description">A Cobertura dos Planos de Saúde Amil muda de acordo com o Plano contratado. Existem opções de Planos de Saúde Amil com cobertura Nacional e Regional. Alguns planos cobrem procedimentos cirúrgicos mais específicos, e outros cobrem procedimentos mais básicos. O Ideal é realizar uma cotação e identificar qual plano de Saúde Amil se enquadra melhor às suas necessidades</p>
                    <a href="#formulario" class="btn btn-danger btn-round">
                        <i class="now-ui-icons ui-2_favourite-28"></i> SEJA CLIENTE AMIL TAMBÉM
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-image">
                    <img src="assets/img/bg27.jpg" class="rounded" alt="">
                </div>
                <div class="info text-center">
                    <h4 class="info-title">PREÇOS DOS PLANOS DE SAÚDE AMIL
                    </h4>
                    <p class="description">Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado.
                        Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)
                    </p>
                    <a href="#formulario" class="btn btn-danger btn-round">
                        <i class="now-ui-icons ui-2_favourite-28"></i> SEJA CLIENTE AMIL TAMBÉM
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>