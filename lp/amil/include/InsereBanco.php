<?php

if(isset($_POST['cadastrar'])){
    $nome            = trim(strip_tags($_POST['nome']));
    $email           = trim(strip_tags($_POST['email']));
    $telefone         = trim(strip_tags($_POST['telefone']));
    $telefoneAlternativo  = trim(strip_tags($_POST['telefoneAlternativo']));
    $possuicnpj      = trim(strip_tags($_POST['possuicnpj']));
    $cnpj            = trim(strip_tags($_POST['cnpj']));
    $estado          = trim(strip_tags($_POST['estado']));
    $cidade          = trim(strip_tags($_POST['cidade']));
    $quantidade      = trim(strip_tags($_POST['quantidade']));
    $operadora       = trim(strip_tags($_POST['operadora']));
    $mensagem        = trim(strip_tags($_POST['mensagem']));

    $insert = "INSERT INTO dbleads ( nome, email, telefone, telefoneAlternativo, possuicnpj, cnpj, estado, cidade, quantidade, operadora, mensagem ) VALUES ( :nome, :email, :telefone, :telefoneAlternativo, :possuicnpj, :cnpj, :estado, :cidade, :quantidade, :operadora, :mensagem )";
    try{

        $result = $conexao->prepare($insert);

        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':telefoneAlternativo', $telefoneAlternativo, PDO::PARAM_STR);
        $result->bindParam(':possuicnpj', $possuicnpj, PDO::PARAM_STR);
        $result->bindParam(':cnpj', $cnpj, PDO::PARAM_STR);
        $result->bindParam(':estado', $estado, PDO::PARAM_STR);
        $result->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $result->bindParam(':quantidade', $quantidade, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':mensagem', $mensagem, PDO::PARAM_STR);

        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '
                    <script type="text/javascript">
window.location = "obrigado.php";
            </script>';

            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erro</strong> ao cadastrar o usuário.
                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>