<div class="container">
    <div class="row">
        <div class="col-md-8 ml-auto mr-auto text-center">
            <div class="card card-testimonial card-plain">
                <h2 class="title" style="color:#08377F">AINDA TEM DÚVIDAS?</h2>
                <p class="card-description" style="color:#000">Solicite uma cotação e saiba mais sobre os Planos de Saúde Amil </p>
                <a href="#formulario" class="btn btn-info btn-round btn-lg">
                    <i class="now-ui-icons ui-2_favourite-28"></i> QUERO COTAR AGORA
                </a>
            </div>
        </div>
    </div>
</div>