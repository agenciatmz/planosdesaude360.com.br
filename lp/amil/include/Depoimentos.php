<div class="container">
    <div class="row">
        <div class="col-md-6 ml-auto mr-auto text-center">
            <h2 class="title" style="color:#08377F">O PLANO AMIL É BOM?</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 ml-auto mr-auto text-center">
            <div class="card card-testimonial card-plain">
                <div class="card-avatar">
                    <a href="#">
                        <img class="img img-raised rounded" src="assets/img/michael.png">
                    </a>
                </div>
                <div class="card-body">
                    <h3 class="card-title">Arlete Rezende</h3>
                    <h6 class="category text-primary">Empreendedora e cliente Amil Saúde</h6>
                    <p class="card-description" style="color:#000">"Sempre que precisei da Amil nunca tive problemas! pelo contrário, a rede de hospital é muito ampla, não só hospital, clínicas, consultórios,  atendimentos rápidos independente se é em minha cidade ou em outro local, é um plano bem completo, vale cada centavo. A facilidade no agendamento das consultas também é fenomenal. Enfim, Recomendo muito. "
                    </p>
                </div>
                <a href="#formulario" class="btn btn-info btn-round">
                    <i class="now-ui-icons ui-2_favourite-28"></i> SEJA CLIENTE AMIL TAMBÉM
                </a>
            </div>
        </div>
    </div>
</div>