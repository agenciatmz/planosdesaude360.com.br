<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000">
    <div class="card-header text-center">
        <h2 class="title" style="color:#08377F">COTAÇÃO ONLINE</h2>
    </div>
    <div class="card-body text-left">
        <div class="row">

            <div class="col-md-12 pr-2">
                <?php echo $msgClientesSucesso; ?>
                <?php echo $msgClientesErro; ?>
                <?php echo $e; ?>
                <div class="alert alert-info" role="alert">

                    <div class="container">


                        <strong>Dica:</strong> quem possui CNPJ economiza até  <strong>30%</strong> nos Planos de Saúde Amil à partir de 2 vidas!
                    </div>
                </div>
            </div>
            <div class="col-md-12 pr-2">
                <select class="selectpicker" data-size="7" data-style="btn btn-round btn-simple" title="Você possui CNPJ?" name="possuicnpj" id="possuicnpj" required>
                    <option value="Pme">Sim</option>
                    <option value="Adesão">Não</option>
                </select>
            </div>
            <div class="col-md-12 pr-2">
                <div class="form-group box Pme">
                    <label>CNPJ ou Razão Social</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons business_briefcase-24"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Insira seu CNPJ ou Razão Social" name="cnpj" id="cnpj" >
                    </div>
                </div>
            </div>
            <div class="col-md-6 pr-2">
                <label>Nome</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons users_circle-08"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Insira seu nome..." aria-label="Insira seu nome..." name="nome" id="nome" required>
                </div>
            </div>
            <div class="col-md-6 pl-2">
                <div class="form-group">
                    <label>E-mail</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons ui-1_email-85"></i></span>
                        </div>
                        <input type="email" class="form-control" placeholder="Insira seu E-mail..." name="email" id="email" required>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pr-2">
                <label>Celular</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
                    </div>
                    <input  type="text" class="form-control phone_with_ddd" placeholder="Insira com DDD"  name="telefone" id="telefone" required>
                </div>
            </div>
            <div class="col-md-6 pl-2">
                <div class="form-group">
                    <label>Telefone Alternativo</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
                        </div>
                        <input type="text" class="form-control phone" placeholder="Insira com DDD" name="telefoneAlternativo" id="telefoneAlternativo">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 pr-2">


                <label for="estado" class="col-form-label">Estado</label>

                <select id="estado" name="estado" class="form-control selectpicker" data-style="btn btn-default btn-round"   data-size="7" required><option> Choose</option>
                </select>
            </div>
            <div class="col-md-6 pl-2">
                <label for="cidade" class="col-form-label">Cidade</label>

                <select id="cidade" name="cidade" class="form-control selectpicker" data-style="btn btn-default btn-round"   data-size="7" required>
                    <option> Choose</option>
                </select>
            </div>
            <div class="col-md-12 pr-2">
                <label>Quantidade de Pessoas</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons users_single-02"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Quantidade de Vidas"  name="quantidade" required>
                </div>
            </div>
            <div class="d-none">
                <label>Celular</label>
                <div class="input-group">

                    <input  type="text" class="form-control" value="Amil" name="operadora" id="operadora">
                </div>
            </div>
            <div class="col-md-12 pr-2">
                <div class="form-group">
                    <label>Deixe uma mensagem sobre o que precisa (Opcional)</label>
                    <textarea name="mensagem" class="form-control" id="message" rows="6" ></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info btn-lg btn-round pull-right">Solicitar Cotação</button>
            </div>
        </div>
    </div>
</form>