<nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Agência TresMeiaZero">
            <img src="LandingPages/assets/img/Saude/intermedica/logo-color.png" width="180">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" data-nav-image="./assets/img/blurred-image-1.jpg" data-color="orange">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#conheca"l>
                        <i class="now-ui-icons tech_tv"></i>
                        <p>Sobre nós</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#redecredenciada">
                        <i class="now-ui-icons files_single-copy-04" aria-hidden="true"></i>
                        <p>Rede Credenciada</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#planos">
                        <i class="now-ui-icons files_box" aria-hidden="true"></i>
                        <p>Planos</p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#depoimento">
                        <i class="now-ui-icons gestures_tap-01" aria-hidden="true"></i>
                        <p>Depoimentos</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="header">
    <div class="page-header">
        <div class="page-header-image" style="background-image: url('LandingPages/assets/img/Saude/intermedica/bg14.jpg');"></div>
        <div class="content-center" id="formulario">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <h1 class="title" style="font-size: 4.8em"> Chega de pagar caro no seu plano de saúde</h1>
                        <div class="row">
                            <div class="col-md-7">
                                <img src="LandingPages/assets/img/Saude/intermedica/header.png">
                            </div>
                            <div class="col-md-5">
                                <h4 class="description" style="color:#fff">
                                    Os Planos de saúde Intermédica são ideais para quem procura por uma opção com qualidade e rede de atendimento ampla. É possível contratar o plano de saúde com ou sem coparticipação, inclusive com opção de cobertura local. 
                                </h4>
                                <h4 class="description" style="color:#fff">
                                    Os planos de saúde Intermédica abrangem Rede Própria e Rede Credenciada certificada, com acesso a todas as especialidades médicas e profissionais referenciados.
                                </h4>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-12"  style="top: 43px;">
                            <a href="FormLandingPagesSaudeIntermedica.php" class="btn btn-success btn-lg pull-left" style="color:#fff; font-weight: 500; width: 100%; font-size: 20px">Preencher formulário</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-3" id="conheca">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-left">
                <h1 class="title" style="font-size: 3em">
                    <span style="color: #e5b463">SOBRE NÓS</span>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 ">
                <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">O Grupo NotreDame Intermédica (GNDI) é pioneiro em Medicina Preventiva e oferece as melhores soluções em saúde. </p>
                <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Fundada há 50 anos em São Paulo (SP), a Intermédica se destaca por oferecer serviços de qualidade nas Redes Própria e Credenciada de Centros Clínicos, Hospitais, prontos-socorros e maternidades.</p>
                <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Além disso o Grupo NotreDame Intermédica se diferencia pelo relacionamento transparente, ético e comprometido com seus beneficiários. Prestamos serviços personalizados a preços acessíveis.</p>
                <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">Como resultado disso Temos um dos melhores índices de satisfação de atendimento segundo nossos próprios beneficiários, de acordo com indicadores da ANS (Agência Nacional de Saúde Suplementar).</p>
                <a href="FormLandingPagesSaudeIntermedica.php" class="btn btn-success btn-lg pull-left" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
            </div>
            <div class="col-md-5 ">
                <img src="LandingPages/assets/img/Saude/intermedica/imgPersonagem360.png"  class="img-responsive d-none d-sm-block"  style="position: relative; bottom: 50px;">
            </div>
        </div>
    </div>
    <div class="container" id="redecredenciada">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-right">
                <h1 class="title" style="font-size: 3em">
                    <span style="color: #e5b463">
                    REDE CREDENCIADA
                    </span>
                </h1>
            </div>
        </div>
        <div class="row" id="redecredenciada">
            <div class="col-md-5">
                <img src="LandingPages/assets/img/Saude/intermedica/imgHospitalFlat.png" class="img-responsive pull-left d-none d-sm-block" width="800px"  style="position: relative; top: 60px;">
            </div>
            <div class="col-md-7 text-right">
                <p class="description" style="color: #9A9A9A;font-size: 20px;line-height: 32px;">
                    HOSPITAL MONTEMAGNO<br>
                    HOSPITAL SANTA CECÍLIA<br>
                    HOSPITAL CRUZEIRO DO SUL<br>
                    HOSPITAL E MATERNIDADE GUARULHOS<br>
                    HOSPITAL NOVA VIDA<br>
                    HOSPITAL E MATERNIDADE INTERMÉDICA ABC<br>
                    HOSPITAL E MATERNIDADE RENASCENÇA<br>
                    HOSPITAL BOSQUE DA SAÚDE<br>
                    HOSPITAL E MATERNIDADE NOSSA SENHORA DO ROSÁRIO<br>
                    HOSPITAL E MATERNIDADE SACRECOEUR<br>
                    HOSPITAL FAMILY<br>
                    HOSPITAL BAETA NEVES<br>
                    HOSPITAL SÃO BERNARDO<br>
                    Mais centenas de Clínicas e Hospitais com diversas especialidades à sua disposição.
                </p>
                <a href="FormLandingPagesSaudeIntermedica.php" class="btn btn-success btn-lg pull-right" style="color:#fff; font-weight: 500">Solicite um orçamento agora</a>
            </div>
        </div>
    </div>
</div>
<div class="pricing-3"  id="planos">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto text-center">
                <h2 class="title">CONHEÇA AS LINHAS DE PLANO DE SAÚDE DA INTERMÉDICA</h2>
                <div class="section-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card card-pricing"  data-background-color="orange">
                    <div class="card-body">
                        <h2 class="category">
                        Linha Smart</h2>
                        <ul>
                            <li>Assistência médica com alta qualidade e custo acessível</li>
                            <li>Ampla gama de opções para contratação regionalizada com ou sem participação</li>
                            <li>Rede própria certificada e credenciada, com excelência na prestação de serviços</li>
                            <br><br>
                        </ul>
                        <a href="FormLandingPagesSaudeIntermedica.php"  class="btn btn-warning btn-lg btn-round">
                        Cotação Online
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-pricing" style="background-color: #D09E3B">
                    <div class="card-body">
                        <h2 class="category" style="color:#fff">
                        Linha Premium</h2>
                        <ul>
                            <li style="color:#fff">Atendimento personalizado com conforto e qualidade</li>
                            <li style="color:#fff">Rede credenciada altamente <br>qualificada</li>
                            <li style="color:#fff">Rede própria certificada e credenciada, com excelência na prestação de serviços</li>
                            <br><br>
                        </ul>
                        <a href="FormLandingPagesSaudeIntermedica.php"  class="btn btn-warning btn-lg btn-round">
                        Cotação Online
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-pricing" style="background-color: #9C9C9C">
                    <div class="card-body">
                        <h2 class="category" style="color:#fff">
                        Linha Advance</h2>
                        <ul>
                            <li style="color:#fff">Cobertura em todo território nacional</li>
                            <li style="color:#fff">Rede própria certificada e credenciada com excelência na prestação de serviços</li>
                            <li style="color:#fff">Disponibilidade de reembolso</li>
                            <li style="color:#fff">Assistência viagem nacional</li>
                            <li style="color:#fff">Ala exclusiva de atendimento oncológico</li>
                        </ul>
                        <a href="FormLandingPagesSaudeIntermedica.php"  class="btn btn-warning btn-lg btn-round">
                        Cotação Online
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-pricing" style="background-color: #000">
                    <div class="card-body">
                        <h2 class="category" style="color:#fff">
                        Linha Infinity</h2>
                        <ul>
                            <li style="color:#fff">Os melhores serviços e coberturas em todo o País</li>
                            <li style="color:#fff">Check-up anual preventivo</li>
                            <li style="color:#fff">Ala exclusiva de atendimento oncológico</li>
                            <li style="color:#fff">Coleta domiciliar</li>
                            <li style="color:#fff">Courier</li>
                        </ul>
                        <a href="FormLandingPagesSaudeIntermedica.php"  class="btn btn-warning btn-lg btn-round">
                        Cotação Online
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonials-2" style="background-image: url('LandingPages/assets/img/Saude/intermedica/bg15.jpg');" id="depoimento">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleIndicators2" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators2" data-slide-to="1" class=""></li>
                        <li data-target="#carouselExampleIndicators2" data-slide-to="2" class=""></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item justify-content-center">
                            <div class="card card-testimonial card-plain">
                                <div class="card-body">
                                    <h5 class="card-description">"Temos uma parceria de sucesso com a Intermédica há oito anos, sempre priorizando a qualidade e excelência no atendimento ao nosso maior patrimônio: nossos funcionários e familiares."
                                    </h5>
                                    <h3 class="card-title" style="color: #fff">Milton Martins</h3>
                                    <div class="card-footer">
                                        <h6 class="category text-primary" style="color: #fff">Gerente de Recursos Humanos e Jurídico da Dura Automotive</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item justify-content-center active">
                            <div class="card card-testimonial card-plain">
                                <div class="card-body">
                                    <h5 class="card-description">"A Notredame Intermédica é um dos benefícios mais elogiados dos nossos executivos sênior. E sempre que precisamos, nos dá o suporte na hora exata."
                                    </h5>
                                    <h3 class="card-title" style="color: #fff"> Caroline Duque</h3>
                                    <div class="card-footer">
                                        <h6 class="category text-primary" style="color: #fff">Diretora de RH da LATAM</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item justify-content-center ">
                            <div class="card card-testimonial card-plain">
                                <div class="card-body">
                                    <h5 class="card-description">"Na minha opinião, é o único Convênio médico que pratica um preço justo aqui em São Paulo. A Qualidade do serviço e rede credenciada são excelentes, não troco o Plano da minha família por nenhum outro"
                                    </h5>
                                    <h3 class="card-title" style="color: #fff"> Hélio de Oliveira</h3>
                                    <div class="card-footer">
                                        <h6 class="category text-primary" style="color: #fff">Técnico em logística</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="max-width: 900px;">
                <div class="modal-content">
                    <div class="modal-header justify-content-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                    </div>
                    <div class="modal-body" >
                   <iframe width="800" height="600" src="http://app.agenciatresmeiazero.com.br/FormLandingPagesSaudeIntermedica.php" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
<footer class="footer " style="background-color:#c07300; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>