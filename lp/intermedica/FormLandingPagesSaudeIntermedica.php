
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("LandingPages/includes/IncludeLandingPagesHeaderScripts.php"); ?>
    <body>
      <nav class="navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand"  rel="tooltip" title="" data-placement="bottom" target="_blank" data-original-title="Agência TresMeiaZero">
            <img src="LandingPages/assets/img/Saude/intermedica/logo-color.png" width="180">
            </a>
            
        </div>
        
    </div>
</nav>
<div class="header">
    <div class="page-header">
        <div class="page-header-image" style="background-image: url('LandingPages/assets/img/Saude/intermedica/bg14.jpg');"></div>
        <div class="content-center" id="formulario" style="padding-top: 250px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="title" style="font-size: 3em"> Preencha o formulário e economize no seu plano de saúde</h2>
                        <div class="col-md-9 ml-auto mr-auto text-center" style="font-size:20px">
                            <div class="alert alert-info" role="alert">
                                <div class="container" >
                                    <strong style="font-size:20px">Dica:</strong> quem possui CNPJ economiza até  <strong style="font-size:20px">30%</strong> nos Planos de Saúde Intermedica à partir de 2 vidas!
                                </div>
                            </div></div>
                        <div class="row">

                               <iframe width="1200" height="700" src="http://app.agenciatresmeiazero.com.br/FormSaudeIntermedica.php" frameborder="0" allowfullscreen></iframe>
                    </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



<footer class="footer " style="background-color:#c07300; color:#fff">
    <div class="col-md-12">
        <div class="container">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>, Desenvolvido por
                <a href="http://agenciatresmeiazero.com.br/home" target="_blank" style="color:#fff">#agênciatrêsmeiazero</a>.
            </div>
        </div>
    </div>
</footer>
    </body>
    <?php include("LandingPages/includes/IncludeLandingPagesFooterScripts.php"); ?>
</html>

