$(function() {

  $.validator.setDefaults({

      errorClass: 'form-control-feedback text-danger',
    highlight: function(element) {
      $(element)
        .closest('.form-group ')
        .addClass('has-danger');
    },
      unhighlight: function(element) {
      $(element)
        .closest('.form-group')
        .removeClass('has-danger');
    }

  });


  $("#contact-form").validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      nome: {
        required: true

      },
        telefone: {
            required: true,
            number: true
        },
        corretora: {
            required: true
        }

    },

    messages: {
      email: {
        required: 'Insira um email válido',
        email: 'E-mail <em>inválido</em>, tente novamente .',
        remote: $.validator.format("{0} is already associated with an account.")
      },
        nome: {
            required: 'Insira seu nome'
        },
        telefone: {
            required: 'Insira seu Telefone'
        },
        corretora: {
            required: 'Insira o nome da corretora'
        }
    }
  });

});