<div class="container">
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto text-left">
            <h1 class="title" style="font-size: 3em"> <div class="lateralbarConteudo"> </div>

                <span style="color: #e5b463">CONHEÇA À AGÊNCIA TRÊS<span style="color: #68036b">MEIA</span>ZERO</span></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 ">
            <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">A TrêsMeiaZero é uma agência de Marketing Digital que atende clientes em todo o Brasil nos mais variados segmentos.<br><br>
                Seus Sócios fundadores: Felipe, Paulo e Dennis, tem vasta experiência no mercado de seguros, mais especificamente no de leads (Indicações) para corretoras de Planos de, Saúde e esse acabou se tornando o principal ramo de atividade da #TMZ considerada por muitos, referência em qualidade nesse mercado.</p>
            <a href="#formulario" data-scroll id="botaoshake2" class="btn btn-warning btn-lg pull-left" style="color:#660069; font-weight: 500">Solicite um orçamento agora</a>

        </div>
        <div class="col-md-5 ">
           <img src="assets/img/imgPersonagem360.png" class="img-responsive pull-right d-none d-sm-block" width="430px" style="position: relative; bottom: 90px;">
        </div>
    </div>
</div>
<div class="container" id="depoimentos">
    <div class="row">
        <div class="col-md-12 ml-auto mr-auto text-right">
            <h1 class="title" style="font-size: 3em"><span style="color: #e5b463">PARCERIAS DE SUCESSO<div class="lateralbarConteudoRight"> </div></span></h1>
        </div>
    </div>
    <div class="row">

        <div class="col-md-5">
            <img src="assets/img/imgParceria.png" class="img-responsive pull-left d-none d-sm-block" width="430px" style="position: relative; bottom: 90px;">
        </div>
        <div class="col-md-7 text-right">
            <p class="description" style="    color: #9A9A9A;font-size: 20px;line-height: 32px;">"Tive algumas experiências péssimas com outras agências, cheguei a perder grandes equipes por conta de indicações ruins. Procurando por uma nova alternativa encontrei a trêsmeiazero, o atendimento me passou confiança, fizemos um teste, e hoje os corretores tem vontade de trabalhar com a gente por conta da qualidade das indicações, só tenho à agradecer o pessoal ".</p>
            <p class="description" style="font-weight: 800; color: #68036b;">- Lucas Rezende, Gerente Comercial.</p>


            <a href="#formulario" data-scroll id="botaoshake3"  class="btn btn-warning btn-lg pull-right" style="color:#fff; font-weight: 500; background-color: #68036b">Seja nosso parceiro também! Saiba mais aqui</a>
        </div>
    </div>
</div>