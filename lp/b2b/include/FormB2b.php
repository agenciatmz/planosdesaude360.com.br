<form role="form" id="contact-form" action="" method="post" enctype="multipart/form-data" style="color: #000" >

    <div class="card-body text-left">
        <div class="row">

            <div class="col-md-12 pr-2">
                <?php echo $msgClientesSucesso; ?>
                <?php echo $msgClientesErro; ?>
                <?php echo $e; ?>

            </div>


            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-form-label">Nome</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons users_circle-08"></i></span>
                        </div>
                        <input class="form-control" name="nome" id="nome" placeholder="Insira seu nome" type="text" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-form-label">E-mail</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons ui-1_email-85"></i></span>
                        </div>
                        <input class="form-control" name="email" id="email" placeholder="Insira seu email" type="email" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                <label class="col-form-label">Telefone</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
                    </div>
                    <input  type="text" class="form-control phone_with_ddd" placeholder="Insira seu telefone com DDD"  name="telefone" id="telefone" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">
                </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-form-label">Corretora onde entrega produção</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Insira o nome da corretora" name="corretora" id="corretora" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-form-label">Cargo</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="now-ui-icons tech_mobile"></i></span>
                        </div>
                         <input type="text" class="form-control " placeholder="Insira seu cargo" name="cargo" id="cargo" style="border-top-right-radius: 25px;border-bottom-right-radius: 25px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">


            <div class="d-none">
                <label class="col-form-label">Celular</label>
                <div class="input-group">

                    <input  type="text" class="form-control" value="B2B" name="operadora" id="operadora">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info btn-lg btn-round pull-right">Solicitar Cotação</button>
            </div>
        </div>
    </div>
</form>