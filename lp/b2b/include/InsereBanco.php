<?php

if(isset($_POST['cadastrar'])){
    $nome            = trim(strip_tags($_POST['nome']));
    $email           = trim(strip_tags($_POST['email']));
    $telefone         = trim(strip_tags($_POST['telefone']));
    $corretora       = trim(strip_tags($_POST['corretora']));
    $operadora       = trim(strip_tags($_POST['operadora']));
    $cargo       = trim(strip_tags($_POST['cargo']));

    $insert = "INSERT INTO dbleadsb2b ( nome, email, telefone, corretora, operadora, cargo ) VALUES ( :nome, :email, :telefone, :corretora, :operadora, :cargo )";
    try{

        $result = $conexao->prepare($insert);

        $result->bindParam(':nome', $nome, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':telefone', $telefone, PDO::PARAM_STR);
        $result->bindParam(':corretora', $corretora, PDO::PARAM_STR);
        $result->bindParam(':operadora', $operadora, PDO::PARAM_STR);
        $result->bindParam(':cargo', $cargo, PDO::PARAM_STR);


        $result->execute();
        $contar = $result->rowCount();
        if($contar>0){

            {
                $msgClientesSucesso = '
                    <script type="text/javascript">
setTimeout(function () {
   window.location.href = "obrigado.php"; //will redirect to your blog page (an ex: blog.html)
}, 2000); //will call the function after 2 secs.
            </script>';

            }
        }else{
            $msgClientesErro = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erro</strong> ao cadastrar o usuário.
                    </div>';
        }
    }catch(PDOException $e){
        echo $e;
    }

}else {
    $msg[] = "<b>$name :</b> Desculpe! Ocorreu um erro...";
}
?>