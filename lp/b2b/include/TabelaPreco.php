<div class="pricing-5 section-pricing-5 " id="pricing-5" style="background-image: url('assets/img/bg31.jpg');     background-size: cover;
    background-position: center center;">
    <div class="container">
        <div class="row">

            <div class="col-md-7 ml-auto mr-auto">
                <div class="tab-content tab-space">
                    <div class="tab-pane active" id="personal">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box d-none d-sm-block">
                                    <div class="ribbon"><span>20% OFF</span></div>
                                </div>
                                <div class="card card-pricing card-raised" >
                                    <div class="card-body">

                                        <h3 class="category" style="color: #68036b">ADESÃO <p class="category" style="font-size: 18px; color: #5b70d8">20% DE DESCONTO</p> </h3>

                                        <ul>
                                            <li>
                                                <b style="color: #5b70d8">100 Leads</b> Pessoa Física</li>
                                            <li>
                                                <b style="color: #5b70d8">Leads</b> em tempo real</li>
                                            <li>
                                                <b style="color: #5b70d8">Garantia</b> de interesse</li>
                                            <li>
                                                <b style="color: #5b70d8">Exclusividade</b> garantida</li>
                                            <li>
                                                <b style="color: #5b70d8">Escolha</b> quanto quer receber por dia</li>
                                        </ul>
                                        <a href="#formulario" id="botaoshake4" data-scroll  class="btn btn-warning btn-lg " style="color:#fff; font-weight: 500; background-color: #5b70d8">Aproveitar valor promocional</a>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-pricing card-raised">
                                    <div class="card-body">
                                        <h3 class="category" style="color: #68036b">MESCLADO <p class="category" style="font-size: 18px; color: #55b162">O MAIS VENDIDO</p> </h3>

                                        <ul>
                                            <li>
                                                <b style="color: #55b162">15</b> Leads PF - <b style="color: #55b162">5</b> Leads PME</li>
                                            <li>
                                                                                                <b style="color: #55b162">Leads</b> em tempo real</li>

                                                </li>
                                            <li>
                                                <b style="color: #55b162">Garantia</b> de interesse</li>
                                            <li>
                                                <b style="color: #55b162">Exclusividade</b> garantida</li>
                                            <li>
                                                <b style="color: #55b162">Escolha</b> quanto quer receber por dia</li>
                                        </ul>
                                        <a href="#formulario" data-scroll id="botaoshake5"  class="btn btn-warning btn-lg " style="color:#fff; font-weight: 500; background-color: #55b162">Consultar Valores</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 ">
                <h2 class="title" style="color:#fff"><div class="lateralbarPreco"> </div>Escolha o melhor plano para você</h2>
                <p class="description" style="color:#fff">Precisa de um volume maior de Leads ou quer personalizar seu Pacote conforme as necessidades da sua corretora?<br><br>
             Nós também conseguimos te ajudar. Fale com nosso Gerente de Contas, o Eduardo e ele irá entender sua necessidade e te auxiliar da melhor forma possível.</p>
                <a href="#formulario" data-scroll class="btn btn-warning btn-lg pull-left" id="botaoshake6" style="color:#650067; font-weight: 800;     background-color: #fbc933;">FALAR COM O EDUARDO</a>
                <a href="https://api.whatsapp.com/send?phone=5519999979569&text=Oi%20Eduardo%20tudo%20bem?"  class="btn btn-success btn-lg pull-left" target="_blank" style="color:#316439; font-weight: 800;">WhatsApp </a>

            </div>

        </div>
    </div>
</div>