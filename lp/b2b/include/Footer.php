<footer class="footer footer-big" style="background-color:#780078; color:#fff">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5>Contato</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                atendimento@agenciatresmeiazero.com.br<br>
                                (19) 9 9102-8138
                            </a>
                        </li>
                        

                    </ul>
                </div>
                <div class="col-md-4">
                    <h5>Endereço</h5>
                    <ul class="links-vertical">
                        <li>
                            <a href="#">
                                R. Francisco Glicério, 738<br>
                                Sala 01, Vila Embaré, Valinhos/SP.
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-4">
                    <h5>Redes Socias</h5>
                    <ul class="social-buttons">
                        <li>
                            <a href="https://www.facebook.com/Ag%C3%AAncia-tresmeiazero-564664120394164/"  target="_blank" class="btn btn-icon btn-neutral btn-facebook btn-round">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/ag_tresmeiazero" class="btn btn-icon btn-neutral btn-twitter btn-round"  target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a href="https://www.linkedin.com/company-beta/25018320/"  target="_blank" class="btn btn-icon btn-neutral btn-dribbble btn-round">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/agenciatresmeiazero/"  target="_blank" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#"  class="btn btn-icon btn-neutral btn-google btn-round">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://br.pinterest.com/agenciatrsmeiazero/"  target="_blank" class="btn btn-icon btn-neutral btn-instagram btn-round">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                                   </div>
            </div>
        </div>
        <hr>

    </div>
</footer>


<footer class="footer " style="background-color:#460146; color:#fff">
    <div class="col-md-12">
    <div class="container">
        <div class="copyright">
            ©
            <script>
                document.write(new Date().getFullYear())
            </script>, Desenvolvido por
            <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
        </div>
    </div>
    </div>
</footer>