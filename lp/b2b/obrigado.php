<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Amil Saúde - Cotação</title>
    <meta name="description" content="" />
    <meta name="keywords" content="Plano de Saúde, Plano de Saúde Amil, Amil Saúde" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Amil Saúde - Cotação" />
    <meta property="og:description" content="Os Preços dos Planos de Saúde Amil variam de acordo com a idade dos beneficiários e do tipo de Plano contratado.Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)&#38;nbsp;" />
    <meta property="og:site_name" content="Amil Saúde - Cotação" />
    <meta property="og:url" content="http://lp.360planodesaude.com.br/saude/amil/" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/obrigado.css" rel="stylesheet" />
    <link href="assets/css/demo.css" rel="stylesheet" />
    <script src="assets/js/plugins/sweetalert2.all.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118670369-1"></script>

</head>

<body>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-118670369-1');
    </script>

<body>
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '109436086450222');
    fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=109436086450222&ev=PageView&noscript=1"
    /></noscript><div class="header-1">

    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 ml-auto mr-auto">

                        <h1 class="title">OBRIGADO</h1>
                        <h3 class="description">Em breve nossos consultores entrarão em contato</h3>
                        <div class="col-md-12  ml-auto mr-auto">
                            <a href="index.php"class="btn btn-info btn-lg btn-round">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer " data-background-color="black">
    <div class="container">
        <div class="copyright">
            ©
            <script>
                document.write(new Date().getFullYear())
            </script>, Desenvolvido por
            <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
        </div>
    </div>
</footer>
</body>
<script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
<script src="assets/js/plugins/api-estado-cidade.js"></script>
<script src="assets/js/plugins/custom.js"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/moment.min.js"></script>
<script src="assets/js/plugins/bootstrap-switch.js"></script>
<script src="assets/js/plugins/bootstrap-tagsinput.js"></script>
<script src="assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<script src="assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
</html>






